import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import { environment } from './environments/environment';
import { AppModule } from './app/';
import { enableProdMode } from '@angular/core';
enableProdMode();

platformBrowserDynamic().bootstrapModule(AppModule);
