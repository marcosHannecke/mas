export interface Subscriber {
  'cardHolderType': string,
  'packRequestType': string,
  'nrOfCards': number,
  'title': string,
  'initials': string,
  'surname': string,
  'addressLine1': string,
  'addressLine2': string,
  'addressLine3': string,
  'addressLine4': string,
  'addressLine5': string,
  'postcode': string,
  'countryCode': string,
  'homeTelNr': string,
  'workTelNr': string,
  'mobTelNr': string,
  'cliTelNr': string,
  'currencyCode': string,
  'phones': any,
  'cardSubscriberId': number,
  'productGroupNames': [''],
  'viewingCardNumber': number,
  'vip': any,
  'email': string
}


