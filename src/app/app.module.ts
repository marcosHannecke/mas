import * as Raven from 'raven-js';
import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';

import {appRouter} from './app.routes';
import {AppComponent} from './app.component';
// App views
import {MainViewModule} from './views/main-view/main-view.module';
import {NewSubscriberViewModule} from './views/subscribers/newsubscriber/newsubscriber.module';
import {JoinSubscriberViewModule} from './views/subscribers/joinSubscriber/joinSubscriber.module';
import {LoginModule} from './views/login/login.module';

import {LoggedInGuard} from './services/logged-in.guard';
import {UserService} from './services/user.service';
// App modules/components
import {LayoutsModule} from './components/common/layouts/layouts.module';
import {SearchViewModule} from './views/search/search.module';
import {ProcessViewModule} from './views/process/process.module';
import {CustomerViewModule} from './views/customer/customer.module';
import {NgIdleKeepaliveModule} from '@ng-idle/keepalive';
import {MomentModule} from 'angular2-moment';
import {AdminRequiredService} from './services/adminRequired.service';
import {StatsComponent} from './views/stats/stats.component';
import {StatsTableComponent} from './views/stats/stats-table/stats-table.component';
import {ChartModule} from 'angular-highcharts';
import {
  MatButtonModule,
  MatDatepickerModule,
  MatInputModule,
  MatNativeDateModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatTooltipModule
} from '@angular/material';
import {HistoriesComponent} from './views/histories/histories.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {CapitalizePipe, StringToNumberPipe} from './highlightReverse.pipe';
import {RecentAccountsComponent} from './views/recent-accounts/recent-accounts.component';
import {AdminPanelComponent} from './views/admin-panel/admin-panel.component';
import {AdvancedSearchComponent} from './views/search/advanced-search/advanced-search.component';
import {ToastrModule} from 'ngx-toastr';
import {NotificationsComponent} from './views/admin-panel/notifications/notifications.component';
import {UsersComponent} from './views/admin-panel/users/users.component';
import {Globals} from './app.global';
import {NgDatepickerModule} from 'ng2-datepicker';
import {TokenInterceptor} from './services/token.interceptor';
import {AuthService} from './services/auth.service';
import {JwtInterceptor} from './services/jwt.interceptor';
import {AuthRequiredModule} from './components/common/auth-required/auth-required.module';
import {DpDatePickerModule} from 'ng2-date-picker';
import {NewJoinPaymentComponent} from './views/new-join-payment/new-join-payment.component';
import {PaymentIframeComponent, SafePipe} from './views/payment-iframe/payment-iframe.component';
import {SavePaymentComponent} from './views/save-payment/save-payment.component';
import {AppSettings} from './app.settings';
import {SalesItemsComponent} from "./views/admin-panel/sales-items/sales-items.component";
import {ProductGroupsComponent} from "./views/admin-panel/product-groups/product-groups.component";
import {DragulaModule} from 'ng2-dragula';

/**************************************************************************
 * IMPORTANT: Uncomment block for production to activate Sentry
 *************************************************************************/
Raven
  .config('https://a9586da45a5a48658f11ab1659299d6b@sentry.io/249721')
  .install();

export class RavenErrorHandler implements ErrorHandler {
  appSettings = AppSettings;
  handleError(err: any): void {
    if (this.appSettings.environment === 'live') {
      Raven.captureException(err);
    }
  }
}
/**************************************************************************
 * Uncomment Uncomment block for production to activate Sentry
 *************************************************************************/

@NgModule({
  exports: [
    MatTooltipModule,
    MatInputModule,
    MatButtonModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  declarations: []
})
export class MaterialModule {}


@NgModule({
  declarations: [
    AppComponent,
    StatsComponent,
    StatsTableComponent,
    HistoriesComponent,
    CapitalizePipe,
    RecentAccountsComponent,
    AdminPanelComponent,
    AdvancedSearchComponent,
    NotificationsComponent,
    UsersComponent,
    StringToNumberPipe,
    SafePipe,
    NewJoinPaymentComponent,
    PaymentIframeComponent,
    SavePaymentComponent,
    SalesItemsComponent,
    ProductGroupsComponent,
    // BillingHistoryComponent,
    // DialogsComponent,
  ],
  imports: [
    // Angular modules
    BrowserModule,
    BrowserAnimationsModule,
    // HttpModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    // Views
    CustomerViewModule,
    NewSubscriberViewModule,
    JoinSubscriberViewModule,
    MainViewModule,
    LoginModule,
    ProcessViewModule,
    SearchViewModule,
    NgDatepickerModule,
    // BillingHistoryViewModule,
    // ViewSubscriberViewModule,
    // Modules
    LayoutsModule,
    appRouter,
    NgIdleKeepaliveModule.forRoot(),
    MomentModule,
    ChartModule,
    MaterialModule,
    NgxPaginationModule,
    HttpClientModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
    }),
    AuthRequiredModule,
    DpDatePickerModule,
    DragulaModule
  ],
  exports: [
  ],
  providers: [
    /************************************************************
    * IMPORTANT: Uncomment block for production to activate Sentry
    *************************************************************/
    { provide: ErrorHandler, useClass: RavenErrorHandler },
    /************************************************************
     * Uncomment Uncomment block for production to activate Sentry
     *************************************************************/
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    LoggedInGuard, UserService, AdminRequiredService, Globals, AuthService ],
  bootstrap: [AppComponent]
})

export class AppModule { }
// platformBrowserDynamic().bootstrapModule(AppModule);
