"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var HighlightPipe = /** @class */ (function () {
    function HighlightPipe() {
    }
    HighlightPipe.prototype.transform = function (text, search) {
        var pattern = search.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
        pattern = pattern.split(' ').filter(function (t) {
            return t.length > 0;
        }).join('|');
        var regex = new RegExp(pattern, 'gi');
        return search ? text.replace(regex, function (match) { return "<span class=\"highlight\">" + match + "</span>"; }) : text;
    };
    HighlightPipe = __decorate([
        core_1.Pipe({ name: 'highlight' })
    ], HighlightPipe);
    return HighlightPipe;
}());
exports.HighlightPipe = HighlightPipe;
var ReversePipe = /** @class */ (function () {
    function ReversePipe() {
    }
    ReversePipe.prototype.transform = function (value) {
        return value.slice().reverse();
    };
    ReversePipe = __decorate([
        core_1.Pipe({
            name: 'reverse'
        })
    ], ReversePipe);
    return ReversePipe;
}());
exports.ReversePipe = ReversePipe;
var OrderValidFrom = /** @class */ (function () {
    function OrderValidFrom() {
    }
    OrderValidFrom.prototype.transform = function (array, args) {
        console.log('hereaa', args);
        if (typeof args[0] === 'undefined') {
            return array;
        }
        var direction = args[0][0];
        array.sort(function (a, b) {
            var left = Number(new Date(a[0].validFrom));
            var right = Number(new Date(b[0].validFrom));
            return (direction === '-') ? right - left : left - right;
        });
        return array;
    };
    OrderValidFrom = __decorate([
        core_1.Pipe({
            name: 'orderValidFrom'
        })
    ], OrderValidFrom);
    return OrderValidFrom;
}());
exports.OrderValidFrom = OrderValidFrom;
var CapitalizePipe = /** @class */ (function () {
    function CapitalizePipe() {
    }
    CapitalizePipe.prototype.transform = function (value) {
        if (value) {
            return value.charAt(0).toUpperCase() + value.slice(1);
        }
        return value;
    };
    CapitalizePipe = __decorate([
        core_1.Pipe({ name: 'capitalize' })
    ], CapitalizePipe);
    return CapitalizePipe;
}());
exports.CapitalizePipe = CapitalizePipe;
var PostcodePipe = /** @class */ (function () {
    function PostcodePipe() {
    }
    PostcodePipe.prototype.transform = function (value) {
        // value = value.match(/^([A-Z]{1,2}\d{1,2}[A-Z]?)\s*(\d[A-Z]{2})$/);
        // value.shift();
        // value.join(' ');
        value = value.toUpperCase();
        var exceptions = ['UK', 'ROI', 'IOM', 'CI', 'BFPO'];
        // If Postcode Exception type, return
        exceptions.forEach(function (exp) {
            if (value === exp) {
                return value;
            }
        });
        // If length is < 4, return
        if (value.length < 4) {
            return value;
        }
        var suffix = value.substring(value.length - 3);
        var prefix = value.substring(0, value.length - 3);
        return prefix + ' ' + suffix;
    };
    PostcodePipe = __decorate([
        core_1.Pipe({ name: 'postcode' })
    ], PostcodePipe);
    return PostcodePipe;
}());
exports.PostcodePipe = PostcodePipe;
