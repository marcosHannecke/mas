"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var customer_component_1 = require("./customer.component");
var editCustomer_component_1 = require("./editCustomer.component");
var common_1 = require("@angular/common");
var highlightReverse_pipe_1 = require("../../highlightReverse.pipe");
var CustomerViewModule = /** @class */ (function () {
    function CustomerViewModule() {
    }
    CustomerViewModule = __decorate([
        core_1.NgModule({
            declarations: [customer_component_1.CustomerComponent, editCustomer_component_1.EditCustomerComponent, highlightReverse_pipe_1.PostcodePipe],
            imports: [common_1.CommonModule, forms_1.FormsModule, forms_1.ReactiveFormsModule, router_1.RouterModule],
        })
    ], CustomerViewModule);
    return CustomerViewModule;
}());
exports.CustomerViewModule = CustomerViewModule;
