"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
//
// EDIT CUSTOMER
///
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var validators_service_1 = require("../../services/validators.service");
var subscribers_service_1 = require("../../services/subscribers.service");
var helpers_service_1 = require("../../services/helpers.service");
var postcode_service_1 = require("../../services/postcode.service");
var EditCustomerComponent = /** @class */ (function () {
    function EditCustomerComponent(subscriberService, router, helpers, activateRoute, fb, val, postcodeService) {
        this.subscriberService = subscriberService;
        this.router = router;
        this.helpers = helpers;
        this.activateRoute = activateRoute;
        this.fb = fb;
        this.val = val;
        this.postcodeService = postcodeService;
        this.submitted = false;
        this.success = false;
        this.subsData = {};
        this.spread = false;
    }
    ;
    EditCustomerComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (sessionStorage.getItem('role') != null) {
            this.role = sessionStorage.getItem('role');
        }
        this.activateRoute.queryParams
            .subscribe(function (res) {
            _this.custId = res['id'];
            console.log(_this.custId);
        });
        this.subscriberService.getCustomer(this.custId)
            .subscribe(function (res) {
            _this.customer = res[0];
            console.log(_this.customer);
            // replace null values in customer object to avoid form errors
            for (var _i = 0, _a = Object.keys(_this.customer); _i < _a.length; _i++) {
                var key = _a[_i];
                if (_this.customer[key] == null) {
                    _this.customer[key] = '';
                }
            }
            _this.detailsForm = _this.fb.group({
                "customerId": [_this.customer.id],
                "title": [_this.customer.title, [forms_1.Validators.required, _this.val.specialChar]],
                "initials": [_this.customer.initials, [forms_1.Validators.required, forms_1.Validators.maxLength(3), _this.val.letterSpaces]],
                "email": [_this.customer.email, [forms_1.Validators.maxLength(128), forms_1.Validators.required]],
                "firstname": [_this.customer.firstname, [forms_1.Validators.maxLength(128)]],
                "surname": [_this.customer.surname, [forms_1.Validators.required, forms_1.Validators.maxLength(35), _this.val.letterSpaces]],
                "billingAddressLine1": [_this.customer.billingAddressLine1, [forms_1.Validators.required, forms_1.Validators.maxLength(35), _this.val.specialChar]],
                "billingAddressLine2": [_this.customer.billingAddressLine2, [forms_1.Validators.required, forms_1.Validators.maxLength(35), _this.val.specialChar]],
                "billingAddressLine3": [_this.customer.billingAddressLine3, [forms_1.Validators.maxLength(35)]],
                "billingAddressLine4": [_this.customer.billingAddressLine4, [forms_1.Validators.maxLength(35)]],
                "billingAddressLine5": [_this.customer.billingAddressLine5, [forms_1.Validators.maxLength(35)]],
                "billingPostcode": [_this.customer.billingPostcode, [forms_1.Validators.required, forms_1.Validators.maxLength(9), _this.val.specialChar]],
                "billingCountryCode": [_this.customer.billingCountryCode],
                "phones": _this.fb.group({
                    "homeTelNr": [_this.customer.homeTelNo, [forms_1.Validators.maxLength(15), _this.val.onlyNumbers]],
                    "workTelNr": [_this.customer.workTelNo, [forms_1.Validators.maxLength(15), _this.val.onlyNumbers]],
                    "mobTelNr": [_this.customer.mobTelNo, [forms_1.Validators.maxLength(15), _this.val.onlyNumbers]],
                }, { validator: _this.val.phones })
            });
        }, function (err) {
            _this.error = JSON.parse(err['_body']);
        });
        this.subscribers = JSON.parse(sessionStorage.getItem('billingSubscribers'));
    };
    EditCustomerComponent.prototype.update = function () {
        var _this = this;
        this.spread = false;
        this.submitted = true;
        if (this.detailsForm.valid) {
            var phones = {
                homeTelNo: '',
                workTelNo: '',
                mobTelNo: ''
            };
            var json = Object.assign(this.detailsForm.value, phones);
            json.homeTelNo = this.detailsForm.value.phones.homeTelNr;
            json.workTelNo = this.detailsForm.value.phones.workTelNr;
            json.mobTelNo = this.detailsForm.value.phones.mobTelNr;
            delete json.phones;
            this.subscriberService.urlPut('customers', json)
                .subscribe(function (res) {
                _this.success = true;
            }, function (err) {
                _this.error = JSON.parse(err['_body']);
            });
        }
    };
    EditCustomerComponent.prototype.spreadChanges = function () {
        var _this = this;
        // close dialog
        this.spread = false;
        this.submitted = true;
        // update subscribers data
        if (this.detailsForm.valid) {
            this.subsData.title = this.detailsForm.value.title;
            this.subsData.initials = this.detailsForm.value.initials;
            this.subsData.surname = this.detailsForm.value.surname;
            this.subsData.addressLine1 = this.detailsForm.value.billingAddressLine1;
            this.subsData.addressLine2 = this.detailsForm.value.billingAddressLine2;
            this.subsData.addressLine3 = this.detailsForm.value.billingAddressLine3;
            this.subsData.addressLine4 = this.detailsForm.value.billingAddressLine4;
            this.subsData.addressLine5 = this.detailsForm.value.billingAddressLine5;
            this.subsData.postcode = this.detailsForm.value.billingPostcode;
            this.subsData.countryCode = this.detailsForm.value.billingCountryCode;
            this.subsData.homeTelNr = this.detailsForm.value.phones.homeTelNr;
            this.subsData.workTelNr = this.detailsForm.value.phones.workTelNr;
            this.subsData.mobTelNr = this.detailsForm.value.phones.mobTelNr;
            for (var _i = 0, _a = this.subscribers; _i < _a.length; _i++) {
                var subscriber = _a[_i];
                if (subscriber.id) {
                    this.subsData.subscriberId = subscriber.id;
                }
                else if (subscriber.cardSubscriberId != null) {
                    this.subsData.cardSubscriberId = subscriber.cardSubscriberId;
                }
                console.log(this.subsData);
                this.subscriberService.urlSubscribers('', this.subsData)
                    .subscribe(function (res) {
                    _this.update();
                }, function (err) {
                    _this.error = JSON.parse(err['_body']);
                });
            }
        }
    };
    // FIND ADDRESS
    EditCustomerComponent.prototype.findAddress = function () {
        var _this = this;
        var postcode = this.detailsForm.value.billingPostcode.replace(' ', '');
        this.postcodeService.postcode(postcode)
            .subscribe(function (res) {
            _this.addressList = JSON.parse(res['_body']);
            if (_this.addressList.length === 0) {
                _this.addressList = [{
                        notFound: 'NO ADDRESS FOUND'
                    }];
            }
        }, function (err) {
            if (_this.detailsForm.value.postcode.length === 0) {
                _this.error = {
                    description: 'Please Enter a Postcode.'
                };
            }
            else {
                _this.error = _this.helpers.getError(err);
            }
        });
    };
    // SET ADDRESS
    EditCustomerComponent.prototype.setAddress = function (address) {
        if (address.notFound) {
            this.addressList = null;
            // this.disableInput = false;
        }
        else {
            this.addressList = null;
            // this.disableInput = true;
            // this.addressManually = true;
            var add = this.helpers.nullCheck(address);
            this.detailsForm.controls['billingAddressLine1'].setValue(add.addressLine1.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.detailsForm.controls['billingAddressLine2'].setValue(add.addressLine2.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.detailsForm.controls['billingAddressLine3'].setValue(add.addressLine3.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.detailsForm.controls['billingAddressLine4'].setValue(add.addressLine4.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.detailsForm.controls['billingAddressLine5'].setValue(add.addressLine5.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
        }
    };
    EditCustomerComponent.prototype.backTo = function () {
        this.success = false;
        sessionStorage.setItem('addressIncorrect', 'false');
        this.router.navigate(['/process25']);
    };
    EditCustomerComponent = __decorate([
        core_1.Component({
            selector: 'editCustomer',
            templateUrl: 'editCustomer.component.html',
            styles: ['.col-lg-2.control-label {padding-left: 0; padding-right: 0}'],
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService, helpers_service_1.HelpersService, postcode_service_1.PostcodeService]
        })
    ], EditCustomerComponent);
    return EditCustomerComponent;
}());
exports.EditCustomerComponent = EditCustomerComponent;
