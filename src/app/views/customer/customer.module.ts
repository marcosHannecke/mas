import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CustomerComponent } from "./customer.component";
import { EditCustomerComponent } from "./editCustomer.component";
import {MaterialModule} from '../../app.module';
import { CommonModule } from "@angular/common";
import {PostcodePipe} from "../../highlightReverse.pipe";
import {SearchViewModule} from '../search/search.module';


@NgModule({
  declarations: [CustomerComponent, EditCustomerComponent, PostcodePipe],
  imports     : [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, SearchViewModule],
})

export class CustomerViewModule {}
