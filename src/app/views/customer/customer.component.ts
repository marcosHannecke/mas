import {Component, HostListener, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {HelpersService} from '../../services/helpers.service';
import {PostcodeService} from '../../services/postcode.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Globals} from '../../app.global';
import {RestService} from '../../services/rest.service';
import {HttpClient} from '@angular/common/http';

// CUSTOMER
@Component({
  selector: 'app-customer',
  templateUrl: 'customer.component.html',
  styleUrls: ['customer.component.css'],
  providers: [ HelpersService, PostcodeService, RestService, HttpClient ]
})
export class CustomerComponent implements OnInit {
  url: any;
  role: string;
  customer: any;
  subscriber: any;
  subscribers: any;
  error: any;
  success: any;
  status: string;
  cardError: any;
  hasPaymentCard: any;
  paymentDetailCard: any;
  paymentDetailNumber: any;
  paymentDetailExpiry: any;
  cardTokens = {
    token : '',
    tokenId: '',
    expire: ''
  };
  payMeth: any;
  payMethDialog = false;

  billingInfo: any = {};
  balanceSettled = false;
  voidBalanceCheck = false;
  requireAdmin = false;
  accessGranted: any = false;
  user: any;
  password: any;

  freeviewNotAllowed = false;
  date = new Date();
  editMarketing = true;

  validEmail: string;
  searchDialog: any;
  customerSearchDetails = {
    surname: '',
    postcode: '',
    addressLine1: '',
    addressLine2: '',
    addressLine3: '',
    addressLine4: '',
    addressLine5: '',
  };
  innerWidth: any;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  constructor(private location: Location, public helpers: HelpersService, private router: ActivatedRoute, public route: Router,
              private postcodeService: PostcodeService, private rest: RestService, private http: HttpClient, private globals: Globals) {};

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    console.log('WIDTH', this.innerWidth);
    
    this.url = this.globals.URL;
    // GET USER ROLE
    if (sessionStorage.getItem('role') != null) {
      this.role = sessionStorage.getItem('role');
      console.log(this.role);
    }
    // GET SUBSCRIBER STORED
    if (sessionStorage.getItem('subscriber')) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
      console.log(this.subscriber);
    }
    // GET CUSTOMERS (includes get customer subscribers,  get payment card, get billing info)
    this.getCustomer();

    // IF ADD/UPDATE PAYMENT CARD
    if (sessionStorage.getItem('cardProcess')) {
      this.customer = JSON.parse(sessionStorage.getItem('customer'));

      // GET TOKENS
      this.router.queryParams
        .subscribe(params => {
          this.cardTokens.token = params['token'];
          this.cardTokens.tokenId = params['tokenId'];
          this.cardTokens.expire = params['exp'];
        });

      if (this.cardTokens.token === undefined || this.cardTokens.tokenId === undefined
        || this.cardTokens.token === '' || this.cardTokens.tokenId === '') {
        // If DD failed display error
        if (JSON.parse(sessionStorage.getItem('cardDD'))) {
          this.cardError = {
            message : 'The Direct Debit Instruction was not created.',
            message2: 'Please check the bank details carefully or pay by Debit/Credit Card'
          };
        } else {
          // If Card failed display error
          this.cardError = {
            message : 'The Card details are not correct.',
            message2: 'Please check the expiry date and security code carefully or use Direct Debit.'
          };
        }
        sessionStorage.removeItem('cardProcess');
      }else {
        // SAVE PAYMENT CARD
        const data = {
          customerId : this.customer.id,
          token: this.cardTokens.token,
          tokenId: this.cardTokens.tokenId,
          expiryDate: this.helpers.undefinedToEmptyString(this.cardTokens.expire)
        };
        this.rest.put('/payments/save-payment-card', data)
          .subscribe(() => {
            this.getPaymentCard();
          }, err => {
            this.error = this.helpers.error(err.error.message);
          });
          sessionStorage.removeItem('cardProcess');
      }
    }

  }

  /**
   * GET CUSTOMER WITH SUBSCRIBER ID
   */
  getCustomer() {
    if (this.subscriber) {
      console.log('subscriberId', this.subscriber.id);
      this.rest.get('/customers?q=subscriberId=' + this.subscriber.id)
        .subscribe(res => {
          console.log('customer', res[0]);
          this.customer = res[0];
          // get customer status
          this.customerStatus();
          // Format Postcode
          this.customer.billingPostcode = this.postcodeService.postcodeFormat(this.customer.billingPostcode);
          // Capitalize surname
          this.customer.surname = this.helpers.toTitleCase(this.customer.surname);
          // replace null values in customer object to avoid form errors
          for (const key of Object.keys(this.customer)) {
            if (this.customer[key] == null) {
              this.customer[key] = '';
            }
          }
          if (this.customer.email === 'unknown@mediaaccessservices.net') {
            this.customer.email = '';
          }else {
            this.getEmailVerification();
          }


          this.getCustomerSubscribers();
          this.getPaymentCard();
          this.getBillingInfo();
        }, err => {
          this.error = this.helpers.error(err.error.message);
        });
    } else {
      this.error = {
        message: 'Customer not found'
      }
    }
  }

  /**
   * GET PAYMENT CARD
   */
  getPaymentCard() {
    let payment;
    this.rest.get('/payments/has-payment-card?customerId=' + this.customer.id)
      .subscribe(res => {
        payment = res;
        this.hasPaymentCard = payment.hasPaymentCard;
        this.paymentDetailCard = payment.detail;

        if (payment.expiryDate != null) {
          this.paymentDetailExpiry = {
            expiry: payment.expiryDate,
            expiryYear: Number(payment.expiryDate.substring(payment.expiryDate.length - 2, payment.expiryDate.length)),
            expiryMonth: Number(payment.expiryDate.substring(0, 2))
          }
        }
        if (this.paymentDetailCard != null) {
          if (this.paymentDetailCard.toLowerCase() !== 'direct debit' && this.paymentDetailCard.toLowerCase() !== 'unknown') {
            this.paymentDetailNumber = this.paymentDetailCard.slice(this.paymentDetailCard.length - 4, this.paymentDetailCard.length);
            this.paymentDetailCard = this.paymentDetailCard.slice(0, this.paymentDetailCard.length - 4);
          }
        }
      }, err => {
        console.log(err)
        // this.error = this.helpers.error(err.error.message);
      })

    // this.paymentsService.getUrl('has-payment-card?customerId=', this.customer.id)
    //   .subscribe(res => {
    //     const payment = JSON.parse(res['_body']);
    //     this.hasPaymentCard = payment.hasPaymentCard;
    //     this.paymentDetailCard = payment.detail;
    //
    //     if (payment.expiryDate != null) {
    //       this.paymentDetailExpiry = {
    //         expiry: payment.expiryDate,
    //         expiryYear: Number(payment.expiryDate.substring(payment.expiryDate.length - 2, payment.expiryDate.length)),
    //         expiryMonth: Number(payment.expiryDate.substring(0, 2))
    //       }
    //     }
    //     if (this.paymentDetailCard != null) {
    //       if (this.paymentDetailCard.toLowerCase() !== 'direct debit' && this.paymentDetailCard.toLowerCase() !== 'unknown') {
    //         this.paymentDetailNumber = this.paymentDetailCard.slice(this.paymentDetailCard.length - 4, this.paymentDetailCard.length);
    //         this.paymentDetailCard = this.paymentDetailCard.slice(0, this.paymentDetailCard.length - 4);
    //       }
    //     }
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   })
  }

  // GET BILLING INFO
  getBillingInfo() {
    this.rest.get('/payments/get-balance-owed?customerId=' + this.customer.id )
      .subscribe(res => {
        const resp = res;
        console.log('billing', resp);
        this.billingInfo = {
          owed: resp['balanceOwed']
        }
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  // ADD PAYMENT CARD
  addPaymentCard() {
    sessionStorage.setItem('cardProcess', 'true');
    sessionStorage.setItem('customer', JSON.stringify(this.customer));

    this.payMethDialog = false;
    // Data to send
    const data  = {
      customerId : this.customer.id,
      returnUrl: this.globals.PAYMENT_RETURN_DOMAIN + '/#/save-payment/',
      directDebit: this.payMeth // if TRUE direct debit / if FALSE card
    };
    sessionStorage.setItem('cardDD', JSON.stringify(this.payMeth));
    // const data  = {
    //   customerId : this.customer.id,
    //   returnUrl: this.globals.PAYMENT_RETURN_DOMAIN + '/#/customer/' + this.customer.id,
    //   directDebit: this.payMeth // if TRUE direct debit / if FALSE card
    // };
    // sessionStorage.setItem('cardDD', JSON.stringify(this.payMeth));
    // Data to url string
    const str = Object.keys(data).map(function(key){
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    this.rest.get('/payments/get-payment-card-url?' + str)
      .subscribe(res => {
        console.log(res);
        sessionStorage.setItem('payment-url', res['storeCardUrl']);
        sessionStorage.setItem('return-url', data.returnUrl);
        sessionStorage.setItem('paymentPath', 'update');
        // window.open(url['storeCardUrl'], '');
        this.route.navigate(['/payment'])


        // location.replace(url['storeCardUrl']);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });
  }

  /**
   * Settle Balance owed
   */
  settleBalance() {
    const data = {
      customerId: this.customer.id,
      amount: this.billingInfo.owed
    };

    this.http.put(this.globals.API_ENDPOINT + '/payments/settle-balance', data, {responseType: 'text'})
      .subscribe(() => {
        this.balanceSettled = true;
      }, err => {
          err = JSON.parse(err.error);
          console.log(err);
          this.error = this.helpers.error(err.message);
      })

    // this.paymentsService.putUrl('settle-balance', data)
    //   .subscribe(res => {
    //     console.log(res);
    //     this.balanceSettled = true;
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   })
  }

  /**
   * Void Balance owed
   */
  voidBalance() {
    const data = {
      customerId: this.customer.id,
    };

    this.http.put(this.globals.API_ENDPOINT + '/payments/void-balance', data, {responseType: 'text'})
      .subscribe(() => {
        // console.log(res);
        this.voidBalanceCheck = false;
        this.balanceSettled = true;
      }, err => {
          err = JSON.parse(err.error);
          console.log(err);
          this.error = this.helpers.error(err.message);
      });

    // this.rest.put('/payments/void-balance', data)
    //   .subscribe(res => {
    //     console.log(res);
    //     this.voidBalanceCheck = false;
    //     this.balanceSettled = true;
    //   }, err => {
    //     console.log(err);
    //     // this.error = this.helpers.error(err.error.message);
    //   })

    // this.paymentsService.putUrl('void-balance', data)
    //   .subscribe(res => {
    //     console.log(res);
    //     this.voidBalanceCheck = false;
    //     this.balanceSettled = true;
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   })
  }

  /**
   * GET CUSTOMER SUBSCRIBERS
   */
  getCustomerSubscribers() {
    this.rest.get('/subscribers?q=customerId=' + this.subscriber.customerId)
      .subscribe(res => {
        this.subscribers = res;
        for (const sub of this.subscribers) {
          sub.postcode = this.postcodeService.postcodeFormat(sub.postcode);
          sub.surname = this.helpers.toTitleCase(sub.surname);
        }
        this.isActive();
        sessionStorage.setItem('billingSubscribers', JSON.stringify(this.subscribers));
        console.log(this.subscribers);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Customer Status
   */
  customerStatus() {
    this.rest.get('/customers/status/' + this.customer.id)
      .subscribe(res => {
        this.status = res['status'];
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * SUBSCRIBER ACTIVE / INACTIVE / Entitlements Awaiting Activation
   */
  isActive() {
    for (const sub of this.subscribers){
      this.rest.get('/subscribers/status/' + sub.id)
        .subscribe(res => {
          sub.active = res['status'].toUpperCase();
      // this.rest.get('/entitlements?subscriberId=' + sub.id + '&productType=subscription')
      //   .subscribe(res => {
      //     resp = res;
      //     console.log('resp', resp);
      //     if (!sub.enabled) {
      //       sub.active = false;
      //     }else {
      //       if (resp.length === 0) {
      //         // INACTIVE
      //         sub.active = false;
      //       }else if (resp.length > 0) {
      //         let active = true;
      //         for (const ent of resp) {
      //           if (!ent.activated) {
      //             active = false;
      //           }
      //         }
      //         if (active) {
      //           // ACTIVE
      //           sub.active = true;
      //         }else {
      //           // Entitlements Awaiting Activation
      //           sub.active = 'AW';
      //         }
      //       }
      //     }
      //   }, err => {
      //     this.error = this.helpers.error(err.error.message);
      //   });
      //
      //
      // this.rest.get('/entitlements?subscriberId=' + sub.id + '&productType=ppv_enabler')
      //   .subscribe(res => {
      //     resp = res;
      //     console.log('resp', resp);
      //     if (!sub.enabled) {
      //       sub.active = false;
      //     }else {
      //       if (resp.length === 0) {
      //         // INACTIVE
      //         sub.active = false;
      //       }else if (resp.length > 0) {
      //         let active = true;
      //         for (const ent of resp) {
      //           if (!ent.activated) {
      //             active = false;
      //           }
      //         }
      //         if (active) {
      //           // ACTIVE
      //           sub.active = true;
      //         }else {
      //           // Entitlements Awaiting Activation
      //           sub.active = 'AW';
      //         }
      //       }
      //     }
        }, err => {
          this.error = this.helpers.error(err.error.message);
        });
    }
  }

  // REQUEST ADDITIONAL CARD
  requestAdditionalCard() {
    sessionStorage.setItem('customerEmail', JSON.stringify(this.customer.email));
    this.route.navigate(['/newsubscriber']);
  }

  // JOIN CARD
  joinCard() {
    this.customerSearchDetails = {
      surname: this.customer.surname,
      postcode: this.customer.billingPostcode,
      addressLine1: this.customer.billingAddressLine1,
      addressLine2: this.customer.billingAddressLine2,
      addressLine3: this.customer.billingAddressLine3,
      addressLine4: this.customer.billingAddressLine4,
      addressLine5: this.customer.billingAddressLine5,
    };

    sessionStorage.removeItem('joinSubscriber');
    sessionStorage.removeItem('searchForm');
    this.subscriber.viewingCardNumber = '';
    sessionStorage.setItem('subscriber', JSON.stringify(this.subscriber));
    sessionStorage.setItem('customerId', this.customer.id);
    sessionStorage.setItem('cardSubscriberId', this.subscriber.cardSubscriberId);
    // this.route.navigate(['/joinSubscriber']);
  }

  // CHECK ADMIN
  checkIfAdmin() {
    this.accessGranted = '';
    this.user = '';
    this.password = '';
    if (this.role === 'admin') {
      this.accessGranted = 'admin';
    }else {
      this.requireAdmin = true;
    }
  }


  /**
   * Authorise
   */
  authorize() {
    this.requireAdmin = false;

    this.helpers.adminRequired(this.user, this.password)
      .subscribe(res => {
        const resp = res;
        if (resp['roles'][0].name === 'ROLE_ADMIN' || resp['roles'][0].name === 'ROLE_SUPER_ADMIN'
          || resp['roles'][0].name === 'ROLE_SUPERVISOR') {
          this.accessGranted = 'admin';
        }else {
          this.error = {message : 'Access Denied'};
          // SET TO FALSE AND EMPTY USER/PASS
          this.voidBalanceCheck = false;
          this.user = '';
          this.password = '';
        }
      }, err => {
        this.error = this.helpers.error(err.error.message);
        this.voidBalanceCheck = false;
        this.user = '';
        this.password = '';
      })
  }

  /**
   * Location back
   */
  back() {
    this.location.back();
  }

  /**
   * Save marketing preferences
   */
  saveMarketing() {
    const data = this.customer;
    data.customerId = data.id;
    delete data.id;
    delete data.createdAt;
    delete data.updatedAt;
    if (data.email === '') {
      data.email = 'unknown@mediaaccessservices.net';
    }

    this.rest.put('/customers', data)
      .subscribe(() => {
        this.getCustomer();
        this.editMarketing = true;
        this.success = true;
      }, err => {
        this.error = this.helpers.error(err.error.message);
        this.editMarketing = true;
      });
  }

  /**
   * Get Email Verification -> Get last email verification results from KICKBOX
   */
    getEmailVerification() {
      const data = {
        email: this.customer.email
      };
      let resp;
      this.rest.post('/email-verifications/find-by-email', data)
        .subscribe(res => {
          resp = res['body'][0];
          if (resp) {
            this.validEmail = resp.verificationResults.result.toLowerCase();
          }else {
            this.validEmail = 'notYetValidated'
          }
        }, err => {
          this.error = this.helpers.error(err.error.message);
        })
    }

}

