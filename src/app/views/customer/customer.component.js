"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var subscribers_service_1 = require("../../services/subscribers.service");
var payments_service_1 = require("../../services/payments.service");
var helpers_service_1 = require("../../services/helpers.service");
var products_service_1 = require("../../services/products.service");
// CUSTOMER
var CustomerComponent = /** @class */ (function () {
    function CustomerComponent(location, helpers, productsService, subscriberService, router, route, paymentsService) {
        this.location = location;
        this.helpers = helpers;
        this.productsService = productsService;
        this.subscriberService = subscriberService;
        this.router = router;
        this.route = route;
        this.paymentsService = paymentsService;
        this.cardTokens = {
            token: '',
            tokenId: ''
        };
        this.payMethDialog = false;
        this.billingInfo = {};
        this.balanceSettled = false;
        this.voidBalanceCheck = false;
        this.requireAdmin = false;
        this.accessGranted = false;
        this.freeviewNotAllowed = false;
    }
    ;
    CustomerComponent.prototype.ngOnInit = function () {
        var _this = this;
        // GET USER ROLE
        if (sessionStorage.getItem('role') != null) {
            this.role = sessionStorage.getItem('role');
            console.log(this.role);
        }
        // GET SUBSCRIBER STORED
        if (sessionStorage.getItem('subscriber')) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
            console.log(this.subscriber);
        }
        // GET CUSTOMERS (includes get customer subscribers,  get payment card, get billing info)
        this.getCustomer();
        // IF ADD/UPDATE PAYMENT CARD
        if (sessionStorage.getItem('cardProcess')) {
            this.customer = JSON.parse(sessionStorage.getItem('customer'));
            // GET TOKENS
            this.router.queryParams
                .subscribe(function (params) {
                _this.cardTokens.token = params['token'];
                _this.cardTokens.tokenId = params['tokenId'];
            });
            if (this.cardTokens.token == undefined || this.cardTokens.tokenId == undefined
                || this.cardTokens.token == '' || this.cardTokens.tokenId == '') {
                this.cardError = {
                    message: 'Payment details could not be validated.',
                    message2: 'Please Check details were correct or select a different payment method.'
                };
                sessionStorage.removeItem('cardProcess');
            }
            else {
                // SAVE PAYMENT CARD
                var data = {
                    customerId: this.customer.id,
                    token: this.cardTokens.token,
                    tokenId: this.cardTokens.tokenId
                };
                this.paymentsService.putUrl('save-payment-card', data)
                    .subscribe(function (res) {
                    _this.getPaymentCard();
                }, function (err) {
                    _this.error = _this.helpers.getError(err);
                });
                sessionStorage.removeItem('cardProcess');
            }
        }
    };
    // GET CUSTOMER
    CustomerComponent.prototype.getCustomer = function () {
        var _this = this;
        // ADVANCED SEARCH: GET CUSTOMER WITH CUSTOMER ID
        // if(sessionStorage.getItem('customerId') != null){
        //   this.subscriberService.getCustomerId(sessionStorage.getItem('customerId'))
        //     .subscribe(res => {
        //       console.log('res', res)
        //       this.customer = res;
        //       // replace null values in customer object to avoid form errors
        //       for (var key of Object.keys(this.customer)) {
        //         if(this.customer[key] == null){
        //           this.customer[key] = '';
        //         }
        //       }
        //       if(this.customer.email == 'unknown@mediaaccessservices.net'){
        //         this.customer.email = '';
        //       }
        //       this.getCustomerSubscribers();
        //       this.getPaymentCard();
        //       // GET BILLING INFO
        //       this.getBillingInfo();
        //     }, err => {
        //       this.error = this.helpers.getError(err);
        //     })
        // }else{
        // GET CUSTOMER WITH SUBSCRIBER ID
        console.log('subscriberId', this.subscriber.id);
        this.subscriberService.getCustomer(this.subscriber.id)
            .subscribe(function (res) {
            _this.customer = res[0];
            console.log('customer', _this.customer);
            // replace null values in customer object to avoid form errors
            for (var _i = 0, _a = Object.keys(_this.customer); _i < _a.length; _i++) {
                var key = _a[_i];
                if (_this.customer[key] == null) {
                    _this.customer[key] = '';
                }
            }
            if (_this.customer.email == 'unknown@mediaaccessservices.net') {
                _this.customer.email = '';
            }
            _this.getCustomerSubscribers();
            _this.getPaymentCard();
            // GET BILLING INFO
            _this.getBillingInfo();
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
        // }
    };
    // GET PAYMENT CARD
    CustomerComponent.prototype.getPaymentCard = function () {
        var _this = this;
        this.paymentsService.getUrl('has-payment-card?customerId=', this.customer.id)
            .subscribe(function (res) {
            _this.hasPaymentCard = JSON.parse(res['_body']).hasPaymentCard;
            _this.paymentDetailCard = JSON.parse(res['_body']).detail;
            if (_this.paymentDetailCard.toLowerCase() !== 'direct debit' && _this.paymentDetailCard.toLowerCase() !== 'unknown') {
                _this.paymentDetailNumber = _this.paymentDetailCard.slice(_this.paymentDetailCard.length - 4, _this.paymentDetailCard.length);
                _this.paymentDetailCard = _this.paymentDetailCard.slice(0, _this.paymentDetailCard.length - 4);
            }
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // GET BILLING INFO
    CustomerComponent.prototype.getBillingInfo = function () {
        var _this = this;
        this.paymentsService.getUrl('get-balance-owed?customerId=', this.customer.id)
            .subscribe(function (res) {
            var resp = JSON.parse(res['_body']);
            console.log('billing', resp);
            _this.billingInfo = {
                owed: resp.balanceOwed
            };
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // ADD PAYMENT CARD
    CustomerComponent.prototype.addPaymentCard = function () {
        var _this = this;
        sessionStorage.setItem('cardProcess', 'true');
        sessionStorage.setItem('customer', JSON.stringify(this.customer));
        this.payMethDialog = false;
        // Data to send
        var data = {
            customerId: this.customer.id,
            returnUrl: window.location.href,
            directDebit: this.payMeth
        };
        // Data to url string
        var str = Object.keys(data).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
        }).join('&');
        this.paymentsService.getUrl('get-payment-card-url?', str)
            .subscribe(function (res) {
            console.log(res);
            var url = JSON.parse(res['_body']);
            location.replace(url.storeCardUrl);
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // SETTLE BALANCE
    CustomerComponent.prototype.settleBalance = function () {
        var _this = this;
        var data = {
            customerId: this.customer.id,
            amount: this.billingInfo.owed
        };
        this.paymentsService.putUrl('settle-balance', data)
            .subscribe(function (res) {
            console.log(res);
            _this.balanceSettled = true;
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // VOID BALANCE
    CustomerComponent.prototype.voidBalance = function () {
        var _this = this;
        var data = {
            customerId: this.customer.id,
        };
        this.paymentsService.putUrl('void-balance', data)
            .subscribe(function (res) {
            console.log(res);
            _this.voidBalanceCheck = false;
            _this.balanceSettled = true;
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // GET CUSTOMER SUBSCRIBERS
    CustomerComponent.prototype.getCustomerSubscribers = function () {
        var _this = this;
        this.subscriberService.getCustomerSubscribers(this.subscriber.customerId)
            .subscribe(function (res) {
            _this.subscribers = res;
            _this.isActive();
            sessionStorage.setItem('billingSubscribers', JSON.stringify(_this.subscribers));
            console.log(_this.subscribers);
        }, function (err) {
            _this.error = JSON.parse(err['_body']);
        });
    };
    // SUBSCRIBER ACTIVE / INACTIVE / Entitlements Awaiting Activation
    CustomerComponent.prototype.isActive = function () {
        var _this = this;
        var _loop_1 = function (sub) {
            this_1.productsService.getById('E', sub.id)
                .subscribe(function (res) {
                if (!sub.enabled) {
                    sub.active = false;
                }
                else {
                    if (res.length === 0) {
                        // INACTIVE
                        sub.active = false;
                    }
                    else if (res.length > 0) {
                        var active = true;
                        for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                            var ent = res_1[_i];
                            if (!ent.activated) {
                                active = false;
                            }
                        }
                        if (active) {
                            // ACTIVE
                            sub.active = true;
                        }
                        else {
                            // Entitlements Awaiting Activation
                            sub.active = 'AW';
                        }
                    }
                }
            }, function (err) {
                _this.error = JSON.parse(err['_body']);
            });
        };
        var this_1 = this;
        for (var _i = 0, _a = this.subscribers; _i < _a.length; _i++) {
            var sub = _a[_i];
            _loop_1(sub);
        }
    };
    // REQUEST ADDITIONAL CARD
    CustomerComponent.prototype.requestAdditionalCard = function () {
        sessionStorage.setItem('customerEmail', JSON.stringify(this.customer.email));
        this.route.navigate(['/newsubscriber']);
    };
    // JOIN CARD
    CustomerComponent.prototype.joinCard = function () {
        sessionStorage.removeItem('joinSubscriber');
        sessionStorage.removeItem('searchForm');
        this.subscriber.viewingCardNumber = '';
        sessionStorage.setItem('subscriber', JSON.stringify(this.subscriber));
        sessionStorage.setItem('customerId', this.customer.id);
        sessionStorage.setItem('cardSubscriberId', this.subscriber.cardSubscriberId);
        this.route.navigate(['/joinSubscriber']);
    };
    // CHECK ADMIN
    CustomerComponent.prototype.checkIfAdmin = function () {
        this.accessGranted = '';
        this.user = '';
        this.password = '';
        if (this.role == 'admin') {
            this.accessGranted = 'admin';
        }
        else {
            this.requireAdmin = true;
        }
    };
    // AUTHORIZE
    CustomerComponent.prototype.authorize = function () {
        var _this = this;
        this.requireAdmin = false;
        this.helpers.adminRequired(this.user, this.password)
            .subscribe(function (res) {
            var resp = JSON.parse(res['_body']);
            if (resp.roles[0].name == 'ROLE_ADMIN') {
                _this.accessGranted = 'admin';
            }
            else {
                _this.error = { message: 'Access Denied' };
                // SET TO FALSE AND EMPTY USER/PASS
                _this.voidBalanceCheck = false;
                _this.user = '';
                _this.password = '';
            }
        }, function (err) {
            _this.error = _this.helpers.getError(err);
            _this.voidBalanceCheck = false;
            _this.user = '';
            _this.password = '';
        });
    };
    // BACK
    CustomerComponent.prototype.back = function () {
        this.location.back();
    };
    CustomerComponent.prototype.log = function () {
        console.log(this.payMeth);
    };
    CustomerComponent = __decorate([
        core_1.Component({
            selector: 'customer',
            templateUrl: 'customer.component.html',
            styleUrls: ['customer.component.css'],
            providers: [subscribers_service_1.SubscribersService, payments_service_1.PaymentsService, products_service_1.ProductsService, helpers_service_1.HelpersService]
        })
    ], CustomerComponent);
    return CustomerComponent;
}());
exports.CustomerComponent = CustomerComponent;
