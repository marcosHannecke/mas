import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ValidatorsService } from '../../services/validators.service';
import { HelpersService } from '../../services/helpers.service';
import { PostcodeService } from '../../services/postcode.service';
import {Router, ActivatedRoute} from '@angular/router';
import { RestService } from '../../services/rest.service';

@Component({
  selector: 'editCustomer',
  templateUrl: 'editCustomer.component.html',
  styles: ['.col-lg-2.control-label {padding-left: 0; padding-right: 0}'],
  providers: [ValidatorsService, HelpersService, PostcodeService, RestService]
})
export class EditCustomerComponent implements OnInit {
  detailsForm: FormGroup;
  submitted = false;
  customer: any;
  custId: any;
  error: any;
  success: any = false;
  message: string;
  subscribers: any;
  subsData: Subscriber = <any>{};
  spread = false;
  addressList: any;
  role: any;
  emailVerificationResponse: any;
  didYouMean: any;
  noEmail = false;

  constructor(private router: Router, private helpers: HelpersService,
              private activateRoute: ActivatedRoute, private fb: FormBuilder, private  val: ValidatorsService,
              private postcodeService: PostcodeService, private rest: RestService) {};

  ngOnInit() {
    if (sessionStorage.getItem('role') != null) {
      this.role = sessionStorage.getItem('role');
    }

    this.activateRoute.queryParams
      .subscribe(res => {
        this.custId = res['id'];
        console.log(this.custId);
      });

    this.rest.get('/customers?q=subscriberId=' + this.custId)
    // this.subscriberService.getCustomer(this.custId)
      .subscribe(res => {
        this.customer = res[0];
        console.log(this.customer);
        // replace null values in customer object to avoid form errors
        for (const key of Object.keys(this.customer)) {
          if (this.customer[key] == null) {
            this.customer[key] = '';
          }
        }

        if (this.customer.email === 'unknown@mediaaccessservices.net' ) {
          this.customer.email = '';
          this.noEmail = true;
        }

        this.detailsForm = this.fb.group({
          "customerId" : [this.customer.id],
          "title": [this.customer.title, [Validators.required, this.val.specialChar]],
          "initials" : [this.customer.initials.toUpperCase(), [Validators.required, Validators.maxLength(3), this.val.letterSpaces]],
          "email": [this.customer.email,  [Validators.maxLength(128)]],
          "firstname": [this.customer.firstname,  [Validators.maxLength(128)]],
          "surname" : [this.customer.surname, [Validators.required,  Validators.maxLength(35), this.val.letterSpaces]],
          "billingAddressLine1" : [this.customer.billingAddressLine1, [Validators.required, Validators.maxLength(35), this.val.specialChar]],
          "billingAddressLine2" : [this.customer.billingAddressLine2, [Validators.required, Validators.maxLength(35), this.val.specialChar]],
          "billingAddressLine3" : [this.customer.billingAddressLine3, [Validators.maxLength(35)] ],
          "billingAddressLine4" : [this.customer.billingAddressLine4, [Validators.maxLength(35)]],
          "billingAddressLine5" : [this.customer.billingAddressLine5 , [Validators.maxLength(35)]],
          "billingPostcode" : [this.postcodeService.postcodeFormat(this.customer.billingPostcode.toUpperCase()), [Validators.required, Validators.maxLength(9), this.val.specialChar]],
          "billingCountryCode" : [this.customer.billingCountryCode],
          "phones": this.fb.group({
            "homeTelNr": [this.customer.homeTelNo, [Validators.maxLength(15), this.val.onlyNumbers]],
            "workTelNr": [this.customer.workTelNo, [Validators.maxLength(15), this.val.onlyNumbers]],
            "mobTelNr": [this.customer.mobTelNo, [Validators.maxLength(15), this.val.onlyNumbers]],
          }, {validator: this.val.phones})
        });

        if (this.customer.email.length > 0) {
          this.getEmailVerification();
        }

      }, err => {
        this.error = this.helpers.error(err.error.message);
      });

    this.subscribers = JSON.parse(sessionStorage.getItem('billingSubscribers'));

  }

  /**
   * Update customer details
   */
  update() {
    // close dialog
    this.spread = false;
    this.submitted = true;

    if (this.detailsForm.valid) {
      const phones = {
        homeTelNo: '',
        workTelNo: '',
        mobTelNo: ''
      };
      const json = Object.assign(this.detailsForm.value, phones);
      json.homeTelNo = this.detailsForm.value.phones.homeTelNr;
      json.workTelNo = this.detailsForm.value.phones.workTelNr;
      json.mobTelNo = this.detailsForm.value.phones.mobTelNr;
      delete json.phones;

      // Initials to uppercase
      json.initials = json.initials.toUpperCase();
      // Capitalize surname
      json.surname = this.helpers.toTitleCase(json.surname);
      // Marketing preferences
      json.marketingPreferences = this.customer.marketingPreferences;

      if (json.email === '') {
        json.email = 'unknown@mediaaccessservices.net'
      }

      this.rest.put('/customers', json)
        .subscribe(res => {
          this.success = true;
        }, err => {
          this.error = this.helpers.error(err.error.message);
        })
    }
  }

  /**
   * Spread changes to all customer accounts (subscribers)
   */
  spreadChanges() {
    // close dialog
    this.spread = false;
    this.submitted = true;
    // update subscribers data
    if (this.detailsForm.valid) {
      this.subsData.title = this.detailsForm.value.title;
      this.subsData.initials = this.detailsForm.value.initials.toUpperCase();
      this.subsData.surname = this.helpers.toTitleCase(this.detailsForm.value.surname);
      this.subsData.addressLine1 = this.detailsForm.value.billingAddressLine1;
      this.subsData.addressLine2 = this.detailsForm.value.billingAddressLine2;
      this.subsData.addressLine3 = this.detailsForm.value.billingAddressLine3;
      this.subsData.addressLine4 = this.detailsForm.value.billingAddressLine4;
      this.subsData.addressLine5 = this.detailsForm.value.billingAddressLine5;
      this.subsData.postcode = this.detailsForm.value.billingPostcode;
      this.subsData.countryCode = this.detailsForm.value.billingCountryCode;
      this.subsData.homeTelNr = this.detailsForm.value.phones.homeTelNr;
      this.subsData.workTelNr = this.detailsForm.value.phones.workTelNr;
      this.subsData.mobTelNr = this.detailsForm.value.phones.mobTelNr;

      for (const subscriber of this.subscribers) {
        if (subscriber.id) {
          this.subsData.subscriberId = subscriber.id;
        } else if (subscriber.cardSubscriberId != null) {
          this.subsData.cardSubscriberId = subscriber.cardSubscriberId;
        }

        console.log(this.subsData);

        this.rest.put('/subscribers/', this.subsData)
          .subscribe(res => {
            this.update();
          }, err => {
            this.error = this.helpers.error(err.error.message);
          })

        // this.subscriberService.urlSubscribers('', this.subsData)
        //   .subscribe(res => {
        //     this.update();
        //   }, err => {
        //     this.error = JSON.parse(err['_body']);
        //   })
      }
    }
  }

  // FIND ADDRESS
  findAddress() {
    const postcode = this.detailsForm.value.billingPostcode.replace(' ', '');

    this.postcodeService.postcode(postcode)
      .subscribe(res => {
        this.addressList = res;
        if (this.addressList.length === 0) {
          this.addressList = [{
            notFound : 'NO ADDRESS FOUND'
          }]
        }
      }, err => {
        if (this.detailsForm.value.postcode.length === 0) {
          this.error = {
            description: 'Please Enter a Postcode.'
          }
        }else {
          this.error = this.helpers.error(err.error.message);
        }
      })
  }

  // SET ADDRESS
  setAddress(address) {
    if (address.notFound) {
      this.addressList = null;
      // this.disableInput = false;
    }else {
      this.addressList = null;
      // this.disableInput = true;
      // this.addressManually = true;
      const add = this.helpers.nullCheck(address);
      this.detailsForm.controls['billingAddressLine1'].setValue(add.addressLine1.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.detailsForm.controls['billingAddressLine2'].setValue(add.addressLine2.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.detailsForm.controls['billingAddressLine3'].setValue(add.addressLine3.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.detailsForm.controls['billingAddressLine4'].setValue(add.addressLine4.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.detailsForm.controls['billingAddressLine5'].setValue(add.addressLine5.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
    }
  }


  backTo() {
    this.success = false;
    sessionStorage.setItem('addressIncorrect', 'false');
    this.router.navigate(['/process25']);
  }

  /**
   * Email Verification
   */
  validateEmail() {
    let email;
    if (this.detailsForm.value.email) {
      this.emailVerificationResponse = 'loading';
      email = this.detailsForm.value.email;
      const data  = {
        customerId: this.customer.id,
        email: email,
        type: 'KICKBOX'
      };

      let resp;
      this.rest.post('/email-verifications', data)
        .subscribe(res => {
          resp = res;
          this.emailVerificationResponse = resp.body.verificationResults.result;
          // set did you mean value
          if (resp.body.verificationResults.didYouMean != null) {
            this.didYouMean = resp.body.verificationResults.didYouMean;
          }
        }, err => {
          if (err.error.message.includes('403')) {
            this.emailVerificationResponse = 'Insufficient Balance';
          }else {
            this.emailVerificationResponse = false;
            this.error = this.helpers.error(err.error.message);
          }
        });
    }
  }

  /**
   * Patch Email value
   */
  patchEmail() {
    this.detailsForm.patchValue({
      email: this.didYouMean
    });
    this.emailVerificationResponse = false;
  }

  /**
   * NO EMAIL CHECKBOX
   */
  noEmailChecked() {
    this.noEmail = !this.noEmail;
    if (this.noEmail) {
      this.detailsForm.patchValue({email: '' });
      this.emailVerificationResponse = null;
    }else {
      this.detailsForm.patchValue({email: '' })
    }
  }


  /**
   * Get Email Verification -> Get last email verification results from KICKBOX
   */
  getEmailVerification() {
    const data = {
      email: this.customer.email
    };
    let resp;
    this.rest.post('/email-verifications/find-by-email', data)
      .subscribe(res => {
        resp = res['body'][0];
        this.emailVerificationResponse = resp.verificationResults.result;
        console.log('verification', resp)
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }
}


interface Subscriber {
  title: any,
  initials: any,
  surname: any,
  addressLine1: any,
  addressLine2: any,
  addressLine3: any,
  addressLine4: any,
  addressLine5: any,
  postcode: any,
  countryCode: any,
  homeTelNr: any,
  workTelNr: any,
  mobTelNr: any,
  subscriberId ?: any;
  cardSubscriberId ?: any;
}
