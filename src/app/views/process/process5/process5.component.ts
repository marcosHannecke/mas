import { Component, OnInit } from '@angular/core';
import{ FormGroup, FormBuilder, Validators } from '@angular/forms';
import {ValidatorsService} from '../../../services/validators.service';
import {Router, ActivatedRoute, Params} from '@angular/router';

// PROCESS 5: I need to make my existing viewing card work with my new set top box
@Component({
  selector: 'process5',
  templateUrl: 'process5.component.html',
  providers: [ValidatorsService]
})
export class Process5Component implements OnInit {
  subscriber : any;
  error : any;

  constructor(private router : Router){};

  ngOnInit(){
    this.router.navigate(['/process4'], { queryParams: { rePair: 1 } })
  }
}
