"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
// PROCESS 5: I need to make my existing viewing card work with my new set top box
var Process5Component = /** @class */ (function () {
    function Process5Component(subscriberService, router) {
        this.subscriberService = subscriberService;
        this.router = router;
    }
    ;
    Process5Component.prototype.ngOnInit = function () {
        this.router.navigate(['/process4'], { queryParams: { rePair: 1 } });
    };
    Process5Component = __decorate([
        core_1.Component({
            selector: 'process5',
            templateUrl: 'process5.component.html',
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService]
        })
    ], Process5Component);
    return Process5Component;
}());
exports.Process5Component = Process5Component;
