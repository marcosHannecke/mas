"use strict";
//
// PROCESS 19: Pin and Ratings
//
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var helpers_service_1 = require("../../../services/helpers.service");
var products_service_1 = require("../../../services/products.service");
var core_2 = require("@angular/core");
var Process19Component = /** @class */ (function () {
    function Process19Component(helpers, subscriberService, productsService, router, fb, val) {
        this.helpers = helpers;
        this.subscriberService = subscriberService;
        this.productsService = productsService;
        this.router = router;
        this.fb = fb;
        this.val = val;
        this.success = false;
        this.watershed = 'Y';
    }
    ;
    Process19Component.prototype.ngOnInit = function () {
        var _this = this;
        //GET QUERY PARAMS
        this.router.queryParams
            .subscribe(function (params) {
            _this.rapb = params['rapb'];
        });
        //GET 'Resend All Personal Bits' IF STORED
        if (sessionStorage.getItem('rapb')) {
            this.rapb = sessionStorage.getItem('rapb');
        }
        if (this.rapb == 1) {
            this.rapb = {
                step1: '1. Go to Home',
                step2: '2. Enter Pin',
                step3: '3. Select Family',
                step4: '4. Select Family Setting',
                step5: "5. Set 'Hide Adult'  off"
            };
            sessionStorage.setItem('rapb', '1');
        }
        //GET SUBSCRIBER STORED
        if (sessionStorage.getItem('subscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
            //GET PIN AND RATINGS
            this.subscriberService.urlGet('get-pin-and-ratings?cardSubscriberId=' + this.subscriber.cardSubscriberId)
                .subscribe(function (res) {
                _this.pinRatings = JSON.parse(res["_body"]);
                _this.checkWatershed();
                _this.spendLimit();
                console.log(_this.pinRatings);
            }, function (err) {
                _this.error = _this.helpers.getError(err);
            });
        }
    };
    Process19Component.prototype.checkWatershed = function () {
        for (var i = 17; i <= 19; i++) {
            if (this.pinRatings['RATING-BLOCK'].rating_setting[i] == 'N') {
                return this.watershed = 'N';
            }
        }
    };
    Process19Component.prototype.spendLimit = function () {
        this.indSpendLimit = this.pinRatings['INDIVIDUAL-SPEND-LIMIT'] / 100;
        this.gloSpendLimit = this.pinRatings['GLOBAL-SPEND-LIMIT'] / 100;
    };
    //RESEND ALL PERSONAL BITS
    Process19Component.prototype.resendAllPersonalBits = function () {
        var _this = this;
        var data = {
            cardSubscriberId: this.subscriber.cardSubscriberId
        };
        this.productsService.urlProducts('E', 'resend-all-personal-bits', data)
            .subscribe(function (res) {
            console.log(res);
            _this.success = true;
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    Process19Component = __decorate([
        core_1.Component({
            selector: 'process19',
            templateUrl: 'process19.component.html',
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService, products_service_1.ProductsService, helpers_service_1.HelpersService]
        })
    ], Process19Component);
    return Process19Component;
}());
exports.Process19Component = Process19Component;
var YesNo = /** @class */ (function () {
    function YesNo() {
    }
    YesNo.prototype.transform = function (text) {
        if (text == 'Y') {
            return 'Yes';
        }
        else if (text == 'N') {
            return 'No';
        }
    };
    YesNo = __decorate([
        core_2.Pipe({ name: 'yesNo' })
    ], YesNo);
    return YesNo;
}());
exports.YesNo = YesNo;
