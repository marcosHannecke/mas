//
// PROCESS 19: Pin and Ratings
//

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from '../../../services/helpers.service';
import {Router, ActivatedRoute} from '@angular/router';
import {PipeTransform, Pipe} from '@angular/core';
import { RestService } from '../../../services/rest.service';
import { HttpClient} from '@angular/common/http';
import {Globals} from '../../../app.global';

@Component({
  selector: 'process19',
  templateUrl: 'process19.component.html',
  providers: [ValidatorsService, HelpersService, RestService]
})
export class Process19Component implements OnInit {
  addressForm: FormGroup;
  subscriber: any;
  error: any;
  success = false;
  pinRatings: any;
  watershed = 'Y';
  indSpendLimit: number;
  gloSpendLimit: number;
  rapb: any;

  constructor(private helpers: HelpersService, private router: ActivatedRoute, private fb: FormBuilder,
              private  val: ValidatorsService, private rest: RestService, private http: HttpClient, private globals: Globals) {};
  ngOnInit() {
    // GET QUERY PARAMS
    this.router.queryParams
      .subscribe(params => {
        this.rapb = params['rapb'];
      });

    // GET 'Resend All Personal Bits' IF STORED
    if (sessionStorage.getItem('rapb')) {
      this.rapb = sessionStorage.getItem('rapb');
    }

    if (this.rapb == 1) {
      this.rapb = {
        step1: '1. Go to Home',
        step2: '2. Enter Pin',
        step3: '3. Select Family',
        step4: '4. Select Family Setting',
        step5: '5. Set \'Hide Adult\'  off'
      };
      sessionStorage.setItem('rapb', '1');
    }

    // GET SUBSCRIBER STORED
    if (sessionStorage.getItem('subscriber') != null) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));

      // GET PIN AND RATINGS
      this.rest.get('/subscribers/get-pin-and-ratings?cardSubscriberId=' + this.subscriber.cardSubscriberId)
        .subscribe(res => {
          this.pinRatings = res;
          this.checkWatershed();
          this.spendLimit();
          console.log(this.pinRatings);
        }, err => {
          this.error = this.helpers.error(err.error.message);
        })
    }
  }

  /**
   * Check watershed
   * @returns {string}
   */
  checkWatershed() {
    for (var i = 17; i <= 19; i++) {
      if (this.pinRatings['RATING-BLOCK'].rating_setting[i] == 'N') {
       return this.watershed = 'N'
      }
    }
  }

  /**
   * Spend Limit
   */
  spendLimit(){
    this.indSpendLimit = this.pinRatings['INDIVIDUAL-SPEND-LIMIT'] / 100;
    this.gloSpendLimit = this.pinRatings['GLOBAL-SPEND-LIMIT'] / 100;
  }

  /**
   * RESEND ALL PERSONAL BITS
   */
  resendAllPersonalBits() {
    const data = {
      cardSubscriberId : this.subscriber.cardSubscriberId
    };

    this.http.put(this.globals.API_ENDPOINT + '/entitlements/resend-all-personal-bits', data, {responseType: 'text'})
      .subscribe(res => {
        console.log(res);
        this.success = true;
      }, err => {
        err = JSON.parse(err.error);
        console.log(err);
        if (err.fieldErrors) {
          this.error = err.fieldErrors[0];
        }else {
          this.error = this.helpers.error(err.message);
        }
      })

  }
}


@Pipe({ name: 'yesNo' })
export class YesNo implements PipeTransform {
  transform(text: string): string {
    if (text == 'Y') {
      return 'Yes'
    }else if (text == 'N') {
      return  'No'
    }
  }
}
