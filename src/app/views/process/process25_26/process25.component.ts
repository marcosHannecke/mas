//
// PROCESS 25: Request replacement card
//
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from '../../../services/helpers.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'process25',
  templateUrl: 'process25.component.html',
  styles : [ '.details .fa{padding: 4px 6px; border-radius: 50%;}' +
'.details .fa-map-marker{color: white; background: #e47700; padding: 4px 7px;}' +
  '.col-lg-2.control-label {padding-left: 0; padding-right: 0}'],
  providers: [ValidatorsService, HelpersService]
})
export class Process25Component implements OnInit {
  addressForm: FormGroup;
  requestForm: FormGroup;
  submitted: boolean = false;
  subscriber: any;
  error: any;
  success: boolean = false;
  // replacementReasonCode = RRcode;
  adviseUndelivered: boolean = false;

  address: boolean = true;
  succ: any;
  urgent: any;
  params: any;

  title = 'Request Replacement Card';
  options = [];


  constructor(private helpers: HelpersService, private router: ActivatedRoute, private fb: FormBuilder,
              private  val: ValidatorsService, private http: HttpClient) {
  };

  ngOnInit() {
    // this.router.params
    //   .subscribe(res => {
    //     this.urgent = res;
    //     if (this.urgent.u == 'undelivered') {
    //       this.adviseUndelivered = true;
    //     }
    //   });


    //
    //   if (this.urgent.u == 'undelivered') {
    //     this.requestForm = this.fb.group({
    //       "replacementReasonCode" : ["E1", Validators.required],
    //       "urgent" : [false]
    //     });
    //   }else {
    //     this.requestForm = this.fb.group({
    //       "replacementReasonCode" : ["", Validators.required],
    //       "urgent" : [false]
    //     });
    //   }
    //
    //   if (this.urgent.u == "y"){
    //     this.requestForm.controls['urgent'].setValue(true)
    //   }
    //
    //   if (sessionStorage.getItem('subscriber') != null) {
    //     this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    //     this.addressForm = this.fb.group({
    //       "cardSubscriberId" : [this.subscriber.cardSubscriberId],
    //       "title": [this.subscriber.title, [Validators.required, this.val.specialChar]],
    //       "initials" : [this.subscriber.initials.toUpperCase(), [Validators.required, Validators.maxLength(3), this.val.letterSpaces]],
    //       "surname" : [this.subscriber.surname, [Validators.required,  Validators.maxLength(35), this.val.letterSpaces]],
    //       "addressLine1" : [ this.subscriber.addressLine1, [Validators.required, Validators.maxLength(35), this.val.specialChar]],
    //       "addressLine2" : [this.subscriber.addressLine2, [Validators.required, Validators.maxLength(35), this.val.specialChar]],
    //       "addressLine3" : [this.subscriber.addressLine3 ],
    //       "addressLine4" : [this.subscriber.addressLine4],
    //       "addressLine5" : [this.subscriber.addressLine5],
    //       "postcode" :[this.subscriber.postcode, [Validators.required, Validators.maxLength(9), this.val.specialChar]],
    //       "countryCode" : [this.subscriber.countryCode],
    //     });
    //   }
    // }


    // requestCard() {
    //   this.submitted = true;
    //
    //   // Success message if pin and rating TRUE ??
    //    this.succ = {
    //     address: false,
    //     card : false
    //   };
    //
    //   if (!this.address) {
    //     this.updateAddress()
    //   }else {
    //     this.succ.address = true;
    //     this.requestReplacementCard();
    //   }
    // }

    // requestReplacementCard() {
    //   if (this.requestForm.valid)  {
    //     const data = {
    //       "cardSubscriberId" : this.subscriber.cardSubscriberId,
    //       "replacementReasonCode" : this.requestForm.value.replacementReasonCode,
    //       "urgent": this.requestForm.value.urgent
    //     };
    //     this.http.put(AppSettings.API_ENDPOINT + '/subscribers/request-replacement-card', data, {responseType: 'text'})
    //       .subscribe(res => {
    //         console.log(res);
    //         this.succ.card = true;
    //         if (this.succ.card && this.succ.address) {
    //           this.success = true;
    //         }
    //       }, err => {
    //         err = JSON.parse(err.error);
    //         console.log(err);
    //         if (err.fieldErrors) {
    //           this.error = err.fieldErrors[0];
    //         }else {
    //           this.error = this.helpers.error(err.message);
    //         }
    //       })
    //   }
    // }

    // updateAddress() {
    //   if (this.addressForm.valid && this.requestForm.valid) {
    //     let json = this.addressForm.value;
    //
    //     // Initials to uppercase
    //     json.initials = json.initials.toUpperCase();
    //     // Capitalize surname
    //     json.surname = this.helpers.toTitleCase(json.surname);
    //
    //     this.http.put(AppSettings.API_ENDPOINT + '/subscribers', json, {responseType: 'text'})
    //       .subscribe(res => {
    //         console.log(res);
    //         this.succ.address = true;
    //         this.requestReplacementCard();
    //         if (this.succ.card && this.succ.address) {
    //           this.success = true;
    //         }
    //       }, err => {
    //         err = JSON.parse(err.error);
    //         console.log(err);
    //         if (err.fieldErrors) {
    //           this.error = err.fieldErrors[0];
    //         }else {
    //           this.error = this.helpers.error(err.message);
    //         }
    //       })
    //   }
    // }
  }
}
