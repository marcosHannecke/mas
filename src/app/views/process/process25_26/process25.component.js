"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
//
// PROCESS 25: Request replacement card
//
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var helpers_service_1 = require("../../../services/helpers.service");
var Process25Component = /** @class */ (function () {
    function Process25Component(helpers, subscriberService, router, fb, val) {
        this.helpers = helpers;
        this.subscriberService = subscriberService;
        this.router = router;
        this.fb = fb;
        this.val = val;
        this.submitted = false;
        this.success = false;
        this.replacementReasonCode = RRcode;
        this.adviseUndelivered = false;
        this.address = true;
    }
    ;
    Process25Component.prototype.ngOnInit = function () {
        var _this = this;
        this.router.params
            .subscribe(function (res) {
            _this.urgent = res;
            if (_this.urgent.u == 'undelivered') {
                _this.adviseUndelivered = true;
            }
        });
        if (this.urgent.u == 'undelivered') {
            this.requestForm = this.fb.group({
                "replacementReasonCode": ["E1", forms_1.Validators.required],
                "urgent": [false]
            });
        }
        else {
            this.requestForm = this.fb.group({
                "replacementReasonCode": ["", forms_1.Validators.required],
                "urgent": [false]
            });
        }
        if (this.urgent.u == "y") {
            this.requestForm.controls['urgent'].setValue(true);
        }
        if (sessionStorage.getItem('subscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
            this.addressForm = this.fb.group({
                "cardSubscriberId": [this.subscriber.cardSubscriberId],
                "title": [this.subscriber.title, [forms_1.Validators.required, this.val.specialChar]],
                "initials": [this.subscriber.initials, [forms_1.Validators.required, forms_1.Validators.maxLength(3), this.val.letterSpaces]],
                "surname": [this.subscriber.surname, [forms_1.Validators.required, forms_1.Validators.maxLength(35), this.val.letterSpaces]],
                "addressLine1": [this.subscriber.addressLine1, [forms_1.Validators.required, forms_1.Validators.maxLength(35), this.val.specialChar]],
                "addressLine2": [this.subscriber.addressLine2, [forms_1.Validators.required, forms_1.Validators.maxLength(35), this.val.specialChar]],
                "addressLine3": [this.subscriber.addressLine3],
                "addressLine4": [this.subscriber.addressLine4],
                "addressLine5": [this.subscriber.addressLine5],
                "postcode": [this.subscriber.postcode, [forms_1.Validators.required, forms_1.Validators.maxLength(9), this.val.specialChar]],
                "countryCode": [this.subscriber.countryCode],
            });
        }
    };
    Process25Component.prototype.requestCard = function () {
        this.submitted = true;
        //Success message if pin and rating TRUE
        this.succ = {
            address: false,
            card: false
        };
        if (!this.address) {
            this.updateAddress();
        }
        else {
            this.succ.address = true;
            this.requestReplacementCard();
        }
    };
    Process25Component.prototype.requestReplacementCard = function () {
        var _this = this;
        if (this.requestForm.valid) {
            var data = {
                "cardSubscriberId": this.subscriber.cardSubscriberId,
                "replacementReasonCode": this.requestForm.value.replacementReasonCode,
                "urgent": this.requestForm.value.urgent
            };
            this.subscriberService.urlSubscribers('request-replacement-card', data)
                .subscribe(function (res) {
                console.log(res);
                _this.succ.card = true;
                if (_this.succ.card && _this.succ.address) {
                    _this.success = true;
                }
            }, function (err) {
                _this.error = _this.helpers.getError(err);
            });
        }
    };
    Process25Component.prototype.updateAddress = function () {
        var _this = this;
        if (this.addressForm.valid && this.requestForm.valid) {
            var json = this.addressForm.value;
            this.subscriberService.urlSubscribers('', json)
                .subscribe(function (res) {
                console.log(res);
                _this.succ.address = true;
                _this.requestReplacementCard();
                if (_this.succ.card && _this.succ.address) {
                    _this.success = true;
                }
            }, function (err) {
                _this.error = _this.helpers.getError(err);
            });
        }
    };
    Process25Component = __decorate([
        core_1.Component({
            selector: 'process25',
            templateUrl: 'process25.component.html',
            styles: ['.details .fa{padding: 4px 6px; border-radius: 50%;}' +
                    '.details .fa-map-marker{color: white; background: #e47700; padding: 4px 7px;}' +
                    '.col-lg-2.control-label {padding-left: 0; padding-right: 0}'],
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService, helpers_service_1.HelpersService]
        })
    ], Process25Component);
    return Process25Component;
}());
exports.Process25Component = Process25Component;
// Replacement Reason Codes
var RRcode = [
    {
        "code": "A1",
        "text": "Faulty"
    },
    {
        "code": "A2",
        "text": "Faulty on Arrival"
    },
    {
        "code": "A5",
        "text": "Press Select - Unable to Clear"
    },
    {
        "code": "A6",
        "text": "Damaged on Receipt"
    },
    {
        "code": "B1",
        "text": "Damaged through Misuse"
    },
    {
        "code": "B2",
        "text": "No Current Period Card"
    },
    {
        "code": "B4",
        "text": "Lost Card"
    },
    {
        "code": "B5",
        "text": "Stolen Card"
    },
    {
        "code": "C1",
        "text": "Card Not Received"
    },
    {
        "code": "CC",
        "text": "Changeover Replaced"
    },
    {
        "code": "D1",
        "text": "Staff/VIP Replacement"
    },
    {
        "code": "E1",
        "text": "Reissue Undelivered Card"
    },
    {
        "code": "R1",
        "text": "Lost Retailer Card"
    },
    {
        "code": "R2",
        "text": "Stolen Retailer Card"
    },
    {
        "code": "X1",
        "text": "WAP Re-instatement"
    },
    {
        "code": "X2",
        "text": "IVR Re-instatement"
    }
];
