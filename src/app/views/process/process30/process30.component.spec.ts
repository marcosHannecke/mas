import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Process30Component } from './process30.component';

describe('Process30Component', () => {
  let component: Process30Component;
  let fixture: ComponentFixture<Process30Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Process30Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Process30Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
