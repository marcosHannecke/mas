//
// PROCESS 30:
//
import { Component, OnInit } from '@angular/core';
import {HelpersService} from  '../../../services/helpers.service';
import {NavigationExtras, Router} from '@angular/router';
import {RestService} from '../../../services/rest.service';

@Component({
  selector: 'app-process30',
  templateUrl: './process30.component.html',
  styleUrls: ['./process30.component.css'],
  providers: [ HelpersService, RestService]
})
export class Process30Component implements OnInit {
  error : boolean;
  public success: boolean = false;
  subscriber : any;
  undeliveredFlag : boolean;
  title = 'Request replacement card';
  urgent = false;
  options = ['C1'];


  constructor(private helpers : HelpersService, private router : Router, private rest: RestService){};
  ngOnInit(){
    if(sessionStorage.getItem('subscriber')){
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    }

    this.rest.get('/viewing-cards/' + this.subscriber.viewingCardId)
      .subscribe(res =>{
        this.undeliveredFlag = res['undeliveredFlag'];
        // this.undeliveredFlagCheck();
      }, err =>{
        this.error = this.helpers.error(err.error.message);
      });
  }

  // undeliveredFlagCheck(){
  //   let navigationExtras: NavigationExtras = {
  //     queryParams: { 'process': 28 },
  //   };
  //   if(this.undeliveredFlag){
  //     this.router.navigate(['/process25/undelivered'], navigationExtras);
  //   }else if(!this.undeliveredFlag){
  //     this.router.navigate(['/process25/n'], navigationExtras);
  //   }
  // }
}


