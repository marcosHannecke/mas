import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {Process4Component, Process3Component} from "./process3_4/process3_4.component";
import {Process5Component} from './process5/process5.component';
import {Process8Component} from './process8/process8.component';
import {Process16Component} from './process16/process16.component';
import {Process17Component} from './process17/process17.component';
import {Process19Component} from './process19/process19.component';
import {Process20Component} from './process20/process20.component';
import {Process21Component} from './process21/process21.component';
import {Process23Component} from './process23/process23.component';
import {Process24Component} from './process24/process24.component';
import {Process25Component} from './process25_26/process25.component';
import {Process27Component} from './process27/process27.component';
import {Process28Component} from './process28/process28.component';
import {Process32Component} from './process32/process32.component';
import {Process33Component} from './process33/process33.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule} from '@angular/router';
import {YesNo} from '../process/process19/process19.component';
import { ReplacementProcessComponent } from './replacement-process/replacement-process.component';
import { Process29Component } from './process29/process29.component';
import { Process30Component } from './process30/process30.component';

@NgModule({
  declarations: [Process4Component, Process3Component, Process5Component, Process8Component, Process16Component, Process17Component,
    Process19Component, Process20Component,Process21Component, Process23Component, Process24Component, Process28Component,
    YesNo, Process25Component,Process27Component, Process32Component, Process33Component, ReplacementProcessComponent, Process29Component, Process30Component],
  imports: [BrowserModule, FormsModule, ReactiveFormsModule, RouterModule],
})

export class ProcessViewModule {}
