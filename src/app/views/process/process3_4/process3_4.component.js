"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
// PROCESS 3: Advise customer to insert card and follow onscreen instructions.
var Process3Component = /** @class */ (function () {
    function Process3Component() {
        this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    }
    ;
    Process3Component = __decorate([
        core_1.Component({
            selector: 'process3',
            templateUrl: 'process3.component.html',
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService]
        })
    ], Process3Component);
    return Process3Component;
}());
exports.Process3Component = Process3Component;
// PROCESS 4: Set CARD STB relationship
var Process4Component = /** @class */ (function () {
    function Process4Component(router, fb, val, subscribersService) {
        this.router = router;
        this.fb = fb;
        this.val = val;
        this.subscribersService = subscribersService;
        this.submitted = false;
        this.success = false;
        this.show = false;
        this.rePair = 0;
        this.rePairMessage = false;
    }
    ;
    Process4Component.prototype.ngOnInit = function () {
        var _this = this;
        this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
        this.router.queryParams
            .subscribe(function (params) {
            _this.rePair = params['rePair'];
            console.log(_this.rePair);
        });
        this.form = this.fb.group({
            // "make": ['', [Validators.required, Validators.maxLength(2), this.val.specialChar]],
            // "model": ['', [Validators.required, Validators.maxLength(2), this.val.specialChar]],
            "hardwareVersion": ['', [forms_1.Validators.required, forms_1.Validators.minLength(6), forms_1.Validators.maxLength(6), this.val.specialChar]],
            "serialNbr": ['', [forms_1.Validators.required, forms_1.Validators.maxLength(11), this.val.specialChar]],
        });
        this.formId = this.fb.group({
            "receiverIdBody": ['', [forms_1.Validators.required, forms_1.Validators.maxLength(16), this.val.specialChar]],
            "receiverIdCheckDigit": ['', [forms_1.Validators.required, forms_1.Validators.maxLength(1), this.val.specialChar]],
        });
    };
    ;
    Process4Component.prototype.proceed = function () {
        var _this = this;
        this.submitted = true;
        this.rePairMessage = false;
        if (this.rePair == 1) {
            this.rePair = 0;
            this.rePairMessage = true;
            var data = {
                cardSubscriberId: this.subscriber.cardSubscriberId
            };
            this.subscribersService.urlSubscribers('re-pair-card', data)
                .subscribe(function (res) { console.log(res); }, function (err) { _this.error = JSON.parse(err['_body']).fieldErrors[0]; });
        }
        if (this.form.valid) {
            var stbCheckDigit = '';
            var serialNbr = this.form.value.serialNbr;
            // SPLIT DATA
            var hV = this.form.value.hardwareVersion;
            var make = hV.slice(0, 2);
            var model = hV.slice(2, 4);
            hV = hV.slice(4, 6);
            this.message = {
                "make": make,
                "model": model,
                "hardwareVersion": hV
            };
            console.log(make, model, hV);
            if (serialNbr.length == 11) {
                stbCheckDigit = serialNbr[10];
                serialNbr = serialNbr.slice(0, 10);
            }
            // this.message = this.form.value;
            this.message.cardSubscriberId = this.subscriber.cardSubscriberId;
            this.message.stbCheckDigit = stbCheckDigit;
            this.message.serialNbr = serialNbr;
            console.log(this.message, 'messageeeeeeee');
            if (this.formId.valid) {
                this.message.receiverIdBody = this.formId.value.receiverIdBody;
                this.message.receiverIdCheckDigit = this.formId.value.receiverIdCheckDigit;
            }
            console.log('message', this.message);
            this.subscribersService.urlSubscribers('set-card-stb-relationship', this.message)
                .subscribe(function (res) {
                _this.success = res;
            }, function (err) {
                _this.cases(JSON.parse(err['_body']));
            });
        }
    };
    Process4Component.prototype.cases = function (c) {
        console.log(c.message);
        if (c.message !== undefined) {
            if (c.message.includes('CE1')) {
                this.caseBox = {
                    message: 'Invalid STB serial number',
                    submessage: 'Please check and try again.'
                };
                console.log(this.caseBox.message);
            }
            else if (c.message.includes('CE2')) {
                this.show = true;
            }
            else if (c.message.includes('CE3')) {
                this.caseBox = {
                    message: 'Receiver ID is not recognised! ',
                    submessage: 'Customer should contact Sky.'
                };
                this.show = true;
            }
            else if (c.message.includes('CE4')) {
                this.caseBox = {
                    message: 'Invalid Receiver ID.',
                    submessage: 'Please check and try again.'
                };
                this.show = true;
            }
            else if (c.message.includes('CE5')) {
                this.caseBox = {
                    message: 'Mismatch between the STB Serial Number and Receiver ID. ',
                    submessage: 'Customer should contact Sky.'
                };
                this.show = true;
            }
            else {
                console.log('here');
                this.error = c.message;
            }
        }
    };
    Process4Component = __decorate([
        core_1.Component({
            selector: 'process4',
            templateUrl: 'process4.component.html',
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService]
        })
    ], Process4Component);
    return Process4Component;
}());
exports.Process4Component = Process4Component;
