import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidatorsService } from '../../../services/validators.service';
import { HelpersService } from '../../../services/helpers.service';
import { HttpClient } from '@angular/common/http';
import {Router , ActivatedRoute, Params, NavigationExtras} from '@angular/router';
import {Globals} from '../../../app.global';

// PROCESS 3: Advise customer to insert card and follow onscreen instructions.
@Component({
  selector: 'process3',
  templateUrl: 'process3.component.html',
  providers: [ValidatorsService]
})
export class Process3Component {
  subscriber: any;
  constructor() {
    this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
  };
}

// PROCESS 4: Set CARD STB relationship
@Component({
  selector: 'process4',
  templateUrl: 'process4.component.html',
  providers: [ValidatorsService, HelpersService, HttpClient]
})
export class Process4Component implements OnInit {
  form: FormGroup;
  formId: FormGroup;
  submitted = false;
  success: any = false;
  error: any;
  message: any;
  subscriber: any;
  caseBox: any;
  show = false;
  rePair: any = 0;
  rePairMessage = false;

  constructor(private router: ActivatedRoute, private fb: FormBuilder, private  val: ValidatorsService,
              private helpers: HelpersService, private http: HttpClient, private globals: Globals) {};

  ngOnInit()  {
    this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));

    this.router.queryParams
      .subscribe(params => {
        this.rePair = params['rePair'];
        console.log(this.rePair);
      });

    this.form = this.fb.group({
      // "make": ['', [Validators.required, Validators.maxLength(2), this.val.specialChar]],
      // "model": ['', [Validators.required, Validators.maxLength(2), this.val.specialChar]],
      'hardwareVersion': ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6), this.val.specialChar]],
      'serialNbr': ['', [Validators.required, Validators.maxLength(11), this.val.specialChar]],
      // "stbCheckDigit": ['', [Validators.maxLength(1), this.val.specialChar]],
    });

    this.formId = this.fb.group({
      'receiverIdBody': ['', [Validators.required, Validators.maxLength(16), this.val.specialChar]],
      'receiverIdCheckDigit': ['', [Validators.required, Validators.maxLength(1), this.val.specialChar]],
    });
  };

  proceed() {
    this.submitted = true;
    this.rePairMessage = false;

    if (this.rePair === 1) {
      this.rePair = 0;
      this.rePairMessage = true;
      const data = {
        cardSubscriberId : this.subscriber.cardSubscriberId
      };

      this.http.put(this.globals.API_ENDPOINT + '/subscribers/re-pair-card', data, {responseType: 'text'})
      // this.rest.put('/subscribers/re-pair-card', data)
        // this.subscribersService.urlSubscribers('re-pair-card', data)
        .subscribe(
          res => {console.log(res); },
          err => {
            err = JSON.parse(err.error);
            console.log(err);
            if (err.fieldErrors) {
              this.error = err.fieldErrors[0];
            }else {
              this.error = this.helpers.error(err.message);
            }
            console.log('ERROR', this.error);
          }
        )
    }

    if (this.form.valid) {
      let stbCheckDigit = '';
      let serialNbr = this.form.value.serialNbr;

      // SPLIT DATA
      let hV = this.form.value.hardwareVersion;
      let make = hV.slice(0, 2);
      let model = hV.slice(2, 4);
      hV = hV.slice(4, 6);

      this.message = {
        "make": make,
        "model": model,
        "hardwareVersion": hV
      };

      console.log(make, model, hV);

      if (serialNbr.length == 11) {
        stbCheckDigit = serialNbr[10];
        serialNbr = serialNbr.slice(0, 10);
      }

      // this.message = this.form.value;
      this.message.cardSubscriberId = this.subscriber.cardSubscriberId;
      this.message.stbCheckDigit = stbCheckDigit;
      this.message.serialNbr = serialNbr;

      // console.log(this.message, 'messageeeeeeee');
      if (this.formId.valid) {
        this.message.receiverIdBody = this.formId.value.receiverIdBody;
        this.message.receiverIdCheckDigit = this.formId.value.receiverIdCheckDigit;
      }
      console.log('message', this.message);

      this.http.put(this.globals.API_ENDPOINT + '/subscribers/set-card-stb-relationship', this.message, {responseType: 'text'})
      // this.rest.put('/subscribers/set-card-stb-relationship', this.message)

      // this.subscribersService.urlSubscribers('set-card-stb-relationship', this.message)
        .subscribe(res => {
            this.success = true;
          }, err => {
            err = JSON.parse(err.error);
             console.log(err);
            if (err.fieldErrors) {
              this.error = err.fieldErrors[0];
            }else {
              this.error = this.helpers.error(err.message);
            }
            console.log('ERROR', this.error);

            // this.cases(err.error);
          }
        )
    }
  }


  cases(c) {
    console.log(c.message);
    if (c.message !== undefined) {
      if (c.message.includes('CE1')) {
        this.caseBox = {
          message : 'Invalid STB serial number',
          submessage: 'Please check and try again.'
        };
        console.log(this.caseBox.message);
      }else if (c.message.includes('CE2')) {
        this.show = true;
      }else if (c.message.includes('CE3')) {
        this.caseBox = {
          message : 'Receiver ID is not recognised! ',
          submessage: 'Customer should contact Sky.'
        };
        this.show = true;
      }else if (c.message.includes('CE4')) {
        this.caseBox = {
          message : 'Invalid Receiver ID.',
          submessage: 'Please check and try again.'
        };
        this.show = true;
      }else if (c.message.includes('CE5')) {
        this.caseBox = {
          message : 'Mismatch between the STB Serial Number and Receiver ID. ',
          submessage: 'Customer should contact Sky.'
        };
        this.show = true;
      }else {
        console.log('here');
        this.error = c.message;
      }
    }
  }
}

interface message {
  "cardSubscriberId": string,
  "make": string,
  "model": string,
  "hardwareVersion": string,
  "serialNbr": string,
  "stbCheckDigit": string,
  "receiverIdBody": string,
  "receiverIdCheckDigit": string
}
