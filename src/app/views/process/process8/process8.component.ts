import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ValidatorsService } from '../../../services/validators.service';
import { HelpersService } from '../../../services/helpers.service';
import {Router} from '@angular/router';
import {RestService} from '../../../services/rest.service';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../../app.global';

// PROCESS 8: Edit Address
@Component({
  selector: 'process8',
  templateUrl: 'process8.component.html',
  styles: ['.col-lg-2.control-label {padding-left: 0; padding-right: 0}'],
  providers: [ValidatorsService, HelpersService, RestService]
})
export class Process8Component implements OnInit {
  addressForm: FormGroup;
  submitted = false;
  subscriber: any;
  customer: any;
  error: any;
  success = false;
  message: string;
  spreadChanges = false;
  addressList: any;
  role: any;

  constructor(private helpers: HelpersService, private  val: ValidatorsService, private router: Router,
              private fb: FormBuilder, private rest: RestService, private http: HttpClient, private globals: Globals) {};

  ngOnInit() {
    if (sessionStorage.getItem('role') != null) {
      this.role = sessionStorage.getItem('role');
    }

    if (sessionStorage.getItem('subscriber') != null) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
      // replace null values in customer object to avoid form errors
      for (const key of Object.keys(this.subscriber)) {
        if (this.subscriber[key] == null) {
          this.subscriber[key] = '';
        }
      }
      this.addressForm = this.fb.group({
        // "cardSubscriberId" : [this.subscriber.cardSubscriberId],
        'title': [this.subscriber.title, [Validators.required, this.val.specialChar]],
        'initials': [this.subscriber.initials.toUpperCase(), [Validators.required, Validators.maxLength(3), this.val.letterSpaces]],
        'surname' : [this.subscriber.surname, [Validators.required,  Validators.maxLength(35), this.val.letterSpaces]],
        'addressLine1': [this.subscriber.addressLine1, [Validators.required, Validators.maxLength(35), this.val.specialChar]],
        'addressLine2' : [this.subscriber.addressLine2, [Validators.required, Validators.maxLength(35), this.val.specialChar]],
        'addressLine3' : [this.subscriber.addressLine3 ],
        'addressLine4' : [this.subscriber.addressLine4],
        'addressLine5' : [this.subscriber.addressLine5],
        'postcode' : [this.subscriber.postcode, [Validators.required, Validators.maxLength(9), this.val.specialChar]],
        'countryCode' : [this.subscriber.countryCode],
        'phones': this.fb.group({
          'homeTelNr': [this.subscriber.homeTelNo, [Validators.maxLength(15), this.val.onlyNumbers]],
          'workTelNr': [this.subscriber.workTelNo, [Validators.maxLength(15), this.val.onlyNumbers]],
          'mobTelNr': [this.subscriber.mobTelNo, [Validators.maxLength(15), this.val.onlyNumbers]],
        }, {validator: this.val.phones}),
        'cliTelNr': [this.subscriber.cliTelNo],
      });
    }

    // GET CUSTOMER DETAILS TO SPREAD
    this.getCustomer();
  }

  /**
   * Get customer
   */
  private getCustomer() {
    this.rest.get('/customers?q=subscriberId=' + this.subscriber.id)
      .subscribe(res => {
        this.customer = res[0];
        if (this.customer.email === 'unknown@mediaaccessservices.net') {
          this.customer.email = '';
        }
        console.log(this.customer);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })

    // this.subscriberService.getCustomer(this.subscriber.id)
    //   .subscribe(res => {
    //     console.log('getcustomer',res[0])
    //     this.customer = res[0];
    //     if (this.customer.email === 'unknown@mediaaccessservices.net') {
    //       this.customer.email = '';
    //     }
    //     console.log(this.customer);
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   })
  }

  spread() {
    // Close dialog
    this.spreadChanges = false;
    this.submitted = true;

    if (this.addressForm.valid) {

      // Update customer details
      this.customer.title = this.addressForm.value.title;
      this.customer.initials = this.addressForm.value.initials.toUpperCase();
      this.customer.surname = this.helpers.toTitleCase(this.addressForm.value.surname);
      this.customer.billingAddressLine1 = this.addressForm.value.addressLine1;
      this.customer.billingAddressLine2 = this.addressForm.value.addressLine2;
      this.customer.billingAddressLine3 = this.addressForm.value.addressLine3;
      this.customer.billingAddressLine4 = this.addressForm.value.addressLine4;
      this.customer.billingAddressLine5 = this.addressForm.value.addressLine5;
      this.customer.billingPostcode = this.addressForm.value.postcode;
      this.customer.billingCountryCode = this.addressForm.value.countryCode;
      this.customer.mobTelNo = this.addressForm.value.phones.mobTelNr;
      this.customer.homeTelNo = this.addressForm.value.phones.homeTelNr;
      this.customer.workTelNo = this.addressForm.value.phones.workTelNr;
      this.customer.customerId = this.customer.id;
      if (this.customer.email.length === 0 || this.customer.email === undefined) {
        this.customer.email = 'unknown@mediaaccessservices.net';
      }

      delete this.customer.createdAt;
      delete this.customer.updatedAt;
      delete this.customer.id;

      this.http.put(this.globals.API_ENDPOINT + '/customers', this.customer, {responseType: 'text'})
        .subscribe(() => {
          this.updateAddress();
        }, err => {
          err = JSON.parse(err.error);
          console.log(err);
          if (err.fieldErrors) {
            this.error = err.fieldErrors[0];
          }else {
            this.error = this.helpers.error(err.message);
          }
        })
    }
  }

  updateAddress() {
    this.spreadChanges = false;
    this.submitted = true;
    if (this.addressForm.valid) {
      const json = this.addressForm.value;
      json.homeTelNr = this.addressForm.value.phones.homeTelNr;
      json.workTelNr = this.addressForm.value.phones.workTelNr;
      json.mobTelNr = this.addressForm.value.phones.mobTelNr;

      if (this.subscriber.id) {
        json.subscriberId = this.subscriber.id;
      }else if (this.subscriber.cardSubscriberId != null) {
        json.cardSubscriberId = this.subscriber.cardSubscriberId;
      }
      delete json.phones;

      // Initials to uppercase
      json.initials = json.initials.toUpperCase();
      // Capitalize surname
      json.surname = this.helpers.toTitleCase(json.surname);


      this.http.put(this.globals.API_ENDPOINT + '/subscribers/', json, {responseType: 'text'})
        .subscribe(() => {
          this.success = true;
        }, err => {
            err = JSON.parse(err.error);
            console.log(err);
            if (err.fieldErrors) {
              this.error = err.fieldErrors[0];
            }else {
              this.error = this.helpers.error(err.message);
            }
        })
    }
  }

  /**
   * Find address with postcode
   */
  findAddress() {
    const postcode = this.addressForm.value.postcode.replace(' ', '');

    this.rest.get('/postcode?postcode=' + postcode)
      .subscribe(res => {
        this.addressList = res;
        if (this.addressList.length === 0) {
          this.addressList = [{
            notFound : 'NO ADDRESS FOUND'
          }]
        }
      }, err => {
        if (this.addressForm.value.postcode.length === 0) {
          this.error = {
            description: 'Please Enter a Postcode.'
          }
        }else {
          this.error = this.helpers.error(err.error.message);
        }
      })
  }

  // SET ADDRESS
  setAddress(address) {
    if (address.notFound) {
      this.addressList = null;
      // this.disableInput = false;
    }else {
      this.addressList = null;
      // this.disableInput = true;
      // this.addressManually = true;
      const add = this.helpers.nullCheck(address);
      this.addressForm.controls['addressLine1'].setValue(add.addressLine1.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.addressForm.controls['addressLine2'].setValue(add.addressLine2.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.addressForm.controls['addressLine3'].setValue(add.addressLine3.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.addressForm.controls['addressLine4'].setValue(add.addressLine4.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.addressForm.controls['addressLine5'].setValue(add.addressLine5.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
    }

  }

  backTo() {
    this.success = false;
    sessionStorage.setItem('addressIncorrect', 'false');
    this.router.navigate(['/process25']);
  }
}
