"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var helpers_service_1 = require("../../../services/helpers.service");
var postcode_service_1 = require("../../../services/postcode.service");
// PROCESS 8: Edit Address
var Process8Component = /** @class */ (function () {
    function Process8Component(helpers, subscriberService, router, fb, val, postcodeService) {
        this.helpers = helpers;
        this.subscriberService = subscriberService;
        this.router = router;
        this.fb = fb;
        this.val = val;
        this.postcodeService = postcodeService;
        this.submitted = false;
        this.success = false;
        this.spreadChanges = false;
    }
    ;
    Process8Component.prototype.ngOnInit = function () {
        if (sessionStorage.getItem('role') != null) {
            this.role = sessionStorage.getItem('role');
        }
        if (sessionStorage.getItem('subscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
            // replace null values in customer object to avoid form errors
            for (var _i = 0, _a = Object.keys(this.subscriber); _i < _a.length; _i++) {
                var key = _a[_i];
                if (this.subscriber[key] == null) {
                    this.subscriber[key] = '';
                }
            }
            this.addressForm = this.fb.group({
                // "cardSubscriberId" : [this.subscriber.cardSubscriberId],
                "title": [this.subscriber.title, [forms_1.Validators.required, this.val.specialChar]],
                "initials": [this.subscriber.initials, [forms_1.Validators.required, forms_1.Validators.maxLength(3), this.val.letterSpaces]],
                "surname": [this.subscriber.surname, [forms_1.Validators.required, forms_1.Validators.maxLength(35), this.val.letterSpaces]],
                "addressLine1": [this.subscriber.addressLine1, [forms_1.Validators.required, forms_1.Validators.maxLength(35), this.val.specialChar]],
                "addressLine2": [this.subscriber.addressLine2, [forms_1.Validators.required, forms_1.Validators.maxLength(35), this.val.specialChar]],
                "addressLine3": [this.subscriber.addressLine3],
                "addressLine4": [this.subscriber.addressLine4],
                "addressLine5": [this.subscriber.addressLine5],
                "postcode": [this.subscriber.postcode, [forms_1.Validators.required, forms_1.Validators.maxLength(9), this.val.specialChar]],
                "countryCode": [this.subscriber.countryCode],
                "phones": this.fb.group({
                    "homeTelNr": [this.subscriber.homeTelNo, [forms_1.Validators.maxLength(15), this.val.onlyNumbers]],
                    "workTelNr": [this.subscriber.workTelNo, [forms_1.Validators.maxLength(15), this.val.onlyNumbers]],
                    "mobTelNr": [this.subscriber.mobTelNo, [forms_1.Validators.maxLength(15), this.val.onlyNumbers]],
                }, { validator: this.val.phones }),
                "cliTelNr": [this.subscriber.cliTelNo],
            });
        }
        // GET CUSTOMER DETAILS TO SPREAD
        this.getCustomer();
    };
    // GET CUSTOMER
    Process8Component.prototype.getCustomer = function () {
        var _this = this;
        this.subscriberService.getCustomer(this.subscriber.id)
            .subscribe(function (res) {
            _this.customer = res[0];
            if (_this.customer.email == 'unknown@mediaaccessservices.net') {
                _this.customer.email = '';
            }
            console.log(_this.customer);
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    Process8Component.prototype.spread = function () {
        var _this = this;
        // Close dialog
        this.spreadChanges = false;
        this.submitted = true;
        if (this.addressForm.valid) {
            // Update customer details
            this.customer.title = this.addressForm.value.title;
            this.customer.initials = this.addressForm.value.initials;
            this.customer.surname = this.addressForm.value.surname;
            this.customer.billingAddressLine1 = this.addressForm.value.addressLine1;
            this.customer.billingAddressLine2 = this.addressForm.value.addressLine2;
            this.customer.billingAddressLine3 = this.addressForm.value.addressLine3;
            this.customer.billingAddressLine4 = this.addressForm.value.addressLine4;
            this.customer.billingAddressLine5 = this.addressForm.value.addressLine5;
            this.customer.billingPostcode = this.addressForm.value.postcode;
            this.customer.billingCountryCode = this.addressForm.value.countryCode;
            this.customer.mobTelNo = this.addressForm.value.phones.mobTelNr;
            this.customer.homeTelNo = this.addressForm.value.phones.homeTelNr;
            this.customer.workTelNo = this.addressForm.value.phones.workTelNr;
            this.customer.customerId = this.customer.id;
            if (this.customer.email.length == 0 || this.customer.email == undefined) {
                this.customer.email = 'unknown@mediaaccessservices.net';
            }
            delete this.customer.createdAt;
            delete this.customer.updatedAt;
            delete this.customer.id;
            this.subscriberService.urlPut('customers', this.customer)
                .subscribe(function (res) {
                _this.updateAddress();
            }, function (err) {
                _this.error = JSON.parse(err['_body']);
            });
            // console.log('FORM', this.addressForm.value,);
            // console.log('CUSTOMER', this.customer);
        }
    };
    Process8Component.prototype.updateAddress = function () {
        var _this = this;
        this.spreadChanges = false;
        this.submitted = true;
        if (this.addressForm.valid) {
            var json = this.addressForm.value;
            json.homeTelNr = this.addressForm.value.phones.homeTelNr;
            json.workTelNr = this.addressForm.value.phones.workTelNr;
            json.mobTelNr = this.addressForm.value.phones.mobTelNr;
            if (this.subscriber.id) {
                json.subscriberId = this.subscriber.id;
            }
            else if (this.subscriber.cardSubscriberId != null) {
                json.cardSubscriberId = this.subscriber.cardSubscriberId;
            }
            delete json.phones;
            this.subscriberService.urlSubscribers('', json)
                .subscribe(function (res) {
                _this.success = true;
            }, function (err) {
                // this.error = JSON.parse(err["_body"]);
                _this.error = _this.helpers.getError(err);
            });
        }
    };
    // FIND ADDRESS
    Process8Component.prototype.findAddress = function () {
        var _this = this;
        var postcode = this.addressForm.value.postcode.replace(' ', '');
        this.postcodeService.postcode(postcode)
            .subscribe(function (res) {
            _this.addressList = JSON.parse(res['_body']);
            if (_this.addressList.length === 0) {
                _this.addressList = [{
                        notFound: 'NO ADDRESS FOUND'
                    }];
            }
        }, function (err) {
            if (_this.addressForm.value.postcode.length === 0) {
                _this.error = {
                    description: 'Please Enter a Postcode.'
                };
            }
            else {
                _this.error = _this.helpers.getError(err);
            }
        });
    };
    // SET ADDRESS
    Process8Component.prototype.setAddress = function (address) {
        if (address.notFound) {
            this.addressList = null;
            // this.disableInput = false;
        }
        else {
            this.addressList = null;
            // this.disableInput = true;
            // this.addressManually = true;
            var add = this.helpers.nullCheck(address);
            this.addressForm.controls['addressLine1'].setValue(add.addressLine1.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.addressForm.controls['addressLine2'].setValue(add.addressLine2.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.addressForm.controls['addressLine3'].setValue(add.addressLine3.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.addressForm.controls['addressLine4'].setValue(add.addressLine4.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.addressForm.controls['addressLine5'].setValue(add.addressLine5.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
        }
    };
    Process8Component.prototype.backTo = function () {
        this.success = false;
        sessionStorage.setItem('addressIncorrect', 'false');
        this.router.navigate(['/process25']);
    };
    Process8Component = __decorate([
        core_1.Component({
            selector: 'process8',
            templateUrl: 'process8.component.html',
            styles: ['.col-lg-2.control-label {padding-left: 0; padding-right: 0}'],
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService, helpers_service_1.HelpersService, postcode_service_1.PostcodeService]
        })
    ], Process8Component);
    return Process8Component;
}());
exports.Process8Component = Process8Component;
