"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
//
// PROCESS 27: Cancel Request replacement card
//
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var Process27Component = /** @class */ (function () {
    function Process27Component(subscriberService, router, fb, val) {
        this.subscriberService = subscriberService;
        this.router = router;
        this.fb = fb;
        this.val = val;
        this.submitted = false;
        this.success = false;
        this.replacementReasonCode = RRcode;
        this.activated = false;
        this.address = true;
    }
    ;
    Process27Component.prototype.ngOnInit = function () {
        var _this = this;
        this.requestForm = this.fb.group({
            "replacementReasonCode": ["", forms_1.Validators.required],
            "urgent": [false]
        });
        this.router.params
            .subscribe(function (res) { return _this.urgent = res; });
        if (this.urgent.u == "y") {
            this.requestForm.controls['urgent'].setValue(true);
        }
        if (sessionStorage.getItem('subscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
            this.addressForm = this.fb.group({
                "cardSubscriberId": [this.subscriber.cardSubscriberId],
                "title": [this.subscriber.title, [forms_1.Validators.required, this.val.specialChar]],
                "initials": [this.subscriber.initials, [forms_1.Validators.required, forms_1.Validators.maxLength(3), this.val.letterSpaces]],
                "surname": [this.subscriber.surname, [forms_1.Validators.required, forms_1.Validators.maxLength(35), this.val.letterSpaces]],
                "addressLine1": [this.subscriber.addressLine1, [forms_1.Validators.required, forms_1.Validators.maxLength(35), this.val.specialChar]],
                "addressLine2": [this.subscriber.addressLine2, [forms_1.Validators.required, forms_1.Validators.maxLength(35), this.val.specialChar]],
                "addressLine3": [this.subscriber.addressLine3],
                "addressLine4": [this.subscriber.addressLine4],
                "addressLine5": [this.subscriber.addressLine5],
                "postcode": [this.subscriber.postcode, [forms_1.Validators.required, forms_1.Validators.maxLength(9), this.val.specialChar]],
                "countryCode": [this.subscriber.countryCode],
            });
        }
    };
    Process27Component.prototype.requestCard = function () {
        var _this = this;
        this.submitted = true;
        if (this.requestForm.valid) {
            var data = {
                "cardSubscriberId": this.subscriber.cardSubscriberId,
                "replacementReasonCode": this.requestForm.value.replacementReasonCode,
                "urgent": false
            };
            this.subscriberService.urlSubscribers('request-replacement-card', data)
                .subscribe(function (res) {
                console.log(res);
                _this.success = true;
            }, function (err) {
                _this.error = JSON.parse(err["_body"]);
            });
        }
    };
    Process27Component = __decorate([
        core_1.Component({
            selector: 'process27',
            templateUrl: 'process27.component.html',
            styles: ['.details .fa{padding: 4px 6px; border-radius: 50%;}' +
                    '.details .fa-map-marker{color: white; background: #e47700; padding: 4px 7px;}' +
                    '.col-lg-2.control-label {padding-left: 0; padding-right: 0}'],
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService]
        })
    ], Process27Component);
    return Process27Component;
}());
exports.Process27Component = Process27Component;
// Replacement Reason Codes
var RRcode = [
    {
        "code": "O1",
        "text": "Found missing card"
    },
    {
        "code": "O2",
        "text": "Card now working"
    },
];
