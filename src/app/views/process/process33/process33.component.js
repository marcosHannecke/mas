"use strict";
//
// PROCESS 33: Resend Automatically Entitled services
//
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var products_service_1 = require("../../../services/products.service");
var Process33Component = /** @class */ (function () {
    function Process33Component(productsService, router) {
        this.productsService = productsService;
        this.router = router;
        this.rae = true;
        this.success = false;
    }
    ;
    Process33Component.prototype.ngOnInit = function () {
        if (sessionStorage.getItem('subscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
        }
    };
    Process33Component.prototype.send = function () {
        var _this = this;
        var data = {
            cardSubscriberId: this.subscriber.cardSubscriberId
        };
        this.productsService.urlProducts('E', 'resend-auto-entitled-services', data)
            .subscribe(function (res) {
            console.log(res);
            _this.success = true;
        }, function (err) {
            _this.error = JSON.parse(err["_body"]);
        });
    };
    Process33Component = __decorate([
        core_1.Component({
            selector: 'process33',
            templateUrl: 'process33.component.html',
            providers: [products_service_1.ProductsService]
        })
    ], Process33Component);
    return Process33Component;
}());
exports.Process33Component = Process33Component;
