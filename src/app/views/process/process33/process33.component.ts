//
// PROCESS 33: Resend Automatically Entitled services
//

import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {HelpersService} from '../../../services/helpers.service';
import {Globals} from '../../../app.global';

@Component({
  selector: 'process33',
  templateUrl: 'process33.component.html',
  providers: [HelpersService]
})
export class Process33Component implements OnInit {
  rae : boolean = true;
  subscriber : any;
  error : any;
  success : boolean = false;

  constructor(private router : Router, private http: HttpClient, private helpers: HelpersService, private globals: Globals) {};
  ngOnInit(){
    if(sessionStorage.getItem('subscriber') != null) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    }
  }

  send(){
    const data = {
      cardSubscriberId : this.subscriber.cardSubscriberId
    };

    this.http.put(this.globals.API_ENDPOINT + '/entitlements/resend-auto-entitled-services', data, {responseType: 'text'})
      .subscribe(res => {
        console.log(res);
        this.success = true;
      }, err => {
        err = JSON.parse(err.error);
        console.log(err);
        if (err.fieldErrors) {
          this.error = err.fieldErrors[0];
        }else {
          this.error = this.helpers.error(err.message);
        }
      })
  }
}

