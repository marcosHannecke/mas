//
// PROCESS 28:
//
import { Component, OnInit } from '@angular/core';
import {HelpersService} from  '../../../services/helpers.service';
import {NavigationExtras, Router} from '@angular/router';
import {RestService} from '../../../services/rest.service';

@Component({
  selector: 'process28',
  templateUrl: 'process28.component.html',
  styles : [''],
  providers: [ HelpersService, RestService]
})
export class Process28Component implements OnInit {
  error : boolean;
  public success: boolean = false;
  subscriber : any;
  undeliveredFlag : boolean;
  title = 'Request replacement card';
  urgent:any = false;
  options = ['E1'];


  constructor(private helpers : HelpersService, private router : Router, private rest: RestService){};
  ngOnInit(){
    if(sessionStorage.getItem('subscriber')){
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    }

    this.rest.get('/viewing-cards/' + this.subscriber.viewingCardId)
      .subscribe(res =>{
        this.undeliveredFlag = res['undeliveredFlag'];
        if(this.undeliveredFlag){
          this.urgent = 'undelivered';
        }else if(!this.undeliveredFlag){
          this.urgent = 'n';
        }
      }, err =>{
        this.error = this.helpers.error(err.error.message);
      });
  }

}


