"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
//
// PROCESS 28:
//
var core_1 = require("@angular/core");
var subscribers_service_1 = require("../../../services/subscribers.service");
var helpers_service_1 = require("../../../services/helpers.service");
var Process28Component = /** @class */ (function () {
    function Process28Component(subscriberService, helpers, router) {
        this.subscriberService = subscriberService;
        this.helpers = helpers;
        this.router = router;
        this.success = false;
    }
    ;
    Process28Component.prototype.ngOnInit = function () {
        var _this = this;
        if (sessionStorage.getItem('subscriber')) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
        }
        this.subscriberService.viewingCardDetails(this.subscriber.viewingCardId)
            .subscribe(function (res) {
            _this.undeliveredFlag = res.undeliveredFlag;
            _this.undeliveredFlagCheck();
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    Process28Component.prototype.undeliveredFlagCheck = function () {
        if (this.undeliveredFlag) {
            this.router.navigate(['/process25/undelivered']);
        }
        else if (!this.undeliveredFlag) {
            this.router.navigate(['/process25/n']);
        }
    };
    Process28Component = __decorate([
        core_1.Component({
            selector: 'process28',
            templateUrl: 'process28.component.html',
            styles: [''],
            providers: [subscribers_service_1.SubscribersService, helpers_service_1.HelpersService]
        })
    ], Process28Component);
    return Process28Component;
}());
exports.Process28Component = Process28Component;
