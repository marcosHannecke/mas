"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var process3_4_component_1 = require("./process3_4/process3_4.component");
var process5_component_1 = require("./process5/process5.component");
var process8_component_1 = require("./process8/process8.component");
var process16_component_1 = require("./process16/process16.component");
var process17_component_1 = require("./process17/process17.component");
var process19_component_1 = require("./process19/process19.component");
var process20_component_1 = require("./process20/process20.component");
var process21_component_1 = require("./process21/process21.component");
var process23_component_1 = require("./process23/process23.component");
var process24_component_1 = require("./process24/process24.component");
var process25_component_1 = require("./process25_26/process25.component");
var process27_component_1 = require("./process27/process27.component");
var process28_component_1 = require("./process28/process28.component");
var process32_component_1 = require("./process32/process32.component");
var process33_component_1 = require("./process33/process33.component");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var process19_component_2 = require("../process/process19/process19.component");
var ProcessViewModule = /** @class */ (function () {
    function ProcessViewModule() {
    }
    ProcessViewModule = __decorate([
        core_1.NgModule({
            declarations: [process3_4_component_1.Process4Component, process3_4_component_1.Process3Component, process5_component_1.Process5Component, process8_component_1.Process8Component, process16_component_1.Process16Component, process17_component_1.Process17Component,
                process19_component_1.Process19Component, process20_component_1.Process20Component, process21_component_1.Process21Component, process23_component_1.Process23Component, process24_component_1.Process24Component, process28_component_1.Process28Component,
                process19_component_2.YesNo, process25_component_1.Process25Component, process27_component_1.Process27Component, process32_component_1.Process32Component, process33_component_1.Process33Component],
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, forms_1.ReactiveFormsModule, router_1.RouterModule],
        })
    ], ProcessViewModule);
    return ProcessViewModule;
}());
exports.ProcessViewModule = ProcessViewModule;
