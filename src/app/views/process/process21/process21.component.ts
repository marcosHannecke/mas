//
// PROCESS 21: I am paying for my subscription but I cannot see the channels.
//

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from '../../../services/helpers.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../../app.global';

@Component({
  selector: 'process21',
  templateUrl: 'process21.component.html',
  styles: ['.steps ul{padding: 0;} .steps ul li{margin: 10px 0;} .steps ul li strong{line-height: 26px;} .steps .btn:hover{background: #0f3b68; border-color: #0f3b68; cursor: default;}'],
  providers: [ValidatorsService, HelpersService]
})
export class Process21Component implements OnInit {
  billingInfo : any;
  error : any;
  success : boolean = false;
  subscriber : any;
  VCNCheck : boolean = false;

  process6 : boolean = false;

  constructor(private helpers: HelpersService, private router: Router, private fb: FormBuilder,
              private  val: ValidatorsService, private http: HttpClient, private globals: Globals) {};

  ngOnInit(){
    if(sessionStorage.getItem('billingInfo')){
      this.billingInfo = JSON.parse(sessionStorage.getItem('billingInfo'));
    }
    if(sessionStorage.getItem('subscriber')){
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    }
    console.log(this.subscriber.id);
    console.log(this.billingInfo);

    if(this.billingInfo.owed  == null){
      this.error = {
        message1: "This action cannot be processed because not billing info found.",
      };
    } else if(this.billingInfo.owed > 0 ){
      this.error = {
        message1: "This action cannot be processed because of balance outstanding.",
        message2: "Please settle outstanding balance first."
      };
    }else if(this.billingInfo.owed == 0 ){
      this.VCNCheck = true;
    }
  }

  /**
   * RESEND ALL PERSONAL BITS
   */
  resendAllPersonalBits(){
    this.VCNCheck = false;
    const data = {
      cardSubscriberId : this.subscriber.cardSubscriberId
    };

    this.http.put(this.globals.API_ENDPOINT + '/entitlements/resend-all-personal-bits', data, {responseType: 'text'})
      .subscribe(() => {
        this.success= true;
      }, err => {
        err = JSON.parse(err.error);
        console.log(err);
        if (err.fieldErrors) {
          this.error = err.fieldErrors[0];
        }else {
          this.error = this.helpers.error(err.message);
        }
      });

  }
}

