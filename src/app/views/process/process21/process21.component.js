"use strict";
//
// PROCESS 21: I am paying for my subscription but I cannot see the channels.
//
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var products_service_1 = require("../../../services/products.service");
var helpers_service_1 = require("../../../services/helpers.service");
var Process21Component = /** @class */ (function () {
    function Process21Component(helpers, subscriberService, productsService, router, fb, val) {
        this.helpers = helpers;
        this.subscriberService = subscriberService;
        this.productsService = productsService;
        this.router = router;
        this.fb = fb;
        this.val = val;
        this.success = false;
        this.VCNCheck = false;
        this.process6 = false;
    }
    ;
    Process21Component.prototype.ngOnInit = function () {
        if (sessionStorage.getItem('billingInfo')) {
            this.billingInfo = JSON.parse(sessionStorage.getItem('billingInfo'));
        }
        if (sessionStorage.getItem('subscriber')) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
        }
        console.log(this.subscriber.id);
        console.log(this.billingInfo);
        if (this.billingInfo.owed == null) {
            this.error = {
                message1: "This action cannot be processed because not billing info found.",
            };
        }
        else if (this.billingInfo.owed > 0) {
            this.error = {
                message1: "This action cannot be processed because of balance outstanding.",
                message2: "Please settle outstanding balance first."
            };
        }
        else if (this.billingInfo.owed == 0) {
            this.VCNCheck = true;
        }
    };
    //RESEND ALL PERSONAL BITS
    Process21Component.prototype.resendAllPersonalBits = function () {
        var _this = this;
        this.VCNCheck = false;
        var data = {
            cardSubscriberId: this.subscriber.cardSubscriberId
        };
        this.productsService.urlProducts('E', 'resend-all-personal-bits', data)
            .subscribe(function (res) {
            console.log(res);
            _this.success = true;
        }, function (err) {
            // this.error = JSON.parse(err["_body"]);
            _this.error = _this.helpers.getError(err);
        });
    };
    Process21Component = __decorate([
        core_1.Component({
            selector: 'process21',
            templateUrl: 'process21.component.html',
            styles: ['.steps ul{padding: 0;} .steps ul li{margin: 10px 0;} .steps ul li strong{line-height: 26px;} .steps .btn:hover{background: #0f3b68; border-color: #0f3b68; cursor: default;}'],
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService, products_service_1.ProductsService, helpers_service_1.HelpersService]
        })
    ], Process21Component);
    return Process21Component;
}());
exports.Process21Component = Process21Component;
