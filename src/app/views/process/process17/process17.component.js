"use strict";
//
// PROCESS 17: View my subscription channels on my Ipad and other devices
//
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var Process17Component = /** @class */ (function () {
    function Process17Component(router) {
        this.router = router;
        this.success = false;
        this.advise = true;
    }
    ;
    Process17Component.prototype.ngOnInit = function () {
        if (sessionStorage.getItem('subscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
        }
    };
    Process17Component.prototype.proceed = function () {
        this.router.navigate(['/process19'], { queryParams: { rapb: 1 } });
    };
    Process17Component = __decorate([
        core_1.Component({
            selector: 'process17',
            templateUrl: 'process17.component.html',
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService]
        })
    ], Process17Component);
    return Process17Component;
}());
exports.Process17Component = Process17Component;
