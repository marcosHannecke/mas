//
// PROCESS 17: View my subscription channels on my Ipad and other devices
//

import { Component, OnInit } from '@angular/core';
import {Router, Params} from '@angular/router';

@Component({
  selector: 'process17',
  templateUrl: 'process17.component.html',
})
export class Process17Component implements OnInit {
  subscriber : any;
  error : any;
  success : boolean = false;
  advise : boolean = true;

  constructor( private router : Router){};
  ngOnInit(){

    if(sessionStorage.getItem('subscriber') != null) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    }
  }

  proceed(){
    this.router.navigate(['/process19'], { queryParams: { rapb: 1 } })
  }

}
