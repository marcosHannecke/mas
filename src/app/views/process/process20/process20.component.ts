//
// PROCESS 20: Edit Pin and Ratings
//
import { Component, OnInit } from '@angular/core';
import{ FormGroup, FormBuilder, Validators } from '@angular/forms';
import {ValidatorsService} from '../../../services/validators.service';
import { Location } from '@angular/common';
import {Router} from '@angular/router';
import {HelpersService} from '../../../services/helpers.service';
import {HttpClient} from '@angular/common/http';
import {RestService} from '../../../services/rest.service';
import {Globals} from '../../../app.global';

@Component({
  selector: 'process20',
  templateUrl: 'process20.component.html',
  providers: [ValidatorsService, RestService, HelpersService]
})
export class Process20Component implements OnInit {
  pinRatingsForm : FormGroup;
  subscriber : any;
  error : any;
  success : boolean = false;
  submitted : boolean = false;
  pinRatings : any;
  rating : any[];
  initialVal : any;

  constructor(private location: Location, private router: Router, private fb: FormBuilder,
              private val: ValidatorsService, private rest: RestService, private http: HttpClient,
              private helpers: HelpersService, private globals: Globals) {};

  ngOnInit() {
    if (sessionStorage.getItem('subscriber') != null) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));

      this.rest.get('/subscribers/get-pin-and-ratings?cardSubscriberId='  + this.subscriber.cardSubscriberId)
        .subscribe(res => {
          this.pinRatings = res;
          // FORM
          this.pinRatingsForm = this.fb.group({
            "pin": [this.pinRatings.PIN, [Validators.required, Validators.maxLength(4), this.val.onlyNumbers]],
            "rating": ""
          });
          this.ratingModel();
          this.rating = this.pinRatings['RATING-BLOCK'].rating_setting;
        }, err => {
          this.error = this.helpers.error(err.error.message);
        })

      // this.subscriberService.urlGet('get-pin-and-ratings?cardSubscriberId=' + this.subscriber.cardSubscriberId)
      //   .subscribe(res => {
      //     this.pinRatings = JSON.parse(res["_body"]);
      //     //FORM
      //     this.pinRatingsForm = this.fb.group({
      //       "pin": [this.pinRatings.PIN, [Validators.required, Validators.maxLength(4), this.val.onlyNumbers]],
      //       "rating": ""
      //     });
      //     this.ratingModel();
      //     this.rating = this.pinRatings['RATING-BLOCK'].rating_setting;
      //   },err =>{
      //     // this.error = JSON.parse(err["_body"]);
      //     this.error = err;
      //   })
    }
  }

  // ACTUAL RATING ON RADIO BOXES
  ratingModel(){
    if(this.pinRatings){
      for(var i = 1; i <= 5; i++){
       if(this.pinRatings['RATING-BLOCK'].rating_setting[i] == 'Y'){
          if(i == 1){
            this.pinRatingsForm.controls['rating'].setValue('U');
          }else if(i == 2){
            this.pinRatingsForm.controls['rating'].setValue('PG');
          }else if(i == 3){
            this.pinRatingsForm.controls['rating'].setValue('12');
          }else if(i == 4){
            this.pinRatingsForm.controls['rating'].setValue('15');
          }else if(i == 5){
            this.pinRatingsForm.controls['rating'].setValue('18');
          }
       }
      }
      this.initialVal = this.pinRatingsForm.value.rating;
    }
  }

  //SET RATING
  setRating(){
    console.log(this.rating);
    var U = ['Y','Y','Y','Y','Y'];
    var PG = ['N','Y','Y','Y','Y'];
    var twelve = ['N','N','Y','Y','Y'];
    var fifteen = ['N','N','N','Y','Y'];
    var eighteen = ['N','N','N','N','Y'];

    for(var i = 1; i <= 5; i++){
      if(this.pinRatingsForm.value.rating == 'U'){
        this.rating[i] = U[i-1];
      }
      if(this.pinRatingsForm.value.rating == 'PG'){
        this.rating[i] = PG[i-1];
      }
      if(this.pinRatingsForm.value.rating == '12'){
        this.rating[i] = twelve[i-1];
      }
      if(this.pinRatingsForm.value.rating == '15'){
        this.rating[i] = fifteen[i-1];
      }
      if(this.pinRatingsForm.value.rating == '18'){
        this.rating[i] = eighteen[i -1];
      }
    }

  }


  processForm(){
    this.submitted = true;

    //Success message if pin and rating TRUE
    var success = {
      pin: false,
      rating : false
    };

    if(this.pinRatingsForm.valid){
      //PIN data
      const data = {
        cardSubscriberId : this.subscriber.cardSubscriberId,
        pin: this.pinRatingsForm.value.pin
      };
      // Change Pin Request, if value have changed
      if(this.pinRatings.PIN != this.pinRatingsForm.value.pin){
        this.http.put(this.globals.API_ENDPOINT + '/subscribers/change-pin', data, {responseType: 'text'})
          .subscribe(res => {
            console.log(res);
            success.pin = true;
            if(success.pin && success.rating){
              this.success = true;
            }
          }, err => {
            err = JSON.parse(err.error);
            console.log(err);
            if (err.fieldErrors) {
              this.error = err.fieldErrors[0];
            }else {
              this.error = this.helpers.error(err.message);
            }
          })
      }else{
        success.pin = true;
        if(success.pin && success.rating){
          this.success = true;
        }
      }

      // Rating data
      this.setRating();
      const data2 = {
        cardSubscriberId : this.subscriber.cardSubscriberId,
        ratings: this.rating
      };
      //Change ratings Request, if value have changed
      if(this.initialVal != this.pinRatingsForm.value.rating){
        this.http.put(this.globals.API_ENDPOINT + '/subscribers/change-ratings', data2, {responseType: 'text'})
          .subscribe(res => {
            console.log(res);
            success.rating = true;
            if(success.pin && success.rating){
              this.success = true;
            }
          }, err => {
            err = JSON.parse(err.error);
            console.log(err);
            if (err.fieldErrors) {
              this.error = err.fieldErrors[0];
            }else {
              this.error = this.helpers.error(err.message);
            }
          })
      }else{
        success.rating = true;
        if(success.pin && success.rating){
          this.success = true;
        }
      }
    } //Form ? valid
  }//Process form


  goBack(){
    this.location.back();
  }

}

