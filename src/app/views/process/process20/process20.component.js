"use strict";
//
// PROCESS 20: Edit Pin and Ratings
//
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var Process20Component = /** @class */ (function () {
    function Process20Component(location, subscriberService, router, fb, val) {
        this.location = location;
        this.subscriberService = subscriberService;
        this.router = router;
        this.fb = fb;
        this.val = val;
        this.success = false;
        this.submitted = false;
    }
    ;
    Process20Component.prototype.ngOnInit = function () {
        var _this = this;
        if (sessionStorage.getItem('subscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
            this.subscriberService.urlGet('get-pin-and-ratings?cardSubscriberId=' + this.subscriber.cardSubscriberId)
                .subscribe(function (res) {
                _this.pinRatings = JSON.parse(res["_body"]);
                //FORM
                _this.pinRatingsForm = _this.fb.group({
                    "pin": [_this.pinRatings.PIN, [forms_1.Validators.required, forms_1.Validators.maxLength(4), _this.val.onlyNumbers]],
                    "rating": ""
                });
                _this.ratingModel();
                _this.rating = _this.pinRatings['RATING-BLOCK'].rating_setting;
            }, function (err) {
                // this.error = JSON.parse(err["_body"]);
                _this.error = err;
            });
        }
    };
    // ACTUAL RATING ON RADIO BOXES
    Process20Component.prototype.ratingModel = function () {
        if (this.pinRatings) {
            for (var i = 1; i <= 5; i++) {
                if (this.pinRatings['RATING-BLOCK'].rating_setting[i] == 'Y') {
                    if (i == 1) {
                        this.pinRatingsForm.controls['rating'].setValue('U');
                    }
                    else if (i == 2) {
                        this.pinRatingsForm.controls['rating'].setValue('PG');
                    }
                    else if (i == 3) {
                        this.pinRatingsForm.controls['rating'].setValue('12');
                    }
                    else if (i == 4) {
                        this.pinRatingsForm.controls['rating'].setValue('15');
                    }
                    else if (i == 5) {
                        this.pinRatingsForm.controls['rating'].setValue('18');
                    }
                }
            }
            this.initialVal = this.pinRatingsForm.value.rating;
        }
    };
    //SET RATING
    Process20Component.prototype.setRating = function () {
        console.log(this.rating);
        var U = ['Y', 'Y', 'Y', 'Y', 'Y'];
        var PG = ['N', 'Y', 'Y', 'Y', 'Y'];
        var twelve = ['N', 'N', 'Y', 'Y', 'Y'];
        var fifteen = ['N', 'N', 'N', 'Y', 'Y'];
        var eighteen = ['N', 'N', 'N', 'N', 'Y'];
        for (var i = 1; i <= 5; i++) {
            if (this.pinRatingsForm.value.rating == 'U') {
                this.rating[i] = U[i - 1];
            }
            if (this.pinRatingsForm.value.rating == 'PG') {
                this.rating[i] = PG[i - 1];
            }
            if (this.pinRatingsForm.value.rating == '12') {
                this.rating[i] = twelve[i - 1];
            }
            if (this.pinRatingsForm.value.rating == '15') {
                this.rating[i] = fifteen[i - 1];
            }
            if (this.pinRatingsForm.value.rating == '18') {
                this.rating[i] = eighteen[i - 1];
            }
        }
    };
    Process20Component.prototype.processForm = function () {
        var _this = this;
        this.submitted = true;
        //Success message if pin and rating TRUE
        var success = {
            pin: false,
            rating: false
        };
        if (this.pinRatingsForm.valid) {
            //PIN data
            var data = {
                cardSubscriberId: this.subscriber.cardSubscriberId,
                pin: this.pinRatingsForm.value.pin
            };
            // Change Pin Request, if value have changed
            if (this.pinRatings.PIN != this.pinRatingsForm.value.pin) {
                this.subscriberService.urlSubscribers('change-pin', data)
                    .subscribe(function (res) {
                    console.log(res);
                    success.pin = true;
                    if (success.pin && success.rating) {
                        _this.success = true;
                    }
                }, function (err) {
                    _this.error = JSON.parse(err["_body"]);
                });
            }
            else {
                success.pin = true;
                if (success.pin && success.rating) {
                    this.success = true;
                }
            }
            // Rating data
            this.setRating();
            var data2 = {
                cardSubscriberId: this.subscriber.cardSubscriberId,
                ratings: this.rating
            };
            //Change ratings Request, if value have changed
            if (this.initialVal != this.pinRatingsForm.value.rating) {
                this.subscriberService.urlSubscribers('change-ratings', data2)
                    .subscribe(function (res) {
                    console.log(res);
                    success.rating = true;
                    if (success.pin && success.rating) {
                        _this.success = true;
                    }
                }, function (err) {
                    _this.error = JSON.parse(err["_body"]);
                });
            }
            else {
                success.rating = true;
                if (success.pin && success.rating) {
                    this.success = true;
                }
            }
        } //Form ? valid
    }; //Process form
    Process20Component.prototype.goBack = function () {
        this.location.back();
    };
    Process20Component = __decorate([
        core_1.Component({
            selector: 'process20',
            templateUrl: 'process20.component.html',
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService]
        })
    ], Process20Component);
    return Process20Component;
}());
exports.Process20Component = Process20Component;
