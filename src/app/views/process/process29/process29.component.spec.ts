import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Process29Component } from './process29.component';

describe('Process29Component', () => {
  let component: Process29Component;
  let fixture: ComponentFixture<Process29Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Process29Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Process29Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
