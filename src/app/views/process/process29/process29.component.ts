//
// PROCESS 29:
//
import { Component, OnInit } from '@angular/core';
import {HelpersService} from  '../../../services/helpers.service';
import {NavigationExtras, Router} from '@angular/router';
import {RestService} from '../../../services/rest.service';

@Component({
  selector: 'app-process29',
  templateUrl: './process29.component.html',
  styleUrls: ['./process29.component.css'],
  providers: [ HelpersService, RestService]
})
export class Process29Component implements OnInit {
  error : boolean;
  public success: boolean = false;
  subscriber : any;
  undeliveredFlag : boolean;
  title = 'Request replacement card';
  urgent = false;
  options = ['E1'];


  constructor(private helpers : HelpersService, private router : Router, private rest: RestService){};
  ngOnInit(){
    if(sessionStorage.getItem('subscriber')){
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    }
  }
}


