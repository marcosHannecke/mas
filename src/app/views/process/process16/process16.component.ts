//
// PROCESS 16: Resend All Personal Bits
//

import { Component, OnInit } from '@angular/core';
import {Router, Params} from '@angular/router';

@Component({
  selector: 'process16',
  templateUrl: 'process16.component.html',
  providers: []
})
export class Process16Component implements OnInit {
  subscriber : any;
  error : any;
  success : boolean = false;
  advise : boolean = true;

  constructor( private router : Router){};
  ngOnInit(){

    if(sessionStorage.getItem('subscriber') != null) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    }
  }

  proceed(){
    this.router.navigate(['/process19'], { queryParams: { rapb: 1 } })
  }

}
