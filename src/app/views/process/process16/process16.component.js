"use strict";
//
// PROCESS 16: Resend All Personal Bits
//
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var Process16Component = /** @class */ (function () {
    function Process16Component(router) {
        this.router = router;
        this.success = false;
        this.advise = true;
    }
    ;
    Process16Component.prototype.ngOnInit = function () {
        if (sessionStorage.getItem('subscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
        }
    };
    Process16Component.prototype.proceed = function () {
        this.router.navigate(['/process19'], { queryParams: { rapb: 1 } });
    };
    Process16Component = __decorate([
        core_1.Component({
            selector: 'process16',
            templateUrl: 'process16.component.html',
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService]
        })
    ], Process16Component);
    return Process16Component;
}());
exports.Process16Component = Process16Component;
