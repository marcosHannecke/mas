"use strict";
//
// PROCESS 32:
//
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var products_service_1 = require("../../../services/products.service");
var Process32Component = /** @class */ (function () {
    function Process32Component(productsService, router) {
        this.productsService = productsService;
        this.router = router;
        this.error = false;
        this.success = false;
    }
    ;
    Process32Component.prototype.ngOnInit = function () {
    };
    Process32Component = __decorate([
        core_1.Component({
            selector: 'process32',
            templateUrl: 'process32.component.html',
            providers: [products_service_1.ProductsService]
        })
    ], Process32Component);
    return Process32Component;
}());
exports.Process32Component = Process32Component;
