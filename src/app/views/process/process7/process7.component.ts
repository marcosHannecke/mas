import { Component, OnInit } from '@angular/core';

// PROCESS 7:  I have two Sky boxes, I want to move my subscription to the one in the other room.
@Component({
  selector: 'process7',
  templateUrl: 'process7.component.html',
  providers: []
})
export class Process7Component implements OnInit {
  subscriber : any;
  entitlements : any;
  error : any;
  foundInactive : boolean = false;

  constructor() {}

  ngOnInit(){

  }

}

