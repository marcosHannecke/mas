"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var products_service_1 = require("../../../services/products.service");
var helpers_service_1 = require("../../../services/helpers.service");
// PROCESS 7:  I have two Sky boxes, I want to move my subscription to the one in the other room.
var Process7Component = /** @class */ (function () {
    function Process7Component(helpers, router, fb, validators, subscribersService, productsService) {
        this.helpers = helpers;
        this.router = router;
        this.fb = fb;
        this.validators = validators;
        this.subscribersService = subscribersService;
        this.productsService = productsService;
        this.foundInactive = false;
    }
    Process7Component.prototype.ngOnInit = function () {
    };
    Process7Component = __decorate([
        core_1.Component({
            selector: 'process7',
            templateUrl: 'process7.component.html',
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService, products_service_1.ProductsService, helpers_service_1.HelpersService]
        })
    ], Process7Component);
    return Process7Component;
}());
exports.Process7Component = Process7Component;
