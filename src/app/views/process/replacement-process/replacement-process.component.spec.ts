import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplacementProcessComponent } from './replacement-process.component';

describe('ReplacementProcessComponent', () => {
  let component: ReplacementProcessComponent;
  let fixture: ComponentFixture<ReplacementProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplacementProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplacementProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
