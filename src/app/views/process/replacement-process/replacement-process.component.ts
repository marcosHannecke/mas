import {Component, Input, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from '../../../services/helpers.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../../app.global';

@Component({
  selector: 'app-replacement-process',
  templateUrl: './replacement-process.component.html',
  styleUrls: ['./replacement-process.component.css'],
  providers: [ValidatorsService, HelpersService]
})
export class ReplacementProcessComponent implements OnInit {


  @Input()
  title: String;
  @Input()
  urgent: any;
  @Input()
  options: Array<string>;

  addressForm: FormGroup;
  requestForm: FormGroup;
  submitted: boolean = false;
  subscriber: any;
  error: any;
  success: boolean = false;
  replacementReasonCode = [];
  adviseUndelivered: boolean = false;

  address: boolean = true;
  succ: any;
  params: any;

  constructor(private helpers: HelpersService, private router: ActivatedRoute, private fb: FormBuilder,
              private  val: ValidatorsService, private http: HttpClient, private globals: Globals) {};
  ngOnInit() {
    console.log('urgent', this.urgent);
    this.router.params
      .subscribe(res => {
        this.urgent = res;
        if (this.urgent.u == 'undelivered') {
          this.adviseUndelivered = true;
        }
      });

    if (this.options.length == 0) {
      this.replacementReasonCode = RRcode;
    }else {
      RRcode.forEach(code => {
        this.options.forEach(option => {
          if(code.code == option ) {
            console.log(option, code);
            this.replacementReasonCode.push(code);
          }
        })
      })
    }

    if (this.urgent.u == 'undelivered') {
      this.requestForm = this.fb.group({
        "replacementReasonCode" : ["E1", Validators.required],
        "urgent" : [false]
      });
    }else {
      this.requestForm = this.fb.group({
        "replacementReasonCode" : ["", Validators.required],
        "urgent" : [false]
      });
    }

    if (this.urgent.u == "y"){
      this.requestForm.controls['urgent'].setValue(true)
    }

    if (sessionStorage.getItem('subscriber') != null) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
      this.addressForm = this.fb.group({
        "cardSubscriberId" : [this.subscriber.cardSubscriberId],
        "title": [this.subscriber.title, [Validators.required, this.val.specialChar]],
        "initials" : [this.subscriber.initials.toUpperCase(), [Validators.required, Validators.maxLength(3), this.val.letterSpaces]],
        "surname" : [this.subscriber.surname, [Validators.required,  Validators.maxLength(35), this.val.letterSpaces]],
        "addressLine1" : [ this.subscriber.addressLine1, [Validators.required, Validators.maxLength(35), this.val.specialChar]],
        "addressLine2" : [this.subscriber.addressLine2, [Validators.required, Validators.maxLength(35), this.val.specialChar]],
        "addressLine3" : [this.subscriber.addressLine3 ],
        "addressLine4" : [this.subscriber.addressLine4],
        "addressLine5" : [this.subscriber.addressLine5],
        "postcode" :[this.subscriber.postcode, [Validators.required, Validators.maxLength(9), this.val.specialChar]],
        "countryCode" : [this.subscriber.countryCode],
      });
    }
  }


  requestCard() {
    this.submitted = true;

    // Success message if pin and rating TRUE
    this.succ = {
      address: false,
      card : false
    };

    if (!this.address) {
      this.updateAddress()
    }else {
      this.succ.address = true;
      this.requestReplacementCard();
    }
  }

  requestReplacementCard() {
    if (this.requestForm.valid)  {
      const data = {
        "cardSubscriberId" : this.subscriber.cardSubscriberId,
        "replacementReasonCode" : this.requestForm.value.replacementReasonCode,
        "urgent": this.requestForm.value.urgent
      };
      this.http.put(this.globals.API_ENDPOINT + '/subscribers/request-replacement-card', data, {responseType: 'text'})
        .subscribe(res => {
          console.log(res);
          this.succ.card = true;
          if (this.succ.card && this.succ.address) {
            this.success = true;
          }
        }, err => {
          err = JSON.parse(err.error);
          console.log(err);
          if (err.fieldErrors) {
            this.error = err.fieldErrors[0];
          }else {
            this.error = this.helpers.error(err.message);
          }
        })
    }
  }

  updateAddress() {
    if (this.addressForm.valid && this.requestForm.valid) {
      let json = this.addressForm.value;

      // Initials to uppercase
      json.initials = json.initials.toUpperCase();
      // Capitalize surname
      json.surname = this.helpers.toTitleCase(json.surname);

      this.http.put(this.globals.API_ENDPOINT + '/subscribers', json, {responseType: 'text'})
        .subscribe(res => {
          console.log(res);
          this.succ.address = true;
          this.requestReplacementCard();
          if (this.succ.card && this.succ.address) {
            this.success = true;
          }
        }, err => {
          err = JSON.parse(err.error);
          console.log(err);
          if (err.fieldErrors) {
            this.error = err.fieldErrors[0];
          }else {
            this.error = this.helpers.error(err.message);
          }
        })
    }
  }
}

// Replacement Reason Codes
let RRcode = [
  {
    "code" : "A1",
    "text" : "Viewing Card was working and is now faulty"
  },
  {
    "code" : "A2",
    "text" : "Viewing Card was faulty on arrival"
  },
  {
    "code" : "A5",
    "text" : "Customer unable to clear \"Press select\" OSM"
  },
  {
    "code" : "A6",
    "text" : "Viewing Card was damaged on receipt"
  },
  {
    "code" : "B1",
    "text" : "Viewing Card has been damaged through misuse"
  },
  {
    "code" : "B2",
    "text" : "Customer has and old type/out of date viewing card"
  },
  {
    "code" : "B4",
    "text" : "Viewing Card has been lost"
  },
  {
    "code" : "B5",
    "text" : "Viewing Card has been stolen"
  },
  {
    "code" : "B6",
    "text" : "Customer has reinstated without a card"
  },
  {
    "code" : "C1",
    "text" : "Replacement Viewing Card has not been received"
  },
  // {
  //   "code" : "CC",
  //   "text" : "Changeover Replaced"
  // },
  {
    "code" : "D1",
    "text" : "Replacement card for Staff /VIP"
  },
  {
    "code" : "E1",
    "text" : "Please Reissue Undelivered Replacement Card"
  },
  {
    "code" : "P2",
    "text" : "Problem with your VC OSM on a white P2 card"
  }
  // {
  //   "code" : "R1",
  //   "text" : "Lost Retailer Card"
  // },
  // {
  //   "code" : "R2",
  //   "text" : "Stolen Retailer Card"
  // },
  // {
  //   "code" : "X1",
  //   "text" : "WAP Re-instatement"
  // },
  // {
  //   "code" : "X2",
  //   "text" : "IVR Re-instatement"
  // }
];

