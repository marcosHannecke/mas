/*
*  PROCESS 23
*
*/

import { Component, OnInit, Input } from '@angular/core';
import {ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from  '../../../services/helpers.service';
import {Router} from '@angular/router';
import {RestService} from '../../../services/rest.service';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../../app.global';

@Component({
  selector: 'process23',
  templateUrl: 'process23.component.html',
  styles: ['.customerSubscribers h3{ margin-top: 7px; margin-bottom: 7px;}' +
  '.steps ul{padding: 0;} .steps ul li{margin: 10px 0;} .steps ul li strong{line-height: 26px;} .steps .btn:hover{background: #0f3b68; border-color: #0f3b68; cursor: default;}'],
  providers: [ValidatorsService, HelpersService, RestService]
})
export class Process23Component implements OnInit {
  private subscriber: any;
  public customerSubscribers : any = [];
  public picked: any;
  public advise: boolean = false;
  public error: boolean = false;
  public success: boolean = false;
  public cancel: boolean = false;
  public insert: boolean = false;
  private selected: any = [];
  private noSelected: any = [];
  public pickAccount: boolean = false;
  public joinExit: boolean = false;

  constructor(private helpers: HelpersService, private router: Router, private rest: RestService,
              private http: HttpClient, private globals: Globals) {};

  ngOnInit(){
    if(sessionStorage.getItem('subscriber') != null){
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    }
    // GET CUSTOMER SUBSCRIBERS
    this.getCustomerSubscribers();
  }

  pick(subs){
    this.picked = subs;
    console.log(this.picked);
  }

  insertProceed(){
    sessionStorage.setItem('subscriber', JSON.stringify(this.picked));
    this.router.navigate(['/process19'], { queryParams: { rapb: 1 } });
  }

  //GET CUSTOMER SUBSCRIBERS
  getCustomerSubscribers(){
    this.rest.get('/subscribers?q=customerId=' + this.subscriber.customerId)
      .subscribe(res => {
        this.isActive(res);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });

    // this.subscriberService.getCustomerSubscribers(this.subscriber.customerId)
    //   .subscribe(res => {
    //     this.isActive(res);
    //   }, err => {
    //     this.error = JSON.parse(err["_body"]);
    //   })
  }

  // SUBSCRIBER ACTIVE -> ADD TO customerSubscribers
  // INACTIVE / Entitlements Awaiting Activation
  isActive(res){
    let resp;
    for(let sub of res){
      // this.rest.get('/entitlements?subscriberId=' + sub.id + '&productType=subscription')
      //   .subscribe(res => {
      //     resp = res;
      //     console.log('resp', resp);
      //     if (!sub.enabled) {
      //       sub.active = false;
      //     }else {
      //       if (resp.length === 0) {
      //         // INACTIVE
      //         sub.active = false;
      //       }else if (resp.length > 0) {
      //         let active = true;
      //         for (const ent of resp) {
      //           if (!ent.activated) {
      //             active = false;
      //           }
      //         }
      //         if (active) {
      //           // ACTIVE
      //           sub.active = true;
      //         }else {
      //           // Entitlements Awaiting Activation
      //           sub.active = 'AW';
      //         }
      //       }
      //     }
      //   }, err => {
      //     this.error = this.helpers.error(err.error.message);
      //   });
      //
      //
      // this.rest.get('/entitlements?subscriberId=' + sub.id + '&productType=ppv_enabler')
      //   .subscribe(res => {
      //     resp = res;
      //     console.log('resp', resp);
      //     if (!sub.enabled) {
      //       sub.active = false;
      //     }else {
      //       if (resp.length === 0) {
      //         // INACTIVE
      //         sub.active = false;
      //       }else if (resp.length > 0) {
      //         let active = true;
      //         for (const ent of resp) {
      //           if (!ent.activated) {
      //             active = false;
      //           }
      //         }
      //         if (active) {
      //           // ACTIVE
      //           sub.active = true;
      //         }else {
      //           // Entitlements Awaiting Activation
      //           sub.active = 'AW';
      //         }
      //       }
      //     }
      //   }, err => {
      //     this.error = this.helpers.error(err.error.message);
      //   });
      this.rest.get('/entitlements?subscriberId=' + sub.id + '&productType=subscription')
        .subscribe(res => {
          resp = res;
          if(!sub.enabled){
            sub.active = false;
          }else{
            if(resp.length == 0){
              // INACTIVE
              sub.active = false;
              this.rest.get('/entitlements?subscriberId=' + sub.id + '&productType=ppv_enabler')
                .subscribe(res => {
                  resp = res;
                  if(!sub.enabled){
                    sub.active = false;
                  }else{
                    if(resp.length == 0){
                      // INACTIVE
                      sub.active = false;
                    }else if(resp.length > 0){
                      var active = true;
                      for(let ent of resp){
                        if(!ent.activated){
                          active = false;
                        }
                      }
                      if(active){
                        // ACTIVE
                        sub.active = true;
                        if(sub.viewingCardNumber != null){
                          this.customerSubscribers.push(sub);
                          this.noSelected.push(sub);
                          // ACTIVE SUBSCRIBERS 'CHECK'
                          this.advise = true;
                        }
                      }else{
                        // Entitlements Awaiting Activation
                        sub.active = 'AW';
                      }
                    }
                  }
                }, err =>{
                  this.error = this.helpers.error(err.error.message);
                })
            }else if(resp.length > 0){
              var active = true;
              for(let ent of resp){
                if(!ent.activated){
                  active = false;
                }
              }
              if(active){
                // ACTIVE
                sub.active = true;
                if(sub.viewingCardNumber != null){
                  this.customerSubscribers.push(sub);
                  this.noSelected.push(sub);
                  // ACTIVE SUBSCRIBERS 'CHECK'
                  this.advise = true;
                }
              }else{
                // Entitlements Awaiting Activation
                sub.active = 'AW';
              }
            }
          }
        }, err =>{
          this.error = this.helpers.error(err.error.message);
        });

    }
  }

  //CHECK
  checked(i, value, subs){
    if(value){
      this.selected.push(subs);
      for(var b = 0; b < this.noSelected.length; b++){
        if(this.noSelected[b].id == subs.id){
          this.noSelected.splice(b, 1)
        }
      }
    }else if(!value){
      for(var a = 0; a< this.selected.length; a++){
        if(this.selected[a].id == subs.id){
          this.selected.splice(a, 1);
          this.noSelected.push(subs);
        }
      }
    }
  }

  // ALL CHECKED DIRECTLY TO PICK
  allChecked(){
    if(this.selected.length == this.customerSubscribers.length){
      this.pickAccount = true;
    }else{
      this.cancel = true;
    }
  }

  //GET ENTITLEMENTS - CANCEL 1/2
  getEntitlements(){
    this.cancel = false;
    for(let subs of this.noSelected){
      if(subs.active || subs.active == 'AW'){

        this.rest.get('/entitlements?q=subscriberId=' + subs.id)
          .subscribe(res => {
              console.log('entitlements', res);
              this.cancelAccount(res, subs.id)
            }, err => {
              this.error = this.helpers.error(err.error.message);
            }
          );
      }
    }
    this.selectedSubscribersCheck();
  }
  //GET ENTITLEMENTS - CANCEL 2/2
  cancelAccount(res, id){
    let entitlementsNames = [];
    for (let ent of res) {
      entitlementsNames.push(ent.productGroupName)
    }
    let data = {
      "subscriberId": id,
      "addedProductGroupNames": [],
      "removedProductGroupNames": entitlementsNames
    };

    this.http.put(this.globals.API_ENDPOINT + '/entitlements', data, {responseType: 'text'})
      .subscribe(() => {
        console.log('update', res);
      }, err => {
        err = JSON.parse(err.error);
        console.log(err);
        if (err.fieldErrors) {
          this.error = err.fieldErrors[0];
        }else {
          this.error = this.helpers.error(err.message);
        }
      })

  }

  selectedSubscribersCheck(){
    this.cancel = false;
    if(this.selected.length > 0){
      this.picked = this.selected[0];
      this.pickAccount = true;
    }else{
      this.joinExit = true;
    }
  }

}
