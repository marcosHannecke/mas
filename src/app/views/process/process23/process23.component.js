"use strict";
/*
*  PROCESS 23
*
*/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var products_service_1 = require("../../../services/products.service");
var helpers_service_1 = require("../../../services/helpers.service");
var Process23Component = /** @class */ (function () {
    function Process23Component(helpers, subscriberService, productsService, router) {
        this.helpers = helpers;
        this.subscriberService = subscriberService;
        this.productsService = productsService;
        this.router = router;
        this.customerSubscribers = [];
        this.advise = false;
        this.error = false;
        this.success = false;
        this.cancel = false;
        this.insert = false;
        this.selected = [];
        this.noSelected = [];
        this.pickAccount = false;
        this.joinExit = false;
    }
    ;
    Process23Component.prototype.ngOnInit = function () {
        if (sessionStorage.getItem('subscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
        }
        // GET CUSTOMER SUBSCRIBERS
        this.getCustomerSubscribers();
    };
    Process23Component.prototype.pick = function (subs) {
        this.picked = subs;
        console.log(this.picked);
    };
    Process23Component.prototype.insertProceed = function () {
        sessionStorage.setItem('subscriber', JSON.stringify(this.picked));
        this.router.navigate(['/process19'], { queryParams: { rapb: 1 } });
    };
    //GET CUSTOMER SUBSCRIBERS
    Process23Component.prototype.getCustomerSubscribers = function () {
        var _this = this;
        this.subscriberService.getCustomerSubscribers(this.subscriber.customerId)
            .subscribe(function (res) {
            _this.isActive(res);
        }, function (err) {
            _this.error = JSON.parse(err["_body"]);
        });
    };
    // SUBSCRIBER ACTIVE -> ADD TO customerSubscribers
    // INACTIVE / Entitlements Awaiting Activation
    Process23Component.prototype.isActive = function (res) {
        var _this = this;
        var _loop_1 = function (sub) {
            this_1.productsService.getById('E', sub.id)
                .subscribe(function (res) {
                if (!sub.enabled) {
                    sub.active = false;
                }
                else {
                    if (res.length == 0) {
                        // INACTIVE
                        sub.active = false;
                    }
                    else if (res.length > 0) {
                        var active = true;
                        for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                            var ent = res_1[_i];
                            if (!ent.activated) {
                                active = false;
                            }
                        }
                        if (active) {
                            // ACTIVE
                            sub.active = true;
                            if (sub.viewingCardNumber != null) {
                                _this.customerSubscribers.push(sub);
                                _this.noSelected.push(sub);
                                // ACTIVE SUBSCRIBERS 'CHECK'
                                _this.advise = true;
                            }
                        }
                        else {
                            // Entitlements Awaiting Activation
                            sub.active = 'AW';
                        }
                    }
                }
            }, function (err) {
                _this.error = _this.helpers.getError(err);
            });
        };
        var this_1 = this;
        for (var _i = 0, res_2 = res; _i < res_2.length; _i++) {
            var sub = res_2[_i];
            _loop_1(sub);
        }
    };
    //CHECK
    Process23Component.prototype.checked = function (i, value, subs) {
        if (value) {
            this.selected.push(subs);
            for (var b = 0; b < this.noSelected.length; b++) {
                if (this.noSelected[b].id == subs.id) {
                    this.noSelected.splice(b, 1);
                }
            }
        }
        else if (!value) {
            for (var a = 0; a < this.selected.length; a++) {
                if (this.selected[a].id == subs.id) {
                    this.selected.splice(a, 1);
                    this.noSelected.push(subs);
                }
            }
        }
    };
    // ALL CHECKED DIRECTLY TO PICK
    Process23Component.prototype.allChecked = function () {
        if (this.selected.length == this.customerSubscribers.length) {
            this.pickAccount = true;
        }
        else {
            this.cancel = true;
        }
    };
    //GET ENTITLEMENTS - CANCEL 1/2
    Process23Component.prototype.getEntitlements = function () {
        var _this = this;
        this.cancel = false;
        var _loop_2 = function (subs) {
            if (subs.active || subs.active == 'AW') {
                this_2.productsService.getById('E', subs.id)
                    .subscribe(function (res) {
                    console.log('entitlements', res);
                    _this.cancelAccount(res, subs.id);
                }, function (error) {
                    // this.error = JSON.parse(error["_body"])
                    _this.error = _this.helpers.getError(error);
                });
            }
        };
        var this_2 = this;
        for (var _i = 0, _a = this.noSelected; _i < _a.length; _i++) {
            var subs = _a[_i];
            _loop_2(subs);
        }
        this.selectedSubscribersCheck();
    };
    //GET ENTITLEMENTS - CANCEL 2/2
    Process23Component.prototype.cancelAccount = function (res, id) {
        var _this = this;
        var entitlementsNames = [];
        for (var _i = 0, res_3 = res; _i < res_3.length; _i++) {
            var ent = res_3[_i];
            entitlementsNames.push(ent.productGroupName);
        }
        var data = {
            "subscriberId": id,
            "addedProductGroupNames": [],
            "removedProductGroupNames": entitlementsNames
        };
        this.productsService.updateEntitlements('E', data)
            .subscribe(function (res) {
            console.log('update', res);
        }, function (error) {
            // this.error = JSON.parse(error["_body"])
            _this.error = _this.helpers.getError(error);
        });
    };
    Process23Component.prototype.selectedSubscribersCheck = function () {
        this.cancel = false;
        if (this.selected.length > 0) {
            this.picked = this.selected[0];
            this.pickAccount = true;
        }
        else {
            this.joinExit = true;
        }
    };
    Process23Component = __decorate([
        core_1.Component({
            selector: 'process23',
            templateUrl: 'process23.component.html',
            styles: ['.customerSubscribers h3{ margin-top: 7px; margin-bottom: 7px;}' +
                    '.steps ul{padding: 0;} .steps ul li{margin: 10px 0;} .steps ul li strong{line-height: 26px;} .steps .btn:hover{background: #0f3b68; border-color: #0f3b68; cursor: default;}'],
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService, products_service_1.ProductsService, helpers_service_1.HelpersService]
        })
    ], Process23Component);
    return Process23Component;
}());
exports.Process23Component = Process23Component;
