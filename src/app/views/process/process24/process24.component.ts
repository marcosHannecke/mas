/*
*  PROCESS 24
*
*/
import { Component, OnInit, Input } from '@angular/core';
import {ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from  '../../../services/helpers.service';
import {Router} from '@angular/router';
import {RestService} from '../../../services/rest.service';

@Component({
  selector: 'process24',
  templateUrl: 'process24.component.html',
  styles: ['.customerSubscribers h3{ margin-top: 7px; margin-bottom: 7px;}' +
  '.steps ul{padding: 0;} .steps ul li{margin: 10px 0;} .steps ul li strong{line-height: 26px;} .steps .btn:hover{background: #0f3b68; border-color: #0f3b68; cursor: default;}'],
  providers: [ValidatorsService,HelpersService, RestService]
})
export class Process24Component implements OnInit {
  private subscriber : any;
  public customerSubscribers : any = [];
  public picked : any;
  public advise : boolean = false;
  public error : boolean = false;
  public success: boolean = false;
  public continueHybrid : boolean = false;
  public insert : boolean = false;
  private selected : any = [];
  private noSelected : any = [];
  public pickAccount : boolean = false;
  public joinExit : boolean = false;

  constructor(private helpers: HelpersService, private router: Router, private rest: RestService){};

  ngOnInit(){
    if(sessionStorage.getItem('subscriber') != null){
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    }
    // GET CUSTOMER SUBSCRIBERS
    this.getCustomerSubscribers();
  }

  proceed16(){
    sessionStorage.setItem('subscriber', JSON.stringify(this.picked));
    // this.router.navigate(['/process16'], { queryParams: { p: 'process16'} } );
    this.router.navigate(['/process19'], { queryParams: { rapb: '1'} } );
  }

  pick(subs){
    this.picked = subs;
    console.log(this.picked);
  }

  noneOfThese(){
    sessionStorage.setItem('subscriber', JSON.stringify(this.picked));
    this.router.navigate(['/viewSubscriber/' + this.subscriber.id], { queryParams: { p: 'process15'} } );
  }

  //GET CUSTOMER SUBSCRIBERS
  getCustomerSubscribers(){
    this.rest.get('/subscribers?q=customerId=' + this.subscriber.customerId)
      .subscribe(res => {
        this.isActive(res);
      }, err=>{
        this.error = this.helpers.error(err.error.message);
      });

    // this.subscriberService.getCustomerSubscribers(this.subscriber.customerId)
    //   .subscribe(res => {
    //     this.isActive(res);
    //   }, err=>{
    //     this.error = JSON.parse(err["_body"]);
    //   })
  }

  // SUBSCRIBER ACTIVE -> ADD TO customerSubscribers
  // INACTIVE / Entitlements Awaiting Activation
  isActive(res){
    let resp;
    for(let sub of res){
      this.rest.get('/entitlements?q=subscriberId=' + sub.id)
        .subscribe(response =>{
          resp = response;
          if(!sub.enabled){
            sub.active = false;
          }else {
            if (resp.length == 0) {
              // INACTIVE
              sub.active = false;
            } else if (resp.length > 0) {
              var active = true;
              for (let ent of resp) {
                if (!ent.activated) {
                  active = false;
                }
              }
              if (active) {
                // ACTIVE
                sub.active = true;
                if (sub.viewingCardNumber != null) {
                  this.customerSubscribers.push(sub);
                  this.picked = this.customerSubscribers[0];
                  //ACTIVE SUBSCRIBERS 'CHECK'
                  this.advise = true;
                }
              } else {
                // Entitlements Awaiting Activation
                sub.active = 'AW';
              }
            }
          }
        }, err =>{
          this.error = this.helpers.error(err.error.message);
        })

      // this.productsService.getById('E', sub.id)
      //   .subscribe(res =>{
      //     if(!sub.enabled){
      //       sub.active = false;
      //     }else {
      //       if (res.length == 0) {
      //         // INACTIVE
      //         sub.active = false;
      //       } else if (res.length > 0) {
      //         var active = true;
      //         for (let ent of res) {
      //           if (!ent.activated) {
      //             active = false;
      //           }
      //         }
      //         if (active) {
      //           // ACTIVE
      //           sub.active = true;
      //           if (sub.viewingCardNumber != null) {
      //             this.customerSubscribers.push(sub);
      //             this.picked = this.customerSubscribers[0];
      //             //ACTIVE SUBSCRIBERS 'CHECK'
      //             this.advise = true;
      //           }
      //         } else {
      //           // Entitlements Awaiting Activation
      //           sub.active = 'AW';
      //         }
      //       }
      //     }
      //   }, err =>{
      //     this.error = this.helpers.getError(err);
      //   })
    }
  }

}
