"use strict";
/*
*  PROCESS 24
*
*/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var products_service_1 = require("../../../services/products.service");
var helpers_service_1 = require("../../../services/helpers.service");
var Process24Component = /** @class */ (function () {
    function Process24Component(helpers, subscriberService, productsService, router) {
        this.helpers = helpers;
        this.subscriberService = subscriberService;
        this.productsService = productsService;
        this.router = router;
        this.customerSubscribers = [];
        this.advise = false;
        this.error = false;
        this.success = false;
        this.continueHybrid = false;
        this.insert = false;
        this.selected = [];
        this.noSelected = [];
        this.pickAccount = false;
        this.joinExit = false;
    }
    ;
    Process24Component.prototype.ngOnInit = function () {
        if (sessionStorage.getItem('subscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
        }
        // GET CUSTOMER SUBSCRIBERS
        this.getCustomerSubscribers();
    };
    Process24Component.prototype.proceed16 = function () {
        sessionStorage.setItem('subscriber', JSON.stringify(this.picked));
        // this.router.navigate(['/process16'], { queryParams: { p: 'process16'} } );
        this.router.navigate(['/process19'], { queryParams: { rapb: '1' } });
    };
    Process24Component.prototype.pick = function (subs) {
        this.picked = subs;
        console.log(this.picked);
    };
    Process24Component.prototype.noneOfThese = function () {
        sessionStorage.setItem('subscriber', JSON.stringify(this.picked));
        this.router.navigate(['/viewSubscriber/' + this.subscriber.id], { queryParams: { p: 'process15' } });
    };
    //GET CUSTOMER SUBSCRIBERS
    Process24Component.prototype.getCustomerSubscribers = function () {
        var _this = this;
        this.subscriberService.getCustomerSubscribers(this.subscriber.customerId)
            .subscribe(function (res) {
            _this.isActive(res);
        }, function (err) {
            _this.error = JSON.parse(err["_body"]);
        });
    };
    // SUBSCRIBER ACTIVE -> ADD TO customerSubscribers
    // INACTIVE / Entitlements Awaiting Activation
    Process24Component.prototype.isActive = function (res) {
        var _this = this;
        var _loop_1 = function (sub) {
            this_1.productsService.getById('E', sub.id)
                .subscribe(function (res) {
                if (!sub.enabled) {
                    sub.active = false;
                }
                else {
                    if (res.length == 0) {
                        // INACTIVE
                        sub.active = false;
                    }
                    else if (res.length > 0) {
                        var active = true;
                        for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                            var ent = res_1[_i];
                            if (!ent.activated) {
                                active = false;
                            }
                        }
                        if (active) {
                            // ACTIVE
                            sub.active = true;
                            if (sub.viewingCardNumber != null) {
                                _this.customerSubscribers.push(sub);
                                _this.picked = _this.customerSubscribers[0];
                                //ACTIVE SUBSCRIBERS 'CHECK'
                                _this.advise = true;
                            }
                        }
                        else {
                            // Entitlements Awaiting Activation
                            sub.active = 'AW';
                        }
                    }
                }
            }, function (err) {
                _this.error = _this.helpers.getError(err);
            });
        };
        var this_1 = this;
        for (var _i = 0, res_2 = res; _i < res_2.length; _i++) {
            var sub = res_2[_i];
            _loop_1(sub);
        }
    };
    Process24Component = __decorate([
        core_1.Component({
            selector: 'process24',
            templateUrl: 'process24.component.html',
            styles: ['.customerSubscribers h3{ margin-top: 7px; margin-bottom: 7px;}' +
                    '.steps ul{padding: 0;} .steps ul li{margin: 10px 0;} .steps ul li strong{line-height: 26px;} .steps .btn:hover{background: #0f3b68; border-color: #0f3b68; cursor: default;}'],
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService, products_service_1.ProductsService, helpers_service_1.HelpersService]
        })
    ], Process24Component);
    return Process24Component;
}());
exports.Process24Component = Process24Component;
