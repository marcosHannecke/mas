"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var billing_history_component_1 = require("./billing-history.component");
var BILLINGHISTORY_ROUTER = [
    {
        path: '',
        component: billing_history_component_1.BillingHistoryComponent,
    },
];
exports.billingHistoryRouter = router_1.RouterModule.forChild(BILLINGHISTORY_ROUTER);
