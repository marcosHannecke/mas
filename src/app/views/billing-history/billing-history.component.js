"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var subscribers_service_1 = require("../../services/subscribers.service");
var payments_service_1 = require("../../services/payments.service");
var helpers_service_1 = require("../../services/helpers.service");
var core_2 = require("@angular/core");
var animations_1 = require("@angular/animations");
var angular_highcharts_1 = require("angular-highcharts");
var BillingHistoryComponent = /** @class */ (function () {
    function BillingHistoryComponent(subscribersService, paymentsService, helpers, router, activatedRoute) {
        this.subscribersService = subscribersService;
        this.paymentsService = paymentsService;
        this.helpers = helpers;
        this.router = router;
        this.activatedRoute = activatedRoute;
        // PAGINATION
        this.showNumber = 10;
        this.p = 1;
        this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'
        ];
        this.allBillingEntries = [];
        this.allBillingEntriesFound = [];
        this.limit = 3; // Surf & Pay subscriptions limit
        // views
        this.collapse = true;
        this.collapsePPV = true;
        this.view = true;
        this.viewsPanel = true;
        this.views = {
            // TR: true,
            SP: true,
            BE: false,
            S: false,
            BEG: false,
            SG: false,
            SPG: false,
        };
        this.cAt = true;
        this.showSubsCharts = false;
        this.state = 'small';
    }
    BillingHistoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        // DATE
        this.date = new Date();
        this.year = this.date.getFullYear();
        this.month = this.date.getMonth();
        this.day = this.date.getDate();
        this.years = [this.year - 3, this.year - 2, this.year - 1, this.year];
        this.monthBEG = this.month;
        this.yearBEG = this.year;
        // GET SUBSCRIBER STORED
        if (sessionStorage.getItem('subscriber')) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
            console.log(this.subscriber);
        }
        // GET CUSTOMER SUBSCRIBERS STORED
        if (sessionStorage.getItem('billingSubscribers')) {
            this.customerSubscribers = JSON.parse(sessionStorage.getItem('billingSubscribers'));
            console.log('this.customerSubscribers', this.customerSubscribers);
        }
        // GET QUERY ID
        this.activatedRoute.params
            .map(function (params) { return params['id']; })
            .subscribe(function (id) {
            _this.customerId = id;
        });
        // GET BILLING HISTORY
        this.getBillingHistory();
        // GET TRANSACTIONS
        // this.getTransactions();
        // GET CUSTOMER
        this.getCustomer();
    };
    // GET TRANSACTIONS
    BillingHistoryComponent.prototype.getTransactions = function () {
        var _this = this;
        this.paymentsService.getUrl('get-billing-info?customerId=' + this.customerId + '&breakdown=', 'TRANSACTION')
            .subscribe(function (res) {
            var obj = JSON.parse(res['_body']);
            obj = obj.billingHistory;
            _this.transactions = Array.from(Object.keys(obj), function (k) { return obj[k]; });
            // sort by date
            _this.transactions.sort(function (a, b) {
                a = new Date(a.createdAt);
                b = new Date(b.createdAt);
                return a > b ? -1 : a < b ? 1 : 0;
            });
            // ADD DESCRIPTION AND LENGTH
            for (var _i = 0, _a = _this.transactions; _i < _a.length; _i++) {
                var tr = _a[_i];
                tr.description = [];
                tr.subsLength = 0;
                if (tr.relatedPpvs.length > 0) {
                    console.log(tr.relatedPpvs[0].description);
                    tr.description.push(tr.relatedPpvs[0].description);
                }
                if (tr.relatedSubscriptions.length > 0) {
                    for (var _b = 0, _c = tr.relatedSubscriptions; _b < _c.length; _b++) {
                        var trSub = _c[_b];
                        for (var _d = 0, _e = trSub.subscriptionComponents; _d < _e.length; _d++) {
                            var comp = _e[_d];
                            for (var _f = 0, _g = comp.billingEntries; _f < _g.length; _f++) {
                                var be = _g[_f];
                                for (var _h = 0, _j = be.relatedTransactions; _h < _j.length; _h++) {
                                    var tranRef = _j[_h];
                                    if (tranRef.reference === tr.reference) {
                                        tr.description.push(comp.billingEntries[0].description);
                                        tr.subsLength += 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            _this.transactionsFound = _this.transactions;
            console.log('TRANSACTIONS', _this.transactions);
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // GET BILLING HISTORY
    BillingHistoryComponent.prototype.getBillingHistory = function () {
        var _this = this;
        this.paymentsService.getUrl('get-billing-info?customerId=' + this.customerId + '&breakdown=', 'BILLING_ENTRY')
            .subscribe(function (res) {
            _this.billingInfo = JSON.parse(res['_body']);
            // GET ALL BILLING ENTRIES
            _this.getBillingEntries();
            // GET SUBS TOTAL AMOUNT CHARGED
            _this.totalSubsCharged();
            // GET DATA FOR EACH SUBSCRIBER
            _this.subscriberData();
            // GET PPV TOTAL AMOUNT CHARGED
            _this.totalPPVCharged();
            // CHARTS DATA
            _this.getLineChartData();
            console.log('Billing Info', _this.billingInfo);
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // CREATED AT
    BillingHistoryComponent.prototype.getCreatedAt = function (subscription) {
        var createdAt;
        for (var _i = 0, _a = subscription.subscriptionComponents; _i < _a.length; _i++) {
            var subComp = _a[_i];
            for (var _b = 0, _c = subComp.billingEntries; _b < _c.length; _b++) {
                var be = _c[_b];
                if (createdAt === undefined) {
                    createdAt = be.createdAt;
                }
                if (be.createdAt < createdAt) {
                    createdAt = be.createdAt;
                }
            }
        }
        return createdAt;
    };
    // GET ALL BILLING ENTRIES
    BillingHistoryComponent.prototype.getBillingEntries = function () {
        for (var _i = 0, _a = this.billingInfo.billingHistory.subscriptions; _i < _a.length; _i++) {
            var sub = _a[_i];
            for (var _b = 0, _c = sub.subscriptionComponents; _b < _c.length; _b++) {
                var sC = _c[_b];
                for (var _d = 0, _e = sC.billingEntries; _d < _e.length; _d++) {
                    var be = _e[_d];
                    this.allBillingEntries.push(be);
                    this.allBillingEntriesFound.push(be);
                }
            }
        }
        for (var _f = 0, _g = this.billingInfo.billingHistory.ppvs; _f < _g.length; _f++) {
            var ppv = _g[_f];
            this.allBillingEntries.push(ppv);
            this.allBillingEntriesFound.push(ppv);
        }
        this.allBillingEntries.sort(function (a, b) {
            return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
        });
        this.allBillingEntriesFound.sort(function (a, b) {
            return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
        });
        this.allBillingEntries.reverse();
        this.allBillingEntriesFound.reverse();
    };
    //  GET DATA FOR EACH SUBSCRIBER
    BillingHistoryComponent.prototype.subscriberData = function () {
        for (var _i = 0, _a = this.customerSubscribers; _i < _a.length; _i++) {
            var sub = _a[_i];
            sub.dataSub = [];
            sub.dataPPV = [];
            for (var _b = 0, _c = this.billingInfo.billingHistory.subscriptions; _b < _c.length; _b++) {
                var subscription = _c[_b];
                for (var _d = 0, _e = subscription.subscriptionComponents; _d < _e.length; _d++) {
                    var subscriber = _e[_d];
                    // console.log(sub.id, subscriber.subscriberId)
                    if (sub.id == subscriber.subscriberId) {
                        sub.dataSub.push(subscriber);
                    }
                }
            }
            for (var _f = 0, _g = this.billingInfo.billingHistory.ppvs; _f < _g.length; _f++) {
                var ppv = _g[_f];
                if (sub.id == ppv.subscriberId) {
                    sub.dataPPV.push(ppv);
                }
            }
        }
        console.log('this.customerSubscribers', this.customerSubscribers);
    };
    // REVERSE CREATED AT
    BillingHistoryComponent.prototype.sortCreatedAt = function (arr) {
        arr.reverse();
        this.cAt = !this.cAt;
    };
    // GET LINE CHART DATA
    BillingHistoryComponent.prototype.getLineChartData = function (m) {
        var date;
        var day;
        var month;
        var year;
        var newData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        var newDataYear = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        // newData = this.lineChartDataMonth[0].data.slice();
        // let beY = sortBillingEntriesByYear();
        for (var _i = 0, _a = this.allBillingEntries; _i < _a.length; _i++) {
            var be = _a[_i];
            date = new Date(be.createdAt * 1000);
            month = date.getMonth();
            day = date.getDate();
            year = date.getFullYear();
            if (year == this.yearBEG) {
                newDataYear[month] += be.amountCharged;
            }
            if (month == this.monthBEG && this.year == year) {
                newData[day - 1] += be.amountCharged;
            }
        }
        // HIGH CHARTS YEAR
        this.chartYear = new angular_highcharts_1.Chart({
            chart: {
                type: 'areaspline'
            },
            title: {
                text: 'Billing Entries by Year'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
                    'September', 'October', 'November', 'December']
            },
            series: [{
                    name: 'AMOUNT CHARGED ' + this.subscriber.currencyCode,
                    data: newDataYear
                }]
        });
        // HIGH CHARTS MONTH
        this.chartMonth = new angular_highcharts_1.Chart({
            chart: {
                type: 'areaspline'
            },
            title: {
                text: 'Billing Entries by Month'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                tickInterval: 1,
                minRange: 1,
            },
            series: [{
                    name: 'AMOUNT CHARGED ' + this.subscriber.currencyCode,
                    data: newData
                }]
        });
    };
    // GET CUSTOMER
    BillingHistoryComponent.prototype.getCustomer = function () {
        var _this = this;
        this.subscribersService.getCustomer(this.subscriber.id)
            .subscribe(function (res) {
            _this.customer = res[0];
            console.log('Customer', _this.customer);
        });
    };
    // COLLAPSE SUBSCRIPTION
    BillingHistoryComponent.prototype.collapseSub = function (id) {
        this.state = (this.state === 'small' ? 'large' : 'small');
        for (var _i = 0, _a = this.billingInfo.billingHistory.subscriptions; _i < _a.length; _i++) {
            var sub = _a[_i];
            if (id === sub.subscriptionId) {
                if (sub.show === true) {
                    sub.show = false;
                }
                else {
                    sub.show = true;
                }
            }
            else {
                sub.show = false;
            }
        }
    };
    // COLLAPSE TRANSACTIONS
    BillingHistoryComponent.prototype.collapseTransactions = function (id) {
        console.log(id);
        this.state = (this.state === 'small' ? 'large' : 'small');
        for (var _i = 0, _a = this.transactions; _i < _a.length; _i++) {
            var tr = _a[_i];
            if (id === tr.transactionId) {
                if (tr.show === true) {
                    tr.show = false;
                }
                else {
                    tr.show = true;
                }
            }
            else {
                tr.show = false;
            }
        }
    };
    // COLLAPSE SUBSCRIPTION
    BillingHistoryComponent.prototype.collapseSubcriber = function (id) {
        this.state = (this.state === 'small' ? 'large' : 'small');
        for (var _i = 0, _a = this.customerSubscribers; _i < _a.length; _i++) {
            var sub = _a[_i];
            if (id === sub.viewingCardNumber) {
                if (sub.show === true) {
                    sub.show = false;
                }
                else {
                    sub.show = true;
                }
            }
            else {
                sub.show = false;
            }
        }
    };
    // COLLAPSE TRANSACTIONS
    BillingHistoryComponent.prototype.collapseTrans = function (ent, billingEntries) {
        for (var _i = 0, billingEntries_1 = billingEntries; _i < billingEntries_1.length; _i++) {
            var be = billingEntries_1[_i];
            if (be.id !== ent.id) {
                be.show = false;
            }
        }
        if (ent.show === true) {
            ent.show = false;
        }
        else {
            ent.show = true;
        }
    };
    // MATCH CUSTOMER SUBSCRIBERS
    BillingHistoryComponent.prototype.matchSubscribers = function (id) {
        for (var _i = 0, _a = this.customerSubscribers; _i < _a.length; _i++) {
            var sub = _a[_i];
            if (sub.id === id) {
                return sub.title + ' ' + sub.surname;
            }
        }
    };
    BillingHistoryComponent.prototype.getCardSubscriberId = function (id) {
        for (var _i = 0, _a = this.customerSubscribers; _i < _a.length; _i++) {
            var sub = _a[_i];
            if (sub.id === id) {
                return sub.cardSubscriberId;
            }
        }
    };
    // TOTAL SUBSCRIPTIONS AMOUNT CHARGED
    BillingHistoryComponent.prototype.totalSubsCharged = function () {
        this.totalSubsAmount = 0;
        this.subsCompLength = 0;
        for (var _i = 0, _a = this.billingInfo.billingHistory.subscriptions; _i < _a.length; _i++) {
            var sub = _a[_i];
            for (var _b = 0, _c = sub.subscriptionComponents; _b < _c.length; _b++) {
                var subsComp = _c[_b];
                if (subsComp.status.toLocaleLowerCase() === 'active') {
                    this.subsCompLength += 1;
                }
                for (var _d = 0, _e = subsComp.billingEntries; _d < _e.length; _d++) {
                    var bi = _e[_d];
                    this.totalSubsAmount += bi.amountCharged;
                }
            }
        }
    };
    // TOTAL PPVs AMOUNT CHARGED
    BillingHistoryComponent.prototype.totalPPVCharged = function () {
        this.totalPPVAmount = 0;
        for (var _i = 0, _a = this.billingInfo.billingHistory.ppvs; _i < _a.length; _i++) {
            var ppv = _a[_i];
            this.totalPPVAmount += ppv.amountCharged;
        }
    };
    // CHANGE VIEW
    BillingHistoryComponent.prototype.changeView = function (view) {
        for (var _i = 0, _a = Object.keys(this.views); _i < _a.length; _i++) {
            var key = _a[_i];
            this.views[key] = false;
            if (view == key) {
                this.views[key] = true;
            }
        }
    };
    // BILLING ENTRIES FILTER
    BillingHistoryComponent.prototype.beFilter = function (obj) {
        var found = [];
        var a = false;
        for (var _i = 0, obj_1 = obj; _i < obj_1.length; _i++) {
            var p = obj_1[_i];
            a = p.productGroupTitleDescription.toLowerCase().includes(this.beFil.toLowerCase());
            if (a) {
                found.push(p);
            }
        }
        this.allBillingEntriesFound = found;
    };
    // FILTER
    BillingHistoryComponent.prototype.trFilter = function () {
        this.transactionsFound = [];
        var found;
        for (var _i = 0, _a = this.transactions; _i < _a.length; _i++) {
            var tr = _a[_i];
            found = false;
            for (var key in tr) {
                if (tr[key].toString().toLowerCase().includes(this.trFil.toLowerCase())) {
                    found = true;
                }
            }
            if (found) {
                this.transactionsFound.push(tr);
            }
        }
    };
    BillingHistoryComponent = __decorate([
        core_1.Component({
            selector: 'app-billing-history',
            templateUrl: './billing-history.component.html',
            styleUrls: ['./billing-history.component.css'],
            animations: [
                animations_1.trigger('fadeInOut', [
                    animations_1.transition(':enter', [
                        animations_1.style({ transform: 'translateY(-50px)', opacity: 0 }),
                        animations_1.animate('250ms', animations_1.style({ transform: 'translateY(0)', opacity: 1 }))
                    ]),
                    animations_1.transition(':leave', [
                        animations_1.style({ transform: 'translateY(0)', opacity: 1 }),
                        animations_1.animate('250ms', animations_1.style({ transform: 'translateY(-100%)', opacity: 0 }))
                    ])
                ])
            ],
            providers: [payments_service_1.PaymentsService, helpers_service_1.HelpersService, subscribers_service_1.SubscribersService],
        })
    ], BillingHistoryComponent);
    return BillingHistoryComponent;
}());
exports.BillingHistoryComponent = BillingHistoryComponent;
var PcPipe = /** @class */ (function () {
    function PcPipe() {
    }
    PcPipe.prototype.transform = function (value) {
        // value = value.match(/^([A-Z]{1,2}\d{1,2}[A-Z]?)\s*(\d[A-Z]{2})$/);
        // value.shift();
        // value.join(' ');
        value = value.toUpperCase();
        var exceptions = ['UK', 'ROI', 'IOM', 'CI', 'BFPO'];
        // If Postcode Exception type, return
        exceptions.forEach(function (exp) {
            if (value === exp) {
                return value;
            }
        });
        // If length is < 4, return
        if (value.length < 4) {
            return value;
        }
        var suffix = value.substring(value.length - 3);
        var prefix = value.substring(0, value.length - 3);
        return prefix + ' ' + suffix;
    };
    PcPipe = __decorate([
        core_2.Pipe({ name: 'pc' })
    ], PcPipe);
    return PcPipe;
}());
exports.PcPipe = PcPipe;
