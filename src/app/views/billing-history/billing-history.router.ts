import {Routes, RouterModule} from "@angular/router";
import {BillingHistoryComponent} from "./billing-history.component";

const BILLINGHISTORY_ROUTER: Routes = [
  {
    path: '',
    component: BillingHistoryComponent,
  },
];

export const billingHistoryRouter = RouterModule.forChild(BILLINGHISTORY_ROUTER);
