import {AfterViewInit, Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HelpersService } from '../../services/helpers.service';
import { PostcodeService } from '../../services/postcode.service';
import {PipeTransform, Pipe} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ValidatorsService } from '../../services/validators.service';
import { RestService } from '../../services/rest.service';
import { HttpClient } from '@angular/common/http';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import { Chart } from 'angular-highcharts';
import {Globals} from '../../app.global';

@Component({
  selector: 'app-billing-history',
  templateUrl: './billing-history.component.html',
  styleUrls: ['./billing-history.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({transform: 'translateY(-50px)', opacity: 0}),
        animate('250ms', style({transform: 'translateY(0)', opacity: 1}))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        style({transform: 'translateY(0)', opacity: 1}),
        animate('250ms', style({transform: 'translateY(-100%)', opacity: 0}))
      ])
    ])
  ],
  providers: [HelpersService, PostcodeService, ValidatorsService, RestService],

})
export class BillingHistoryComponent implements OnInit {

  // PAGINATION
  showNumber = 10;
  p = 1;
  date: any;
  year: any;
  month: any;
  day: any;
  years: any;
  monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];

  subscriber: any;
  customer: any;
  customerSubscribers: any;
  customerId: number;
  error: any;
  success: any;
  billingInfo: any;
  show: true;
  allBillingEntries: any[] = [];
  allBillingEntriesFound: any[] = [];
  ppvCount = 0;

  limit = 3; // Surf & Pay subscriptions limit

  // views
  collapse = true;
  collapsePPV = true;
  view = true;
  viewsPanel = true;
  views: any = {
    TR: true,
    SP: false,
    BE: false,
    S: false,
    BEG: false,
    SG: false,
    SPG: false,
  };
  cAt = true;

  be: any; // billing entries
  rt: any; // related transactions
  description: any; // billing entries description
  showSubsCharts = false;

  totalSubsAmount: any;
  subsCompLength: number;
  totalPPVAmount: any;
  beFil: any;
  trFil: any;
  monthBEG: any;
  yearBEG: any;


  refund: any;
  showRefundDialog: any;
  refundForm: FormGroup;
  reasonOptions = ['Billing Error', 'Overpayment', 'Goodwill', 'Customer Care', 'Sky equipment removed'];
  submitted = false;
  requireAdmin: any;
  user: any;
  password: any;
  authorisingUserId: any;

  state = 'small';

  transactions: any;
  transactionsFound: any;

  // HIGH CHARTS
  chartYear: any;
  chartMonth: any;

  constructor(private helpers: HelpersService, private router: Router, private  globals: Globals,
              private activatedRoute: ActivatedRoute, private postcodeService: PostcodeService,  private fb: FormBuilder,
              private val: ValidatorsService, private rest: RestService, private http: HttpClient) { }

  ngOnInit() {

    // DATE
    this.date = new Date();
    this.year = this.date.getFullYear();
    this.month = this.date.getMonth();
    this.day = this.date.getDate();

    this.years = [this.year - 3, this.year - 2, this.year - 1, this.year];
    this.monthBEG = this.month;
    this.yearBEG = this.year;

    // GET SUBSCRIBER STORED
    if (sessionStorage.getItem('subscriber')) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
      console.log(this.subscriber);
    }

    // GET CUSTOMER SUBSCRIBERS STORED
    if (sessionStorage.getItem('billingSubscribers')) {
      this.customerSubscribers = JSON.parse(sessionStorage.getItem('billingSubscribers'));
      for (const sub of this.customerSubscribers) {
        sub.postcode = this.postcodeService.postcodeFormat(sub.postcode);
        sub.surname = this.helpers.toTitleCase(sub.surname);
      }
      console.log('this.customerSubscribers', this.customerSubscribers);
    }

    // GET QUERY ID
    this.activatedRoute.params
      .map(params => params['id'])
      .subscribe((id) => {
        this.customerId = id;
      });

    if (this.customerId && this.subscriber) {
      // GET BILLING HISTORY
      this.getBillingHistory();

      // GET TRANSACTIONS
      this.getTransactions();

      // GET CUSTOMER
      this.getCustomer();
    } else {
      this.error = {
        message: 'Customer not found'
      }
    }

  }

  // GET TRANSACTIONS
  getTransactions() {
    this.rest.get('/payments/get-billing-info?customerId=' + this.customerId + '&breakdown=TRANSACTION' )
      .subscribe(res => {
        const obj = res['billingHistory'];
        this.transactions = Array.from(Object.keys(obj), k => obj[k]);
        // sort by date
        this.transactions.sort(function(a, b) {
          a = new Date(a.createdAt);
          b = new Date(b.createdAt);
          return a > b ? -1 : a < b ? 1 : 0;
        });

        // ADD DESCRIPTION AND LENGTH
        for (const tr of this.transactions){
          tr.description = [];
          tr.subsLength = 0;

          if (tr.relatedPpvs.length > 0) {
            console.log(tr.relatedPpvs[0].description);
            tr.description.push( tr.relatedPpvs[0].description);
          }

          if (tr.relatedSubscriptions.length > 0) {
            for (const trSub of tr.relatedSubscriptions) {
              for (const comp of trSub.subscriptionComponents) {
                for (const be of comp.billingEntries){
                  for (const tranRef of be.relatedTransactions){
                    if (tranRef.reference === tr.reference) {
                      tr.description.push(comp.billingEntries[0].description);
                      tr.subsLength += 1;
                    }
                  }
                }
              }
            }
          }
        }
        this.transactionsFound  = this.transactions;
        console.log('TRANSACTIONS', this.transactions);
      }, err => {
        if (err.status === 500) {
          this.error = {
            'message': '500 error in transactions'
          };
        }else {
          this.error = this.helpers.error(err.error.message);
        }
      });
  }

  /**
   * Authorize to proceed refund
   */
  authorize(e) {
    // this.requireAdmin = false;
    //
    // this.helpers.adminRequired(this.user, this.password)
    //   .subscribe(res => {
    //     // SET TO FALSE AND EMPTY USER/PASS
    //     this.user = '';
    //     this.password = '';
    //
    //     const resp = res;
    //     if (resp['roles'][0].name === 'ROLE_ADMIN' || resp['roles'][0].name === 'ROLE_SUPER_ADMIN'
    //       || resp['roles'][0].name === 'ROLE_SUPERVISOR') {
    //       // this.accessGranted = 'admin';
    //       this.authorisingUserId = resp['id'];
    //       this.paymentRefund();
    //     } else {
    //       this.refund = null;
    //       this.error = {message : 'Access Denied'};
    //     }
    //   }, err => {
    //     console.log(err);
    //     this.refund = null;
    //     this.error = this.helpers.error(err.error.message);
    //     this.user = '';
    //     this.password = '';
    //   })
    if (e === 'admin') {
      this.paymentRefund();
    } else {
      this.refund = null;
      this.error = {message : 'Access Denied'};
    }
  }

  /**
   * Auth require error
   * @param e -> event emitted by auth-required component
   */
  authError(e) {
    this.error = e;
    // SET TO FALSE
    this.refund = null;
  }


  /**
   * Show refund dialog
   * @param e --> $event to stop propagation
   * @param tr --> transaction selected
   */
  refundDialog(e, tr) {
    this.showRefundDialog = true;
    this.submitted = false;
    this.refund = tr;
    e.stopPropagation();

    /**
     * Refund form
     */
    this.refundForm = this.fb.group({
      'refund': ['', [Validators.required, this.val.onlyMoney]],
      'reasonDropdown': ['', [Validators.required ]],
      'reasonTextarea': [''],
      'manualRefund': [false]
    });
  }
  /**
   * Payment refunds
   */
  paymentRefund() {
    if (this.refund.manualRefund === undefined) {
      this.refund.manualRefund = false;
    }
   const data = {
     spTransactionId: this.refund.transactionId,
     amount: this.refundForm.value.refund,
     manualRefund: this.refundForm.value.manualRefund,
     refundCategory: this.refundForm.value.reasonDropdown,
     refundText: this.refundForm.value.reasonTextarea,
     authorisingUserId: this.authorisingUserId
   };
    if (this.refundForm.valid) {
      this.http.put(this.globals.API_ENDPOINT + '/payments/refund-transaction', data, {responseType: 'text'})
        .subscribe(() => {
          this.refund = null;
          this.success = true;
        }, err => {
          err = JSON.parse(err.error);
          if (err.fieldErrors) {
            this.error = err.fieldErrors[0];
          }else {
            this.error = this.helpers.error(err.message);
            this.error.detail = this.helpers.errorDetail(err);
          }
          this.refund = null;
        })
    }
  }


  // GET BILLING HISTORY
  getBillingHistory() {
    this.rest.get('/payments/get-billing-info?customerId=' + this.customerId + '&breakdown=BILLING_ENTRY' )
      .subscribe(res => {
        this.billingInfo = res;
        // GET ALL BILLING ENTRIES
        this.getBillingEntries();
        // GET SUBS TOTAL AMOUNT CHARGED
        this.totalSubsCharged();
        // GET DATA FOR EACH SUBSCRIBER
        this.subscriberData();
        // GET PPV TOTAL AMOUNT CHARGED
        this.totalPPVCharged();
        // GET PPV COUNT
        this.getPpvCount();
        // CHARTS DATA
        this.getLineChartData();
        console.log('Billing Info', this.billingInfo);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });
  }

  // PPV COUNT
  getPpvCount() {
    for (const ppv of this.billingInfo.billingHistory.ppvs) {
      console.log('PPV', ppv);
      if (!ppv.productGroupTitleDescription.includes('Pay Per Night')) {
        this.ppvCount += 1;
      }
    }
  };

  // CREATED AT
  getCreatedAt(subscription) {
    let createdAt;

    for (const subComp of subscription.subscriptionComponents) {
      for (const be of subComp.billingEntries) {
        if (createdAt === undefined) {
          createdAt = be.createdAt;
        }
        if ( be.createdAt < createdAt) {
          createdAt = be.createdAt;
        }
      }
    }
    return createdAt;
  }

  // GET ALL BILLING ENTRIES
  getBillingEntries() {
    for (const sub of this.billingInfo.billingHistory.subscriptions){
      for (const sC of sub.subscriptionComponents){
        for (const be of sC.billingEntries){
          this.allBillingEntries.push(be);
          this.allBillingEntriesFound.push(be);
        }
      }
    }
    for (const ppv of this.billingInfo.billingHistory.ppvs ){
      this.allBillingEntries.push(ppv);
      this.allBillingEntriesFound.push(ppv);
    }
    this.allBillingEntries.sort(function(a, b) {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
    });
    this.allBillingEntriesFound.sort(function(a, b) {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
    });

    this.allBillingEntries.reverse();
    this.allBillingEntriesFound.reverse();
  }

  //  GET DATA FOR EACH SUBSCRIBER
  subscriberData() {
    for (const sub of this.customerSubscribers){
      sub.dataSub = [];
      sub.dataPPV = [];
      for (const subscription of this.billingInfo.billingHistory.subscriptions){
        for (const subscriber of subscription.subscriptionComponents){
          // console.log(sub.id, subscriber.subscriberId)
          if (sub.id === subscriber.subscriberId) {
            sub.dataSub.push(subscriber);
          }
        }
      }
      for (const ppv of this.billingInfo.billingHistory.ppvs){
        if (sub.id === ppv.subscriberId) {
          sub.dataPPV.push(ppv);
        }
      }
    }
    console.log('this.customerSubscribers', this.customerSubscribers)
  }

  // REVERSE CREATED AT
  sortCreatedAt(arr) {
    arr.reverse();
    this.cAt = !this.cAt;
  }


  // GET LINE CHART DATA
  getLineChartData(m?: number) {
    let date;
    let day;
    let month;
    let year;
    const newData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    const newDataYear = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    // newData = this.lineChartDataMonth[0].data.slice();
    // let beY = sortBillingEntriesByYear();
    for (const be of this.allBillingEntries){
      date = new Date(be.createdAt * 1000);

      month = date.getMonth();
      day = date.getDate();
      year = date.getFullYear();

      if (year === this.yearBEG) {
        newDataYear[month] += be.amountCharged;
      }
      if (month === this.monthBEG && this.year === year) {
        newData[day - 1] += be.amountCharged;
      }
    }

    // HIGH CHARTS YEAR
    this.chartYear = new Chart({
      chart: {
        type: 'areaspline'
      },
      title: {
        text: 'Billing Entries by Year'
      },
      credits: {
        enabled: false
      },
      colors: [
        'lightslategrey'
      ],
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug',
          'Sept', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        title: {
          text: 'AMOUNT CHARGED'
        }
      },
      series: [{
        name: 'AMOUNT CHARGED ' + this.subscriber.currencyCode,
        data: newDataYear
      }]
    });

    // HIGH CHARTS MONTH

    this.chartMonth = new Chart({
      chart: {
        type: 'areaspline'
      },
      title: {
        text: 'Billing Entries by Month'
      },
      credits: {
        enabled: false
      },
      colors: [
        'lightslategrey'
      ],
      xAxis: {
        tickInterval: 1,
        minRange: 1,
      },
      yAxis: {
        title: {
          text: 'AMOUNT CHARGED'
        }
      },
      series: [{
        name: 'AMOUNT CHARGED ' + this.subscriber.currencyCode,
        data: newData
      }]
    });
  }

  // GET CUSTOMER
  getCustomer() {
    this.rest.get('/customers?q=subscriberId=' + this.subscriber.id)
      .subscribe( res => {
        this.customer = res[0];
        console.log('Customer', this.customer);
      })
  }

  // COLLAPSE SUBSCRIPTION
  collapseSub(id) {
    this.state = (this.state === 'small' ? 'large' : 'small');

    for (const sub of this.billingInfo.billingHistory.subscriptions){
      if (id === sub.subscriptionId) {
        if (sub.show === true) {
          sub.show = false;
        }else {
          sub.show = true;
        }
      }else {
        sub.show = false;
      }
    }
  }

  // COLLAPSE TRANSACTIONS
  collapseTransactions(id) {
    console.log(id);

    this.state = (this.state === 'small' ? 'large' : 'small');

    for (const tr of this.transactions){
      if (id === tr.transactionId) {
        if (tr.show === true) {
          tr.show = false;
        }else {
          tr.show = true;
        }
      }else {
        tr.show = false;
      }
    }
  }

  // COLLAPSE SUBSCRIPTION
  collapseSubcriber(id) {
    this.state = (this.state === 'small' ? 'large' : 'small');

    for (const sub of this.customerSubscribers){
      if (id === sub.viewingCardNumber) {
        if (sub.show === true) {
          sub.show = false;
        }else {
          sub.show = true;
        }
      }else {
        sub.show = false;
      }
    }
  }

  // COLLAPSE TRANSACTIONS
  collapseTrans(ent, billingEntries) {
    for (const be of billingEntries) {
      if (be.id !== ent.id) {
        be.show = false;
      }
    }
    if (ent.show === true) {
      ent.show = false
    }else {
      ent.show = true;
    }
  }

  // MATCH CUSTOMER SUBSCRIBERS
  matchSubscribers(id) {
    for (const sub of this.customerSubscribers){
      if (sub.id === id) {
        return sub.title + ' ' + sub.surname ;
      }
    }

  }

  /**
   * Get card subscriber id
   * @param id --> subscriber id
   */
  getCardSubscriberId(id) {
    for (const sub of this.customerSubscribers){
      if (sub.id === id) {
        return sub.cardSubscriberId ;
      }
    }
  }

  // TOTAL SUBSCRIPTIONS AMOUNT CHARGED
  totalSubsCharged() {
    this.totalSubsAmount = 0;
    this.subsCompLength = 0;
    for (const sub of this.billingInfo.billingHistory.subscriptions){
      for (const subsComp of sub.subscriptionComponents){
        if (subsComp.status.toLocaleLowerCase() === 'active') {
          this.subsCompLength += 1;
        }
        for (const bi of subsComp.billingEntries){
          this.totalSubsAmount += bi.amountCharged
        }
      }
    }
  }

  // TOTAL PPVs AMOUNT CHARGED
  totalPPVCharged() {
    this.totalPPVAmount = 0;
    for (const ppv of this.billingInfo.billingHistory.ppvs){
      this.totalPPVAmount += ppv.amountCharged;
    }
  }

  // CHANGE VIEW
  changeView(view) {
    for (const key of Object.keys(this.views)) {
      this.views[key] = false;
      if (view === key) {
        this.views[key] = true;
      }
    }
  }

  // BILLING ENTRIES FILTER
  beFilter(obj) {
    const found = [];
    let a = false;
    for (const p of obj) {
      a = p.productGroupTitleDescription.toLowerCase().includes(this.beFil.toLowerCase());
      if (a) {
        found.push(p);
      }
    }
    this.allBillingEntriesFound = found;
  }
  // FILTER
  trFilter() {
    this.transactionsFound = this.helpers.jsonFilter(this.transactions, this.trFil);
  }

  log() {
    console.log(this.refundForm.value)
  }
}


@Pipe({ name: 'pc' })
export class PcPipe implements PipeTransform {
  transform(value: any) {
    // value = value.match(/^([A-Z]{1,2}\d{1,2}[A-Z]?)\s*(\d[A-Z]{2})$/);
    // value.shift();
    // value.join(' ');

    value = value.toUpperCase();
    const exceptions = [ 'UK', 'ROI', 'IOM', 'CI', 'BFPO' ];

    // If Postcode Exception type, return
    exceptions.forEach(exp =>  {
      if (value ===  exp)  {
        return value;
      }
    });

    // If length is < 4, return
    if (value.length < 4) {
      return value;
    }

    const suffix = value.substring(value.length - 3);
    const prefix = value.substring(0, value.length - 3);
    return prefix + ' ' + suffix;
  }

}
