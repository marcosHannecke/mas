import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {billingHistoryRouter} from "./billing-history.router";
import {BillingHistoryComponent, PcPipe} from "./billing-history.component";
import { ChartModule } from 'angular-highcharts';
import {NgxPaginationModule} from 'ngx-pagination';
import {MaterialModule} from "../../app.module";
import {MatInputModule} from "@angular/material";
import {AuthRequiredModule} from '../../components/common/auth-required/auth-required.module';
// import {PostcodePipe} from '../../highlightReverse.pipe';


@NgModule({
  declarations: [BillingHistoryComponent, PcPipe],
  imports: [CommonModule, FormsModule, ReactiveFormsModule,
    RouterModule, ChartModule, billingHistoryRouter, NgxPaginationModule,
    MaterialModule, AuthRequiredModule],
})

export class BillingHistoryViewModule {}
