"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var billing_history_router_1 = require("./billing-history.router");
var billing_history_component_1 = require("./billing-history.component");
var angular_highcharts_1 = require("angular-highcharts");
var ngx_pagination_1 = require("ngx-pagination");
var app_module_1 = require("../../app.module");
// import {PostcodePipe} from '../../highlightReverse.pipe';
var BillingHistoryViewModule = /** @class */ (function () {
    function BillingHistoryViewModule() {
    }
    BillingHistoryViewModule = __decorate([
        core_1.NgModule({
            declarations: [billing_history_component_1.BillingHistoryComponent, billing_history_component_1.PcPipe],
            imports: [common_1.CommonModule, forms_1.FormsModule, forms_1.ReactiveFormsModule,
                router_1.RouterModule, angular_highcharts_1.ChartModule, billing_history_router_1.billingHistoryRouter, ngx_pagination_1.NgxPaginationModule,
                app_module_1.MaterialModule],
        })
    ], BillingHistoryViewModule);
    return BillingHistoryViewModule;
}());
exports.BillingHistoryViewModule = BillingHistoryViewModule;
