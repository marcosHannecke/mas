import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewJoinPaymentComponent } from './new-join-payment.component';

describe('NewJoinPaymentComponent', () => {
  let component: NewJoinPaymentComponent;
  let fixture: ComponentFixture<NewJoinPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewJoinPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewJoinPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
