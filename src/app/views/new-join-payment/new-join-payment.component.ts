import { Component, OnInit } from '@angular/core';
import {HelpersService} from '../../services/helpers.service';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {RestService} from '../../services/rest.service';


@Component({
  selector: 'app-new-join-payment',
  templateUrl: './new-join-payment.component.html',
  styleUrls: ['./new-join-payment.component.css'],
  providers: [HelpersService, RestService]
})
export class NewJoinPaymentComponent implements OnInit {
  hasPaymentCard: any;
  cardTokens = {
    token: '',
    tokenId: '',
    expire: ''
  };

  cardError: any;

  constructor(private route: Router, private router: ActivatedRoute,
              private helpers: HelpersService, private rest: RestService) {}

  ngOnInit() {
    // Scroll top
    this.helpers.scrollTop();

    this.router.queryParams
      .subscribe(res => {
        this.hasPaymentCard = res['hasPaymentCard'];
        console.log(this.hasPaymentCard);
      });

    // GET CARD TOKENS
    this.router.queryParams
      .subscribe(params => {
        this.cardTokens.token = params['token'];
        this.cardTokens.tokenId = params['tokenId'];
        this.cardTokens.expire = params['exp'];
      });


    if ((this.cardTokens.token == undefined || this.cardTokens.tokenId == undefined
      || this.cardTokens.token == '' || this.cardTokens.tokenId == '') && this.hasPaymentCard != 1) {
      // this.cardError = {
      //   message : 'Sorry we could not collect your payment details, ',
      // }
      sessionStorage.setItem('saved', 'saved');
    }else {
      sessionStorage.setItem('cardTokens', JSON.stringify(this.cardTokens));
      sessionStorage.setItem('saved', 'saved');
    }
  }


  goBack () {
    // this.route.navigate(['/joinSubscriber']);
  }
}
