import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { PostcodeService } from '../../services/postcode.service';
import { HelpersService} from '../../services/helpers.service';
import { RestService } from '../../services/rest.service';

@Component({
  selector: 'app-recent-accounts',
  templateUrl: './recent-accounts.component.html',
  styleUrls: ['./recent-accounts.component.css'],
  providers: [PostcodeService, HelpersService, RestService]
})
export class RecentAccountsComponent implements OnInit, OnDestroy {
  // PAGINATION
  showNumber = 10;
  p = 1;

  subscribers: any;
  subscribersFound: any;
  filter: any;
  getAll: any;
  error: any;

  constructor(public router: Router, private postcodeService: PostcodeService, private helpers: HelpersService,
              private rest: RestService) {}

  ngOnInit() {
    this.getSubscribers();
  };
  ngOnDestroy() {
    this.getAll.unsubscribe();
  }

  /**
   * Get x number of subscribers
   */
  getSubscribers() {
    this.getAll = this.rest.get('/subscribers?size=500&sortBy=createdAt&sortOrder=desc&page=0')
      .subscribe(res => {
        this.subscribers = res;
        this.subscribersFound = res;
        for (const r of this.subscribersFound){
          r.postcode = this.postcodeService.postcodeFormat(r.postcode);
          r.surname = this.helpers.toTitleCase(r.surname);
          r.initials = r.initials.toUpperCase();
        }
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Edit subscriber
   * @param subs --> subscriber selected to edit
   */
  editSubscriber(subs) {
    sessionStorage.setItem('subscriber', JSON.stringify(subs));
    this.router.navigate(['/process8']);
  }

  /**
   * Filter
   */
  subFilter() {
    this.subscribersFound = this.helpers.jsonFilter(this.subscribers, this.filter);
  }
}

