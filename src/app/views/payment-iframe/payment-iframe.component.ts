import {Component, OnInit, PipeTransform, Pipe, OnDestroy} from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import {Observable} from '../../../../node_modules/rxjs';
import {HelpersService} from '../../services/helpers.service';

import * as $ from 'jquery';
import {Location} from '@angular/common';
import {Router} from '@angular/router';

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

@Component({
  selector: 'app-payment-iframe',
  templateUrl: './payment-iframe.component.html',
  styleUrls: ['./payment-iframe.component.css'],
  providers: [HelpersService]
})
export class PaymentIframeComponent implements OnInit, OnDestroy {

  url: any;
  source: any;

  urlSafe: SafeResourceUrl;
  checkPayment: any;
  subscriber: any;
  paymentPath: any;

  constructor(private sanitizer: DomSanitizer, private router: Router, private location: Location, private helpers: HelpersService) {}

  ngOnInit() {
    // Scroll top
    this.helpers.scrollTop();

    sessionStorage.removeItem('cardTokens');
    this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    this.paymentPath = sessionStorage.getItem('paymentPath');

    let saved;
    this.checkPayment =  Observable.interval(200 )
      .subscribe(() => {
        saved = sessionStorage.getItem('saved');
        console.log(saved);
        if (saved == 'saved') {
          sessionStorage.setItem('saved', '');
          if (this.paymentPath === 'join') {
            this.router.navigate(['/joinSubscriberEntitlements'])
          } else if (this.paymentPath === 'create') {
            this.router.navigate(['/newsubscriberEntitlements'])
          } else {
            this.router.navigate(['/customer/' + this.subscriber.customerId]);
          }
        }

      });
    this.url = sessionStorage.getItem('payment-url');
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);

  }

  ngOnDestroy() {
    this.checkPayment.unsubscribe();
  }
  log () {
    // this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl('http://stage.vca.mediaaccessservices.net/#/customer/184006?exp=02%2F23&token=6c8e5d29-dfaa-41a7-a9dd-d13ec3325fcf&tokenId=bc990a92-311a-480e-ae08-f2f88213340f?exp=02%2F20&token=67ad72ad-0acf-4aa6-85b2-f3420c73b526&tokenId=bc990a92-311a-480e-ae08-f2f88213340f?exp=02%2F20&token=68abefb5-625e-4d38-bc9b-5feb93ceee3f&tokenId=bc990a92-311a-480e-ae08-f2f88213340f');
    console.log($('#myIframe'));
    console.log(document.getElementById('myIframe'));
    console.log(this.urlSafe);
  }

}

