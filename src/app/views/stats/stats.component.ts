import {
  AfterViewInit,
  Component, OnDestroy,
  OnInit,
  QueryList, ViewChild,
  ViewChildren,
} from '@angular/core';
import {StatsTableComponent} from './stats-table/stats-table.component';
import { Chart } from 'angular-highcharts';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HelpersService} from "../../services/helpers.service";
import {Globals} from "../../app.global";
import {TableExport} from 'tableexport';
import { DatepickerOptions } from 'ng2-datepicker';
import {RestService} from "../../services/rest.service";
import {Observable} from 'rxjs/Rx';
import {DashboardService} from "../../services/dashboard.service";
import {DatePickerComponent, DatePickerDirective} from 'ng2-date-picker';
import {HttpClient} from '@angular/common/http';
import {ValidatorsService} from "../../services/validators.service";

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css'],
  providers: [ HelpersService, DashboardService, ValidatorsService]
})
export class StatsComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChildren(StatsTableComponent)
  tabs: QueryList<StatsTableComponent>;

  @ViewChild('dateDirectivePicker') datePickerDirective: DatePickerDirective;
  @ViewChild('dateDirectivePickerChurn') dateDirectivePickerChurn: DatePickerDirective;
  @ViewChild('dateDirectivePickerSRA') dateDirectivePickerSRA: DatePickerDirective;
  @ViewChild('dateDirectivePickerMR') dateDirectivePickerMR: DatePickerDirective;
  // PAGINATION
  showNumber = 20;
  p = 1;

  getOverviewButton = true;

  date = + Date.now();
  dayDimmed: any;

  PPNDepthTabs = false;
  SubscriptionDepthTabs = true;
  tables = {
    detailedSubscription: false
  };

  // Admin required
  requireAdmin: any;
  user: any;
  password: any;
  authorisingUserId: any;

  // Churn target
  targetDialog: any;
  target = '0';
  targetForm: FormGroup;
  submitted = false;

  // HIGH CHARTS YEAR
  chart: any;

  error: any;
  subscriptions: any;
  subscriptionsDates: any;
  subscriptionsTitles: any;
  noSubscription: any;
  noSubscriptionTitles: any;

  registrations: any;
  // totalSubscriptions = 0;
  totalPPN = 0;
  detailedSubscriptions: any;
  PPNreports: any;
  PCM: any; // PPN customer movements
  IPPVOPPVviews: any = [];
  IPPVOPPVbuys: any = [];
  dSelected = {
    all: true,
    OTT: false,
    'RH Monthly': false,
    'TVX Excellence': false,
    'TVX Premier': false,
    XP: false
  };

  table: any;
  pricepoint = [];
  totalByPricePoint: any = [];
  pricepointTotal: any = [];
  fileName = 'Subscriptions';
  options: DatepickerOptions;
  options2: DatepickerOptions;

  // KPIs
  trends: Trends = {
    date: '',
    subscriptions_GBR: '0',
    subscriptions_IRL: '0',
    customers_GBR: '0',
    customers_IRL: '0',
    registered_ppn: '0',
    active_ppn: '0',
  };
  mr: any;
  monthlyRevenue: any; // monthly revenue for graph
  monthlyChurn: any;
//  monthlyChurnGraph: any;

  monthlyChurnGraph = {
    total: [],
    system: [],
    voluntary: []
  };

  monthlyArpu: any;
  annualArpu: any;
  monthlyArpuGraph: any; // monthly arpu for graph

  businessVolume = {
    monthly: [],
    daily: []
  };

  // Observable for interval
  observable: any;

  // GRAPH
  graph = 'Business Volume';
  switchARPU: any  = 1;
  switchARPUIRL: any  = 1;
  graphOpt = {
    monetaryValue: 'All',
    period: 'daily',
    option: 'Net Churn',
    time: 7
  };

  monthPickerConfig: any;
  churnPickerConfig: any;
  dayPickerConfig: any;
  churnDate: any = null;
  ARPUDate: any = null;
  SRADate: any = + Date.now();
  SRADateOpen = false;
  MRDate: any = null;

  subscription;

  constructor(private fb: FormBuilder, public helpers: HelpersService, public globals: Globals,
              private rest: RestService , public dashboard: DashboardService, private http: HttpClient,
              private validators: ValidatorsService) {
              // close SRA calendar when click outside
              document.addEventListener('click', () => this.SRADateOpen = false);
        }

  ngOnInit() {
    // TARGET FORM
    this.targetForm = this.fb.group({
      'target': [this.target, [ Validators.required, Validators.maxLength(9), this.validators.onlyMoney, this.validators.ZeroHundred]]
    });

    // Datepicker options max date
    const date = new Date();
    date.setDate(date.getDate() - 1);
    this.options = {
      maxDate: date,
      displayFormat: 'DD-MMM-YYYY ',
    };
    const date2 = new Date();
    date2.setDate(date2.getDate() - 2);
    this.options2 = {
      maxDate: date2
    };

    // Monthpicker options
    this.monthPickerConfig = {
      format: 'MM-YYYY',
      max: (date.getMonth() - 1) + '-' + date.getFullYear()
    };
    this.churnPickerConfig = {
      format: 'MM-YYYY',
      max: (date.getMonth()) + '-' + date.getFullYear()
    };

    // date yesterday
    // this.date = this.date - (this.globals.ONE_DAY_SECONDS * 1000);
    this.date = this.globals.yesterdayMilliseconds;

    // getChurnTarget
    this.getTarget();

    // dashboard KPIs requests
    this.dashboard.getMonthlyRevenue();
    this.dashboard.getMonthlyChurnDefault(Date.now());
    this.dashboard.getAnnualARPUDefault();
    // this.dashboard.getSRADefault(this.globals.yesterday, null ); // Subscriptions, Registered PPN, Active PPN
    this.dashboard.getSRA(this.globals.firstDayYesterdayMonth, null ); // Subscriptions, Registered PPN, Active PPN
    console.log('firstDayCurrentMonth', this.globals.firstDayCurrentMonth);
    this.dashboard.getMonthlyARPUDefault(+ this.globals.lastDayPreviousOfMonth);
    this.dashboard.getBusinessVolume();
    this.dashboard.getMRDefault(+ this.globals.lastDayPreviousOfMonth);

    // dashboard KPIs
    this.dashboardKPIs();

    // stats tables
    this.getSubscriptions();
    this.getOverallNoSubscriptions();
    this.getDetailed();
    this.getPPNreports();
    this.getIPPVOPPVviews();
    this.getIPPVOPPVbuys();
    this.getDimmed();
    this.getRegistrations();
  }

  // After View Init
  ngAfterViewInit() {
    console.log('TABS', this.tabs);
    if (this.date) {
      this.tabs.forEach(t => t.active = false);
      const selectedTab = this.tabs.find(tab => tab.selected);
      if (!selectedTab) {
        this.tabs.first.selected = true;
        this.tabs.first.active = true;
      }
    }
  }

  // On destroy
  ngOnDestroy() {
    sessionStorage.removeItem('detailed');
    this.observable.unsubscribe();
  }

  /**
   * Dashboard KPIs
   */
  dashboardKPIs(type ?) {
    let trends: any = false;
    let monthlyRevenue: any = false;
    let monthlyChurn: any = false;
    let monthlyArpu: any = false;
    let monthlyArpuGraph: any = false;
    let monthlyChurnGraph: any = false;
    let AnnualArpu: any = false;
    let businessVolume: any = false;
    if (type) {
      businessVolume = true; // prevents refreshing the graph
    }
    let mr: any = false;

    // Interval
    this.observable = Observable.interval(200)
      .take(1000) // max number of times
      .takeWhile(() =>  !trends || !monthlyRevenue || !monthlyArpu || !AnnualArpu || !monthlyArpuGraph || !monthlyChurnGraph || !businessVolume  || !monthlyChurn || !mr) // take while
      .finally(() => {
        if (!trends || !monthlyRevenue || !monthlyArpu || !AnnualArpu || !monthlyArpuGraph || !monthlyChurnGraph || !businessVolume  || !monthlyChurn || !mr) {
          this.error = {
             message: 'Please reload stats page'
          }
        }
      })
      .subscribe(() => {
        console.log('interval');
        // Trends -> Subscriptions, Registered PPN, Active PPN
        if (sessionStorage.getItem('dashboardTrends') && !trends) {
          trends = JSON.parse(sessionStorage.getItem('dashboardTrends')).data;
          Object.keys(trends)
            .map(key => {
              this.trends = {
                date: key,
                subscriptions_GBR: trends[key].subscriptions_GBR,
                subscriptions_IRL: trends[key].subscriptions_IRL,
                customers_GBR: trends[key].active_customers_GBR,
                customers_IRL: trends[key].active_customers_IRL,
                registered_ppn: trends[key].registered_ppn,
                active_ppn: trends[key].active_ppn,
              };
              return key;
            });
        }
        // Monthly Revenue Graph
        if (sessionStorage.getItem('dashboardMonthlyRevenue') && !monthlyRevenue) {
          monthlyRevenue = JSON.parse(sessionStorage.getItem('dashboardMonthlyRevenue')).data['Total Revenue'];
          this.monthlyRevenue = Object.keys(monthlyRevenue)
            .map(key => {
              return {
                date: key,
                GBR:  monthlyRevenue[key]['GBR'],
                IRL:  monthlyRevenue[key]['IRL']
              };
            });
            // this.monthlyRevenue.sort();
        }
        // Monthly Churn
        if (sessionStorage.getItem('dashboardMonthlyChurn') && !monthlyChurn) {
          monthlyChurn = JSON.parse(sessionStorage.getItem('dashboardMonthlyChurn')).data;
          console.log('monthlyChurn', monthlyChurn);
          Object.keys(monthlyChurn)
            .forEach(key => {
              this.monthlyChurn = monthlyChurn[key];
            });
        }

        // Monthly Chrun Graph

        if (sessionStorage.getItem('dashboardMonthlyChurnGraph') && !monthlyChurnGraph ) {
          monthlyChurnGraph = JSON.parse(sessionStorage.getItem('dashboardMonthlyChurnGraph')).data;
          this.monthlyChurnGraph.total = Object.keys(monthlyChurnGraph['Total'])
            .map(key => {
              return {
                date: (new Date(Number(key) * 1000)),
                value:  monthlyChurnGraph['Total'][key]
              };
            });
          this.monthlyChurnGraph.system = Object.keys(monthlyChurnGraph['TotalSystem'])
            .map(key => {
              return {
                date: (new Date(Number(key) * 1000)),
                value:  monthlyChurnGraph['TotalSystem'][key]
              };
            });
          this.monthlyChurnGraph.voluntary = Object.keys(monthlyChurnGraph['TotalVoluntary'])
            .map(key => {
              return {
                date: (new Date(Number(key) * 1000)),
                value:  monthlyChurnGraph['TotalVoluntary'][key]
              };
            });
          console.log('this.monthlyChurnGraph', this.monthlyChurnGraph);
        }

        // Monthly ARPU
        if (sessionStorage.getItem('dashboardMonthlyArpu') && !monthlyArpu) {
          monthlyArpu = JSON.parse(sessionStorage.getItem('dashboardMonthlyArpu')).data;
          Object.keys(monthlyArpu)
            .forEach(key => {
              this.monthlyArpu = monthlyArpu[key];
              console.log('monthlyArpu', this.monthlyArpu)
            });
        }

        // Monthly ARPU Graph
        if (sessionStorage.getItem('dashboardARPUGraph') && !monthlyArpuGraph ) {
          monthlyArpuGraph = JSON.parse(sessionStorage.getItem('dashboardARPUGraph')).data;
          this.monthlyArpuGraph = Object.keys(monthlyArpuGraph)
            .map(key => {
              return {
                date: key,
                GBR:  monthlyArpuGraph[key]['GBR'],
                IRL:  monthlyArpuGraph[key]['IRL']
              };
            });
        }

        // Monthly Revenue (not graph)
        if (sessionStorage.getItem('dashboardMonthlyR') && !mr) {
          mr = JSON.parse(sessionStorage.getItem('dashboardMonthlyR')).data;
          Object.keys(mr)
            .forEach(key => {
              this.mr = mr[key];
            });
          console.log('this.mr', this.mr);
        }
        // Annual ARPU
        if (sessionStorage.getItem('dashboardAnnualArpu') && !AnnualArpu) {
          AnnualArpu = JSON.parse(sessionStorage.getItem('dashboardAnnualArpu')).data;
          Object.keys(AnnualArpu)
            .forEach(key => {
              this.annualArpu = AnnualArpu[key];
              console.log('AnnualArpu', this.annualArpu)
            });
        }
        // Business volume
        if (sessionStorage.getItem('dashboardBusinessVolume') && !businessVolume ) {
          businessVolume = JSON.parse(sessionStorage.getItem('dashboardBusinessVolume')).data;

          Object.keys(businessVolume['Business_volume_daily'])
            .forEach(key => {
              businessVolume['Business_volume_daily'][key].date  = key;
              this.businessVolume.daily.push(businessVolume['Business_volume_daily'][key]);
            });
          Object.keys(businessVolume['Business_volume_monthly'])
            .forEach(key => {
              businessVolume['Business_volume_monthly'][key].date = (new Date(Number(key) * 1000));
              this.businessVolume.monthly.push(businessVolume['Business_volume_monthly'][key]);
            });
          console.log('businessVolume', this.businessVolume);
          // Get charts
          this.getChart();
        }
      });
  }

  /**
   * Get overall subscriptions
   */
  getSubscriptions() {
    console.log('this.date', this.date);


    this.subscriptions = false;
    this.rest.getNoSimplified('/report/overall-subscription?timestamp=' + Math.trunc(+ this.date / 1000) +
      '&breakdown=day&breakdown=operation&breakdown=subscription')
      .subscribe(res => {
        this.subscriptions = res['data'];
        this.subscriptionsDates = res['data'];
        this.subscriptionsDates = Object.keys(this.subscriptions)
          .map(key => {
            return key;
          });
        this.subscriptions = Object.keys(this.subscriptions)
          .map(key => {
            console.log('kkkkkk', key);
            return this.subscriptions[key];
          });

        // subscription titles
        this.subscriptionsTitles = Object.keys(this.subscriptions[0][Object.keys(this.subscriptions[0]).map(key => key)[0]])
          .map(key => {
            const data = {
              title: key,
              order: ''
            };
            return data;
          });


        this.getSubscriptionsOrder();
        console.log('this.subscriptionsTitles', this.subscriptionsTitles);


        if (this.table != null) {
          this.table.remove();
        }
        this.table = null;
        console.log('subscriptions', this.subscriptions);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Get overall no subscriptions
   */
  getOverallNoSubscriptions() {
    this.rest.getNoSimplified('/report/overall?timestamp=' + Math.trunc(+ this.date / 1000) +
      '&breakdown=day&breakdown=operation&breakdown=subscription')
      .subscribe(res => {
        // this.noSubscriptionTitles = Object.keys(res['data'][Object.keys(res['data']).map(k => k)[0]]);
        Object.keys(res['data']).map(d => {
          Object.keys(res['data'][d]).map(t => {
            this.noSubscriptionTitles = Object.keys(res['data'][d]['addition']).map(title => title);
          } )
        });
        this.noSubscription = Object.keys(res['data']).map(d => {
         const data = {
           title: d,
           rows: Object.keys(res['data'][d]).map(k => k),
           content: res['data'][d]
         };
         return data;
        });

        console.log('noSubscriptionTtitles', this.noSubscriptionTitles, res);


      }, err => {
        this.error = this.helpers.error(err.error.message);
      })

  }

  /**
   *  Get Detailed Subscriptions
   */
  getDetailed() {
    this.getDimmed();
    this.detailedSubscriptions = false;

    this.rest.getNoSimplified('/report/detailed?timestamp=' + Math.trunc(+ this.date / 1000) +
      '&breakdown=category&breakdown=day&breakdown=operation&breakdown=subscription')
      .subscribe(res => {
        this.detailedSubscriptions = {};
        const detailed = res['data'];
        sessionStorage.setItem('detailed', JSON.stringify(detailed));
        this.getDetailSubscriptionType('all');
        console.log('DETAILED', this.detailedSubscriptions);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });
  }

  /**
   * Get PPN reports
   */
  getPPNreports() {
    this.PPNreports = false;
    this.rest.getNoSimplified('/report/ppn?timestamp=' + Math.trunc(+ this.date / 1000)
      + '&breakdown=category&breakdown=day&breakdown=operation')
      .subscribe(res => {
        const reports = res['data'];
        Object.keys(reports)
          .map(k => {
            reports[k] = Object.keys(reports[k])
              .map(key => {
                return reports[k][key];
              });
          });
        this.PPNreports = reports;
        this.PPNCustomerMovement(this.PPNreports);
        console.log(this.PPNreports );
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   *  Calculate PPN customer movement for PPN reports
   * @param reports --> PPN reports formatted
   * @constructor
   */
  PPNCustomerMovement(reports) {
    this.PCM = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    for (let i = 0 ; i < 9; i++) {
        Object.keys(reports['New business by source'][i])
        .forEach(k => {
          this.PCM[i] += Number(reports['New business by source'][i][k])
        });
      Object.keys(reports['Churn'][i])
        .forEach(k => {
          this.PCM[i] += Number(reports['Churn'][i][k])
        })
    }
  }

  /**
   * Get IPPV OPPV views
   */
  getIPPVOPPVviews() {
    this.IPPVOPPVviews = [];
    this.rest.getNoSimplified('/report/ippv-oppv-views?timestamp=' + Math.trunc(+ this.date / 1000)
      + '&breakdown=day&breakdown=product_type')
      .subscribe(res => {
        this.IPPVOPPVviews = res['data'];
        // Add dates to json
        this.IPPVOPPVviews = Object.keys( this.IPPVOPPVviews)
            .map(k => {
              this.IPPVOPPVviews[k].date = k;
              return  this.IPPVOPPVviews[k];
            });
        this.IPPVOPPVviews.reverse();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }
  /**
   * Get IPPV OPPV buys
   */
  getIPPVOPPVbuys() {
    this.IPPVOPPVbuys = [];
    this.rest.getNoSimplified('/report/ippv-oppv-buys?timestamp=' + Math.trunc(+ this.date / 1000)
      + '&breakdown=day&breakdown=product_type')
      .subscribe(res => {
        this.IPPVOPPVbuys = res['data'];
        // Add dates to json
        this.IPPVOPPVbuys = Object.keys(this.IPPVOPPVbuys)
            .map(k => {
              this.IPPVOPPVbuys[k].date = k;
              return this.IPPVOPPVbuys[k];
            });
        this.IPPVOPPVbuys.reverse();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }
  /**
   * Get registrations
   */
  getRegistrations() {
    this.registrations = false;
    this.rest.getNoSimplified('/report/registrations?timestamp=' + Math.trunc(+ this.date / 1000)
      + '&breakdown=day&breakdown=registration_type')
      .subscribe(res => {
        this.registrations = res['data'];
        // Add dates and total to json
        this.registrations = Object.keys(this.registrations)
          .map(k => {
            this.registrations[k].date = k;
            this.registrations[k].skyTotal = Number(this.registrations[k]['Sky Pack Requests'])
              + Number(this.registrations[k]['Sky Joins']);
            return this.registrations[k];
          });
        this.registrations.reverse();
      }, err => {
        console.log(err);
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Generate CSV
   */
  generateCSV() {
    if (this.table == null) {
     this.table = new TableExport(document.getElementsByClassName('table'), {
       bootstrap: true,
       exportButtons: false
     });
     const tables =  this.table.getExportData();
     const tablesArray = [];
     Object.keys(tables)
       .forEach(k => {
         tables[k]['csv']['data'] = tables[k]['csv']['data'].replace(/"/g, ' ');
         tablesArray.push(tables[k]['csv']['data'] )
       });
      download_csv(tablesArray.join('\n \n \n'), this.fileName);
    } else {
      this.table.remove();
      this.table = null;
    }

    function download_csv(data, fileName) {
      const csv = data;
      const hiddenElement = document.createElement('a');
      hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
      hiddenElement.target = '_blank';
      hiddenElement.download =  fileName + '_reports.csv';
      hiddenElement.click();
    }
  }
  /**
   * Get Business Volume Chart
   */
  getChart() {
    const time = this.graphOpt.time;
    const period = this.graphOpt.period;
    const mV = this.graphOpt.monetaryValue;

    // Get Customers
    function getCustomers(data, t, monetaryValue) {
      const array = [];
      for ( let i =  data.length - t; i <  data.length; i ++ ) {
        if (monetaryValue === 'All') {
          array.push(Number(data[i]['GBR']['CUSTOMERS']) + Number(data[i]['IRL']['CUSTOMERS']));
        }else if (monetaryValue === 'GBR') {
          array.push(Number(data[i]['GBR']['CUSTOMERS']));
        }else if (monetaryValue === 'IRL') {
          array.push(Number(data[i]['IRL']['CUSTOMERS']));
        }
      }
      return array;
    }
    // Get Subscribers
    function getSubscribers(data, t, monetaryValue) {
      const array = [];
      for ( let i =  data.length - t; i <  data.length; i ++ ) {
        if (monetaryValue === 'All') {
          array.push(Number(data[i]['GBR']['SUBSCRIPTIONS']) + Number(data[i]['IRL']['SUBSCRIPTIONS']));
        }else if (monetaryValue === 'GBR') {
          array.push(Number(data[i]['GBR']['SUBSCRIPTIONS']));
        }else if (monetaryValue === 'IRL') {
          array.push(Number(data[i]['IRL']['SUBSCRIPTIONS']));
        }
      }
      return array;
    }

    // Get ActivePpn
    function getActivePpn(data, t, monetaryValue) {
      const array = [];
      for ( let i =  data.length - t; i <  data.length; i ++ ) {
        if (monetaryValue === 'All') {
          array.push(Number(data[i]['GBR']['ACTIVE_PPN']) + Number(data[i]['IRL']['ACTIVE_PPN']));
        }else if (monetaryValue === 'GBR') {
          array.push(Number(data[i]['GBR']['ACTIVE_PPN']));
        }else if (monetaryValue === 'IRL') {
          array.push(Number(data[i]['IRL']['ACTIVE_PPN']));
        }
      }
      return array;
    }
    // Get chart data
    const data1 = getCustomers(this.businessVolume[period], time, mV);
    const data2 = getSubscribers(this.businessVolume[period], time, mV);
    const data3 = getActivePpn(this.businessVolume[period], time, mV);

    // Get chart dates
    const dates = [];
    let date;
    console.log('this.businessVolume[period]', this.businessVolume[period]);
    for ( let i =  this.businessVolume[period].length - this.graphOpt.time; i <  this.businessVolume[period].length; i ++ ) {
      if (period === 'monthly') {
        console.log(this.businessVolume[period][i].date);
        date = this.businessVolume[period][i].date;
        dates.push(this.globals.monthNames[Number(date.getUTCMonth())] + '-' + date.getUTCFullYear());
      } else {
        date = new Date (this.businessVolume[period][i].date);
        dates.push(date.toLocaleDateString());
      // .replace(/\//g, '-')
      }
    }

    this.chart  = new Chart({
      chart: {
        type: 'line',
        zoomType: 'x'
      },
      title: {
        text: mV + ' Business Volume ' + this.helpers.toTitleCase(period)
      },
      credits: {
        enabled: false
      },
      xAxis: {
        categories: dates
      },
      series: [
        {
          name: 'CUSTOMERS',
          data: data1
        },
        {
          name: 'SUBSCRIPTIONS',
          data: data2
        },
        {
          name: 'ACTIVE_PPN',
          data: data3
        }
      ]
    });
  }

  /**
   * Get monthly revenue chart
   */
  getRevenueChart() {
    const dates = [];
    const data1 = [];
    let date;
    let date2;
    for ( let i =  (this.monthlyRevenue.length - 1) - this.graphOpt.time; i <  this.monthlyRevenue.length - 1; i ++ ) {
      // console.log('new Date(Number(this.monthlyRevenue[i].date)', new Date(Number(this.monthlyRevenue[i].date) * 1000));
      date = new Date(this.monthlyRevenue[i].date * 1000);
      date2 = ( new Date(this.monthlyRevenue[i].date * 1000 ));
      dates.push(this.globals.monthNames[date2.getUTCMonth()] + '-' + date2.getUTCFullYear());
      data1.push(Number(this.monthlyRevenue[i][this.graphOpt.monetaryValue]));
    }

    this.chart  = new Chart({
      chart: {
        type: 'line',
        zoomType: 'x'
      },
      title: {
        text: 'Monthly Revenue'
      },
      credits: {
        enabled: false
      },
      xAxis: {
        categories: dates
      },
      yAxis: {
        title: {
          text: 'Currency'
        }
      },
      series: [
        {
          name: this.graphOpt.monetaryValue,
          data: data1
        }
      ]
    });
  }

  /**
   * Get arpu chart
   */
  getARPUChart() {
    const dates = [];
    const data1 = [];
    let date;
    let date2;
    for (let i = (this.monthlyArpuGraph.length - 1) - this.graphOpt.time; i < this.monthlyArpuGraph.length - 1; i++) {
      // console.log('new Date(Number(this.monthlyArpuGraph[i].date)', new Date(Number(this.monthlyArpuGraph[i].date) * 1000));
      date = new Date(this.monthlyArpuGraph[i].date * 1000);
      date2 = ( new Date(this.monthlyArpuGraph[i].date * 1000));
      dates.push(this.globals.monthNames[date2.getUTCMonth()] + '-' + date2.getUTCFullYear());
      data1.push(Number(this.monthlyArpuGraph[i][this.graphOpt.monetaryValue]));
    }

    this.chart = new Chart({
      chart: {
        type: 'line',
        zoomType: 'x'
      },
      title: {
        text: 'ARPU'
      },
      credits: {
        enabled: false
      },
      xAxis: {
        categories: dates
      },
      yAxis: {
        title: {
          text: 'Currency'
        }
      },
      series: [
        {
          name: this.graphOpt.monetaryValue,
          data: data1
        }
      ]
    });
  }

  /**
   * Get Churn chart
   */

  getChurnChart() {
    const time = this.graphOpt.time;
    // Get Customers
    function getTotal(data, t) {
      const array = [];
      for ( let i =  data.length - t; i <  data.length; i ++ ) {
          array.push(Number(data[i].value));
      }
      return array;
    }
    // Get Subscribers
    function getSystem(data, t) {
      const array = [];
      for ( let i =  data.length - t; i <  data.length; i ++ ) {
        array.push(Number(data[i].value));
      }
      return array;
    }

    // Get ActivePpn
    function getVoluntary(data, t) {
      const array = [];
      for ( let i =  data.length - t; i <  data.length; i ++ ) {
        array.push(Number(data[i].value));
      }
      return array;
    }

    const dataSystem = getSystem(this.monthlyChurnGraph.system, time);
    const dataVoluntary = getVoluntary(this.monthlyChurnGraph.voluntary, time);


    // Get chart dates
    const dates = [];
    let date;
    console.log('this.monthlyChurnGraph[total]', this.monthlyChurnGraph.total);
    for ( let i =  this.monthlyChurnGraph.total.length - this.graphOpt.time; i <  this.monthlyChurnGraph.total.length; i ++ ) {
        console.log(this.monthlyChurnGraph.total[i].date);
        date = this.monthlyChurnGraph.total[i].date;
        dates.push(this.globals.monthNames[Number(date.getUTCMonth())] + '-' + date.getUTCFullYear());

    }

    // If the option is Ner Churn we display the Total churn percentage
    if( this.graphOpt.option == 'Net Churn'){
      const dataTotal = getTotal(this.monthlyChurnGraph.total, time);

      this.chart = new Chart({
        chart: {
          type: 'line',
          zoomType: 'x'
        },
        title: {
          text: 'Churn'
        },
        credits: {
          enabled: false
        },
        xAxis: {
          categories: dates
        },
        yAxis: {
          title: {
            text: 'Percentage'
          }
        },
        series: [
          {
            name: 'Total Churn',
            data: dataTotal
          }
        ]
      });
    } else {
      // If the Option is Split we display both the Voluntary and the system
      this.chart = new Chart({
        chart: {
          type: 'line',
          zoomType: 'x'
        },
        title: {
          text: 'Churn'
        },
        credits: {
          enabled: false
        },
        xAxis: {
          categories: dates
        },
        yAxis: {
          title: {
            text: 'Percentage'
          }
        },
        series: [
          {
            name: 'VOLUNTARY CANCELATIONS',
            data: dataVoluntary
          },
          {
            name: 'INVOLUNTARY CANCELATIONS',
            data: dataSystem
          },
        ]
      });
    }
  }

  /**
   * Get total subscriptions
   */
  // getTotalSubscriptions() {
  //   this.totalSubscriptions = 0;
  //   for (const sub of this.subscriptions ){
  //     if (sub.closing_balance.Overall > 0) {
  //       this.totalSubscriptions = sub.closing_balance.Overall;
  //     }
  //     if (sub.closing_balance.PPN > 0) {
  //       this.totalPPN = sub.closing_balance.PPN;
  //     }
  //   }
  //   console.log(this.totalSubscriptions);
  // }

  /**
   * Change tab
   * @param {StatsTableComponent} tab selected
   */
  changeTab(tab: StatsTableComponent ) {
    this.fileName = tab.title;
    if (this.fileName === 'Subscription') {
      this.getfileName();
    }
    console.log(this.fileName);
    this.getOverviewButton = tab.title !== 'Subscription';
    // Set overview tables to false
    this.tabs.forEach(t => {t.selected = false; t.active = false });
    // Set in depth tables to false
    Object.keys(this.tables).forEach(v => this.tables[v] = false);
    // Set overview table
    tab.selected = true;
    tab.active = true;
    // Change tabs menu
    this.PPNDepthTabs = false;
    this.SubscriptionDepthTabs = false;
    if (tab.title === 'PPN') {
      this.PPNDepthTabs = true;
    }else if (tab.title === 'Subscriptions') {
      this.SubscriptionDepthTabs = true;
    }
  }

  /**
   * Get individual detailed subscriptions
   * @param type -> subscription selected
   */
  getDetailSubscriptionType(type) {

    Object.keys(this.dSelected).forEach( t => {
      this.dSelected[t] = false;
    });
    this.dSelected[type] = true;
    // get data stored
    const detailed = JSON.parse(sessionStorage.getItem('detailed'));
    // Get keys of object
    const keys = Object.keys(detailed)
      .map(key => {
        return key;
      });
    // Create array with detailed subscriptions using keys[]
    for (const k of keys) {
      this.detailedSubscriptions[k] = Object.keys(detailed[k])
        .map(key => {
          return detailed[k][key];
        });
    }

    // get detailed file name
    this.getfileName();

    const titles = this.subscriptionsTitles;

    if (type === 'all') {
      // New business by payment type
      this.detailedSubscriptions['New business by payment type'].forEach(function (cc) {
        let creditCard = 0;
        let directDebit = 0;
        for (const t of titles) {
          if (t.title !== 'Overall') {
            creditCard += Number(cc['Credit Card'][t.title]);
            directDebit += Number(cc['Direct Debit'][t.title]);
          }
        }
        cc['Credit Card'] = creditCard;
        cc['Direct Debit'] = directDebit;
      });

      // New business by payment source
      this.detailedSubscriptions['New business by source'].forEach(function (cc) {
        let agent = 0;
        let ivr = 0;
        let web = 0;
        for (const t of titles) {
          if (t.title !== 'Overall') {
            agent += Number(cc['Agent Addition'][t.title]);
            ivr += Number(cc['IVR Addition'][t.title]);
            web += Number(cc['Web Addition'][t.title]);
          }
        }
        cc['Agent Addition'] = agent;
        cc['IVR Addition'] = ivr;
        cc['Web Addition'] = web;
      });

      // New business by payment frequency
      this.detailedSubscriptions['New business by payment frequency'].forEach(function (cc) {
        let monthly = 0;
        let annual = 0;
        for (const t of titles) {
          if (t.title !== 'Overall') {
            monthly += Number(cc['Monthly'][t.title]);
            annual += Number(cc['Annually'][t.title]);
          }
        }
        cc['Monthly'] = monthly;
        cc['Annually'] = annual;
      });

      // Churn
      this.detailedSubscriptions['Churn'].forEach(function (cc) {
        let customer = 0;
        let reinstatement = 0;
        let system = 0;
        for (const t of titles) {
          if (t.title !== 'Overall') {
            customer += Number(cc['Customer Cancellation'][t.title]);
            reinstatement += Number(cc['Reinstatement'][t.title]);
            system += Number(cc['System Cancellation'][t.title]);
          }
        }
        cc['Customer Cancellation'] = customer;
        cc['Reinstatement'] = reinstatement;
        cc['System Cancellation'] = system;

      });

      // Active subscriptions
      this.detailedSubscriptions['Active subscriptions'].forEach(function (cc, i) {
        let active = 0;
        if (i < 7) {
          cc['Active'] = active;
          for (const t of titles) {
            if (t.title !== 'Overall') {
              active += Number(cc['Active'][t.title]);
            }
          }
        }else if (i >= 7) {
          cc['Active'] = 'N/A'
        }
      });

      // New business by pricepoint
      this.totalByPricePoint = [];
      if (this.detailedSubscriptions['Total subscriptions by pricepoint'][0]) {
        // get pricepoints
        this.pricepoint =
          Object.keys(this.detailedSubscriptions['Total subscriptions by pricepoint'][0])
            .map(key => {
              return key;
            });
        // get totals and sum
        for (const k of this.pricepoint) {
          // console.log('pricepoint', k);
          const item = [];
          this.detailedSubscriptions['Total subscriptions by pricepoint'].forEach(function (cc, i) {
            item[i] = {
              pricepoint: k,
              total: Object.keys(cc[k])
                .map(key => {
                  return cc[k][key]
                }).reduce(getSum, 0)
            };
            function getSum(total, num) {
              return total + Math.round(num);
            }
          });
          // Add pricepoint if at least one total > 0
          for (const i of item) {
            if (i.total > 0) {
              this.totalByPricePoint.push(item);
              break;
            }
          }
        }
        this.getPricepointTotals(this.totalByPricePoint);

        this.totalByPricePoint.sort(function (a, b) {
          return parseFloat(a[0].pricepoint) - parseFloat(b[0].pricepoint)
        });
      }
    }else {
      // New business by payment type
      this.detailedSubscriptions['New business by payment type'].forEach(function (cc) {
        cc['Credit Card'] = Number(cc['Credit Card'][type]);
        cc['Direct Debit'] = Number(cc['Direct Debit'][type]);
      });
      // New business by payment source
      this.detailedSubscriptions['New business by source'].forEach(function (cc) {
        cc['Agent Addition'] = Number(cc['Agent Addition'][type]);
        cc['IVR Addition'] = Number(cc['IVR Addition'][type]);
        cc['Web Addition'] = Number(cc['Web Addition'][type]);
        // cc['Unknown Addition'] = Number(cc['Unknown Addition'][type]);
      });
      // New business by payment frequency
      this.detailedSubscriptions['New business by payment frequency'].forEach(function (cc) {
        cc['Monthly'] = Number(cc['Monthly'][type]);
        cc['Annually'] = Number(cc['Annually'][type]);
      });
      // Churn
      this.detailedSubscriptions['Churn'].forEach(function (cc) {
        cc['Customer Cancellation'] = Number(cc['Customer Cancellation'][type]);
        cc['Reinstatement'] = Number(cc['Reinstatement'][type]);
        cc['System Cancellation'] = Number(cc['System Cancellation'][type]);
      });
      // Active subscriptions
      this.detailedSubscriptions['Active subscriptions'].forEach(function (cc, i) {
        if (i < 7) {
          cc['Active'] = Number(cc['Active'][type]);
        }else if (i >= 7) {
          cc['Active'] = 'N/A'
        }
      });

      // New business by pricepoint
      this.totalByPricePoint = [];
      if (this.detailedSubscriptions['Total subscriptions by pricepoint'][0]) {

        // get pricepoints
          this.pricepoint = [];
          Object.keys(this.detailedSubscriptions['Total subscriptions by pricepoint'][0])
            .map(key => {
              return Object.keys(this.detailedSubscriptions['Total subscriptions by pricepoint'][0][key])
                .forEach(k => {
                  if (k === type) {
                    this.pricepoint.push(key);
                    // console.log(k, type, key);
                  }
                });
            });

        console.log(this.pricepoint);
        // get totals and sum
        for (const k of this.pricepoint) {
          const item = [];
          this.detailedSubscriptions['Total subscriptions by pricepoint'].forEach(function (cc, i) {
            item[i] = {
              pricepoint: k,
              total: Object.keys(cc[k])
                .map(key => {
                  if (key === type) {
                    return Number(cc[k][key])
                  }
                }).filter(function(n){ return n !== undefined })
          };
          });
          // Add pricepoint if at least one total > 0
          for (const i of item) {
            if (i.total > 0) {
              this.totalByPricePoint.push(item);
              break;
            }
          }
          // this.totalByPricePoint.push(item);
        }
        this.getPricepointTotals(this.totalByPricePoint);
        this.totalByPricePoint.sort(function (a, b) {
          return parseFloat(a[0].pricepoint) - parseFloat(b[0].pricepoint)
        });
      }
    }
  }

  /**
   * Get totals by pricepoint
   */
  getPricepointTotals(pp) {
    this.pricepointTotal = [0, 0, 0, 0, 0, 0, 0];
    for (const p of pp) {
      for (let i = 0; i < 7; i++) {
        this.pricepointTotal[i] += Number(p[i].total);
      }
    }
  }

  /**
   * Get dimmed days of week
   */
  getDimmed() {
    const date = new Date(this.date);
    this.dayDimmed = date.getDay();
    if (this.dayDimmed === 0) {
      this.dayDimmed = 7;
    }
  }

  /**
   * get file name
   */
  getfileName() {
    console.log(this.dSelected);
    this.fileName = 'Subscription';
    Object.keys(this.dSelected)
      .forEach(d => {
        if (this.dSelected[d]) {
          if (d === 'XP') {
            d = 'Legacy'
          }
          this.fileName += ' (' + d + ')';
        }
      });
    console.log(this.fileName);
  }

  /**
   * Get dashboard data with selected date
   * @param type
   */
  getData(type) {
    let date;
    if (type === 'C' && this.churnDate) { // Monthly Churn
      // get date
      date = this.churnDate.split('-');
      date = new Date(Date.UTC(date[1], date[0]));
      // reset value
      this.monthlyChurn = null;
      // send date and actual value in case of error
      this.dashboard.getMonthlyChurnDefault(+ date, sessionStorage.getItem('dashboardMonthlyChurn'));
      // remove session storage value
      sessionStorage.removeItem('dashboardMonthlyChurn');
      // initialize interval to check when new value is added
      this.dashboardKPIs('C');
    } else if (type === 'A' && this.MRDate) { // ARPU
      // get date to send
      date = this.MRDate.split('-');
      date = new Date(Date.UTC(Number(date[1]), Number(date[0]) - 1));
      // reset values
      this.annualArpu = null;
      this.monthlyArpu = null;
      /**
       * TODO: Get correct annual date
       */
      // send date and actual value in case of error
      this.dashboard.getAnnualARPU(+ date, sessionStorage.getItem('dashboardAnnualArpu'));
      this.dashboard.getMonthlyARPU(+ date, sessionStorage.getItem('dashboardMonthlyArpu'));
      // delete value session storage
      sessionStorage.removeItem('dashboardAnnualArpu');
      sessionStorage.removeItem('dashboardMonthlyArpu');
      // initialize interval to check when new value is added
      this.dashboardKPIs('A');
    } else if (type === 'SRA' && this.SRADate) { // subscriptions and PPN trends
       // reset values
      this.trends.subscriptions_GBR['COUNT'] = null;
      this.trends.subscriptions_IRL['COUNT'] = null;
      this.trends.customers_GBR['COUNT'] = null;
      this.trends.customers_IRL['COUNT'] = null;
      this.trends.registered_ppn['COUNT'] = null;
      this.trends.active_ppn['COUNT'] = null;
      // send date and actual value in case of error
      this.dashboard.getSRA(+ this.SRADate, sessionStorage.getItem('dashboardTrends'));
      // delete value session storage
      sessionStorage.removeItem('dashboardTrends');
      // initialize interval to check when new value is added
      this.dashboardKPIs('SRA');
    } else if (type === 'MR' && this.MRDate) { // Monthly Revenue
      console.log(this.MRDate);

      // get date to send
      date = this.MRDate.split('-');
      date = new Date(Date.UTC(Number(date[1]), Number(date[0]) - 1));

      console.log(date);
      // reset value
      this.mr = null;
      // send date and actual value in case of error
      this.dashboard.getMR(+ date, sessionStorage.getItem('dashboardMonthlyR'));
      // delete value session storage
      sessionStorage.removeItem('dashboardMonthlyR');
      // initialize interval to check when new value is added
      this.dashboardKPIs('MR');
    }
  }

  /**
   * stop propagation of events
   * @param e: $event
   */
  stopPropagation(e) {
    e.stopPropagation();
  }

  /**
   * Get churn Target
   */
  getTarget() {
    this.rest.get('/accounts/getChurnValue')
      .subscribe(res => {
        this.target = res['newChurnValue'];
        // TARGET FORM
        this.targetForm.patchValue({
          target: this.target.toString()
        });
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Change churn target
   */
  changeTarget() {
    this.submitted = true;

    if (this.targetForm.valid) {
    this.targetDialog = null;
    const data = {
      'newChurnValue': this.targetForm.value.target
    };
    this.http.put(this.globals.API_ENDPOINT + '/accounts/setChurnValue', data, {responseType: 'text'})
      .subscribe(res => {
        this.target = JSON.parse(res)['newChurnValue'];
      }, err => {
        this.getTarget();
        err = JSON.parse(err.error);
        console.log(err);
        if (err.fieldErrors) {
          this.error = err.fieldErrors[0];
        }else {
          this.error = this.helpers.error(err.message);
        }
      })
    }
  }

  /**
   * Require admin
   */
  requireAdminCheck () {
      let role;
      this.helpers.getRole()
        .subscribe(res => {
          role = res['roles'][0].name.toLowerCase();
          console.log(role);
          if (role.includes('admin') || role.includes('supervisor')) {
            this.targetDialog = true;
          } else {
            this.requireAdmin = true;
          }
        });


  }

  /**
  * Auth require error
  * @param e -> event emitted by auth-required component
  */
  authError(e) {
    this.error = e;
  }
  /**
   * Authorize to proceed refund
   */
  authorize(e) {
    this.targetDialog = true;
  }


  /**
  * Get subscriptions order
  */
  getSubscriptionsOrder() {
    function sort(a,b) {
      if (a.order < b.order)
        return -1;
      if (a.order > b.order)
        return 1;
      return 0;
    }
    this.rest.get('/sales-item-groups/findAllDto')
      .subscribe((res: any) => {
        this.subscriptionsTitles.forEach(t => {
          res.forEach(r => {
            if (r.name === t.title) {
              t.order = r.displayingOrder;
            }
          })
        });
        this.subscriptionsTitles.sort(sort)
      })
  }
}

interface Trends {
  date: string,
  subscriptions_GBR: string,
  subscriptions_IRL: string,
  customers_GBR: string,
  customers_IRL: string,
  registered_ppn: string,
  active_ppn: string,
}

