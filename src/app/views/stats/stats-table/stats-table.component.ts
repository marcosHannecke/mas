import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-stats-table',
  templateUrl: './stats-table.component.html',
  styleUrls: ['./stats-table.component.css']
})
export class StatsTableComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  selected: boolean;

  @Input()
  active: boolean;

  constructor() { }

  ngOnInit() {
  }

}
