"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ppv_stats_component_1 = require("./ppv-stats/ppv-stats.component");
var angular_highcharts_1 = require("angular-highcharts");
// import {IMyDrpOptions, IMyDateRangeModel} from 'mydaterangepicker';
var StatsComponent = /** @class */ (function () {
    //
    // public   myDateRangePickerOptions: IMyDrpOptions = {
    //   dateFormat: 'dd.mm.yyyy',
    //   firstDayOfWeek: 'mo',
    //   sunHighlight: true,
    //   height: '34px',
    //   width: '260px',
    //   inline: false,
    //   alignSelectorRight: false,
    //   indicateInvalidDateRange: false
    // };
    // For example initialize to specific date (09.10.2018 - 19.10.2018). It is also possible
    // to set initial date range value using the selDateRange attribute.
    // public model: Object =
    //   {beginDate: {year: 2018, month: 10, day: 9},
    //   endDate: {year: 2018, month: 10, day: 19}};
    function StatsComponent(fb) {
        this.fb = fb;
        this.PPNDepthTabs = false;
        this.SubscriptionDepthTabs = true;
        this.tables = {
            TVXcellence: false,
            Premier: false,
            RHMonthly: false,
            OTT: false,
            PPN: false,
            IPPVOPPV: false,
            PPNBuys: false,
        };
        // HIGH CHARTS YEAR
        this.chart = new angular_highcharts_1.Chart({
            chart: {
                type: 'line'
            },
            title: {
                text: 'Revenue'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
                    'September', 'October', 'November', 'December']
            },
            series: [
                {
                    name: 'AMOUNT CHARGED',
                    data: [65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56]
                },
                {
                    name: 'AMOUNT CHARGED ',
                    data: [28, 48, 40, 19, 86, 27, 90, 80, 81, 56, 55, 40]
                }
            ]
        });
    }
    StatsComponent.prototype.ngOnInit = function () {
        var date = new Date();
        var oneWeekAgo = new Date();
        oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);
        this.form = this.fb.group({
            dateFrom: oneWeekAgo,
            dateTo: date
        });
        // this.onChanges();
    };
    StatsComponent.prototype.ngAfterViewInit = function () {
        this.tabs.forEach(function (t) { return t.active = false; });
        var selectedTab = this.tabs.find(function (tab) { return tab.selected; });
        if (!selectedTab) {
            this.tabs.first.selected = true;
            this.tabs.first.active = true;
        }
    };
    // OVERVIEW TABS
    StatsComponent.prototype.changeTab = function (tab) {
        var _this = this;
        // Set overview tables to false
        this.tabs.forEach(function (t) { t.selected = false; t.active = false; });
        // Set in depth tables to false
        Object.keys(this.tables).forEach(function (v) { return _this.tables[v] = false; });
        // Set overview table
        tab.selected = true;
        tab.active = true;
        // Change tabs menu
        this.PPNDepthTabs = false;
        this.SubscriptionDepthTabs = false;
        if (tab.title === 'PPN') {
            this.PPNDepthTabs = true;
        }
        else if (tab.title === 'Subscriptions') {
            this.SubscriptionDepthTabs = true;
        }
    };
    // IN DEPTH TABS
    StatsComponent.prototype.changeInDepth = function (tab) {
        var _this = this;
        if (this.tables[tab] === false) {
            // Set overview tables to false
            this.tabs.forEach(function (t) { return t.selected = false; });
            // Set in depth tables to false
            Object.keys(this.tables).forEach(function (v) { return _this.tables[v] = false; });
            this.tables[tab] = true;
        }
        else {
            this.tables[tab] = false;
            this.tabs.forEach(function (t) {
                if (t.active) {
                    t.selected = true;
                }
            });
        }
    };
    __decorate([
        core_1.ViewChildren(ppv_stats_component_1.PpvStatsComponent)
    ], StatsComponent.prototype, "tabs", void 0);
    StatsComponent = __decorate([
        core_1.Component({
            selector: 'app-stats',
            templateUrl: './stats.component.html',
            styleUrls: ['./stats.component.css'],
        })
    ], StatsComponent);
    return StatsComponent;
}());
exports.StatsComponent = StatsComponent;
