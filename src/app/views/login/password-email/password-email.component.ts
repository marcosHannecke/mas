import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from '../../../services/helpers.service';
import { RestService } from '../../../services/rest.service';

@Component({
  selector: 'app-password-recover',
  templateUrl: './password-email.component.html',
  styleUrls: ['./password-email.component.css'],
  providers: [ValidatorsService, HelpersService, RestService]
})
export class PasswordEmailComponent implements OnInit {

  form: FormGroup;
  submitted = false;
  error: any;
  success: any;

  constructor(private fb: FormBuilder, private validators: ValidatorsService,
              private helpers: HelpersService, private rest: RestService) { }

  ngOnInit() {
    this.form = this.fb.group({
      'email': ['', [Validators.required, this.validators.email]],
    });
  }

  sendEmail() {
    event.preventDefault();
    this.submitted = true;
    if (this.form.valid) {
      this.rest.postNoSimplified('/principals/forgotten-password', this.form.value)
        .subscribe(res => {
          this.success = {
            message: 'Please check your email to reset your password'
          };
        }, err => {
          this.error = this.helpers.error(err.error.message);
        })
    }
  }
}
