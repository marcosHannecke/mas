import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {LoginComponent} from "./login.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { MomentModule } from 'angular2-moment';
import {PasswordEmailComponent} from "./password-email/password-email.component";
import {RouterModule} from "@angular/router";
import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';


@NgModule({
  declarations: [LoginComponent, PasswordEmailComponent, PasswordRecoveryComponent],
  imports     : [BrowserModule, FormsModule, ReactiveFormsModule, NgIdleKeepaliveModule,RouterModule],
})

export class LoginModule {}
