"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var login_component_1 = require("./login.component");
var forms_1 = require("@angular/forms");
var keepalive_1 = require("@ng-idle/keepalive");
var angular2_moment_1 = require("angular2-moment");
var LoginModule = /** @class */ (function () {
    function LoginModule() {
    }
    LoginModule = __decorate([
        core_1.NgModule({
            declarations: [login_component_1.loginComponent],
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, keepalive_1.NgIdleKeepaliveModule, angular2_moment_1.MomentModule],
        })
    ], LoginModule);
    return LoginModule;
}());
exports.LoginModule = LoginModule;
