import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from '../../../services/helpers.service';
import {ActivatedRoute} from '@angular/router';
import {RestService} from '../../../services/rest.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-password',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.css'],
  providers: [ ValidatorsService, HelpersService, RestService]
})
export class PasswordRecoveryComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  error: any;
  success: any;
  token: string;
  errorPasswordMatch = false;
  showPage = false;

  constructor(private router: Router, private fb: FormBuilder, private validators: ValidatorsService,
            private helpers: HelpersService, route: ActivatedRoute, private rest: RestService) {
    // Get token from url
    this.token = route.snapshot.queryParams.q;
    console.log(this.token);
  }

  ngOnInit() {
    this.form = this.fb.group({
      'password': ['', [Validators.required, Validators.maxLength(255), Validators.minLength(8), this.validators.forceSpecialChar]],
      'passwordConfirm': ['', [Validators.required]],
    });
    this.checkPasswordToken()
  }

  /**
   *  Recover password page access checking if token valid
   */
  checkPasswordToken() {
    const data = {
      'passwordToken': this.token
    };

    this.rest.postNoSimplified('/principals/check-password-token', data)
      .subscribe(
        () => {
          this.showPage = true;
        },
        err => {
          this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Reset password
   */
  resetPassword() {
    event.preventDefault();
    this.errorPasswordMatch = false;
    this.submitted = true;

    const data = {
      passwordToken: this.token,
      newPassword: this.form.value.password
    };

    if (this.form.value.password !== this.form.value.passwordConfirm) {
      // Passwords don't match
      this.errorPasswordMatch = true;
    }else if (this.form.valid) {
      this.rest.postNoSimplified('/principals/recover-password', data)
        .subscribe(res => {
          console.log(res);
          this.success = {
            message: 'Password has been updated.'
          };
        }, err => {
          this.helpers.error(err.error.message);
      });
    }
  }
}
