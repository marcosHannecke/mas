"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("../../services/user.service");
var subscribers_service_1 = require("../../services/subscribers.service");
var http_1 = require("@angular/http");
var http_service_1 = require("../../services/http.service");
var loginComponent = /** @class */ (function () {
    function loginComponent(userService, router, http, subscribersService, idle) {
        this.userService = userService;
        this.router = router;
        this.http = http;
        this.subscribersService = subscribersService;
        this.idle = idle;
        this.username = '';
        this.password = '';
        this.loggedIn = false;
        this.loading = false;
    }
    // When click login button
    loginComponent.prototype.onSubmit = function () {
        var _this = this;
        this.loading = true;
        if (this.username || this.password) {
            // this.userService.login(this.username, this.password)
            this.userService.login(this.username, this.password)
                .subscribe(function (res) {
                _this.loading = false;
                if (res) {
                    _this.loggedIn = true;
                    _this.getRole();
                    _this.router.navigate(['/search']);
                    _this.idle.watch();
                }
            }, function (err) {
                _this.loading = false;
                console.log(err);
                if (err['_body'].includes('Bad credentials')) {
                    _this.error = {
                        message: 'Incorrect User and Password'
                    };
                }
                else {
                    _this.loading = false;
                    _this.error = {
                        message: 'Unable to log in. ',
                        message2: 'Please contact system administrator.'
                    };
                }
            });
        }
        else {
            this.loading = false;
            this.error = {
                message: 'Please enter User and Password'
            };
        }
    };
    loginComponent.prototype.getRole = function () {
        this.headers = new http_1.Headers();
        this.headers.append('Content-Type', 'application/json');
        this.options = new http_1.RequestOptions({ headers: this.headers });
        this.subscribersService.getRole()
            .subscribe(function (res) {
            var resp = JSON.parse(res['_body']);
            if (resp.roles.length > 0) {
                if (resp.roles[0].name == 'ROLE_ADMIN') {
                    sessionStorage.setItem('role', 'admin');
                }
                else if (resp.roles[0].name == 'ROLE_USER') {
                    sessionStorage.setItem('role', 'user');
                }
            }
            else {
                sessionStorage.setItem('role', 'user');
            }
        });
    };
    loginComponent = __decorate([
        core_1.Component({
            selector: 'login',
            templateUrl: 'login.template.html',
            styles: ['.login-box input{font-size: 15px; height: 45px; ' +
                    'margin: 29px 0px; box-shadow: 0 0 20px 0 rgba(132, 132, 132, 0.2); border-radius: 3px; }' +
                    '.login-box button{padding: 10px 0; border-radius: 50px;}' +
                    '.login-box h3{color: #ffffff;}' +
                    '.login-box .btn-primary{background: #e55d1b; border-color: #e55d1b;}' +
                    '.login-box .btn-primary:hover{background: #ff671d; border-color: #ff671d;}' +
                    '.login-box p{color: rgba(255, 255, 255, 0.58)}' +
                    ' .st0{fill:none;stroke:#FFFFFF;stroke-width:14.8412;stroke-linecap:round;stroke-miterlimit:10;}'
            ],
            providers: [user_service_1.UserService, http_service_1.HttpService, subscribers_service_1.SubscribersService]
        })
    ], loginComponent);
    return loginComponent;
}());
exports.loginComponent = loginComponent;
