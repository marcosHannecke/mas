import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {RestService} from '../../services/rest.service';
import {HelpersService} from '../../services/helpers.service';

import {Idle} from '@ng-idle/core';
import {Globals} from '../../app.global';
import {DashboardService} from '../../services/dashboard.service';

declare var swal: any;

@Component({
  selector: 'login',
  templateUrl: 'login.template.html',
  styles: ['.login-box input{font-size: 15px; height: 45px; ' +
  'margin: 29px 0px; box-shadow: 0 0 20px 0 rgba(132, 132, 132, 0.2); border-radius: 3px; }' +
  '.login-box button{padding: 10px 0; border-radius: 50px;}' +
  '.login-box h3{color: #ffffff;}' +
  '.login-box .btn-primary{background: #e55d1b; border-color: #e55d1b;}' +
  '.login-box .btn-primary:hover{background: #ff671d; border-color: #ff671d;}' +
  '.login-box p{color: rgba(255, 255, 255, 0.58)}' +
  ' .st0{fill:none;stroke:#FFFFFF;stroke-width:14.8412;stroke-linecap:round;stroke-miterlimit:10;}' +
  '.forgotLink {color: rgba(255, 255, 255, 0.58)} .forgotLink:hover {color: white}'
  ],
  providers: [UserService, HelpersService, RestService, DashboardService]
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  error: any ;
  loggedIn = false;
  loading = false;
  pleaseWait = false;

  date = Math.floor(Date.now() / 1000);

  environment;
  showEnvironmentRadio: boolean;

  constructor(private userService: UserService, private router: Router, private idle: Idle, private globals: Globals,
              private rest: RestService, private gb: Globals, private dashboard: DashboardService) {
    sessionStorage.removeItem('notifications');
    // Reload from server only once
    if (sessionStorage.getItem('reloaded') == null) {
      window.location.reload(true);
      sessionStorage.setItem('reloaded', 'true')
    }
    // show environment radio buttons
    this.showEnvironmentRadio = (this.globals.URL === 'http://stage.vca.mediaaccessservices.net'
      || this.globals.URL === 'http://172.16.0.67:8080')
      && this.globals.URL !== 'https://vca.mediaaccessservices.net';
  }

  ngOnInit() {
    if (sessionStorage.getItem('environment') != null) {
      this.environment = sessionStorage.getItem('environment');
    }else {
      this.environment = 'stage';
    }
  }
  /**
   * Login when submit
   * @param e -> $event to prevent default form action
   */
  onSubmit(e) {
    // prevent form submit default
    e.preventDefault();
    // show loading gif on submit
    this.loading = true;
    // Login
    if (this.username || this.password) {
      // this.userService.login(this.username, this.password)
      this.userService.login(this.username, this.password)
        .subscribe(res => {
          // Store tokens
          if ( res['access_token']) {
              sessionStorage.setItem('auth_token', res['access_token']);
              sessionStorage.setItem('expires_in', res['expires_in']);
              sessionStorage.setItem('refresh_token', res['refresh_token']);
            // Get user role
            this.getRole();
            // Navigate to search view
            this.router.navigate(['/search']);
            // Starts idle
            this.idle.watch();
            }
          // Show hide loading giff
          this.loggedIn = true;
          this.loading = false;
        }, err => {
          this.loading = false;
          if (err.error.error_description.includes('Bad credentials')) {
            this.error = {
              message: 'Incorrect User and Password'
            };
          } else if (err.error.error_description.includes('locked')) {
            this.error = {
              message: 'User locked',
              message2: 'Please contact system administrator to unlock.'
            };
          } else {
            this.loading = false;
            this.error = {
              message: 'Unable to log in. ',
              message2: 'Please contact system administrator.'
            }
          }
        });
    }else {
      this.loading = false;
      this.error = {
        message: 'Please enter User and Password'
      };
    }
  }

  /**
   * Get user role
   */
  getRole() {
    this.rest.getNoSimplified('/principals/about-me')
      .subscribe(res => {
        const resp = res;
        if (resp['roles']) {
          if (resp['roles'][0].name === 'ROLE_ADMIN' || resp['roles'][0].name === 'ROLE_SUPER_ADMIN'
            || resp['roles'][0].name === 'ROLE_SUPERVISOR') {
            sessionStorage.setItem('role', 'admin');
            // Get stats dashboard KPIs
            this.dashboardKPIs();
          }else if (resp['roles'][0].name === 'ROLE_USER') {
            sessionStorage.setItem('role', 'user');
          }
        }else {
          sessionStorage.setItem('role', 'user');
        }
      })
  }


  /**
   * Get Stats Dashboard KPIs
   */
  dashboardKPIs() {
    // this.dashboard.getSRADefault(+ this.gb.yesterday, null); // Subscriptions, Registered PPN, Active PPN
    this.dashboard.getSRA(+ this.gb.firstDayYesterdayMonth, null); // Subscriptions, Registered PPN, Active PPN
    this.dashboard.getMonthlyARPUDefault(+ this.gb.lastDayPreviousOfMonth);
    this.dashboard.getAnnualARPUDefault();
    this.dashboard.getMonthlyARPUGraph();
    this.dashboard.getMonthlyChurnGraph();
    this.dashboard.getMonthlyChurnDefault(Date.now());
    this.dashboard.getMonthlyRevenue();
    this.dashboard.getBusinessVolume();
    this.dashboard.getMRDefault(+ this.gb.lastDayPreviousOfMonth);
  }

  /**
   * Change environment
   */
  changeEnvironment() {
    sessionStorage.setItem('environment', this.environment);
    window.location.reload();
    this.pleaseWait = true;
  }
}
