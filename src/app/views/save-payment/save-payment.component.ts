import {Component, OnInit} from '@angular/core';
import {RestService} from '../../services/rest.service';
import {HelpersService} from '../../services/helpers.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-save-payment',
  templateUrl: './save-payment.component.html',
  styleUrls: ['./save-payment.component.css'],
  providers: [RestService, HelpersService]
})
export class SavePaymentComponent implements OnInit {
  customer: any;
  cardTokens = {
    token : '',
    tokenId: '',
    expire: ''
  };
  payMeth: any;
  payMethDialog = false;
  error: any;
  cardError: any;
  subscriber: any;
  paymentPath: any;

  constructor(private rest: RestService, private helpers: HelpersService, private router: ActivatedRoute, private route: Router) { }

  ngOnInit() {
    // Get Payment Path ( join, create , update)
    if (sessionStorage.getItem('paymentPath') != null) {
      this.paymentPath = sessionStorage.getItem('paymentPath');
    }
    // GET SUBSCRIBER IF STORED
    if (sessionStorage.getItem('subscriber') != null) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
    }


    // IF ADD/UPDATE PAYMENT CARD
    if (sessionStorage.getItem('cardProcess')) {
      this.customer = JSON.parse(sessionStorage.getItem('customer'));

      // GET TOKENS
      this.router.queryParams
        .subscribe(params => {
          this.cardTokens.token = params['token'];
          this.cardTokens.tokenId = params['tokenId'];
          this.cardTokens.expire = params['exp'];
        });

      if (this.cardTokens.token === undefined || this.cardTokens.tokenId === undefined
        || this.cardTokens.token === '' || this.cardTokens.tokenId === '') {
        // If DD failed display error
        if (JSON.parse(sessionStorage.getItem('cardDD'))) {

          this.cardError = {
            message : 'The Direct Debit Instruction was not created.',
            message2: 'Please check the bank details carefully or pay by Debit/Credit Card'
          };
        } else {
          // If Card failed display error
          this.cardError = {
            message : 'The Card details are not correct.',
            message2: 'Please check the expiry date and security code carefully or use Direct Debit.'
          };
        }
        sessionStorage.removeItem('cardProcess');
      }else {
        // SAVE PAYMENT CARD
        const data = {
          customerId : this.customer.id,
          token: this.cardTokens.token,
          tokenId: this.cardTokens.tokenId,
          expiryDate: this.helpers.undefinedToEmptyString(this.cardTokens.expire)
        };

        this.rest.put('/payments/save-payment-card', data)
          .subscribe(() => {
            sessionStorage.setItem('saved', 'saved');
            // this.closeWindow();
            // this.getPaymentCard();
          }, err => {
            this.error = this.helpers.error(err.error.message);
          });
        sessionStorage.removeItem('cardProcess');
      }
    } else {

    }
  }

  /**
   * Go back when error or cancel
   */
  goBack() {
    sessionStorage.setItem('saved', 'saved');
    if (this.paymentPath === 'join') {
      sessionStorage.setItem('paymentPath', '');
      this.route.navigate(['/joinSubscriber]']);
    } else if (this.paymentPath = 'create') {
      sessionStorage.setItem('paymentPath', '');
      this.route.navigate(['/newSubscriber']);
    } else {
      sessionStorage.setItem('paymentPath', '');
      this.route.navigate(['/customer/' + this.subscriber.customerId]);
    }

  }

}
