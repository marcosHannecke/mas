import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavePaymentComponent } from './save-payment.component';

describe('SavePaymentComponent', () => {
  let component: SavePaymentComponent;
  let fixture: ComponentFixture<SavePaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavePaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
