import {Component, HostListener, OnInit} from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { HelpersService } from '../../services/helpers.service';
import { PostcodeService } from '../../services/postcode.service';
import { RestService } from '../../services/rest.service';

import * as $ from 'jquery';
import {Globals} from "../../app.global";

@Component({
  selector: 'app-histories',
  templateUrl: './histories.component.html',
  styleUrls: ['./histories.component.css'],
  providers: [ HelpersService, PostcodeService, RestService ],

})
export class HistoriesComponent implements OnInit {
  // PAGINATION
  showNumber = 10;
  p = 1;

  subscriber: any;
  subscribers: any;
  customer: any;
  subscriberSelected: any;
  entitlements: any;
  id: number; // customer id from router params
  revisionId: number;
  snapDate: any;
  snapAuditor: any;
  error: any;
  info: any;
  histories: any;
  snapshot: any;
  minDate = new Date(2016, 0, 1);
  maxDate = new Date(2020, 0, 1);
  date1 = + this.gb.lastYear;
  date2 = this.gb.todayTimestamp * 1000;
  categorySelected = 'customer';
  order = true;
  entsViewSelected = 'All';
  compareItems = [];
  compareItemsDates = [];
  compareResults: any;
  marketingPreferences: any;
  compareMessage = 'Please select 2 history items.';


  paymentSnapshotPrevious: any;
  paymentSnapshotNew: any;

  views = {
    history: true,
    compare: false,
    revision: false,
    snapshot: false
  };

  showSubscribers = false;

  innerWidth: any;
  options: any;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  constructor(private activatedRoute: ActivatedRoute, private helpers: HelpersService, private rest: RestService,
              private postcodeService: PostcodeService, private gb: Globals) {}

  ngOnInit() {
    //DATE PICKER OPTIONS
    this.options = {
      displayFormat: 'DD-MMM-YYYY ',
    };


    this.innerWidth = window.innerWidth;
    console.log('WIDTH', this.innerWidth);

    // GET CUSTOMER ID
    this.activatedRoute.params.subscribe(p => {
      this.id = p.id;
    });

    if (sessionStorage.getItem('billingSubscribers')) {
      this.subscribers = JSON.parse(sessionStorage.getItem('billingSubscribers'));
      console.log('SUBSCRIBERS', this.subscribers);
    }

    // GET SUBSCRIBER STORED
    if (sessionStorage.getItem('subscriber')) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
      console.log('SUBSCRIBER', this.subscriber);
      // GET CUSTOMER
      this.getCustomer();
    }

    if (this.subscriber) {
      // GET HISTORIES
      this.getHistories('customer');

      // GET ALL ENTITLEMENT IDS
      this.getAllEntitlements();
    } else {
      this.error = {
        message: 'Customer not found'
      }
    }

  }

  /**
   * Get customer
   */
  getCustomer() {
    this.rest.get('/customers?q=subscriberId=' + this.subscriber.id)
      .subscribe( res => {
        this.customer = res[0];
        console.log('Customer', this.customer);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });
  }

  /**
   * Get histories view
   * @param category selected
   */
  getHistories(category) {
    let id = this.id;
    let param = 'id';

    // check if category is subscriber
    if (typeof category === 'object') {
      this.subscriberSelected = category;
      id = category.id;
      category = 'subscriber';
    }
    if (category === 'payment-token') {
      param = 'customer_id';

    }

    let fromDate: any = +this.date1;
    let toDate: any = +this.date2;
    toDate = toDate + 86399000;

    if (fromDate === 0) {
      fromDate = '';
    }
    if (toDate === 86399000) {
      toDate = ''
    }

    this.rest.getNoSimplified('/history/' + category + '/histories/?parameterName=' + param
      + '&parameterValue=' + id + '&fromDate=' + fromDate + '&toDate=' + toDate)
      .subscribe(res => {
        if (res === null) {
          this.info = {
            message: 'Not entries found'
          }
        } else {
          this.histories = res;
          // remove empty updates from histories
          this.histories = this.histories.filter(h => {
            if (h.updates.length !== 0) {
              return h;
            }
          });

          this.selectCategory(category);
          if (category === 'customer' || category === 'payment-token') {
            if (this.views.snapshot) {
              // histories[0].revisionId, histories[0].resourceId,
              this.getSnapshot(this.histories[0].revisionId, this.histories[0].resourceId,
                this.histories[0].date, this.histories[0].auditor);
            }
          } else if (category === 'subscriber') {
            this.changeView('H');
            console.log('subscriber');
            // Replace object for viewing card number
            for (const h of this.histories) {
              for (const s of h.updates) {
                if (s.propertyName === 'viewingCard') {
                  if (typeof s.newValue === 'object' && s.newValue) {
                    s.newValue = s.newValue.cardNumber;
                  }
                  if (typeof s.previousValue === 'object' && s.previousValue ) {
                    s.previousValue = s.previousValue.cardNumber;
                  }
                }
              }
            }
            if (this.views.snapshot) {
              this.getSnapshot(this.histories[0].id, this.subscriberSelected.id,
                this.subscriberSelected.date, this.subscriberSelected.auditor);
            }
          }
        }
        this.changeView('H');
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });
  }

  /**
   * GET ENTITLEMENTS HISTORY BY SUBS ID
   * @param sub
   */
  getEntitlementsHistoryById(sub ?: any) {
    this.histories = [];

    let fromDate: any = +this.date1;
    let toDate: any = +this.date2;
    toDate = toDate + 86399000;

    if (fromDate === 0) {
      fromDate = '';
    }
    if (toDate === 86399000) {
      toDate = ''
    }
    if (sub) {
      let ent;
      this.rest.getNoSimplified('history/entitlement/histories/?parameterName=subscriber_id&parameterValue='
        + sub.id + '&fromDate=' + fromDate + '&toDate=' + toDate)
        .subscribe(res => {
          if (res === null) {
            this.info = {
              message: 'Not entries found'
            }
          } else {
            this.selectCategory('entitlement');
             ent = res;

            for (const e of ent) {
              this.histories.push(e);
            }
            ent.historyId = ent.id;
            // get snapshot
            if (this.views.snapshot) {
              this.getSnapshot(this.histories[0].revisionId, this.histories[0].resourceId,
                this.histories[0].date, this.histories[0].auditor);
            }
          }
          this.changeView('H');
        }, err => {
          this.error = this.helpers.error(err.error.message);
        });
    } else {
      let ent;
      this.subscribers.forEach(s => {
        this.rest.getNoSimplified('/history/entitlement/histories/?parameterName=subscriber_id&parameterValue='
          + s.id + '&fromDate=' + fromDate + '&toDate=' + toDate)
          .subscribe(res => {
            if (res === null) {
              this.info = {
                message: 'Not entries found'
              }
            } else {
              this.selectCategory('entitlement');
               ent = res;

              for (const e of ent) {
                this.histories.push(e);
              }
              ent.historyId = ent.id;
              // get snapshot
              if (this.views.snapshot) {
                this.getSnapshot(this.histories[0].revisionId, this.histories[0].resourceId,
                  this.histories[0].date, this.histories[0].auditor);
              }
            }
            this.changeView('H');
          }, err => {
            this.error = this.helpers.error(err.error.message);
          });
      })
    }
  }

  /**
   * GET ALL ENTITLEMENTS HISTORY
   * @param sub
   */
  // entitlementsHistory(sub ?: any) {
  //   this.histories = [];
  //   let ent;
  //   // if sub provided find histories only for sub; if not, find all subscribers entitlement histories
  //   let ents = this.entitlements;
  //   if (sub) {
  //     ents = sub.entitlements;
  //   }
  //   ents.forEach(e => {
  //     this.rest.getNoSimplified('/history/entitlement/' + e + '/histories')
  //       .subscribe(res => {
  //         if (res === null) {
  //           this.info = {
  //             message: 'Not entries found'
  //           }
  //         } else {
  //           this.selectCategory('entitlement');
  //            ent = res;
  //           ent.historyId = e;
  //           this.histories.push(ent);
  //           if (this.views.snapshot) {
  //             this.getSnapshot(this.histories[0].id, this.histories[0].historyId, this.histories[0].date, this.histories[0].auditor);
  //           }
  //         }
  //       }, err => {
  //         this.error = this.helpers.getError(err);
  //       });
  //   });
  // }

  /**
   * SELECT CATEGORY
   * @param category
   */
  selectCategory(category) {
    this.categorySelected = category;
  }

  /**
   * GET SNAPSHOT
   * @param id
   * @param historyId
   * @param snapDate
   * @param snapAuditor
   */
  getSnapshot(id, historyId, snapDate, snapAuditor) {

    // console.log(snapDate, snapAuditor.name);
    if ( snapDate) {
      this.snapDate = snapDate;
    }
    if (snapAuditor) {
      this.snapAuditor = snapAuditor.name;
    } else {
      this.snapAuditor = 'System';
    }

    /*
      If historyId snapshot from entitlements if not, historyId = this.id (customer id)
     */
    if (historyId === undefined || historyId == null) {
      historyId = this.id;
    }
    // check if category is subscriber
    if (this.categorySelected === 'subscriber') {
      historyId = this.subscriberSelected.id;
    }

    this.revisionId = id;
    this.changeView('S');

    this.rest.getNoSimplified('/history/' + this.categorySelected + '/' + historyId + '/histories/' + id + '/snapshot')
      .subscribe(res => {
        this.snapshot = res;
        // Format postcode
        if (this.snapshot.billingPostcode) {
          this.snapshot.billingPostcode = this.postcodeService.postcodeFormat(this.snapshot.billingPostcode);
        }
        if (this.snapshot.postcode) {
          this.snapshot.postcode = this.postcodeService.postcodeFormat(this.snapshot.postcode);
        }
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });
  }

  /**
   * CHANGE VIEW
   * @param view
   */
  changeView(view) {
    if (view === 'C' && this.categorySelected === 'entitlement') {
      this.info = {
        title: 'This option is not available for entitlements.'
      };
      return
    }

    // set all views to false
    Object.keys(this.views).forEach(v => this.views[v] = false);

    if (view === 'H') {
      this.views.history = true;
    } else if (view === 'C') {
      this.views.compare = true;
    } else if (view === 'R') {
      this.views.revision = true;
    } else if (view === 'S') {
      this.views.history = true;
      this.views.snapshot = true;
    }

    if (!this.views.compare) {
      // set all histories to unchecked
      this.histories.forEach(h => h.checked = false);
      this.compareItems = [];
      this.compareItemsDates = [];
    }
  }

  /**
   * GET ALL ENTITLEMENTS IDS
   */
  getAllEntitlements() {
    this.entitlements = [];
    let resp;
    for (let i = 0; i < this.subscribers.length; i++) {
      // add property 'entitlements' to save each subscriber entitlements ids
      this.subscribers[i].entitlements = [];

      this.rest.get('/entitlements/?q=subscriberId=' + this.subscribers[i].id)
        .subscribe(res => {
          resp = res;
          resp.forEach(e => {
            this.subscribers[i].entitlements.push(e.id);
            this.entitlements.push(e.id);
          }, err => {
            this.error = this.helpers.error(err.error.message);
          });
        });
    }
  }

  /**
   * CLOSE ALL OPEN HISTORIES
   */
  closeAll() {
    $('.collapse').removeClass('in');
  }

  // CREATE ARRAY WITH 2 HISTORY ITEMS TO COMPARE
  arrayCompare(checked, history) {
    // add checked property to keep status with change in pagination
    history.checked = !history.checked;
    console.log(history);
    // Declare const to avoid infinite for loop
    const length = this.compareItems.length;
    // If compareItems array is not empty
    if (length > 0) {
      if (!checked.target.checked) {
        for (let i = 0; i < length; i++) {
          if (this.compareItems[i] === history.revisionId && !checked.target.checked) {
            this.compareItems.splice(i, 1);
            this.compareItemsDates.splice(i, 1);
            break;
          }
        }
      } else {
        this.compareItemsDates.push(history.date);
        this.compareItems.push(history.revisionId);
      }
    } else {
      this.compareItemsDates.push(history.date);
      this.compareItems.push(history.revisionId);
    }
    // sort array
    this.compareItems.sort();
    this.compareItemsDates.sort();
    // compare history items
    this.compare();

    console.log('this.compareItemsDates', this.compareItemsDates)
    console.log('this.compareItems', this.compareItems)
  }

  /**
   * COMPARE HISTORIES
   */
  compare() {
    if (this.compareItems.length === 2) {
      let id = this.id;
      console.log(this.categorySelected);
      if (this.categorySelected === 'subscriber') {
        id = this.subscriberSelected.id
      }
      if (this.categorySelected === 'payment-token') {
        id = this.histories[0].resourceId;
      }

      this.rest.getNoSimplified('/history/' + this.categorySelected + '/' + id
        + '/histories/compare?left=' + this.compareItems[0] + '&right=' + this.compareItems[1])
        .subscribe(res => {
          if (!this.views.compare) {
            this.changeView('C');
          }
          this.compareResults = res;
          this.getMarketingPreferences(this.compareResults);
          this.compareMessage = 'Please select 2 history items.';
          if (this.compareResults === null) {
            this.compareResults = [];
            this.compareMessage = 'No differences found.'
          }
          console.log(this.compareResults);
        }, err => {
          this.error = this.helpers.error(err.error.message);
        })
    } else if (this.compareItems.length > 2) {
      this.info = {
        message: 'Please select only 2 items to compare.',
        title: this.compareItems.length + ' items selected'
      }
    } else {
      this.compareResults = [];
    }
  }

  /**
   * Get marketing preferences
   */
  getMarketingPreferences(cr) {
    this.marketingPreferences = null;
    cr.forEach(r => {
      if (r.propertyName === 'marketingPreferences') {
        this.marketingPreferences = r;
      }
    })
  }

  /**
   * Show history by date
   */
  showHistoryByDate() {
    if (this.categorySelected === 'entitlement') {
      this.getEntitlementsHistoryById();
    } else if (this.categorySelected === 'subscriber') {
      this.getHistories(this.subscriberSelected);
    } else {
      this.getHistories(this.categorySelected);
    }
  }

  /**
   * Get payment history snapshots
   * @param index
   * @param event
   */
  getPaymentSnapshots(index, event) {
    const resourceId = this.histories[index].resourceId;
    const revisionIdNew = this.histories[index].revisionId;
    const eClass = event.className;

    let revisionIdPrevious: string;
    if (this.histories[index - 1]) {
      revisionIdPrevious = this.histories[index - 1].revisionId;
    }
    this.paymentSnapshotPrevious = '';
    // get snapshots
    if (eClass.includes('collapsed')) {

      this.rest.getNoSimplified('/history/' + this.categorySelected + '/' + resourceId +
        '/histories/' + revisionIdNew + '/snapshot')
        .subscribe(res => {
          this.paymentSnapshotNew = res;
          console.log(this.paymentSnapshotNew);
        }, err => {
          this.error = this.helpers.error(err.error.message);
        });
      if (revisionIdPrevious) {
        this.rest.getNoSimplified('/history/' + this.categorySelected + '/' + resourceId +
          '/histories/' + revisionIdPrevious + '/snapshot')
          .subscribe(res => {
            this.paymentSnapshotPrevious = res;
            console.log(this.paymentSnapshotPrevious);
          }, err => {
            this.error = this.helpers.error(err.error.message);
          });

      }
    }

    // TODO: ADD POSTCODE PIPE OR SERVICE
  }
}
