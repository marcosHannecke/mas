"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var histories_service_1 = require("../../services/histories.service");
var helpers_service_1 = require("../../services/helpers.service");
var subscribers_service_1 = require("../../services/subscribers.service");
var products_service_1 = require("../../services/products.service");
var $ = require("jquery");
var HistoriesComponent = /** @class */ (function () {
    function HistoriesComponent(activatedRoute, helpers, historiesService, subscribersService, productsService) {
        this.activatedRoute = activatedRoute;
        this.helpers = helpers;
        this.historiesService = historiesService;
        this.subscribersService = subscribersService;
        this.productsService = productsService;
        // PAGINATION
        this.showNumber = 10;
        this.p = 1;
        this.minDate = new Date(2016, 0, 1);
        this.maxDate = new Date(2020, 0, 1);
        this.date1 = new Date();
        this.date2 = new Date();
        this.categorySelected = 'customer';
        this.order = true;
        this.entsViewSelected = 'All';
        this.compareItems = [];
        this.compareResults = [];
        this.views = {
            history: true,
            compare: false,
            revision: false,
            snapshot: false
        };
        this.showSubscribers = false;
    }
    HistoriesComponent.prototype.ngOnInit = function () {
        var _this = this;
        // GET CUSTOMER ID
        this.activatedRoute.params.subscribe(function (p) {
            _this.id = p.id;
        });
        if (sessionStorage.getItem('billingSubscribers')) {
            this.subscribers = JSON.parse(sessionStorage.getItem('billingSubscribers'));
            console.log('SUBSCRIBERS', this.subscribers);
        }
        // GET SUBSCRIBER STORED
        if (sessionStorage.getItem('subscriber')) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
            console.log('SUBSCRIBER', this.subscriber);
            // GET CUSTOMER
            this.getCustomer();
        }
        // GET HISTORIES
        this.getHistories('customer');
        // GET ALL ENTITLEMENT IDS
        this.getAllEntitlements();
    };
    // GET CUSTOMER
    HistoriesComponent.prototype.getCustomer = function () {
        var _this = this;
        this.subscribersService.getCustomer(this.subscriber.id)
            .subscribe(function (res) {
            _this.customer = res[0];
            console.log('CUSTOMER', _this.customer);
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // GET HISTORIES
    HistoriesComponent.prototype.getHistories = function (category) {
        var _this = this;
        var id = this.id;
        // check if category is this.subscribers
        if (typeof category === 'object') {
            this.subscriberSelected = category;
            id = category.id;
            category = 'subscriber';
        }
        this.historiesService.getHistories('history/' + category + '/' + id + '/histories')
            .subscribe(function (res) {
            if (JSON.parse(res['_body']) === null) {
                _this.info = {
                    message: 'Not entries found'
                };
            }
            else {
                _this.selectCategory(category);
                if (category === 'customer') {
                    _this.histories = JSON.parse(res['_body']);
                    if (_this.views.snapshot) {
                        _this.getSnapshot(_this.histories[0].id, null);
                    }
                }
                else if (category === 'subscriber') {
                    console.log('subscriber');
                    _this.histories = JSON.parse(res['_body']);
                    if (_this.views.snapshot) {
                        _this.getSnapshot(_this.histories[0].id, _this.subscriberSelected.id);
                    }
                }
            }
            _this.changeView('H');
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // GET ALL ENTITLEMENTS HISTORY
    HistoriesComponent.prototype.entitlementsHistory = function (sub) {
        var _this = this;
        this.histories = [];
        // if sub provided find histories only for sub; if not, find all subscribers entitlement histories
        var ents = this.entitlements;
        if (sub) {
            ents = sub.entitlements;
        }
        ents.forEach(function (e) {
            _this.historiesService.getHistories('history/entitlement/' + e + '/histories')
                .subscribe(function (res) {
                if (JSON.parse(res['_body']) === null) {
                    _this.info = {
                        message: 'Not entries found'
                    };
                }
                else {
                    _this.selectCategory('entitlement');
                    var ent = JSON.parse(res['_body'])[0];
                    ent.historyId = e;
                    _this.histories.push(ent);
                    if (_this.views.snapshot) {
                        _this.getSnapshot(_this.histories[0].id, _this.histories[0].historyId);
                    }
                }
            }, function (err) {
                _this.error = _this.helpers.getError(err);
            });
        });
    };
    // SELECT CATEGORY
    HistoriesComponent.prototype.selectCategory = function (category) {
        this.categorySelected = category;
    };
    // GET SNAPSHOT
    HistoriesComponent.prototype.getSnapshot = function (id, historyId) {
        var _this = this;
        /*
          If historyId snapshot from entitlements if not, historyId = this.id (customer id)
         */
        if (historyId === undefined || historyId == null) {
            historyId = this.id;
        }
        // check if category is subscriber
        if (this.categorySelected === 'subscriber') {
            historyId = this.subscriberSelected.id;
        }
        // this.helpers.scrollTop();
        this.revisionId = id;
        this.changeView('S');
        this.historiesService.getHistories('history/' + this.categorySelected + '/' + historyId + '/histories/' + id + '/snapshot')
            .subscribe(function (res) {
            _this.snapshot = JSON.parse(res['_body']);
            if (_this.categorySelected === 'entitlement') {
                _this.snapshot = JSON.stringify(_this.snapshot, null, '');
            }
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // CHANGE VIEW
    HistoriesComponent.prototype.changeView = function (view) {
        var _this = this;
        // set all views to false
        Object.keys(this.views).forEach(function (v) { return _this.views[v] = false; });
        if (view === 'H') {
            this.views.history = true;
        }
        else if (view === 'C') {
            this.views.compare = true;
        }
        else if (view === 'R') {
            this.views.revision = true;
        }
        else if (view === 'S') {
            this.views.history = true;
            this.views.snapshot = true;
        }
        if (!this.views.compare) {
            // set all histories to unchecked
            this.histories.forEach(function (h) { return h.checked = false; });
            this.compareItems = [];
        }
    };
    // GET ALL ENTITLEMENTS IDS
    HistoriesComponent.prototype.getAllEntitlements = function () {
        var _this = this;
        this.entitlements = [];
        var _loop_1 = function (i) {
            // add property 'entitlements' to save each subscriber entitlements ids
            this_1.subscribers[i].entitlements = [];
            this_1.productsService.getById('E', this_1.subscribers[i].id)
                .subscribe(function (res) {
                res.forEach(function (e) {
                    _this.subscribers[i].entitlements.push(e.id);
                    _this.entitlements.push(e.id);
                }, function (err) {
                    _this.error = _this.helpers.getError(err);
                });
            });
        };
        var this_1 = this;
        for (var i = 0; i < this.subscribers.length; i++) {
            _loop_1(i);
        }
    };
    // CLOSE ALL OPEN HISTORIES
    HistoriesComponent.prototype.closeAll = function () {
        $('.collapse').removeClass('in');
    };
    // CREATE ARRAY WITH 2 HISTORY ITEMS TO COMPARE
    HistoriesComponent.prototype.arrayCompare = function (checked, history) {
        // add checked property to keep status with change in pagination
        history.checked = !history.checked;
        console.log(history);
        // Declare const to avoid infinite for loop
        var length = this.compareItems.length;
        // If compareItems array is not empty
        if (length > 0) {
            if (!checked.target.checked) {
                for (var i = 0; i < length; i++) {
                    if (this.compareItems[i] === history.id && !checked.target.checked) {
                        this.compareItems.splice(i, 1);
                        break;
                    }
                }
            }
            else {
                this.compareItems.push(history.id);
            }
        }
        else {
            this.compareItems.push(history.id);
        }
        // sort array
        this.compareItems.sort();
        // compare history items
        this.compare();
    };
    // COMPARE HISTORIES
    HistoriesComponent.prototype.compare = function () {
        var _this = this;
        if (this.compareItems.length === 2) {
            // /history/customer/{resourceId}/histories/compare?left=xxx&right=xxx
            this.historiesService.getHistories('history/' + this.categorySelected + '/' + this.id
                + '/histories/compare?left=' + this.compareItems[0] + '&right=' + this.compareItems[1])
                .subscribe(function (res) {
                if (!_this.views.compare) {
                    _this.changeView('C');
                }
                _this.compareResults = JSON.parse(res['_body']);
                console.log(_this.compareResults);
            }, function (err) {
                _this.error = _this.helpers.getError(err);
            });
        }
        else if (this.compareItems.length > 2) {
            this.info = {
                message: 'Please select only 2 items to compare.',
                title: this.compareItems.length + ' items selected'
            };
        }
        else {
            this.compareResults = [];
        }
    };
    HistoriesComponent = __decorate([
        core_1.Component({
            selector: 'app-histories',
            templateUrl: './histories.component.html',
            styleUrls: ['./histories.component.css'],
            providers: [helpers_service_1.HelpersService, histories_service_1.HistoriesService, subscribers_service_1.SubscribersService, products_service_1.ProductsService],
        })
    ], HistoriesComponent);
    return HistoriesComponent;
}());
exports.HistoriesComponent = HistoriesComponent;
