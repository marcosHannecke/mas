"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var validators_service_1 = require("../../../services/validators.service");
var helpers_service_1 = require("../../../services/helpers.service");
var ResultsComponent = /** @class */ (function () {
    function ResultsComponent(helpers, actRouter, router, fb, validators) {
        this.helpers = helpers;
        this.actRouter = actRouter;
        this.router = router;
        this.fb = fb;
        this.validators = validators;
        this.searchParams = {};
        this.parameters = {};
        this.remoteResult = false;
        this.joinBox = false;
        this.viewingCardBox = false;
        this.createNewBox = false;
        this.addViewingCardBox = false;
        this.hidden = true;
        this.submitted = false;
        this.match = false;
    }
    ResultsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.vcnForm = this.fb.group({
            "viewingCardNumber": ['', [forms_1.Validators.required, this.validators.onlyNumbers]]
        });
        if (sessionStorage.getItem('multipleMatch') != null) {
            this.multipleMatch = JSON.parse(sessionStorage.getItem('multipleMatch'));
            console.log(this.multipleMatch, "multiple");
        }
        if (sessionStorage.getItem('remoteMatch') != null) {
            this.remoteResult = this.helpers.nullCheck(JSON.parse(sessionStorage.getItem('remoteMatch')));
            console.log(this.remoteResult, "remoteMatch");
        }
        if (sessionStorage.getItem('searchParameters') != null) {
            this.parameters = this.helpers.nullCheck(JSON.parse(sessionStorage.getItem('searchParameters')));
            console.log(this.parameters, "parameters");
        }
        // Get params for search
        this.actRouter
            .queryParams
            .subscribe(function (params) {
            _this.searchParams.postcode = params['pc'];
            _this.searchParams.viewingCardNumber = params['vcn'];
            _this.searchParams.addressLine1 = params['ad1'];
            _this.searchParams.addressLine2 = params['ad2'];
            _this.searchParams.surname = params['s'];
            console.log(_this.searchParams.viewingCardNumber);
        });
        this.vcnMatch();
    };
    ;
    // PROCEED JOIN SUBSCRIBER from one remote match
    ResultsComponent.prototype.proceedJoin = function () {
        var rR = this.remoteResult[0];
        if (this.surnameCheck) {
            rR.surname = this.parameters.surname;
        }
        if (this.postcodeCheck) {
            rR.postcode = this.parameters.postcode;
        }
        if (this.viewingCardNumberCheck) {
            rR.viewingCardNumber = this.parameters.viewingCardNumber;
        }
        if (this.addressLine1Check) {
            rR.addressLine1 = this.parameters.addressLine1;
        }
        if (this.addressLine2Check) {
            rR.addressLine2 = this.parameters.addressLine2;
        }
        sessionStorage.setItem('joinSubscriber', JSON.stringify(rR));
        this.router.navigate(['/joinSubscriber']);
    };
    // DOES THE CUSTOMER HAVE A VIEWING CARD?
    ResultsComponent.prototype.vcnCheck = function () {
        if (this.searchParams.viewingCardNumber.length > 0) {
            this.joinBox = true;
        }
        else {
            this.viewingCardBox = true;
        }
    };
    ResultsComponent.prototype.vcYes = function () {
        this.viewingCardBox = false;
        this.addViewingCardBox = true;
    };
    ResultsComponent.prototype.vcNo = function () {
        this.viewingCardBox = false;
        this.createNewBox = true;
    };
    // JOIN SUBSCRIBER?
    ResultsComponent.prototype.joinYes = function () {
        this.joinBox = false;
        this.router.navigate(['/joinSubscriber']);
    };
    ResultsComponent.prototype.joinNo = function () {
        this.joinBox = false;
        this.router.navigate(['/search']);
    };
    // Viewing Card Number match
    ResultsComponent.prototype.vcnMatch = function () {
        if (!this.remoteResult) {
            for (var _i = 0, _a = this.multipleMatch; _i < _a.length; _i++) {
                var subs = _a[_i];
                if (subs.viewingCardNumber == this.searchParams.viewingCardNumber) {
                    console.log('subs, search', subs.viewingCardNumber, this.searchParams.viewingCardNumber);
                    this.match = true;
                }
            }
        }
    };
    // ADD VIEWING CARD IF 'Customer not listed below' MULTIPLE LOCAL RESULTS
    ResultsComponent.prototype.addVCProceed = function (number) {
        sessionStorage.setItem('viewingCardNumber', number);
        this.router.navigate(['/joinSubscriber']);
    };
    ResultsComponent = __decorate([
        core_1.Component({
            selector: 'results',
            templateUrl: 'results.component.html',
            styles: ['.details .fa{color: white;padding: 4px 6px; border-radius: 50%;}' +
                    '.details .fa-map-marker{background: #e47700; padding: 4px 7px;}' +
                    '.details .fa-phone{background: #1b9fc6 }' +
                    'del {background: transparent}'],
            providers: [validators_service_1.ValidatorsService, helpers_service_1.HelpersService]
        })
    ], ResultsComponent);
    return ResultsComponent;
}());
exports.ResultsComponent = ResultsComponent;
