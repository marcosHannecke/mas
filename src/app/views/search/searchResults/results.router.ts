import {Routes, RouterModule} from "@angular/router";
import {ResultsComponent} from "./results.component";

const RESULTS_ROUTER: Routes = [
  {
    path: '',
    component: ResultsComponent,
  },
]

export const resultsRouter = RouterModule.forChild(RESULTS_ROUTER);
