import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ResultsComponent} from "./results.component";

import {resultsRouter} from "./results.router";

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule} from '@angular/router';


@NgModule({
  declarations: [ResultsComponent],
  imports     : [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, resultsRouter]
})

export class ResultsViewModule {}
