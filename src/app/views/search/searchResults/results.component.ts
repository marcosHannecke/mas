import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {Router, ActivatedRoute, NavigationExtras} from '@angular/router';
import {ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from '../../../services/helpers.service';
import {RestService} from '../../../services/rest.service';
import {PostcodeService} from '../../../services/postcode.service';
import {Location} from '@angular/common';

@Component({
  selector: 'results',
  templateUrl: 'results.component.html',
  styles: [  '.details .fa{color: white;padding: 4px 6px; border-radius: 50%;}' +
  '.details .fa-map-marker{background: #f7a230; padding: 4px 7px;}' +
  '.details .fa-phone{background: #1c7fe9 }' +
  'del {background: transparent}'],
  providers: [ ValidatorsService, HelpersService, PostcodeService]
})
export class ResultsComponent implements OnInit {
  vcnForm: FormGroup;
  subscriber: any;
  searchParams: SearchParams = <any>{};
  parameters: SearchParams = <any>{};
  error: any;
  searchError: any;
  multipleMatch: any;
  remoteResult: any = false;
  joinBox = false;
  viewingCardBox = false;
  createNewBox = false;
  addViewingCardBox = false;
  hidden = true;
  viewingCardNumber: number;
  submitted = false;
  match = false;

  surnameCheck: any;
  postcodeCheck: any;
  viewingCardNumberCheck: any;
  addressLine1Check: any;
  addressLine2Check: any;

  searchRemoteAvailable = false;

  constructor(private helpers: HelpersService, private actRouter: ActivatedRoute, private router: Router,
              private fb: FormBuilder, private validators: ValidatorsService, private rest: RestService,
              private postcodeService: PostcodeService, public location: Location) {}
  ngOnInit() {
    this.vcnForm = this.fb.group({
      'viewingCardNumber': ['', [Validators.required, this.validators.onlyNumbers]]
    });

    if (sessionStorage.getItem('multipleMatch') != null) {
      this.multipleMatch = JSON.parse(sessionStorage.getItem('multipleMatch'));
      console.log(this.multipleMatch, 'multiple');
    }

    if (sessionStorage.getItem('remoteMatch') != null) {
      this.remoteResult = this.helpers.nullCheck(JSON.parse(sessionStorage.getItem('remoteMatch')));
      console.log(this.remoteResult, 'remoteMatch')
    }
    if (sessionStorage.getItem('searchParameters') != null) {
      this.parameters = this.helpers.nullCheck(JSON.parse(sessionStorage.getItem('searchParameters')));
      console.log(this.parameters, 'parameters')
    }

    if (sessionStorage.getItem('searchForm')) {
      const json = JSON.parse(sessionStorage.getItem('searchForm'));
      if (json.postcode && json.addressLine1 && json.addressLine2) {
        this.searchRemoteAvailable = true;
      }
    }
    // Get params for search
    this.actRouter
      .queryParams
      .subscribe(params => {
        this.searchParams.postcode = params['pc'];
        this.searchParams.viewingCardNumber = params['vcn'];
        this.searchParams.addressLine1 = params['ad1'];
        this.searchParams.addressLine2 = params['ad2'];
        this.searchParams.surname = params['s'];

        console.log(this.searchParams.viewingCardNumber);
      });

    this.vcnMatch();
  };

  /**
   * PROCEED JOIN SUBSCRIBER from one remote match
   */
  proceedJoin() {
    const rR = this.remoteResult[0];
    if (this.surnameCheck) {
      rR.surname = this.parameters.surname;
    }
    if (this.postcodeCheck) {
      rR.postcode = this.parameters.postcode;
    }
    if (this.viewingCardNumberCheck) {
      rR.viewingCardNumber = this.parameters.viewingCardNumber;
    }
    if (this.addressLine1Check) {
      rR.addressLine1 = this.parameters.addressLine1;
    }
    if (this.addressLine2Check) {
      rR.addressLine2 = this.parameters.addressLine2;
    }
    sessionStorage.setItem('joinSubscriber', JSON.stringify(rR));
    this.router.navigate(['/joinSubscriber']);
  }

  /**
   * Search remote
   */
  searchRemote() {
    if (sessionStorage.getItem('searchForm') != null) {
      const json = JSON.parse(sessionStorage.getItem('searchForm'));
      let remoteResults;
      this.rest.get('/subscribers/search-remote?postcode=' + json.postcode + '&addressLine1=' + json.addressLine1
        + '&addressLine2=' + json.addressLine2 + '&surname=' + json.surname)
        .subscribe(res => {
            remoteResults = res;
            console.log(res);
            if (remoteResults.length === 0) {
              // NO MATCH
              this.searchError = {
                message2: 'Details provided do not match any records held by Sky.',
                message3: 'Please double check the Customer\'s address or try the Customer\'s previous address.'
              };
            } else if (remoteResults.length === 1) {
              // ONE MATCH
              if (remoteResults[0].viewingCardNumber !== json.viewingCardNumber) {
                console.log(remoteResults[0].viewingCardNumber, json.viewingCardNumber);
                this.searchError = {
                  message2: 'Details provided do not match the records held by Sky.',
                  message3: 'Please double check the Viewing Card in the Sky box and the Customer\'s address.'
                };
              } else {
                // replace special characters in surname and store 'remoteMatch' and go to /results
                this.replaceStoreNavigate(res);
              }

            } else if (remoteResults.length > 1) {
              // MULTIPLE MATCH
              this.router.navigate(['/joinSubscriber']);
            } else {
              console.log('no results');
              this.router.navigate(['search']);
            }
          },
          err => {
            this.searchError = this.helpers.error(err.error.message);
            if (this.searchError.message.includes('Multiple')) {
              this.router.navigate(['/joinSubscriber']);
            }
          });

    }
  }

  // replace special characters and store 'remoteMatch' and go to /results
  replaceStoreNavigate(res) {
    // res[0].surname = res[0].surname.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'');
    // res[0].initials = res[0].initials.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'');
    res[0] = this.helpers.removeSpecialCharacters(res[0]);
    // Format postcode
    res[0].postcode = this.postcodeService.postcodeFormat(res[0].postcode);
    // Capitalize Surname
    res[0].surname = this.helpers.toTitleCase(res[0].surname);

    console.log('res[0', res[0]);
    sessionStorage.setItem('oneResult', 'true');
    sessionStorage.setItem('remoteMatch', JSON.stringify(res));
    this.router.navigate(['/results']);
  }

  /**
   * DOES THE CUSTOMER HAVE A VIEWING CARD?
   */
  vcnCheck() {
    if (this.searchParams.viewingCardNumber.length > 0) {
      this.joinBox  = true;
    }else {
      this.viewingCardBox = true;
    }
  }
  vcYes() {
    this.viewingCardBox = false;
    this.addViewingCardBox = true;
  }
  vcNo() {
    this.viewingCardBox = false;
    this.createNewBox = true;
  }

  /**
   * JOIN SUBSCRIBER?
   */
  joinYes() {
    this.joinBox = false;
    this.searchRemote();
    // this.router.navigate(['/joinSubscriber']);
  }
  joinNo() {
    this.joinBox = false;
    this.router.navigate(['/search']);
  }

  /**
   * Viewing Card Number match
   */
  vcnMatch() {
    if (!this.remoteResult) {
    for (const subs of this.multipleMatch){
        if (subs.viewingCardNumber === this.searchParams.viewingCardNumber) {
          console.log('subs, search', subs.viewingCardNumber, this.searchParams.viewingCardNumber);
          this.match = true ;
        }
    }
    }
  }

  /**
   * ADD VIEWING CARD IF 'Customer not listed below' MULTIPLE LOCAL RESULTS
   * @param number
   */
  addVCProceed(number) {
    sessionStorage.setItem('viewingCardNumber', number);
    this.searchRemote();
    // this.router.navigate(['/joinSubscriber']);
  }

}

/**
 * INTERFACES
 */
interface SearchParams {
  postcode: string;
  viewingCardNumber: string;
  addressLine1: string;
  addressLine2: string;
  surname: string;
}
