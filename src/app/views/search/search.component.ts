import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ValidatorsService} from '../../services/validators.service';
import {HelpersService} from '../../services/helpers.service';
import {PostcodeService} from '../../services/postcode.service';
import {RestService} from '../../services/rest.service';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: 'search.component.html',
  providers: [ValidatorsService, HelpersService, PostcodeService, RestService ]
})
export class SearchComponent implements OnInit {
  headers: any;
  options: any;
  searchForm: FormGroup;
  searchRemoteForm: FormGroup;
  formData: Data;
  joinBox = false;
  viewingCardBox = false;
  createNewBox = false;
  addViewingCardBox = false;
  showSearchRemote = false;
  subscriber: any;
  subscribers: any;
  noResults = false;
  submitted= false;
  overlay = false;
  skySubscriber: any;
  packages: any;
  error: any;
  info: any;
  searchError: any;
  postcode: any;
  addressList: any;
  navigationExtras: NavigationExtras;
  disableInput = false;
  addressSelectedCheck = false;
  addressSelected = false;
  validPostcode: boolean;
  role: string;

  readOnlyCSI = false;
  readOnlySI = false;
  readOnlyCI = false;
  readOnlySearch = false;

  @Input()
  advanced: boolean;

  @Input()
  customerPage: boolean;

  @Input()
  customerSearchDetails: any;

  @Output() closeCustomerSearch: EventEmitter<any> = new EventEmitter();

  constructor(private router: Router, private fb: FormBuilder, public helpers: HelpersService,
              private validators: ValidatorsService, private postcodeService: PostcodeService,
              private rest: RestService) {}

  ngOnInit() {
    // if search is not dialog in customer page
    if (!this.customerPage) {
      // REMOVE ALL SESSION STORAGE BUT KEEP 'auth_token
      this.helpers.resetSession();
      // Get kickbox balance
      this.getKickboxBalance();
    }

    if (sessionStorage.getItem('role') == null) {
      this.getRole();
    }else {
      this.role = sessionStorage.getItem('role');
    }

    if (this.customerSearchDetails) {
      console.log(this.customerSearchDetails);
      this.showSearchRemote = true;
      this.addressSelected = true;
      this.disableInput = true;
      // FORMS AND VALIDATORS
      this.searchForm = this.fb.group({
        'postcode': [this.customerSearchDetails.postcode, [  Validators.maxLength(9), this.validators.specialChar]],
        // "surname": ['', [ this.validators.letterSpaces, this.validators.specialChar]],
        'surname': [this.customerSearchDetails.surname],
        'viewingCardNumber': ['', [this.validators.onlyNumbers]],
        'cardSubscriberId': ['', [this.validators.onlyNumbers]],
        'customerId': ['', [this.validators.onlyNumbers]],
        'subscriberId': ['', [this.validators.onlyNumbers]]
      });

      this.searchRemoteForm = this.fb.group({
        'addressLine1': [this.customerSearchDetails.addressLine1, [Validators.required]],
        'addressLine2': [this.customerSearchDetails.addressLine2, [Validators.required]],
        'addressLine3': [this.customerSearchDetails.addressLine3],
        'addressLine4': [this.customerSearchDetails.addressLine4],
        'addressLine5': [this.customerSearchDetails.addressLine5],
      });
    }  else {
      // FORMS AND VALIDATORS
      this.searchForm = this.fb.group({
        'postcode': ['', [  Validators.maxLength(9), this.validators.specialChar]],
        // "surname": ['', [ this.validators.letterSpaces, this.validators.specialChar]],
        'surname': [''],
        'viewingCardNumber': ['', [this.validators.onlyNumbers]],
        'cardSubscriberId': ['', [this.validators.onlyNumbers]],
        'customerId': ['', [this.validators.onlyNumbers]],
        'subscriberId': ['', [this.validators.onlyNumbers]]
      });

      this.searchRemoteForm = this.fb.group({
        'addressLine1': ['', [Validators.required]],
        'addressLine2': ['', [Validators.required]],
        'addressLine3': [''],
        'addressLine4': [''],
        'addressLine5': [''],
      });
    }

  };

  /**
   * Postcode validation before search
   */
  postcodeValidation() {
    const postcode = this.searchForm.value.postcode.replace(' ', '');
    this.validPostcode = false;

    if (postcode.length === 0 ) {
      this.validPostcode = true;
      this.search()
    }else {
      this.rest.get('/postcode?postcode=' + postcode)
        .subscribe(res => {
          if (res[0] ||
            postcode.toUpperCase() === 'ROI' ||
            ((postcode.toUpperCase() === 'CI' || postcode.toUpperCase() === 'IOM') && this.role === 'admin' )) {
            this.validPostcode = true;
            console.log('here');
          }
          this.search();
        }, err => {
          this.error = this.helpers.error(err.error.message);
          this.validPostcode = true;
          this.search();
        });
    }
  }

  /**
   * Get user role
   */
  getRole() {
    this.rest.getNoSimplified('/principals/about-me')
      .subscribe(res => {
        if (res['roles'][0]) {
          if (res['roles'][0].name === 'ROLE_ADMIN' || res['roles'][0].name === 'ROLE_SUPER_ADMIN'
            || res['roles'][0].name === 'ROLE_SUPERVISOR') {
            this.role = 'admin';
          } else if (res['roles'][0].name === 'ROLE_USER') {
            this.role = 'user';
          }
        }else {
          this.role = 'user';
        }
      }, err => {
        this.role = 'user';
        console.log('about-me error', err);
      })
  }

  /**
   * Search
   */
  search() {
    sessionStorage.setItem('viewingCardNumber', this.searchForm.value.viewingCardNumber);
    // remove special characters in surname
    const formSurname = this.searchForm.value.surname.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
    this.searchForm.patchValue({surname: this.helpers.toTitleCase(formSurname)});
    const searchForm = (<any>Object).assign(this.searchForm.value, this.searchRemoteForm.value);
    sessionStorage.setItem('searchForm',  JSON.stringify(searchForm));
    // Prepare search queries to send
    const f = this.searchForm.value;
    f.surname = this.helpers.toTitleCase(f.surname);
    sessionStorage.setItem('searchParameters', JSON.stringify(searchForm));

    if (f.viewingCardNumber.length > 0) {
      this.addressSelected = true;
    }

    this.navigationExtras = {
      queryParams: {
        'pc': f.postcode,
        's': f.surname,
        'ad1': f.addressLine1,
        'ad2': f.addressLine2,
        'vcn': f.viewingCardNumber
      }
    };
    this.submitted = true;

    if (f.cardSubscriberId.length > 0) {
      // Advanced search by cardSubscriberId
      this.rest.get('/subscribers/?q=cardSubscriberId=' + f.cardSubscriberId)
        .subscribe(res => {
          if (res[0]) {
            this.router.navigate(['/viewSubscriber/', res[0].id])
          }else {
            this.error = {
              message2: 'Subscriber not Found'
            }
          }
        }, () => {
          this.error = {
            message2: 'Subscriber not Found'
          }
        });
    }else if (f.subscriberId.length > 0) {
      // Advanced search by subscriber id
      this.rest.get('/subscribers/' + f.subscriberId)
        .subscribe(res => {
          this.router.navigate(['/viewSubscriber/', f.subscriberId])
        }, err => {
          this.error = {
            message2: 'Subscriber not Found'
          }
        });
    }else if (f.customerId.length > 0) {
      // Advanced search by customerId
      this.rest.get('/subscribers/?q=customerId=' + f.customerId)
        .subscribe(res => {
          if (res[0]) {
            sessionStorage.setItem('subscriber', JSON.stringify(res[0]));
            this.router.navigate(['/customer/', res[0].customerId])
          } else {
            this.error = {
              message: 'Customer not Found'
            }
          }
        }, () => {
          this.error = {
            message: 'Customer not Found'
          }
        });
    }else {
      // Search
      if (this.searchForm.valid && this.addressSelected && this.validPostcode) {
        if (this.customerPage && this.searchForm.value.viewingCardNumber.length === 0) {
          this.error = {
            message2: 'Viewing Card Number is required for this search'
          }
        } else {
          this.searchLocal();
        }
      } else if (!this.addressSelected) {
        this.addressSelectedCheck = true;
      } else if (!this.validPostcode) {
        this.error = {
          description: 'Please enter a valid postcode'
        }
      }
    }
  }

  /**
   * SEARCH LOCAL
   */
  searchLocal() {
    console.log(this.searchForm.value);
    const json = this.searchForm.value;
    this.rest.get('/subscribers/search-local?postcode=' + json.postcode + '&addressLine1=' + json.addressLine1 + '&addressLine2='
      + json.addressLine2 + '&viewingCardNumber=' + json.viewingCardNumber + '&surname=' + json.surname)
      .subscribe(res => {
          this.subscriber = res;
          // Match viewing card number provided and VCN found (return false == match)
          const m = this.vcnMatch();
          if (this.subscriber.length === 0) { // 0 local match
            // /*Display address fields*/
            this.showSearchRemote = true;
            if (this.searchRemoteForm.valid) {
              this.vcnCheck();
            }
          }else if (this.subscriber.length  === 1 ) { // 1 local match
            if (this.customerPage ) { // if coming from view customer
              if (!m) {  // vcn match with search form
                this.error = {
                  message2: 'An account with this Viewing Card Number already exists'
                };
              } else {
                this.searchRemote();
              }
            } else { // search page
              if (!m) { // vcn match with search form
                // this.router.navigate(['viewSubscriber', res[0].id ] );
                sessionStorage.setItem('multipleMatch', JSON.stringify(res));
                this.router.navigate(['/results'], this.navigationExtras);
              }else {
                sessionStorage.setItem('multipleMatch', JSON.stringify(res));
                this.router.navigate(['/results'], this.navigationExtras);
              }
            }

          }else if (this.subscriber.length > 1 ) { // multiple local matches
            if (this.customerPage) { // if coming from customer
              this.searchRemote();
            } else {
              sessionStorage.setItem('multipleMatch', JSON.stringify(res));
              this.router.navigate(['/results'], this.navigationExtras);
            }
          } else if (!this.customerPage) {
            console.log('no results');
            this.router.navigate(['search'] );
          }
        },
        err => {
          // ERRORS BEFORE SHOWING ADL1 & ADL2
          if (!this.showSearchRemote) {
            if (this.searchForm.value.viewingCardNumber.length > 0) {
              if (this.searchForm.value.surname.length > 0) {
                this.showSearchRemote = true;
              } else if (this.searchForm.value.postcode.length > 0) {
                if (!this.showSearchRemote) {
                  this.addressSelectedCheck = true;
                } else {
                  this.searchError = {
                    notFound: 'We do not have a record for this customer.',
                    message: 'Please enter customer surname.'
                  }
                }
              } else {
                this.showSearchRemote = true;
              }
            } else if (this.searchForm.value.surname.length > 0 && this.searchForm.value.postcode.length > 0) {
              this.searchError = this.helpers.error(err.error.message);
            }
          }else if (this.showSearchRemote) {
            if (this.searchForm.value.postcode.lenght > 0  && this.searchForm.value.surname.lenght > 0 ) {
              this.joinBox = true;
            }else if (this.searchForm.value.viewingCardNumber.length === 0) {
              this.searchError = this.helpers.error(err.error.message);
            }else if (this.searchForm.value.addressLine1.length >  0 && this.searchForm.value.addressLine2.length >  0) {
              this.joinBox = true;
            }
          }
        });
  }

  /**
   * SEARCH REMOTE
   */
  searchRemote() {
    this.formData = this.searchForm.value;
    this.formData.addressLine1 = this.searchRemoteForm.value.addressLine1;
    this.formData.addressLine2 = this.searchRemoteForm.value.addressLine2;

    console.log(this.formData);
    const json = this.formData;
    let remoteResults;
    this.rest.get('/subscribers/search-remote?postcode=' + json.postcode + '&addressLine1=' + json.addressLine1
      + '&addressLine2=' + json.addressLine2 + '&surname=' + json.surname)
      .subscribe(res => {
          remoteResults = res;
          console.log(res);
          if (remoteResults.length === 0) {
            // NO MATCH
            this.error = {
              message2: 'Details provided do not match any records held by Sky.',
              message3: 'Please double check the Customer\'s address or try the Customer\'s previous address.'
            };
          } else if (remoteResults.length === 1) {
            // ONE MATCH
            if (remoteResults[0].viewingCardNumber !== json.viewingCardNumber) {
              console.log(remoteResults[0].viewingCardNumber, json.viewingCardNumber);
              this.error = {
                message2: 'Details provided do not match the records held by Sky.',
                message3: 'Please double check the Viewing Card in the Sky box and the Customer\'s address.'
              };
            } else {
              // replace special characters in surname and store 'remoteMatch' and go to /results
              this.replaceStoreNavigate(res);
            }
          } else if (remoteResults.length > 1) {
            // MULTIPLE MATCH
            this.router.navigate(['/joinSubscriber']);
          } else {
            console.log('no results');
            this.router.navigate(['search']);
          }
        },
        err => {
          this.error = this.helpers.error(err.error.message);
          if (this.error.message.includes('Multiple')) {
            this.router.navigate(['/joinSubscriber']);
          }
        });
  }

  /**
   * replace special characters and store 'remoteMatch' and go to /results
   * @param res
   */
  replaceStoreNavigate(res) {
    // res[0].surname = res[0].surname.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'');
    // res[0].initials = res[0].initials.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'');
    res[0] = this.helpers.removeSpecialCharacters(res[0]);
    // Format postcode
    res[0].postcode = this.postcodeService.postcodeFormat(res[0].postcode);
    // Capitalize Surname
    res[0].surname = this.helpers.toTitleCase(res[0].surname);

    console.log('res[0', res[0]);
    sessionStorage.setItem('oneResult', 'true');
    sessionStorage.removeItem('multipleMatch');
    sessionStorage.setItem('remoteMatch', JSON.stringify(res));
    this.router.navigate(['/results']);
  }

  /**
   * DOES THE CUSTOMER HAVE A VIEWING CARD?
   */
  vcnCheck() {
    if (this.searchForm.value.viewingCardNumber.length > 0) {
      this.joinBox  = true;
    }else {
      if (!this.customerPage) {
        this.viewingCardBox = true;
      } else {
        this.error = {
          message2: 'Viewing Card Number is required for this search'
        }
      }
    }
  }

  /**
   * FIND ADDRESS
   */
  findAddress() {
    const postcode = this.searchForm.value.postcode.replace(' ', '');
    this.rest.get('/postcode?postcode=' + postcode)
      .subscribe(res => {
        console.log('response', res);
        this.addressList = res;
        if (this.addressList.length === 0) {
          this.addressList = [{
            notFound : 'NO ADDRESS FOUND'
          }]
        }
      }, err => {
        if (this.searchForm.value.postcode.length === 0) {
          this.error = {
            description: 'Please Enter a Postcode.'
          }
        }else {
          this.error = this.helpers.error(err.error.message);
        }
      });
  }

  /**
   * SET ADDRESS
   * @param address
   */
  setAddress(address) {
    if (address.notFound) {
      this.addressList = null;
      this.disableInput = false;
    }else {
      this.addressList = null;
      this.disableInput = true;
      this.showSearchRemote = true;
      this.addressSelected = true;
      // this.addressManually = true;
      const add = this.helpers.nullCheck(address);
      this.searchRemoteForm.controls['addressLine1'].setValue(add.addressLine1.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.searchRemoteForm.controls['addressLine2'].setValue(add.addressLine2.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.searchRemoteForm.controls['addressLine3'].setValue(add.addressLine3.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.searchRemoteForm.controls['addressLine4'].setValue(add.addressLine4.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.searchRemoteForm.controls['addressLine5'].setValue(add.addressLine5.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
    }
  }

  vcYes() {
    this.viewingCardBox = false;
    this.addViewingCardBox = true;
  }
  vcNo() {
    this.viewingCardBox = false;
    this.createNewBox = true;
  }

  /**
   * JOIN SUBSCRIBER?
   */
  joinYes() {
    this.joinBox = false;
    this.searchRemote();
  }
  joinNo() {
    this.joinBox = false;
    // this.router.navigate(['/search']);
  }

  /**
   * Viewing Card Number match
    * @returns {boolean}
   */
  vcnMatch() {
    for (const subs of this.subscriber){
      if (subs.viewingCardNumber != null) {
        if (subs.viewingCardNumber === this.searchForm.value.viewingCardNumber) {
          console.log(subs.viewingCardNumber, this.searchForm.value.viewingCardNumber);
          return false;
        }
      }
      return true;
    }
  }
  /**
   * Get kickbox balance
   */
  getKickboxBalance() {
    this.helpers.getRole()
    .subscribe (res => {
      if (res['roles'][0].name.toLowerCase().includes('admin')) {
        this.rest.get('/email-verifications/get-balance')
          .subscribe(resp => {
            const balance = resp['balance'];
            if (balance <= 100) {
              this.info = {
                message: 'Your KICKBOX balance is below  100. ',
                message2: 'Balance: ' + balance
              }
            }
          }, () => this.error = { message2: 'Error getting Kickbox balance. Please contact system administrator.' })
      }
    });


  }
  // CSI || SI || CI SEARCH

  readOnly() {
    const f  = this.searchForm.value;
    this.readOnlyCSI = false;
    this.readOnlySI = false;
    this.readOnlyCI = false;
    this.readOnlySearch = false;
    if (f.cardSubscriberId.length > 0) {
      this.readOnlySI = true;
      this.readOnlyCI = true;
      this.readOnlySearch = true;
    }else if (f.subscriberId.length > 0) {
      this.readOnlyCSI = true;
      this.readOnlyCI = true;
      this.readOnlySearch = true;
    }else if (f.customerId.length > 0) {
      this.readOnlyCSI = true;
      this.readOnlySI = true;
      this.readOnlySearch = true;
    }
  }

  /**
   * Close customer search dialog
   */
  customerPageBack() {
    this.closeCustomerSearch.emit(null);
  }

}


interface Data {
  'surname': string,
  'postcode': string,
  'viewingCardNumber': string,
  'addressLine1': string,
  'addressLine2': string,
}
