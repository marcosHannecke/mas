/** @class */ (function () {
    function class_1() {
    }
    return class_1;
}());
"row wrapper border-bottom white-bg page-heading" >
    /** @class */ (function () {
        function class_2() {
        }
        return class_2;
    }());
"col-lg-10" >
    /** @class */ (function () {
        function class_3() {
        }
        return class_3;
    }());
"breadcrumb" >
    /** @class */ (function () {
        function class_4() {
        }
        return class_4;
    }());
"active" >
    Search;
Subscriber < /strong>
    < /li>
    < /ol>
    < /div>
    < /div>
    < div;
var default_1 = /** @class */ (function () {
    function default_1() {
    }
    return default_1;
}());
"wrapper wrapper-content  fadeInRight" * ngIf;
"!packages"(click) = "addressList = null" >
    /** @class */ (function () {
        function class_5() {
        }
        return class_5;
    }());
"container m-t-md" >
    /** @class */ (function () {
        function class_6() {
        }
        return class_6;
    }());
"row" >
    /** @class */ (function () {
        function class_7() {
        }
        return class_7;
    }());
"col-lg-8 col-lg-offset-2 " >
    /** @class */ (function () {
        function class_8() {
        }
        return class_8;
    }());
"ibox ibox-main float-e-margins" >
    /** @class */ (function () {
        function class_9() {
        }
        return class_9;
    }());
"ibox-title" >
    Search;
Subscriber < /h5>
    < div;
var default_2 = /** @class */ (function () {
    function default_2() {
    }
    return default_2;
}());
"ibox-tools" >
    title;
"Refresh Search";
title = "Refresh Search"(click) = "helpers.reset()" > /** @class */ (function () {
    function class_10() {
    }
    return class_10;
}());
"fa fa-refresh";
aria - hidden;
"true" > /i></a >
    /div>
    < /div>
    < div;
var default_3 = /** @class */ (function () {
    function default_3() {
    }
    return default_3;
}());
"ibox-content" >
;
"searchForm" * ngIf;
"!subscribers && !skySubscriber";
autocomplete = "off" >
    /** @class */ (function () {
        function class_11() {
        }
        return class_11;
    }());
"form-group" >
    /** @class */ (function () {
        function class_12() {
        }
        return class_12;
    }());
"control-label" > Surname < /label>
    < input;
type = "text";
var default_4 = /** @class */ (function () {
    function default_4() {
    }
    return default_4;
}());
"form-control";
name = "surname";
formControlName = "surname";
surname >
     * ngIf;
"searchForm.controls['surname'].invalid  && (searchForm.controls['surname'].touched || submitted)";
var default_5 = /** @class */ (function () {
    function default_5() {
    }
    return default_5;
}());
"error" >
    Surname;
is;
required.(Only, letters)
    < /div>
    < /div>
    < div;
var default_6 = /** @class */ (function () {
    function default_6() {
    }
    return default_6;
}());
"form-group" >
    /** @class */ (function () {
        function class_13() {
        }
        return class_13;
    }());
"control-label" > Postcode < /label>
    < span;
var default_7 = /** @class */ (function () {
    function default_7() {
    }
    return default_7;
}());
"input-group" >
    type;
"text";
formControlName = "postcode";
name = "postcode";
var default_8 = /** @class */ (function () {
    function default_8() {
    }
    return default_8;
}());
"form-control";
maxlength = "9";
autocomplete = "off" >
    /** @class */ (function () {
        function class_14() {
        }
        return class_14;
    }());
"input-group-addon btn-white"[ngClass] = "{'bblue white': searchForm.value.postcode.length > 0}" * ngIf;
"searchForm.value.postcode.length > 0; else buttonDisabled"(click) = "findAddress()" > Find;
address < /a>
    < ng - template;
buttonDisabled >
    /** @class */ (function () {
        function class_15() {
        }
        return class_15;
    }());
"input-group-addon btn-white"[ngClass] = "{'bblue white': searchForm.value.postcode.length > 0}";
disabled > Find;
address < /a>
    < /ng-template>
    < /span>
    < div * ngIf;
"searchForm.controls['postcode'].invalid  && (searchForm.controls['postcode'].touched || submitted)";
var default_9 = /** @class */ (function () {
    function default_9() {
    }
    return default_9;
}());
"error" >
    No;
special;
characters;
allowed
    < /div>
    < div;
var default_10 = /** @class */ (function () {
    function default_10() {
    }
    return default_10;
}());
"findAddress" * ngIf;
"addressList" >
     * ngFor;
"let address of addressList"(click) = "setAddress(address)" >
    {};
{
    address.notFound;
}
{
    {
        address.addressLine1;
    }
}
{
    {
        address.addressLine2;
    }
}
{
    {
        address.addressLine3;
    }
}
{
    {
        address.addressLine4;
    }
}
{
    {
        address.addressLine5;
    }
}
/a>
    < /li>
    < /ul>
    < button;
var default_11 = /** @class */ (function () {
    function default_11() {
    }
    return default_11;
}());
"btn btn-primary"(click) = "showSearchRemote = true; disableInput = false; addressSelected = true";
style = "margin: 0; width: 100%;" > Add;
address;
manually. < /button>
    < /div>
    < /div>
    < div;
var default_12 = /** @class */ (function () {
    function default_12() {
    }
    return default_12;
}());
"form-group" >
    /** @class */ (function () {
        function class_16() {
        }
        return class_16;
    }());
"control-label" > Viewing;
Card;
Number < /label>
    < input;
type = "text";
var default_13 = /** @class */ (function () {
    function default_13() {
    }
    return default_13;
}());
"form-control";
name = "viewingCardNumber";
maxlength = "9";
formControlName = "viewingCardNumber";
viewingCardNumber(keyup) = "addViewingCardBox = false";
autocomplete = "off" >
     * ngIf;
"searchForm.controls['viewingCardNumber'].invalid  && (searchForm.controls['viewingCardNumber'].touched || submitted)";
var default_14 = /** @class */ (function () {
    function default_14() {
    }
    return default_14;
}());
"error" >
    Only;
Numbers
    < /div>
    < div * ngIf;
"addViewingCardBox";
var default_15 = /** @class */ (function () {
    function default_15() {
    }
    return default_15;
}());
"warning" >
    Please;
add;
viewing;
card;
number.
    < /div>
    < /div>
    < /form>
    < form[formGroup];
"searchRemoteForm" * ngIf;
"showSearchRemote";
autocomplete = "off" >
    /** @class */ (function () {
        function class_17() {
        }
        return class_17;
    }());
"form-group" >
    /** @class */ (function () {
        function class_18() {
        }
        return class_18;
    }());
"control-label" > Address;
Line;
1 < /label>
    < input;
type = "text";
var default_16 = /** @class */ (function () {
    function default_16() {
    }
    return default_16;
}());
"form-control";
name = "addressLine1";
formControlName = "addressLine1";
addressLine1[readonly] = "disableInput" >
     * ngIf;
"searchRemoteForm.controls['addressLine1'].hasError('required')  && (searchRemoteForm.controls['addressLine1'].touched || submitted)";
var default_17 = /** @class */ (function () {
    function default_17() {
    }
    return default_17;
}());
"warning" >
    Please;
provide;
address;
line;
1.
    < /div>
    < !-- < h3;
var default_18 = /** @class */ (function () {
    function default_18() {
    }
    return default_18;
}());
"warning" * ngIf;
"showSearchRemote" > Please;
provide;
address;
line;
1. < /h3>-->
    < /div>
    < div;
var default_19 = /** @class */ (function () {
    function default_19() {
    }
    return default_19;
}());
"form-group" >
    /** @class */ (function () {
        function class_19() {
        }
        return class_19;
    }());
"control-label" > Address;
Line;
2 < /label>
    < input;
type = "text";
var default_20 = /** @class */ (function () {
    function default_20() {
    }
    return default_20;
}());
"form-control";
name = "addressLine2";
formControlName = "addressLine2";
addressLine2[readonly] = "disableInput" >
     * ngIf;
"searchRemoteForm.controls['addressLine2'].hasError('required')  && (searchRemoteForm.controls['addressLine2'].touched || submitted)";
var default_21 = /** @class */ (function () {
    function default_21() {
    }
    return default_21;
}());
"warning" >
    Please;
provide;
address;
line;
2.
    < /div>
    < /div>
    < /form>
    < div;
var default_22 = /** @class */ (function () {
    function default_22() {
    }
    return default_22;
}());
"form-group m-t-lg" >
    type;
"button";
var default_23 = /** @class */ (function () {
    function default_23() {
    }
    return default_23;
}());
"btn btn-primary";
value = "Search"(click) = "search()" >
    /div>
    < /div>
    < /div>
    < /div>
    < /div>
    < /div>
    < /div>
    < !--;
--;
--;
--;
-- -
    DIALOG;
BOXES;
--;
--;
--;
--;
--;
-- >
    /** @class */ (function () {
        function class_20() {
        }
        return class_20;
    }());
"overlay info" * ngIf;
"overlay" >
    /** @class */ (function () {
        function class_21() {
        }
        return class_21;
    }());
"overlayBox  fadeIn" >
    /** @class */ (function () {
        function class_22() {
        }
        return class_22;
    }());
"overlayContent text-center" >
    No;
local;
results < br > Search;
on;
Sky ? /h2>
    < div : /** @class */ (function () {
    function class_23() {
    }
    return class_23;
}()) = "overlayButtons m-t-lg" >
    /** @class */ (function () {
        function class_24() {
        }
        return class_24;
    }());
"btn btn-success"(click) = "overlayYes()" > /** @class */ (function () {
    function class_25() {
    }
    return class_25;
}());
"fa fa-check";
style = "color:white;" > /i> YES</button >
    /** @class */ (function () {
        function class_26() {
        }
        return class_26;
    }());
"btn btn-danger m-l-xs"(click) = "overlayNo()" > /** @class */ (function () {
    function class_27() {
    }
    return class_27;
}());
"fa fa-times" > /i> NO</button >
    /div>
    < /div>
    < /div>
    < /div>
    < div;
var default_24 = /** @class */ (function () {
    function default_24() {
    }
    return default_24;
}());
"overlay err" * ngIf;
"noResults" >
    /** @class */ (function () {
        function class_28() {
        }
        return class_28;
    }());
"overlayBox  fadeIn" >
    /** @class */ (function () {
        function class_29() {
        }
        return class_29;
    }());
"overlayContent text-center" >
    /** @class */ (function () {
        function class_30() {
        }
        return class_30;
    }());
"overlayIcon" >
    /** @class */ (function () {
        function class_31() {
        }
        return class_31;
    }());
"fa fa-times fa-5x";
style = "color: red" > /i>
    < /div>
    < h2 > No;
subscribers;
found. < /h2>
    < p > Please;
call;
Sky < /p>
    < div;
var default_25 = /** @class */ (function () {
    function default_25() {
    }
    return default_25;
}());
"overlayButtons m-t-lg" >
    /** @class */ (function () {
        function class_32() {
        }
        return class_32;
    }());
"btn btn-success"(click) = "noResults = false" > OK < /button>
    < /div>
    < /div>
    < /div>
    < /div>
    < !--ERROR;
-- >
    /** @class */ (function () {
        function class_33() {
        }
        return class_33;
    }());
"overlay err" * ngIf;
"error != null" >
    /** @class */ (function () {
        function class_34() {
        }
        return class_34;
    }());
"overlayBox  bounceIn" >
    /** @class */ (function () {
        function class_35() {
        }
        return class_35;
    }());
"overlayContent text-center" >
    /** @class */ (function () {
        function class_36() {
        }
        return class_36;
    }());
"overlayIcon" >
    /** @class */ (function () {
        function class_37() {
        }
        return class_37;
    }());
"fa fa-times fa-5x";
style = "color: #e23237;" > /i>
    < /div>
    < h2 * ngIf;
"error.message" > Error < br > {};
{
    error.message;
}
/h2>
    < h2 > {};
{
    error.description;
}
/h2>
    < div;
var default_26 = /** @class */ (function () {
    function default_26() {
    }
    return default_26;
}());
"overlayButtons m-t-lg" >
    /** @class */ (function () {
        function class_38() {
        }
        return class_38;
    }());
"btn btn-success"(click) = "error = null" > OK < /button>
    < /div>
    < /div>
    < /div>
    < /div>
    < !--JOIN;
CUSTOMER;
TO;
CHANNEL ? -- >
    /** @class */ (function () {
        function class_39() {
        }
        return class_39;
    }()) :  = "overlay info" * ngIf;
"joinBox" >
    /** @class */ (function () {
        function class_40() {
        }
        return class_40;
    }());
"overlayBox  fadeIn" >
    /** @class */ (function () {
        function class_41() {
        }
        return class_41;
    }());
"overlayContent text-center" >
    /** @class */ (function () {
        function class_42() {
        }
        return class_42;
    }());
"overlayIcon" >
    /** @class */ (function () {
        function class_43() {
        }
        return class_43;
    }());
"fa fa-exclamation fa-5x";
style = "color: #ffda70;" > /i>
    < /div>
    < h2;
var default_27 = /** @class */ (function () {
    function default_27() {
    }
    return default_27;
}());
"red" > We;
do
    not;
while (have);
a;
record;
for (this; customer. < /h2>
    < h2 > Would; you)
    like;
to;
add;
this;
customer ? /h2>
    < div : /** @class */ (function () {
    function class_44() {
    }
    return class_44;
}()) = "overlayButtons m-t-lg" >
    /** @class */ (function () {
        function class_45() {
        }
        return class_45;
    }());
"btn btn-success"(click) = "joinYes()" > /** @class */ (function () {
    function class_46() {
    }
    return class_46;
}());
"fa fa-check";
style = "color:white;" > /i> YES</button >
    /** @class */ (function () {
        function class_47() {
        }
        return class_47;
    }());
"btn btn-danger m-l-xs"(click) = "joinNo()" > /** @class */ (function () {
    function class_48() {
    }
    return class_48;
}());
"fa fa-times" > /i> NO</button >
    /div>
    < /div>
    < /div>
    < /div>
    < !--PLEASE;
CLICK;
ON;
FIND;
ADDRESS;
AND;
SELECT;
AN;
ADDRESS;
BEFORE;
SEARCH-- >
    /** @class */ (function () {
        function class_49() {
        }
        return class_49;
    }());
"overlay info" * ngIf;
"addressSelectedCheck" >
    /** @class */ (function () {
        function class_50() {
        }
        return class_50;
    }());
"overlayBox  fadeIn" >
    /** @class */ (function () {
        function class_51() {
        }
        return class_51;
    }());
"overlayContent text-center" >
    /** @class */ (function () {
        function class_52() {
        }
        return class_52;
    }());
"overlayIcon" >
    /** @class */ (function () {
        function class_53() {
        }
        return class_53;
    }());
"fa fa-exclamation fa-5x";
style = "color: #ffda70;" > /i>
    < /div>
    < h2 > Please;
click;
on;
'Find address';
and;
select;
a;
address;
before;
search. < /h2>
    < div;
var default_28 = /** @class */ (function () {
    function default_28() {
    }
    return default_28;
}());
"overlayButtons m-t-lg" >
    /** @class */ (function () {
        function class_54() {
        }
        return class_54;
    }());
"btn btn-success"(click) = "addressSelectedCheck = false" > OK < /button>
    < /div>
    < /div>
    < /div>
    < /div>
    < !--SEARCH;
ERROR-- >
    /** @class */ (function () {
        function class_55() {
        }
        return class_55;
    }());
"overlay info" * ngIf;
"searchError" >
    /** @class */ (function () {
        function class_56() {
        }
        return class_56;
    }());
"overlayBox  fadeIn" >
    /** @class */ (function () {
        function class_57() {
        }
        return class_57;
    }());
"overlayContent text-center" >
    /** @class */ (function () {
        function class_58() {
        }
        return class_58;
    }());
"overlayIcon" >
    /** @class */ (function () {
        function class_59() {
        }
        return class_59;
    }());
"fa fa-exclamation fa-5x";
style = "color: #ffda70;" > /i>
    < /div>
    < h2;
var default_29 = /** @class */ (function () {
    function default_29() {
    }
    return default_29;
}());
"red" > {};
{
    searchError.notFound;
}
/h2>
    < h2 > {};
{
    searchError.message;
}
/h2>
    < div;
var default_30 = /** @class */ (function () {
    function default_30() {
    }
    return default_30;
}());
"overlayButtons m-t-lg" >
    /** @class */ (function () {
        function class_60() {
        }
        return class_60;
    }());
"btn btn-success"(click) = "searchError = null" > OK < /button>
    < /div>
    < /div>
    < /div>
    < /div>
    < !--DOES;
THE;
CUSTOMER;
HAVE;
A;
VIEWING;
CARD ? -- >
    /** @class */ (function () {
        function class_61() {
        }
        return class_61;
    }()) :  = "overlay info" * ngIf;
"viewingCardBox" >
    /** @class */ (function () {
        function class_62() {
        }
        return class_62;
    }());
"overlayBox  fadeIn" >
    /** @class */ (function () {
        function class_63() {
        }
        return class_63;
    }());
"overlayContent text-center" >
    /** @class */ (function () {
        function class_64() {
        }
        return class_64;
    }());
"overlayIcon" >
    /** @class */ (function () {
        function class_65() {
        }
        return class_65;
    }());
"fa fa-exclamation fa-5x";
style = "color: #ffda70;" > /i>
    < /div>
    < h2 > Does;
the;
customer;
have;
a;
viewing;
card ? /h2>
    < div : /** @class */ (function () {
    function class_66() {
    }
    return class_66;
}()) = "overlayButtons m-t-lg" >
    /** @class */ (function () {
        function class_67() {
        }
        return class_67;
    }());
"btn btn-success"(click) = "vcYes()" > /** @class */ (function () {
    function class_68() {
    }
    return class_68;
}());
"fa fa-check";
style = "color:white;" > /i> YES</button >
    /** @class */ (function () {
        function class_69() {
        }
        return class_69;
    }());
"btn btn-danger m-l-xs"(click) = "vcNo()" > /** @class */ (function () {
    function class_70() {
    }
    return class_70;
}());
"fa fa-times" > /i> NO</button >
    /div>
    < /div>
    < /div>
    < /div>
    < !--CREATE;
A;
NEW;
CUSTOMER ? -- >
    /** @class */ (function () {
        function class_71() {
        }
        return class_71;
    }()) :  = "overlay info" * ngIf;
"createNewBox" >
    /** @class */ (function () {
        function class_72() {
        }
        return class_72;
    }());
"overlayBox  fadeIn" >
    /** @class */ (function () {
        function class_73() {
        }
        return class_73;
    }());
"overlayContent text-center" >
    /** @class */ (function () {
        function class_74() {
        }
        return class_74;
    }());
"overlayIcon" >
    /** @class */ (function () {
        function class_75() {
        }
        return class_75;
    }());
"fa fa-exclamation fa-5x";
style = "color: #ffda70;" > /i>
    < /div>
    < h2 > Would;
you;
like;
to;
create;
a;
new customer;
and;
request;
viewing;
card ? /h2>
    < div : /** @class */ (function () {
    function class_76() {
    }
    return class_76;
}()) = "overlayButtons m-t-lg" >
    /** @class */ (function () {
        function class_77() {
        }
        return class_77;
    }());
"btn btn-success"[routerLink] = "['/newsubscriber']" > /** @class */ (function () {
    function class_78() {
    }
    return class_78;
}());
"fa fa-check";
style = "color:white;" > /i> YES</button >
    /** @class */ (function () {
        function class_79() {
        }
        return class_79;
    }());
"btn btn-danger m-l-xs"(click) = "createNewBox = false" > /** @class */ (function () {
    function class_80() {
    }
    return class_80;
}());
"fa fa-times" > /i> NO</button >
    /div>
    < /div>
    < /div>
    < /div>;
