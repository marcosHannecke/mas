"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var validators_service_1 = require("../../services/validators.service");
var helpers_service_1 = require("../../services/helpers.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var user_service_1 = require("../../services/user.service");
var postcode_service_1 = require("../../services/postcode.service");
var SearchComponent = /** @class */ (function () {
    function SearchComponent(router, fb, helpers, validators, subscribersService, postcodeService, userService) {
        this.router = router;
        this.fb = fb;
        this.helpers = helpers;
        this.validators = validators;
        this.subscribersService = subscribersService;
        this.postcodeService = postcodeService;
        this.userService = userService;
        this.joinBox = false;
        this.viewingCardBox = false;
        this.createNewBox = false;
        this.addViewingCardBox = false;
        this.showSearchRemote = false;
        this.noResults = false;
        this.submitted = false;
        this.overlay = false;
        this.disableInput = false;
        this.addressSelectedCheck = false;
        this.addressSelected = false;
    }
    SearchComponent.prototype.ngOnInit = function () {
        // REMOVE ALL SESSION STORAGE BUT KEEP 'auth_token
        // console.log('a', sessionStorage);
        this.helpers.resetSession();
        // console.log('b', sessionStorage);
        // FORMS AND VALIDATORS
        this.searchForm = this.fb.group({
            "postcode": ['', [forms_1.Validators.maxLength(9), this.validators.specialChar]],
            // "surname": ['', [ this.validators.letterSpaces, this.validators.specialChar]],
            "surname": [''],
            "viewingCardNumber": ['', [this.validators.onlyNumbers]],
        });
        this.searchRemoteForm = this.fb.group({
            "addressLine1": ['', [forms_1.Validators.required]],
            "addressLine2": ['', [forms_1.Validators.required]],
        });
    };
    ;
    SearchComponent.prototype.search = function () {
        sessionStorage.setItem('viewingCardNumber', this.searchForm.value.viewingCardNumber);
        var searchForm = Object.assign(this.searchForm.value, this.searchRemoteForm.value);
        sessionStorage.setItem('searchForm', JSON.stringify(searchForm));
        // remove special characters in surname
        var formSurname = this.searchForm.value.surname.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
        this.searchForm.patchValue({ surname: formSurname });
        // Prepare search queries to send
        var f = this.searchForm.value;
        sessionStorage.setItem('searchParameters', JSON.stringify(searchForm));
        if (f.viewingCardNumber.length > 0) {
            this.addressSelected = true;
        }
        this.navigationExtras = {
            queryParams: {
                'pc': f.postcode,
                's': f.surname,
                'ad1': f.addressLine1,
                'ad2': f.addressLine2,
                'vcn': f.viewingCardNumber
            }
        };
        this.submitted = true;
        if (this.searchForm.valid && this.addressSelected) {
            this.searchLocal();
        }
        else if (!this.addressSelected) {
            this.addressSelectedCheck = true;
        }
    };
    // SEARCH LOCAL
    SearchComponent.prototype.searchLocal = function () {
        var _this = this;
        console.log(this.searchForm.value);
        this.subscribersService.searchSubscriberLocal(this.searchForm.value)
            .subscribe(function (res) {
            console.log(res);
            _this.subscriber = res;
            // Match viewing card number provided and VCN found
            var m = _this.vcnMatch();
            if (res.length == 0) {
                // /*Display address fields*/
                _this.showSearchRemote = true;
                if (_this.searchRemoteForm.valid) {
                    _this.vcnCheck();
                }
            }
            else if (res.length == 1) {
                console.log(m);
                if (!m) {
                    // this.router.navigate(['viewSubscriber', res[0].id ] );
                    sessionStorage.setItem('multipleMatch', JSON.stringify(res));
                    _this.router.navigate(['/results'], _this.navigationExtras);
                }
                else {
                    sessionStorage.setItem('multipleMatch', JSON.stringify(res));
                    _this.router.navigate(['/results'], _this.navigationExtras);
                }
            }
            else if (res.length > 1) {
                sessionStorage.setItem('multipleMatch', JSON.stringify(res));
                _this.router.navigate(['/results'], _this.navigationExtras);
            }
            else {
                console.log('no results');
                _this.router.navigate(['search']);
            }
        }, function (err) {
            // ERRORS BEFORE SHOWING ADL1 & ADL2
            if (!_this.showSearchRemote) {
                if (_this.searchForm.value.viewingCardNumber.length > 0) {
                    if (_this.searchForm.value.surname.length > 0) {
                        // this.searchError = {
                        //   notFound: 'We do not have a record for this customer.',
                        //   message: "Please enter customer postcode, click on 'Find address' and try again."
                        // }
                        _this.showSearchRemote = true;
                    }
                    else if (_this.searchForm.value.postcode.length > 0) {
                        if (!_this.showSearchRemote) {
                            _this.addressSelectedCheck = true;
                        }
                        else {
                            _this.searchError = {
                                notFound: 'We do not have a record for this customer.',
                                message: 'Please enter customer surname.'
                            };
                        }
                    }
                    else {
                        // this.searchError = {
                        //   notFound: 'We do not have a record for this customer.',
                        //   message: "Please enter customer surname and postcode, click on 'Find address' and try again."
                        // }
                        _this.showSearchRemote = true;
                    }
                }
                else if (_this.searchForm.value.surname.length > 0 && _this.searchForm.value.postcode.length > 0) {
                    _this.searchError = _this.helpers.getError(err);
                }
            }
            else if (_this.showSearchRemote) {
                if (_this.searchForm.value.postcode.lenght > 0 && _this.searchForm.value.surname.lenght > 0) {
                    _this.joinBox = true;
                }
                else if (_this.searchForm.value.viewingCardNumber.length == 0) {
                    _this.searchError = _this.helpers.getError(err);
                }
                else if (_this.searchForm.value.addressLine1.length > 0 && _this.searchForm.value.addressLine2.length > 0) {
                    _this.joinBox = true;
                }
            }
        });
    };
    // SEARCH REMOTE
    SearchComponent.prototype.searchRemote = function () {
        var _this = this;
        this.formData = this.searchForm.value;
        this.formData.addressLine1 = this.searchRemoteForm.value.addressLine1;
        this.formData.addressLine2 = this.searchRemoteForm.value.addressLine2;
        console.log(this.formData);
        this.subscribersService.searchSubscriberRemote(this.formData)
            .subscribe(function (res) {
            console.log(res);
            if (res.length == 0) {
                // this.error = {message: 'No match'}
                _this.router.navigate(['/joinSubscriber']);
            }
            else if (res.length == 1) {
                // replace special characters in surname and store 'remoteMatch' and go to /results
                _this.replaceStoreNavigate(res);
            }
            else if (res.length > 1) {
                _this.router.navigate(['/joinSubscriber']);
            }
            else {
                console.log('no results');
                _this.router.navigate(['search']);
            }
        }, function (err) {
            _this.error = _this.helpers.getError(err);
            if (_this.error.message.includes('Multiple')) {
                _this.router.navigate(['/joinSubscriber']);
            }
        });
    };
    // replace special characters and store 'remoteMatch' and go to /results
    SearchComponent.prototype.replaceStoreNavigate = function (res) {
        // res[0].surname = res[0].surname.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'');
        // res[0].initials = res[0].initials.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'');
        res[0] = this.helpers.removeSpecialCharacters(res[0]);
        console.log('res[0', res[0]);
        sessionStorage.setItem('oneResult', 'true');
        sessionStorage.setItem('remoteMatch', JSON.stringify(res));
        this.router.navigate(['/results']);
    };
    // DOES THE CUSTOMER HAVE A VIEWING CARD?
    SearchComponent.prototype.vcnCheck = function () {
        if (this.searchForm.value.viewingCardNumber.length > 0) {
            this.joinBox = true;
        }
        else {
            this.viewingCardBox = true;
        }
    };
    // FIND ADDRESS
    SearchComponent.prototype.findAddress = function () {
        var _this = this;
        var postcode = this.searchForm.value.postcode.replace(" ", "");
        this.postcodeService.postcode(postcode)
            .subscribe(function (res) {
            _this.addressList = JSON.parse(res['_body']);
            if (_this.addressList.length == 0) {
                _this.addressList = [{
                        notFound: 'NO ADDRESS FOUND'
                    }];
            }
        }, function (err) {
            if (_this.searchForm.value.postcode.length === 0) {
                _this.error = {
                    description: 'Please Enter a Postcode.'
                };
            }
            else {
                _this.error = _this.helpers.getError(err);
            }
        });
    };
    // SET ADDRESS
    SearchComponent.prototype.setAddress = function (address) {
        if (address.notFound) {
            this.addressList = null;
            this.disableInput = false;
        }
        else {
            this.addressList = null;
            this.disableInput = true;
            this.showSearchRemote = true;
            this.addressSelected = true;
            // this.addressManually = true;
            var add = this.helpers.nullCheck(address);
            this.searchRemoteForm.controls['addressLine1'].setValue(add.addressLine1.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.searchRemoteForm.controls['addressLine2'].setValue(add.addressLine2.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
        }
    };
    SearchComponent.prototype.vcYes = function () {
        this.viewingCardBox = false;
        this.addViewingCardBox = true;
    };
    SearchComponent.prototype.vcNo = function () {
        this.viewingCardBox = false;
        this.createNewBox = true;
    };
    // JOIN SUBSCRIBER?
    SearchComponent.prototype.joinYes = function () {
        this.joinBox = false;
        this.searchRemote();
    };
    SearchComponent.prototype.joinNo = function () {
        this.joinBox = false;
        // this.router.navigate(['/search']);
    };
    // Viewing Card Number match
    SearchComponent.prototype.vcnMatch = function () {
        for (var _i = 0, _a = this.subscriber; _i < _a.length; _i++) {
            var subs = _a[_i];
            if (subs.viewingCardNumber != null) {
                if (subs.viewingCardNumber == this.searchForm.value.viewingCardNumber) {
                    console.log(subs.viewingCardNumber, this.searchForm.value.viewingCardNumber);
                    return false;
                }
            }
            return true;
        }
    };
    SearchComponent = __decorate([
        core_1.Component({
            selector: 'search',
            templateUrl: 'search.component.html',
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService, helpers_service_1.HelpersService, postcode_service_1.PostcodeService, user_service_1.UserService]
        })
    ], SearchComponent);
    return SearchComponent;
}());
exports.SearchComponent = SearchComponent;
