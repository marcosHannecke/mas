"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var search_component_1 = require("./search.component");
var SEARCH_ROUTER = [
    {
        path: '',
        component: search_component_1.SearchComponent,
    },
];
exports.searchRouter = router_1.RouterModule.forChild(SEARCH_ROUTER);
