import {NgModule} from "@angular/core";
import {SearchComponent} from "./search.component";
import {CommonModule} from "@angular/common";

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule} from '@angular/router';

@NgModule({
  declarations: [SearchComponent],
  imports     : [CommonModule, FormsModule, ReactiveFormsModule, RouterModule],
  exports     : [SearchComponent]
})

export class SearchViewModule {}
