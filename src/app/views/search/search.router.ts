import {Routes, RouterModule} from "@angular/router";
import {SearchComponent} from "./search.component";

const SEARCH_ROUTER: Routes = [
  {
    path: '',
    component: SearchComponent,
  },
];

export const searchRouter = RouterModule.forChild(SEARCH_ROUTER);
