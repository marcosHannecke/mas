"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var newsubscriber_component_1 = require("./newsubscriber.component");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var newsubscriberEntitlements_component_1 = require("./newsubscriberEntitlements.component");
var NewSubscriberViewModule = /** @class */ (function () {
    function NewSubscriberViewModule() {
    }
    NewSubscriberViewModule = __decorate([
        core_1.NgModule({
            declarations: [newsubscriber_component_1.NewsubscriberComponent, newsubscriberEntitlements_component_1.NewsubscriberEntitlementsComponent],
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, forms_1.ReactiveFormsModule, router_1.RouterModule],
        })
    ], NewSubscriberViewModule);
    return NewSubscriberViewModule;
}());
exports.NewSubscriberViewModule = NewSubscriberViewModule;
