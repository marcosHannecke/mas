import { Component, OnInit, Input } from '@angular/core';
import {HelpersService} from '../../../services/helpers.service';
import {Router, ActivatedRoute, NavigationExtras, Params} from '@angular/router';
import {RestService} from '../../../services/rest.service';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../../app.global';

@Component({
  selector: 'app-newsubscriber-entitlements',
  templateUrl: 'newsubscriberEntitlements.component.html',
  styles: ['.product-desc{height: 130px; overflow:hidden} .ibox-content .row{min-height: 100px}' +
  '.activeEntitlements .product-box:hover {box-shadow: none;}' +
  '.product-box{border: none} .product-btn button{ background: white} ' +
  '.product-btn button:hover{ background: #e23237; color: white !important}' +
  '.productDescription{-webkit-line-clamp: 2;  overflow: hidden; display: -webkit-box;  -webkit-box-orient: vertical;} ' +
  '.productDescription:hover{-webkit-line-clamp: 6;}' +
  '.showOPPVs{position: relative !important; top: -24px !important; left: 0 !important; transition: all 0.5s;} ' +
  '.showOPPVs .ibox-content {background: rgb(255, 250, 233) !important}' +
  '.angle{position: absolute; right: 5px; bottom: 10px; width: 20px; height: 20px; font-size: 18px; color: #0570e2;}'],
  providers: [ HelpersService, RestService]
})
export class NewsubscriberEntitlementsComponent implements OnInit {
  subscriber: any;
  error: any;
  errorDiscount: any;
  cardError: any;
  overlay: boolean = false;
  productGroups: any = [];
  PGFound: any = [];
  packagesSelected: any[] = [];
  submitted: boolean = false;
  entitlements: boolean = false;
  view1: boolean = true;
  view2: boolean = false;
  filterValue: string;
  created: any = false;
  successMessage: boolean = false;
  hasPaymentCard: any;
  vipVal: any;
  cardTokens = {
    token : '',
    tokenId: '',
    expire: ''
  };
  pleaseWait = true;

  previewDialog: boolean = false;
  discount: any[] = [];
  invalidDiscount: any[] = [];
  validDiscount = false;
  date = this.date = Math.round(new Date().getTime()/1000) + 24*60*60;

  constructor(private router: ActivatedRoute, private route: Router, private helpers: HelpersService,
              private rest: RestService, private http: HttpClient, private globals: Globals) {};

  ngOnInit() {
    // ERRORS DIALOGS
    this.error = {
      subscriber : false,
      entitlements : false,
      vip: false,
      payment : false,
    };

    // GET CARD TOKENS
    // this.router.queryParams
    //   .subscribe(params => {
    //       this.cardTokens.token = params['token'];
    //       this.cardTokens.tokenId = params['tokenId'];
    //       this.cardTokens.expire = params['exp'];
    //   });

    if (sessionStorage.getItem('cardTokens') != null) {
      const cardTokens = JSON.parse(sessionStorage.getItem('cardTokens'));
      this.cardTokens.token = cardTokens['token'];
      this.cardTokens.tokenId = cardTokens['tokenId'];
      this.cardTokens.expire = cardTokens['expire'];
    }

    // HAS PAYMENT CARD OR VIP
    this.router.queryParams
      .subscribe(res => {
        this.hasPaymentCard = res['hasPaymentCard'];
        console.log('payment', this.hasPaymentCard);
      });


    // CHECK TOKENS AND SUBSCRIBER STORED
    if((this.cardTokens.token == undefined || this.cardTokens.tokenId == undefined || this.cardTokens.token == ''
      || this.cardTokens.tokenId == '') && sessionStorage.getItem('subscriber' ) == null  && this.hasPaymentCard != 1) {
      this.cardError = {
        message : 'Sorry we could not collect your payment details, ',
        // message2: 'Please Check details were correct.'
      }
    }else {
      console.log(this.cardTokens)
      // sessionStorage.setItem('cardTokens', JSON.stringify(this.cardTokens));
    }

    // GET STORED DATA FROM FORM TO GET SELLABLES
    this.subscriber = JSON.parse(sessionStorage.getItem('newSubscriber'));
    if(sessionStorage.getItem('newSubscriber') != null) {
      this.subscriber = JSON.parse(sessionStorage.getItem('newSubscriber'));
      this.vipVal = this.subscriber.vip;
      const array = ['cardHolderType=' +  this.subscriber.cardHolderType, 'currencyCode=' + this.subscriber.currencyCode ];
      // ADD CUSTOMER ID FROM SUBSCRIBER STORED
      // if(sessionStorage.getItem('subscriber')){
      //   this.subscriber.customerId = JSON.parse(sessionStorage.getItem('subscriber')).customerId;
      //   array.push('customerId=' + this.subscriber.customerId)
      // }
      let resp;
      this.rest.get('/product-groups/search-sellable?q='+ array +',maxValidFrom=' + this.date)
        .subscribe(res => {
          resp = res;

          function displayingOrder(a, b) {
            if (a.displayingOrder < b.displayingOrder) {return -1; }
            if (a.displayingOrder > b.displayingOrder) { return 1; }
            return 0;
          }

          resp.sort(displayingOrder);
          // resp.sort(this.helpers.alphabetical);
          // Remove OPPV from /get-sellables
          for(let i = 0; i < resp.length; i++) {
            if(resp[i].pricing.oneOff == null || resp[i].pricing.oneOff.indicative == false) {
              if(!resp[i].titleDescription.toLowerCase().includes('retention')) {
                this.productGroups.push(resp[i]);
                this.PGFound.push(resp[i]);
              }
            }
          }
        }, err => {
          this.error = JSON.parse(err['_body']);
          console.log(this.error.message);
        });

      // this.productsService.getSellables(array)
      //   .subscribe(res => {
      //     res.sort(this.helpers.alphabetical);
      //     // Remove OPPV from /get-sellables
      //     for(var i = 0; i < res.length; i++) {
      //       if(res[i].pricing.oneOff == null || res[i].pricing.oneOff.indicative == false) {
      //         if(!res[i].titleDescription.toLowerCase().includes('retention')) {
      //           this.productGroups.push(res[i]);
      //           this.PGFound.push(res[i]);
      //         }
      //       }
      //     }
      //   }, err => {
      //     this.error = JSON.parse(err['_body']);
      //     console.log(this.error.message);
      //   });
    }else {
      this.route.navigate(['/newsubscriber']);

      // this.productsService.getProducts('PG')
      //   .subscribe(res => {
      //     console.log('HEREEEEEEEAAAA',res);
      //     this.productGroups = res;
      //     this.PGFound = res;
      //   }, err => {
      //     this.error = JSON.parse(err['_body']);
      //     console.log(this.error.message);
      //   })
    }

  }

  // RESET ROUTE
  reset() {
    var url = window.location.href;
    var array = url.split('/');
    var path = array[array.length-1];

    this.route.navigateByUrl('blank').then(() => {
      this.route.navigateByUrl(path);
    })
  }

  // PREVIEW DIALOG
  addedRemovedPackages() {
    this.previewDialog = true;
    this.discount = [];
    this.invalidDiscount = [];
  }

  // CREATE SUBSCRIBER
  createSubscriber() {
    // ADD CUSTOMER ID FROM SUBSCRIBER STORED
    if (sessionStorage.getItem('subscriber')) {
      this.subscriber.customerId = JSON.parse(sessionStorage.getItem('subscriber')).customerId;
    }
    delete this.subscriber.vip;

    // Initials to uppercase
    this.subscriber.initials = this.subscriber.initials.toUpperCase();
    // Capitalize surname
    this.subscriber.surname = this.helpers.toTitleCase(this.subscriber.surname);

    // Valid discount
    this.validDiscount = false;
    for (var i = 0; i < this.invalidDiscount.length; i++) {
      if (this.invalidDiscount[i]) {
        this.validDiscount = true;
      }
    }

    if (this.validDiscount) {
      this.errorDiscount = {
        message2: 'Invalid discount value.'
      }
    } else if (this.created) {  // If error and subscriber already created
      // save payment if subscriber else add entitlements
      if (sessionStorage.getItem('subscriber') == null) {
        this.savePayment();
      }else {
        // this.addEntitlements(this.created.id)
        this.vip(this.created.id);
      }
    }else {
      this.rest.post('/subscribers', this.subscriber)
        .subscribe(res => {
          console.log(res['body']);
          this.created = res['body'];

          // save payment if subscriber else add entitlements
          if (sessionStorage.getItem('subscriber') == null && this.hasPaymentCard != 1) {
            this.savePayment();
          } else {
            // this.addEntitlements(this.created.id)
            this.vip(this.created.id);
          }
          // set vip status

        }, err => {
          this.error.subscriber = this.helpers.error(err.error.message);
        })

      // this.subscribersService.addSubscriber(this.subscriber)
      //   .subscribe(res => {
      //     this.created = JSON.parse(res["_body"]);
      //
      //     // save payment if subscriber else add entitlements
      //     if (sessionStorage.getItem('subscriber') == null && this.hasPaymentCard != 1) {
      //       this.savePayment();
      //     } else {
      //       // this.addEntitlements(this.created.id)
      //       this.vip(this.created.id);
      //     }
      //     // set vip status
      //
      //   }, err => {
      //     this.error.subscriber = JSON.parse(err['_body']);
      //   })

    }
  }

  // SAVE PAYMENT (if successful add entitlements)
  savePayment() {
    const data = {
      "customerId": this.created.customerId,
      "tokenId": this.cardTokens.tokenId,
      "token": this.cardTokens.token,
      "expiryDate": this.helpers.undefinedToEmptyString(this.cardTokens.expire)
    };

    this.http.put(this.globals.API_ENDPOINT + '/payments/save-payment-card', data, {responseType: 'text'})
      .subscribe(() => {
        this.addEntitlements(this.created.id);
      }, err => {
        err = JSON.parse(err.error);
        console.log(err);
        if (err.fieldErrors) {
          this.error = err.fieldErrors[0];
        }else {
          this.error = this.helpers.error(err.message);
        }
      })

    // this.paymentsService.putUrl('save-payment-card', data)
    //   .subscribe(res=> {
    //     this.addEntitlements(this.created.id);
    //   }, err => {
    //     this.error.payment = JSON.parse(err["_body"]);
    //   })
  }

  // VIP
  vip(id) {
    if (this.vipVal == 'true') {
      const data = {
        "subscriberId" : id,
        "vip": this.vipVal
      };

      this.http.put(this.globals.API_ENDPOINT + '/subscribers/set-vip', data, {responseType: 'text'})
        .subscribe(res => {
          this.addEntitlements(id)
          console.log(res);
        }, err => {
          err = JSON.parse(err.error);
          console.log(err);
          if (err.fieldErrors) {
            this.error.vip = err.fieldErrors[0];
          }else {
            this.error.vip = this.helpers.error(err.message);
          }
        })
    }else if (this.hasPaymentCard == 1) {
      this.addEntitlements(id)
    }else {
      this.savePayment()
    }
  }


  // ADD ENTITLEMENTS
  addEntitlements(id) {
    var array = [];
    for(var i = 0; i < this.packagesSelected.length; i++ ){
      array[i] = this.packagesSelected[i].name;
    }

    // add discount
    var discount  = {};
    for(var i = 0; i < array.length; i++ ){
      if( this.discount[i] !== undefined){
        if(this.discount[i] > 0) {
          discount[array[i]] = this.discount[i]
        }
      }
    }

    const entitlements = {
      "subscriberId": id,
      "productGroupNames": array,
      "productGroupsDiscounts": discount
    };

    if(array.length > 0) {
      this.rest.post('/entitlements', entitlements)
        .subscribe(res => {
          console.log(res);
          //SHOW SUBSCRIBER CREATED SUCCESSFULLY DIALOG
          if(!this.error.subscriber && !this.error.entitlemnents && !this.error.vip && !this.error.payment){
            this.successMessage = true;
          }
        }, err =>{
          this.error.entitlements = this.helpers.error(err.error.message);
        })
    }else if(!this.error.subscriber && !this.error.entitlemnents && !this.error.vip && !this.error.payment){
      this.successMessage = true;
    }
  }

  // PACKAGES FILTER
  filter() {
    var found = [];
    var a = false;
    var b = false;
    for (var i = 0; i < this.productGroups.length; i++) {
      a = this.productGroups[i].name.toLowerCase().includes(this.filterValue.toLowerCase());
      b = this.productGroups[i].titleDescription.toLowerCase().includes(this.filterValue);
      if (a == true || b == true) {
        found.push(this.productGroups[i]);
      }
    }
    this.PGFound = found;
  }

  // SELECT PACKAGES
  isSelected(pack) {
    var p = false;
    var pos;
    for (var i = 0; i < this.packagesSelected.length; i++) {
      if (this.packagesSelected[i].id == pack.id) {
        p = true;
        pos = i;
      }
    }
    return [p, pos];
  }

  selectPackage(product, index) {
    this.PGFound.splice(index , 1);
    if (!this.isSelected(product)[0]) {
      this.packagesSelected.push(product);
    } else {
      this.packagesSelected.splice(this.isSelected(product)[1], 1);
    }
  }

  // REMOVE ENTITLEMENT
  removePackage(pack, i) {
    console.log(this.packagesSelected);

    this.packagesSelected.splice(i , 1);
    if (!this.isSelected(pack)[0] || pack.length > 0) {
      this.PGFound.push(pack);
    } else {
      this.PGFound.splice(this.isSelected(pack)[1], 1);
    }
  }

  // DISCOUNT VALIDATION
  discountValidation(discount, i) {
    if(discount != null) {
      this.invalidDiscount[i] = false;
      var regex = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
      if (!regex.test(discount) && discount) {
        this.invalidDiscount[i] = true;
      }
    }
  }

}

// @Pipe({
//   name: 'sortOrder'
// })
// export class SortOrder {
//   transform(value) {
//     var sortOrder = [
//       'TV Xcellence (New GBP Customer)',
//       'TV Xcellence (New GBP Customer)',
//       'TV Xcellence Annual (New GBP Customer)',
//       'TV Xcellence Annual (Reinstate GBP Customer)',
//       'Premier (New GBP Customer)',
//       'Premier (Reinstate GBP Customer)',
//       'Premier Annual (New GBP Customer)',
//       'Premier Annual (Reinstate GBP Customer)',
//       'Red Hot Monthly (New GBP Customer)',
//       'Red Hot Monthly (Reinstate GBP Customer)',
//       'Pay Per Night (New GBP Customer)'
//     ]
//     value.sort((a: any, b: any) => {
//
//       console.log('here');
//     });
//
//     return value;
//   }
// }
