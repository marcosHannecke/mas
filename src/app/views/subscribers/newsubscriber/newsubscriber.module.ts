import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {NewsubscriberComponent} from "./newsubscriber.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {RouterModule} from "@angular/router";
import {NewsubscriberEntitlementsComponent} from "./newsubscriberEntitlements.component";

@NgModule({
    declarations: [NewsubscriberComponent, NewsubscriberEntitlementsComponent],
    imports     : [BrowserModule, FormsModule, ReactiveFormsModule, RouterModule],
})

export class NewSubscriberViewModule {}
