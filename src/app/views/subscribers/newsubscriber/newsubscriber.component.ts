import { Component, OnInit, Inject } from '@angular/core';
import {ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from '../../../services/helpers.service';
import {PostcodeService} from '../../../services/postcode.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {Router} from '@angular/router';
import {Subscriber} from '../../../Subscriber';
import {RestService} from '../../../services/rest.service';
import {Globals} from '../../../app.global';

@Component({
    selector: 'app-new-subscriber',
    templateUrl: 'newsubscriber.component.html',
    styles: ['.findAddress{position: absolute; background: white; z-index: 100; } ' +
    '.findAddress{border-bottom: 1px solid #ececec; border-left: 1px solid #ececec;' +
    ' border-right: 1px solid #ececec}' +
    '.findAddress ul{padding: 0; margin: 0; max-height: 400px; overflow-y: scroll; min-width: 450px} ' +
    '.findAddress li{padding: 5px 15px;} .findAddress li:hover{background: #f9f9f9}'],
    providers: [   ValidatorsService, PostcodeService, HelpersService, RestService]
})
export class NewsubscriberComponent implements OnInit {
  entitlements: any;
  form: FormGroup;
  subscriber: any;
  customerEmail: any = '';
  error: any;
  overlay = false;
  newSubscriber: Subscriber = <any>{};
  submitted = false;
  success = false;
  hasPaymentCard: any = false;
  addressManually = true;
  addressList: any;
  match = false;
  noEmail = false;
  checked = true;
  role: string;
  searchForm: any;
  payMethDialog = false;
  payMeth: any;
  invalidPostcode = false;
  emailVerificationResponse: any;
  didYouMean: any;
  url: any;

  constructor(private helpers: HelpersService, private postcodeService: PostcodeService,
              private validatorsService: ValidatorsService,
              private router: Router, private globals: Globals,
              private fb: FormBuilder, private rest: RestService) {}

  ngOnInit() {
    this.url = this.globals.URL;
    let subs;
    // GET USER ROLE
    if (sessionStorage.getItem('role') != null) {
      this.role = sessionStorage.getItem('role');
      console.log(this.role);
    }

    // GET SEARCH FORM IF STORED
    if (sessionStorage.getItem('searchForm')) {
      this.searchForm = JSON.parse(sessionStorage.getItem('searchForm'));
      console.log('searchForm',  this.searchForm );
    }

    // IF 'ADD ADDITIONAL SUBSCRIBER' FROM CUSTOMER
    if (sessionStorage.getItem('subscriber')) {
      this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
      subs = this.subscriber;

      // Get customer email
      if (JSON.parse(sessionStorage.getItem('customerEmail')) != null) {
        this.customerEmail = JSON.parse(sessionStorage.getItem('customerEmail'));
      }
    }

    // IF COMING BACK FROM ERROR
    if (sessionStorage.getItem('newSubscriber')) {
      subs = JSON.parse(sessionStorage.getItem('newSubscriber'));
      this.customerEmail = subs.email;
    } else {
      this.customerEmail = '';
    }


    if (subs) {
      // Check for null and replace with empty string
      subs = this.helpers.nullCheck(subs);
      // change phones to avoid form errors
      if (subs.homeTelNr) {
        subs.homeTelNo = subs.homeTelNr;
        delete subs.homeTelNr;
        subs.workTelNo = subs.workTelNr;
        delete subs.workTelNr;
        subs.mobTelNo = subs.mobTelNr;
        delete subs.mobTelNr;
        subs.cliTelNo = subs.cliTelNr;
        delete subs.cliTelNr;
      }

      // FORM
      this.form = this.fb.group({
        'cardHolderType': [subs.cardHolderType, Validators.required],
        'packRequestType': ['1', Validators.required],
        'nrOfCards': ['1', [Validators.required, this.validatorsService.onlyNumbers]],
        'title': [subs.title, Validators.required],
        'initials': [subs.initials.toUpperCase(), [Validators.required, Validators.maxLength(3), this.validatorsService.letterSpaces]],
        'surname': [subs.surname, [Validators.required, Validators.minLength(2),
          Validators.maxLength(35), this.validatorsService.letterSpaces]],
        'addressLine1': [subs.addressLine1, [Validators.required, Validators.minLength(2),
          Validators.maxLength(35), this.validatorsService.specialChar]],
        'addressLine2': [subs.addressLine2, [Validators.required, Validators.minLength(2),
          Validators.maxLength(35), this.validatorsService.specialChar]],
        'addressLine3': [subs.addressLine3, Validators.minLength(2)],
        'addressLine4': [subs.addressLine4, Validators.minLength(2)],
        'addressLine5': [subs.addressLine5, Validators.minLength(2)],
        'postcode': [subs.postcode, [Validators.required, Validators.minLength(2), Validators.maxLength(9)]],
        'countryCode': [subs.countryCode],
        'phones': this.fb.group({
          'homeTelNr': [subs.homeTelNo, [Validators.maxLength(15), this.validatorsService.onlyNumbers]],
          'workTelNr': [subs.workTelNo, [Validators.maxLength(15), this.validatorsService.onlyNumbers]],
          'mobTelNr': [subs.mobTelNo, [Validators.maxLength(15), this.validatorsService.onlyNumbers]],
        }, {validator: this.validatorsService.phones}),
        'cliTelNr': [subs.cliTelNo],
        'currencyCode': [subs.currencyCode],
        'vip': ['false', Validators.required],
        'email': [this.customerEmail, [Validators.maxLength(128)]],
        'force' : [false],
        'marketingPreferences': this.fb.group({ // <-- the child FormGroup
          'FP_POST': false,
          'FP_EMAIL': false,
          'FP_PHONE': false,
          'FP_SMS': false,
          'TP_POST': false,
          'TP_EMAIL': false,
          'TP_PHONE': false,
          'TP_SMS': false
        })
      });
    } else {
      // DATA FROM SEARCH FORM
      let sF;
      if (this.searchForm != null) {
        sF = {
          surname: this.searchForm.surname,
          addressLine1: this.searchForm.addressLine1,
          addressLine2: this.searchForm.addressLine2,
          addressLine3: this.searchForm.addressLine3,
          addressLine4: this.searchForm.addressLine4,
          addressLine5: this.searchForm.addressLine5,
          postcode: this.searchForm.postcode
        }
      }else {
        sF = {
          surname: '',
          addressLine1: '',
          addressLine2: '',
          postcode: ''
        }
      }
      // display address line 3,4,5 not null
      if (sF.addressLine3 || sF.addressLine4 || sF.addressLine5) {
        this.addressManually = true;
      }

      // FORM
      this.form = this.fb.group({
        'cardHolderType': ['DTH', Validators.required],
        'packRequestType': ['1', Validators.required],
        'nrOfCards': ['1', [Validators.required, this.validatorsService.onlyNumbers]],
        'title': ['Mr', Validators.required],
        'initials': ['', [Validators.required, Validators.maxLength(3), this.validatorsService.letterSpaces]],
        'surname': [sF.surname, [Validators.required, Validators.minLength(2),
                    Validators.maxLength(35), this.validatorsService.letterSpaces]],
        'addressLine1': [sF.addressLine1, [Validators.required, Validators.minLength(2),
                        Validators.maxLength(35), this.validatorsService.specialChar]],
        'addressLine2': [sF.addressLine2, [Validators.required, Validators.minLength(2),
                        Validators.maxLength(35), this.validatorsService.specialChar]],
        'addressLine3': [sF.addressLine3, Validators.minLength(2)],
        'addressLine4': [sF.addressLine4, Validators.minLength(2)],
        'addressLine5': [sF.addressLine5, Validators.minLength(2)],
        'postcode': [sF.postcode, [Validators.required, Validators.minLength(2), Validators.maxLength(9)]],
        'countryCode': ['GBR'],
        'phones': this.fb.group({
          'homeTelNr': ['', [Validators.maxLength(15), this.validatorsService.onlyNumbers]],
          'workTelNr': ['', [Validators.maxLength(15), this.validatorsService.onlyNumbers]],
          'mobTelNr': ['', [Validators.maxLength(15), this.validatorsService.onlyNumbers]],
        }, {validator: this.validatorsService.phones}),
        'cliTelNr': [''],
        'currencyCode': ['GBP'],
        'vip': ['false', Validators.required],
        'email': ['', [ Validators.maxLength(128)]],
        'force' : [false],
        'marketingPreferences': this.fb.group({ // <-- the child FormGroup
          'FP_POST': false,
          'FP_EMAIL': false,
          'FP_PHONE': false,
          'FP_SMS': false,
          'TP_POST': false,
          'TP_EMAIL': false,
          'TP_PHONE': false,
          'TP_SMS': false
        })
      });
    }

    this.detectCountryFromPostcode(this.form.value.postcode);
  };

  // SHOW WARNING IF COUNTRY AND CURRENCY DON'T MATCH
  countryCurrencyMatch() {
    this.match = false;
    if ((this.form.value.currencyCode === 'GBP' && this.form.value.countryCode === 'IRL') ||
       (this.form.value.currencyCode === 'EUR' && this.form.value.countryCode === 'GBR')) {
      this.match = true;
    }
  }


  // PAYMENT METHOD
  paymentMethod() {
    this.submitted = true;
    if (this.form.valid === true && this.form.value.vip === 'false' ) {
      this.payMethDialog = true;
    }else {
      this.processForm();
    }
  }

  // PROCESS AND VALIDATE FORM WHEN SUBMITTED
  processForm() {
    this.payMethDialog = false;
    this.submitted = true;
    this.addressManually = true;

    // UPDATE NEW SUBSCRIBER JSON TO SEND
    this.newSubscriber = this.form.value;
    this.newSubscriber.homeTelNr = this.form.value.phones.homeTelNr;
    this.newSubscriber.workTelNr = this.form.value.phones.workTelNr;
    this.newSubscriber.mobTelNr = this.form.value.phones.mobTelNr;
    delete this.newSubscriber.phones;

    // set email to unknown@mediaaccessservices.net if empty
    if (this.form.value.email === '') {
      this.newSubscriber.email = 'unknown@mediaaccessservices.net';
    }
    sessionStorage.setItem('newSubscriber', JSON.stringify(this.newSubscriber));
    if (this.form.valid === true ) {
      // CHECK IF SUBSCRIBER STORED HAS PAYMENT CARD
      if (this.subscriber) {
        this.rest.get('/payments/has-payment-card?customerId=' + this.subscriber.customerId)
          .subscribe(res => {
            this.hasPaymentCard = res;
            this.proceed();
            console.log(this.hasPaymentCard);
          }, err => {
            this.error = this.helpers.error(err.error.message);
          })
      } else {
        this.proceed();
      }
    }
  }

  // GO TO ENTITLEMENTS PAGE OR GET PAYMENT CARD
  proceed() {

    if (this.hasPaymentCard || this.form.value.vip === 'true') {
      this.router.navigate(['/newsubscriberEntitlements'], { queryParams: { hasPaymentCard: '1' }})
    }else if (!this.hasPaymentCard) {
      // Data to send
      const data = {
        currencyCode: this.newSubscriber.currencyCode,
        title: this.newSubscriber.title,
        initials: this.newSubscriber.initials,
        surname: this.newSubscriber.surname,
        addressLine1: this.newSubscriber.addressLine1,
        postcode: this.newSubscriber.postcode,
        country: this.newSubscriber.countryCode,
        email: this.newSubscriber.email,
        returnUrl: this.globals.PAYMENT_RETURN_DOMAIN + '/#/new-payment',
        directDebit: this.payMeth
      };

      // Data to url string
      const str = Object.keys(data).map(function (key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
      }).join('&');

      // GET PAYMENT CARD
      let url;
      this.rest.get('/payments/get-payment-card-url?' + str)
        .subscribe(res => {
          console.log(res);
          url = res;
          sessionStorage.setItem('payment-url', res['storeCardUrl']);
          sessionStorage.setItem('return-url', data.returnUrl);
          sessionStorage.setItem('paymentPath', 'create');
          this.router.navigate(['/payment'])

          // location.replace(url['storeCardUrl']);
        }, err => {
          this.error = this.helpers.error(err.error.message);
        })

      // this.paymentsService.getUrl('get-payment-card-url?', str)
      //   .subscribe(res => {
      //     console.log(res);
      //     const url = JSON.parse(res['_body']);
      //     location.replace(url.storeCardUrl);
      //   }, err => {
      //     this.error = JSON.parse(err['_body']);
      //   })
    }
  }

  // FIND ADDRESS
  findAddress() {
    const postcode = this.form.value.postcode.replace(' ', '');

    this.postcodeService.postcode(postcode)
      .subscribe(res => {
        this.addressList = res;
        if (this.addressList.length === 0) {
          this.addressList = [{
            notFound: 'NO ADDRESS FOUND'
          }]
        }
      }, err => {
        this.error = this.helpers.error(err.error.message)
      })
  }

  // SET ADDRESS
  setAddress(address) {
    if (address.notFound) {
      this.addressList = null;
    }else {
      this.addressList = null;
      this.addressManually = true;
      const add = this.helpers.nullCheck(address);
      this.form.controls['addressLine1'].setValue(add.addressLine1.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.form.controls['addressLine2'].setValue(add.addressLine2.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.form.controls['addressLine3'].setValue(add.addressLine3.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.form.controls['addressLine4'].setValue(add.addressLine4.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.form.controls['addressLine5'].setValue(add.addressLine5.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
    }
  }

  // Detect country and currency code from post
  detectCountryFromPostcode(pc) {
    if (pc.toLowerCase() === 'roi') {
      this.invalidPostcode = false;
      this.form.patchValue({
        countryCode: 'IRL',
        currencyCode: 'EUR'
      });
    } else {
      const postcode = this.form.value.postcode.replace(' ', '');
      this.postcodeService.postcode(postcode)
        .subscribe(res => {
          if (JSON.parse(res['_body']).length > 0 ) {
            this.invalidPostcode = false;
            this.form.patchValue({
              countryCode: 'GBR',
              currencyCode: 'GBP'
            });
          } else {
            this.form.controls['postcode'].setErrors({'incorrect': true});
            this.invalidPostcode = true;
          }
        }, err => {
          this.error = {
            message: 'Error in postcode validation'
          }
        });
    }
    // else if (pc.toLowerCase() === 'ci') {
    //   this.invalidPostcode = false;
    //   this.form.patchValue({
    //     countryCode: 'GBR',
    //     currencyCode: 'GBP'
    //   });
    // }
  }


  /**
   * Email Verification
   */
  validateEmail() {
    let email;
    if (this.form.value.email) {
      this.emailVerificationResponse = 'loading';
      email = this.form.value.email;
      const data  = {
        email: email,
        type: 'KICKBOX'
      };

      let resp;
      this.rest.post('/email-verifications', data)
        .subscribe(res => {
          resp = res;
          this.emailVerificationResponse = resp.body.verificationResults.result;
          // set did you mean value
          if (resp.body.verificationResults.didYouMean != null) {
            this.didYouMean = resp.body.verificationResults.didYouMean;
          }
        }, err => {
          if (err.error.message.includes('403')) {
            this.emailVerificationResponse = 'Insufficient Balance';
          }else {
            this.emailVerificationResponse = false;
            this.error = this.helpers.error(err.error.message);
          }
        });
    }
  }

  /**
   * Patch Email value
   */
  patchEmail() {
    this.form.patchValue({
      email: this.didYouMean
    });
    this.emailVerificationResponse = false;
  }

  /**
   * NO EMAIL CHECKBOX
   */
  noEmailChecked() {
    this.noEmail = !this.noEmail;
    if (this.noEmail) {
      this.form.patchValue({email: '' });
      this.emailVerificationResponse = null;
    }else {
      this.form.patchValue({email: '' })
    }
  }

}


