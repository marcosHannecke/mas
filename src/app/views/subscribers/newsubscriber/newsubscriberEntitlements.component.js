"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var subscribers_service_1 = require("../../../services/subscribers.service");
var products_service_1 = require("../../../services/products.service");
var payments_service_1 = require("../../../services/payments.service");
var helpers_service_1 = require("../../../services/helpers.service");
var NewsubscriberEntitlementsComponent = /** @class */ (function () {
    function NewsubscriberEntitlementsComponent(router, route, subscribersService, productsService, paymentsService, helpers) {
        this.router = router;
        this.route = route;
        this.subscribersService = subscribersService;
        this.productsService = productsService;
        this.paymentsService = paymentsService;
        this.helpers = helpers;
        this.overlay = false;
        this.productGroups = [];
        this.PGFound = [];
        this.packagesSelected = [];
        this.submitted = false;
        this.entitlements = false;
        this.view1 = true;
        this.view2 = false;
        this.created = false;
        this.successMessage = false;
        this.cardTokens = {
            token: '',
            tokenId: ''
        };
        this.pleaseWait = true;
        this.previewDialog = false;
        this.discount = [];
        this.invalidDiscount = [];
        this.validDiscount = false;
    }
    ;
    NewsubscriberEntitlementsComponent.prototype.ngOnInit = function () {
        var _this = this;
        // ERRORS DIALOGS
        this.error = {
            subscriber: false,
            entitlements: false,
            vip: false,
            payment: false,
        };
        // GET CARD TOKENS
        this.router.queryParams
            .subscribe(function (params) {
            _this.cardTokens.token = params['token'];
            _this.cardTokens.tokenId = params['tokenId'];
            console.log(_this.cardTokens);
        });
        // HAS PAYMENT CARD OR VIP
        this.router.queryParams
            .subscribe(function (res) {
            _this.hasPaymentCard = res['hasPaymentCard'];
            console.log('payment', _this.hasPaymentCard);
        });
        // CHECK TOKENS AND SUBSCRIBER STORED
        if ((this.cardTokens.token == undefined || this.cardTokens.tokenId == undefined || this.cardTokens.token == ''
            || this.cardTokens.tokenId == '') && sessionStorage.getItem('subscriber') == null && this.hasPaymentCard != 1) {
            this.cardError = {
                message: 'Sorry we could not collect your payment details, ',
            };
        }
        else {
            console.log(this.cardTokens);
            // sessionStorage.setItem('cardTokens', JSON.stringify(this.cardTokens));
        }
        // GET STORED DATA FROM FORM TO GET SELLABLES
        this.subscriber = JSON.parse(sessionStorage.getItem('newSubscriber'));
        if (sessionStorage.getItem('newSubscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('newSubscriber'));
            this.vipVal = this.subscriber.vip;
            var array = ['cardHolderType=' + this.subscriber.cardHolderType, 'currencyCode=' + this.subscriber.currencyCode];
            // ADD CUSTOMER ID FROM SUBSCRIBER STORED
            // if(sessionStorage.getItem('subscriber')){
            //   this.subscriber.customerId = JSON.parse(sessionStorage.getItem('subscriber')).customerId;
            //   array.push('customerId=' + this.subscriber.customerId)
            // }
            this.productsService.getSellables(array)
                .subscribe(function (res) {
                res.sort(_this.helpers.alphabetical);
                // Remove OPPV from /get-sellables
                for (var i = 0; i < res.length; i++) {
                    if (res[i].pricing.oneOff == null || res[i].pricing.oneOff.indicative == false) {
                        if (!res[i].titleDescription.toLowerCase().includes('retention')) {
                            _this.productGroups.push(res[i]);
                            _this.PGFound.push(res[i]);
                        }
                    }
                }
            }, function (err) {
                _this.error = JSON.parse(err['_body']);
                console.log(_this.error.message);
            });
        }
        else {
            this.route.navigate(['/newsubscriber']);
            // this.productsService.getProducts('PG')
            //   .subscribe(res => {
            //     console.log('HEREEEEEEEAAAA',res);
            //     this.productGroups = res;
            //     this.PGFound = res;
            //   }, err => {
            //     this.error = JSON.parse(err['_body']);
            //     console.log(this.error.message);
            //   })
        }
    };
    // RESET ROUTE
    NewsubscriberEntitlementsComponent.prototype.reset = function () {
        var _this = this;
        var url = window.location.href;
        var array = url.split('/');
        var path = array[array.length - 1];
        this.route.navigateByUrl('blank').then(function () {
            _this.route.navigateByUrl(path);
        });
    };
    // PREVIEW DIALOG
    NewsubscriberEntitlementsComponent.prototype.addedRemovedPackages = function () {
        this.previewDialog = true;
        this.discount = [];
        this.invalidDiscount = [];
    };
    // CREATE SUBSCRIBER
    NewsubscriberEntitlementsComponent.prototype.createSubscriber = function () {
        var _this = this;
        // ADD CUSTOMER ID FROM SUBSCRIBER STORED
        if (sessionStorage.getItem('subscriber')) {
            this.subscriber.customerId = JSON.parse(sessionStorage.getItem('subscriber')).customerId;
        }
        delete this.subscriber.vip;
        // Valid discount
        this.validDiscount = false;
        for (var i = 0; i < this.invalidDiscount.length; i++) {
            if (this.invalidDiscount[i]) {
                this.validDiscount = true;
            }
        }
        if (this.validDiscount) {
            this.errorDiscount = {
                message2: 'Invalid discount value.'
            };
        }
        else if (this.created) {
            // save payment if subscriber else add entitlements
            if (sessionStorage.getItem('subscriber') == null) {
                this.savePayment();
            }
            else {
                // this.addEntitlements(this.created.id)
                this.vip(this.created.id);
            }
        }
        else {
            this.subscribersService.addSubscriber(this.subscriber)
                .subscribe(function (res) {
                _this.created = JSON.parse(res["_body"]);
                // save payment if subscriber else add entitlements
                if (sessionStorage.getItem('subscriber') == null && _this.hasPaymentCard != 1) {
                    _this.savePayment();
                }
                else {
                    // this.addEntitlements(this.created.id)
                    _this.vip(_this.created.id);
                }
                // set vip status
            }, function (err) {
                _this.error.subscriber = JSON.parse(err['_body']);
            });
        }
    };
    // SAVE PAYMENT (if successful add entitlements)
    NewsubscriberEntitlementsComponent.prototype.savePayment = function () {
        var _this = this;
        var data = {
            "customerId": this.created.customerId,
            "tokenId": this.cardTokens.tokenId,
            "token": this.cardTokens.token
        };
        this.paymentsService.putUrl('save-payment-card', data)
            .subscribe(function (res) {
            _this.addEntitlements(_this.created.id);
        }, function (err) {
            _this.error.payment = JSON.parse(err["_body"]);
        });
    };
    // VIP
    NewsubscriberEntitlementsComponent.prototype.vip = function (id) {
        var _this = this;
        if (this.vipVal == 'true') {
            var data = {
                "subscriberId": id,
                "vip": this.vipVal
            };
            this.subscribersService.urlSubscribers('set-vip', data)
                .subscribe(function (res) {
                _this.addEntitlements(id);
                console.log(res);
            }, function (err) {
                _this.error.vip = JSON.parse(err["_body"]);
            });
        }
        else if (this.hasPaymentCard == 1) {
            this.addEntitlements(id);
        }
        else {
            this.savePayment();
        }
    };
    // ADD ENTITLEMENTS
    NewsubscriberEntitlementsComponent.prototype.addEntitlements = function (id) {
        var _this = this;
        var array = [];
        var entitlements;
        for (var i = 0; i < this.packagesSelected.length; i++) {
            array[i] = this.packagesSelected[i].name;
        }
        // add discount
        var discount = {};
        for (var i = 0; i < array.length; i++) {
            if (this.discount[i] !== undefined) {
                if (this.discount[i] > 0) {
                    discount[array[i]] = this.discount[i];
                }
            }
        }
        entitlements = {
            "subscriberId": id,
            "productGroupNames": array,
            "productGroupsDiscounts": discount
        };
        if (array.length > 0) {
            this.productsService.addProducts('E', entitlements)
                .subscribe(function (res) {
                console.log(res);
                //SHOW SUBSCRIBER CREATED SUCCESSFULLY DIALOG
                if (!_this.error.subscriber && !_this.error.entitlemnents && !_this.error.vip && !_this.error.payment) {
                    _this.successMessage = true;
                }
            }, function (err) {
                _this.error.entitlements = JSON.parse(err["_body"]);
            });
        }
        else if (!this.error.subscriber && !this.error.entitlemnents && !this.error.vip && !this.error.payment) {
            this.successMessage = true;
        }
    };
    // PACKAGES FILTER
    NewsubscriberEntitlementsComponent.prototype.filter = function () {
        var found = [];
        var a = false;
        var b = false;
        for (var i = 0; i < this.productGroups.length; i++) {
            a = this.productGroups[i].name.toLowerCase().includes(this.filterValue.toLowerCase());
            b = this.productGroups[i].titleDescription.toLowerCase().includes(this.filterValue);
            if (a == true || b == true) {
                found.push(this.productGroups[i]);
            }
        }
        this.PGFound = found;
    };
    // SELECT PACKAGES
    NewsubscriberEntitlementsComponent.prototype.isSelected = function (pack) {
        var p = false;
        var pos;
        for (var i = 0; i < this.packagesSelected.length; i++) {
            if (this.packagesSelected[i].id == pack.id) {
                p = true;
                pos = i;
            }
        }
        return [p, pos];
    };
    NewsubscriberEntitlementsComponent.prototype.selectPackage = function (product, index) {
        this.PGFound.splice(index, 1);
        if (!this.isSelected(product)[0]) {
            this.packagesSelected.push(product);
        }
        else {
            this.packagesSelected.splice(this.isSelected(product)[1], 1);
        }
    };
    // REMOVE ENTITLEMENT
    NewsubscriberEntitlementsComponent.prototype.removePackage = function (pack, i) {
        console.log(this.packagesSelected);
        this.packagesSelected.splice(i, 1);
        if (!this.isSelected(pack)[0] || pack.length > 0) {
            this.PGFound.push(pack);
        }
        else {
            this.PGFound.splice(this.isSelected(pack)[1], 1);
        }
    };
    // DISCOUNT VALIDATION
    NewsubscriberEntitlementsComponent.prototype.discountValidation = function (discount, i) {
        if (discount != null) {
            this.invalidDiscount[i] = false;
            var regex = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
            if (!regex.test(discount) && discount) {
                this.invalidDiscount[i] = true;
            }
        }
    };
    NewsubscriberEntitlementsComponent = __decorate([
        core_1.Component({
            selector: 'app-newsubscriber-entitlements',
            templateUrl: 'newsubscriberEntitlements.component.html',
            styles: ['.product-desc{height: 130px; overflow:hidden} .ibox-content .row{min-height: 100px}' +
                    '.activeEntitlements .product-box:hover {box-shadow: none;}' +
                    '.product-box{border: none} .product-btn button{ background: white} ' +
                    '.product-btn button:hover{ background: #e23237; color: white !important}' +
                    '.productDescription{-webkit-line-clamp: 2;  overflow: hidden; display: -webkit-box;  -webkit-box-orient: vertical;} ' +
                    '.productDescription:hover{-webkit-line-clamp: 6;}' +
                    '.showOPPVs{position: relative !important; top: -24px !important; left: 0 !important; transition: all 0.5s;} ' +
                    '.showOPPVs .ibox-content {background: rgb(255, 250, 233) !important}' +
                    '.angle{position: absolute; right: 5px; bottom: 10px; width: 20px; height: 20px; font-size: 18px; color: #0570e2;}'],
            providers: [subscribers_service_1.SubscribersService, products_service_1.ProductsService, payments_service_1.PaymentsService, helpers_service_1.HelpersService]
        })
    ], NewsubscriberEntitlementsComponent);
    return NewsubscriberEntitlementsComponent;
}());
exports.NewsubscriberEntitlementsComponent = NewsubscriberEntitlementsComponent;
// @Pipe({
//   name: 'sortOrder'
// })
// export class SortOrder {
//   transform(value) {
//     var sortOrder = [
//       'TV Xcellence (New GBP Customer)',
//       'TV Xcellence (New GBP Customer)',
//       'TV Xcellence Annual (New GBP Customer)',
//       'TV Xcellence Annual (Reinstate GBP Customer)',
//       'Premier (New GBP Customer)',
//       'Premier (Reinstate GBP Customer)',
//       'Premier Annual (New GBP Customer)',
//       'Premier Annual (Reinstate GBP Customer)',
//       'Red Hot Monthly (New GBP Customer)',
//       'Red Hot Monthly (Reinstate GBP Customer)',
//       'Pay Per Night (New GBP Customer)'
//     ]
//     value.sort((a: any, b: any) => {
//
//       console.log('here');
//     });
//
//     return value;
//   }
// }
