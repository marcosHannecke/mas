"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var subscribers_service_1 = require("../../../services/subscribers.service");
var products_service_1 = require("../../../services/products.service");
var validators_service_1 = require("../../../services/validators.service");
var helpers_service_1 = require("../../../services/helpers.service");
var postcode_service_1 = require("../../../services/postcode.service");
var payments_service_1 = require("../../../services/payments.service");
var forms_1 = require("@angular/forms");
var NewsubscriberComponent = /** @class */ (function () {
    function NewsubscriberComponent(helpers, postcodeService, paymentsService, validatorsService, router, subscribersService, fb, productsService) {
        this.helpers = helpers;
        this.postcodeService = postcodeService;
        this.paymentsService = paymentsService;
        this.validatorsService = validatorsService;
        this.router = router;
        this.subscribersService = subscribersService;
        this.fb = fb;
        this.productsService = productsService;
        this.customerEmail = '';
        this.overlay = false;
        this.newSubscriber = {};
        this.submitted = false;
        this.success = false;
        this.hasPaymentCard = false;
        this.addressManually = false;
        this.match = false;
        this.noEmail = false;
        this.checked = true;
        this.payMethDialog = false;
    }
    NewsubscriberComponent.prototype.ngOnInit = function () {
        var subs;
        // GET USER ROLE
        if (sessionStorage.getItem('role') != null) {
            this.role = sessionStorage.getItem('role');
            console.log(this.role);
        }
        // GET SEARCH FORM IF STORED
        if (sessionStorage.getItem('searchForm')) {
            this.searchForm = JSON.parse(sessionStorage.getItem('searchForm'));
            console.log('searchForm', this.searchForm);
        }
        // IF 'ADD ADDITIONAL SUBSCRIBER' FROM CUSTOMER
        if (sessionStorage.getItem('subscriber')) {
            this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
            subs = this.subscriber;
            // Get customer email
            if (JSON.parse(sessionStorage.getItem('customerEmail')) != null) {
                this.customerEmail = JSON.parse(sessionStorage.getItem('customerEmail'));
            }
        }
        // IF COMING BACK FROM ERROR
        if (sessionStorage.getItem('newSubscriber')) {
            subs = JSON.parse(sessionStorage.getItem('newSubscriber'));
            this.customerEmail = subs.email;
        }
        else {
            this.customerEmail = '';
        }
        if (subs) {
            // Check for null and replace with empty string
            subs = this.helpers.nullCheck(subs);
            // change phones to avoid form errors
            if (subs.homeTelNr) {
                subs.homeTelNo = subs.homeTelNr;
                delete subs.homeTelNr;
                subs.workTelNo = subs.workTelNr;
                delete subs.workTelNr;
                subs.mobTelNo = subs.mobTelNr;
                delete subs.mobTelNr;
                subs.cliTelNo = subs.cliTelNr;
                delete subs.cliTelNr;
            }
            // FORM
            this.form = this.fb.group({
                'cardHolderType': [subs.cardHolderType, forms_1.Validators.required],
                'packRequestType': ['1', forms_1.Validators.required],
                'nrOfCards': ['1', [forms_1.Validators.required, this.validatorsService.onlyNumbers]],
                'title': [subs.title, forms_1.Validators.required],
                'initials': [subs.initials, [forms_1.Validators.required, forms_1.Validators.maxLength(3), this.validatorsService.letterSpaces]],
                'surname': [subs.surname, [forms_1.Validators.required, forms_1.Validators.minLength(2),
                        forms_1.Validators.maxLength(35), this.validatorsService.letterSpaces]],
                'addressLine1': [subs.addressLine1, [forms_1.Validators.required, forms_1.Validators.minLength(2),
                        forms_1.Validators.maxLength(35), this.validatorsService.specialChar]],
                'addressLine2': [subs.addressLine2, [forms_1.Validators.required, forms_1.Validators.minLength(2),
                        forms_1.Validators.maxLength(35), this.validatorsService.specialChar]],
                'addressLine3': [subs.addressLine3, forms_1.Validators.minLength(2)],
                'addressLine4': [subs.addressLine4, forms_1.Validators.minLength(2)],
                'addressLine5': [subs.addressLine5, forms_1.Validators.minLength(2)],
                'postcode': [subs.postcode, [forms_1.Validators.required, forms_1.Validators.minLength(2), forms_1.Validators.maxLength(9)]],
                'countryCode': [subs.countryCode],
                'phones': this.fb.group({
                    'homeTelNr': [subs.homeTelNo, [forms_1.Validators.maxLength(15), this.validatorsService.onlyNumbers]],
                    'workTelNr': [subs.workTelNo, [forms_1.Validators.maxLength(15), this.validatorsService.onlyNumbers]],
                    'mobTelNr': [subs.mobTelNo, [forms_1.Validators.maxLength(15), this.validatorsService.onlyNumbers]],
                }, { validator: this.validatorsService.phones }),
                'cliTelNr': [subs.cliTelNo],
                'currencyCode': [subs.currencyCode],
                'vip': ['false', forms_1.Validators.required],
                'email': [this.customerEmail, [forms_1.Validators.required, this.validatorsService.email]],
                'force': [false]
            });
        }
        else {
            // DATA FROM SEARCH FORM
            var sF = void 0;
            if (this.searchForm != null) {
                sF = {
                    surname: this.searchForm.surname,
                    addressLine1: this.searchForm.addressLine1,
                    addressLine2: this.searchForm.addressLine2,
                    postcode: this.searchForm.postcode
                };
            }
            else {
                sF = {
                    surname: '',
                    addressLine1: '',
                    addressLine2: '',
                    postcode: ''
                };
            }
            // FORM
            this.form = this.fb.group({
                'cardHolderType': ['DTH', forms_1.Validators.required],
                'packRequestType': ['1', forms_1.Validators.required],
                'nrOfCards': ['1', [forms_1.Validators.required, this.validatorsService.onlyNumbers]],
                'title': ['Mr', forms_1.Validators.required],
                'initials': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(3), this.validatorsService.letterSpaces]],
                'surname': [sF.surname, [forms_1.Validators.required, forms_1.Validators.minLength(2),
                        forms_1.Validators.maxLength(35), this.validatorsService.letterSpaces]],
                'addressLine1': [sF.addressLine1, [forms_1.Validators.required, forms_1.Validators.minLength(2),
                        forms_1.Validators.maxLength(35), this.validatorsService.specialChar]],
                'addressLine2': [sF.addressLine2, [forms_1.Validators.required, forms_1.Validators.minLength(2),
                        forms_1.Validators.maxLength(35), this.validatorsService.specialChar]],
                'addressLine3': ['', forms_1.Validators.minLength(2)],
                'addressLine4': ['', forms_1.Validators.minLength(2)],
                'addressLine5': ['', forms_1.Validators.minLength(2)],
                'postcode': [sF.postcode, [forms_1.Validators.required, forms_1.Validators.minLength(2), forms_1.Validators.maxLength(9)]],
                'countryCode': ['GBR'],
                'phones': this.fb.group({
                    'homeTelNr': ['', [forms_1.Validators.maxLength(15), this.validatorsService.onlyNumbers]],
                    'workTelNr': ['', [forms_1.Validators.maxLength(15), this.validatorsService.onlyNumbers]],
                    'mobTelNr': ['', [forms_1.Validators.maxLength(15), this.validatorsService.onlyNumbers]],
                }, { validator: this.validatorsService.phones }),
                'cliTelNr': [''],
                'currencyCode': ['GBP'],
                'vip': ['false', forms_1.Validators.required],
                'email': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(128), this.validatorsService.email]],
                'force': [false]
            });
        }
    };
    ;
    // SHOW WARNING IF COUNTRY AND CURRENCY DON'T MATCH
    NewsubscriberComponent.prototype.countryCurrencyMatch = function () {
        this.match = false;
        if ((this.form.value.currencyCode === 'GBP' && this.form.value.countryCode === 'IRL') ||
            (this.form.value.currencyCode === 'EUR' && this.form.value.countryCode === 'GBR')) {
            this.match = true;
        }
    };
    // PAYMENT METHOD
    NewsubscriberComponent.prototype.paymentMethod = function () {
        this.submitted = true;
        if (this.form.valid === true && this.form.value.vip === 'false') {
            this.payMethDialog = true;
        }
        else {
            this.processForm();
        }
    };
    // PROCESS AND VALIDATE FORM WHEN SUBMITTED
    NewsubscriberComponent.prototype.processForm = function () {
        var _this = this;
        this.payMethDialog = false;
        this.submitted = true;
        this.addressManually = true;
        // UPDATE NEW SUBSCRIBER JSON TO SEND
        this.newSubscriber = this.form.value;
        this.newSubscriber.homeTelNr = this.form.value.phones.homeTelNr;
        this.newSubscriber.workTelNr = this.form.value.phones.workTelNr;
        this.newSubscriber.mobTelNr = this.form.value.phones.mobTelNr;
        delete this.newSubscriber.phones;
        sessionStorage.setItem('newSubscriber', JSON.stringify(this.newSubscriber));
        if (this.form.valid === true) {
            // CHECK IF SUBSCRIBER STORED HAS PAYMENT CARD
            if (this.subscriber) {
                this.paymentsService.getUrl('has-payment-card?customerId=', this.subscriber.customerId)
                    .subscribe(function (res) {
                    _this.hasPaymentCard = JSON.parse(res['_body']).hasPaymentCard;
                    _this.proceed();
                    console.log(_this.hasPaymentCard);
                }, function (err) {
                    _this.error = JSON.parse(err['_body']);
                });
            }
            else {
                this.proceed();
            }
        }
    };
    // GO TO ENTITLEMENTS PAGE OR GET PAYMENT CARD
    NewsubscriberComponent.prototype.proceed = function () {
        var _this = this;
        if (this.hasPaymentCard || this.form.value.vip === 'true') {
            this.router.navigate(['/newsubscriberEntitlements'], { queryParams: { hasPaymentCard: '1' } });
        }
        else if (!this.hasPaymentCard) {
            // Data to send
            var data_1 = {
                currencyCode: this.newSubscriber.currencyCode,
                title: this.newSubscriber.title,
                initials: this.newSubscriber.initials,
                surname: this.newSubscriber.surname,
                addressLine1: this.newSubscriber.addressLine1,
                postcode: this.newSubscriber.postcode,
                country: this.newSubscriber.countryCode,
                email: this.newSubscriber.email,
                returnUrl: window.location.href + 'Entitlements',
                directDebit: this.payMeth
            };
            // Data to url string
            var str = Object.keys(data_1).map(function (key) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(data_1[key]);
            }).join('&');
            // GET PAYMENT CARD
            this.paymentsService.getUrl('get-payment-card-url?', str)
                .subscribe(function (res) {
                console.log(res);
                var url = JSON.parse(res['_body']);
                location.replace(url.storeCardUrl);
            }, function (err) {
                _this.error = JSON.parse(err['_body']);
            });
        }
    };
    // FIND ADDRESS
    NewsubscriberComponent.prototype.findAddress = function () {
        var _this = this;
        var postcode = this.form.value.postcode.replace(' ', '');
        this.postcodeService.postcode(postcode)
            .subscribe(function (res) {
            _this.addressList = JSON.parse(res['_body']);
            if (_this.addressList.length === 0) {
                _this.addressList = [{
                        notFound: 'NO ADDRESS FOUND'
                    }];
            }
        }, function (err) {
            _this.error = JSON.parse(err['_body']);
        });
    };
    // SET ADDRESS
    NewsubscriberComponent.prototype.setAddress = function (address) {
        if (address.notFound) {
            this.addressList = null;
        }
        else {
            this.addressList = null;
            this.addressManually = true;
            var add = this.helpers.nullCheck(address);
            this.form.controls['addressLine1'].setValue(add.addressLine1.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.form.controls['addressLine2'].setValue(add.addressLine2.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.form.controls['addressLine3'].setValue(add.addressLine3.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.form.controls['addressLine4'].setValue(add.addressLine4.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.form.controls['addressLine5'].setValue(add.addressLine5.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
        }
    };
    // NO EMAIL CHECKBOX
    NewsubscriberComponent.prototype.noEmailChecked = function () {
        this.noEmail = !this.noEmail;
        if (this.noEmail) {
            this.form.patchValue({ email: 'unknown@mediaaccessservices.net' });
        }
        else {
            this.form.patchValue({ email: '' });
        }
    };
    NewsubscriberComponent = __decorate([
        core_1.Component({
            selector: 'app-new-subscriber',
            templateUrl: 'newsubscriber.component.html',
            styles: ['.findAddress{position: absolute; background: white; z-index: 100; } ' +
                    '.findAddress{border-bottom: 1px solid #ececec; border-left: 1px solid #ececec;' +
                    ' border-right: 1px solid #ececec}' +
                    '.findAddress ul{padding: 0; margin: 0; max-height: 400px; overflow-y: scroll; min-width: 450px} ' +
                    '.findAddress li{padding: 5px 15px;} .findAddress li:hover{background: #f9f9f9}'],
            providers: [subscribers_service_1.SubscribersService, products_service_1.ProductsService, validators_service_1.ValidatorsService, payments_service_1.PaymentsService, postcode_service_1.PostcodeService, helpers_service_1.HelpersService]
        })
    ], NewsubscriberComponent);
    return NewsubscriberComponent;
}());
exports.NewsubscriberComponent = NewsubscriberComponent;
