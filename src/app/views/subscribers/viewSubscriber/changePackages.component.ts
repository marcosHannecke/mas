import {Component, OnInit} from '@angular/core';
import {HelpersService} from '../../../services/helpers.service';
import {ActivatedRoute} from '@angular/router';
import {RestService} from '../../../services/rest.service';
import {HttpClient} from '@angular/common/http';
import {Location} from '@angular/common';
import {Globals} from '../../../app.global';

declare var $: any;

@Component({
  selector: 'entitlements',
  templateUrl: './changePackages.component.html',
  styles: ['.product-desc{height: 130px; overflow:hidden} .ibox-content .row{min-height: 100px}' +
  '.activeEntitlements .product-box:hover {box-shadow: none;}' +
  '.product-box{border: none} .product-btn button{ background: white} ' +
  '.product-btn button:hover{ background: #e23237; color: white !important}' +
  '.productDescription{-webkit-line-clamp: 2;  overflow: hidden; display: -webkit-box;  -webkit-box-orient: vertical;}' +
  ' .productDescription:hover{-webkit-line-clamp: 6;}' +
  '.showOPPVs{position: relative !important; top: -24px !important; left: 0 !important; transition: all 0.5s;} ' +
  '.showOPPVs .ibox-content {background: rgb(255, 250, 233) !important}' +
  '.angle{position: absolute; right: 5px; bottom: 10px; width: 20px; height: 20px; font-size: 18px; color: #0570e2;}' +
  '.iboxShadow{}' +
  '.pin{width: 150px; margin: 0 auto;}' +
  '.summaryErrorDetail {  -webkit-line-clamp: 1;  display: -webkit-box;  -webkit-box-orient: vertical; overflow: hidden;}' +
  '.summaryErrorDetail:hover {  -webkit-line-clamp: 9 }' +
  '.fa-info-circle { font-size: 1.4em}'],

  providers: [  HelpersService, RestService, Globals ]
})

export class ChangePackagesComponent implements OnInit {
  productGroups: any = [];
  PGFound: any = [];
  error: any;
  subscriber: any;
  subscribers: any = [];
  filterValue: string;
  subscriberEnt: any = [];
  entUpdate: {};
  addEnt: any = [];
  type: any;

  previewDialog = false;
  removeDialog: any = false;

  view1 = true;
  view2 = false;

  packRemoved: any;
  packAdded: any;
  discount: any[] = [];
  invalidDiscount: any[] = [];
  validDiscount = false;

  showCards: any;
  OPPV: any = [];
  oppvPrices: any = {};
  allOPPVvalidated = false;
  OPPVvalidatePin: any;
  subscriberPin: any;
  pin: any;
  errorOPPVvalid: any;
  OPPVposition: any;
  OPPVresponse: any;
  count = 0;

  pleaseWait = false;

  date =  Date.now();
  options: any;
  cancellationNoticePeriod: any;

  requireAdmin: any = false;
  accessGranted: any = false;
  user: any;
  password: any;
  role: any;

  scheduledCancellations: any;
  successErrorPanel = [];
  showSuccessErrorPanel = false;

  overwriteEnt: any;

  subscriptionDetected = true;

  cancellationOptions = [
  'Not Happy with content',
  'Moving house (not taking sky)',
  'Moving to Virgin',
  'Moving to freeview',
  'Too expensive',
  'Lost Job',
  ];

  authorisedAdminUserId: any;

  public _array = Array;

  constructor(private router: ActivatedRoute, private rest: RestService, public location: Location, private helpers: HelpersService,
              public globals: Globals, private http: HttpClient) {}

  ngOnInit() {

    this.role = sessionStorage.getItem('role');
    // DATEPICKER OPTIONS
    this.options = {
      // minDate: this.date, // Minimal selectable date
      barTitleIfEmpty: 'Click to select a date',
      displayFormat: 'DD-MMM-YYYY'
    };

    // GET QUERY PARAMS GET TYPE OF PACKAGES TO SHOW (subscriptions || ppvs)
    this.router.queryParams
      .subscribe(params => {
        this.type = params['type'];
      });

    this.helpers.scrollTop();

    // GET SUBSCRIBER STORED
    this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));

    if (this.type == 1) {
      if (sessionStorage.getItem('sellables') != null) {
        this.OPPVs( this.productGroups = JSON.parse(sessionStorage.getItem('sellables')));
      }else {
        this.pleaseWait = true;
        // GET SELLABLE PACKAGES
        const array = ['subscriberId=' + this.subscriber['id']];

        const date = Math.round(new Date().getTime() / 1000) + 24 * 60 * 60;
        this.rest.get('/product-groups/search-sellable?q=' + array + ',maxValidFrom=' + date , )
          .subscribe(res => {
            this.OPPVs(res);
            this.pleaseWait = false;
          }, err => {
            this.error = this.helpers.error(err.error.message);
          });
      }
    }else if (this.type == 2) {
      if (sessionStorage.getItem('OPPVsellables') != null) {
        this.productGroups = JSON.parse(sessionStorage.getItem('OPPVsellables'));
        this.PGFound = JSON.parse(sessionStorage.getItem('OPPVsellables'));
      }else {
        this.pleaseWait = true;
        // GET OPPV
        this.getOPPV();
      }
    }
    this.getEntitlements();
    this.getNoticePeriod();

    // Get scheduled cancellations
    this.subscriptionsCancellations();
  };

  /**
   * GET THE SUBSCRIBER ENTITLEMENTS
   */
  getEntitlements() {
    let filter;
    if (this.type == 1) {
      filter = 'subscription';
      this.rest.get('/entitlements?subscriberId=' + this.subscriber['id'] + '&productType=' + filter)
        .subscribe(res => {
          this.subscriberEnt = res;
          this.getPPV_enabler();
        }, err => {
          this.error = this.helpers.error(err.error.message);
        })

    } else if (this.type == 2) {
      filter = 'IPPV_OPPV';

      this.rest.get('/entitlements?subscriberId=' + this.subscriber['id'] + '&productType=' + filter)
        .subscribe(res => {
          this.subscriberEnt = res;
          this.getProductGroups();
        }, err => {
          this.error = this.helpers.error(err.error.message);
        })
    }
  }

  /**
   * Get PPV_enabler
   */
  getPPV_enabler() {
    let resp;
    this.rest.get('/entitlements?subscriberId=' + this.subscriber['id'] + '&productType=ppv_enabler')
      .subscribe(res => {
        resp = res;
        for (const r of resp) {
          let found = false;
          this.subscriberEnt.forEach(e => {
            if (e.productGroupName == r.productGroupName) {
              found = true;
            }
          });
          if (!found) {
            this.subscriberEnt.push(r);
          }
        }
        this.getProductGroups();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * REMOVE OPPV FROM SELLABLES
   * @param res
   * @constructor
   */
  OPPVs(res) {
    const sellables = [];
    // Remove OPPV from /get-sellables
    for (let i = 0; i < res.length; i++) {
      if (res[i].pricing.oneOff == null || res[i].pricing.oneOff.indicative == false) {
        sellables.push(res[i])
      }
    }
    // sellables.sort(this.helpers.alphabetical);

    // sort sales items group array by display order
    function displayingOrder(a, b) {
      if (a.displayingOrder < b.displayingOrder) {return -1; }
      if (a.displayingOrder > b.displayingOrder) { return 1; }
      return 0;
    }

    sellables.sort(displayingOrder);

    this.productGroups = sellables;
    this.PGFound = sellables;
  };

  /**
   * ONLY APPLY THIS TO PORTLAND OTHER CUSTOMERS MAY HAVE DIFFERENT REQUIREMENTS
   * If subscriber has OPPV entitlement, remove sellables from same day to avoid adding it twice
   */
  removeEntitlementSameDaySellables() {
    // entitlement date
    let entDate;
    let entDay;
    let entMonth;
    let entYear;

    // sellable date
    let sellableDate;
    let sellableDay;
    let sellableMonth;
    let sellableYear;

    let sellables; // variable to store array filtered;

    for (const ent of this.addEnt) {
      entDate = new Date(ent.validTo * 1000);
      entDay = entDate.getDate();
      entMonth = entDate.getMonth();
      entYear = entDate.getFullYear();
      // Filter array
      sellables = this.PGFound.filter(f => {
        sellableDate = new Date(f[f.length - 1].validTo * 1000);
        sellableDay = sellableDate.getDate();
        sellableMonth = sellableDate.getMonth();
        sellableYear = sellableDate.getFullYear();
        if (entDay !== sellableDay || entMonth !== sellableMonth || entYear !== sellableYear) {
          return f;
        }
      });
      this.productGroups = sellables;
      this.PGFound = sellables;
    }
  }

  /**
   * GET OPPV
   */
  getOPPV() {
    let arr = [];
    const array = ['subscriberId=' + this.subscriber['id'], 'productTypesOnly=OPPV_ONLY|IPPV_OPPV'];
    const date = Math.round(new Date().getTime() / 1000 + 24 * 60 * 60 );

    // Function to sort ppvs by validFrom date
    function validFromSort(a, b) {
      if (a[0].validFrom < b[0].vlaidFrom) {
        return -1;
      }
      if (a[0].validFrom > b[0].validFrom) {
        return 1;
      }
      return 0;
    }

    let resp;
    this.rest.get('/product-groups/search-sellable/grouped-by-product?q=subscriberId%3D' + this.subscriber['id']
      + '%2CproductTypesOnly%3DOPPV_ONLY%7CIPPV_OPPV,maxValidFrom=' + date )
      .subscribe(res => {
        resp = res;
        arr = Object.keys(resp).map(key => resp[key]);
        this.productGroups = arr.sort(validFromSort);
        this.PGFound = arr.sort(validFromSort);
        this.pleaseWait = false;
        this.removeEntitlementSameDaySellables();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });
  }


  /**
   * GET PRODUCT GROUPS FROM ENTITLEMENTS 1/2
   */
  getProductGroups() {
    for (let i  = 0; i < this.subscriberEnt.length; i++) {
      let data;
      this.rest.get( '/product-groups/' + this.subscriberEnt[i].productGroupId)
        .subscribe(res => {
           data = res;
          // get price at time of purchase if it's not ppn reg
          if (this.subscriberEnt[i].productGroupName !== 'PG_RH_PPNReg') {
            this.getProductGroups2(this.subscriberEnt[i], data);
          }else {
            this.addEnt.push(data);
          }
        }, err => {
          this.error = this.helpers.error(err.error.message);
        })
    }
  }

  /**
   * GET PRODUCT GROUPS FROM ENTITLEMENTS 2/2
   * @param ent
   * @param data
   */
  getProductGroups2(ent, data) {

    // '/simplified/subscription-components?q=subscriberId=181236,productGroupId=2602565,cmsId=9&sortBy=createdAt&sortOrder=DESC&page=0&size=1'
    let resp;
    this.rest.get('/subscription-components?q=subscriberId=' + this.subscriber.id + ',productGroupId=' +
      ent.productGroupId + ',cmsId=9&sortBy=createdAt&sortOrder=DESC&page=0&size=1')
      .subscribe(r => {
        resp = r;
        if (this.type == 2) {
          this.addEnt.push(data);
          this.removeEntitlementSameDaySellables();
        } else if (resp[0]) {
          data.pricing.subscription.initialAmount = resp[0]['initialAmount'];
          data.pricing.subscription.recurringAmount = resp[0]['recurringAmount'];
          this.addEnt.push(data);
        }else {
          console.log('marcos', data, this.addEnt);
          if (data.pricing.subscription) {
            data.pricing.subscription.initialAmount = 'NA';
            data.pricing.subscription.recurringAmount = 'NA';
          }
          this.addEnt.push(data);
        }
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });
  }


  /**
   * PREVIEW DIALOG
   */
  addedRemovedPackages() {

    this.previewDialog = true;
    this.discount = [];
    this.invalidDiscount = [];
    const removed = [];
    const packAdded = [];
    this.oppvPrices = {};

    // PACKAGE REMOVED
    for (const ent of this.subscriberEnt){
      let found = true;
      for (const added of this.addEnt){
        if (added.id === ent.productGroupId) {
          found = false;
          break;
        }
      }
      if (found === true) {
        let data;
        this.rest.get('/product-groups/' + ent.productGroupId)
          .subscribe(res => {
             data = res;
            // get price at time of purchase if it's not ppn reg, PPV or VIP
            if (this.type != 2 && !this.subscriber.vip && data.productTypeStats.toLowerCase().includes('subscription')) {
              data.checked = false;
              // get earliest cancellation date
              this.getEarliestCancellation(ent, data);
              let resp;
              this.rest.get('/subscription-components?q=subscriberId=' + this.subscriber.id + ',productGroupId=' +
                ent.productGroupId + ',cmsId=9&sortBy=createdAt&sortOrder=DESC&page=0&size=1')
                .subscribe(r => {
                  resp = r;
                  if (resp[0]) {
                    data.pricing.subscription.initialAmount = resp[0]['initialAmount'];
                    data.pricing.subscription.recurringAmount = resp[0]['recurringAmount'];
                  }else {
                    data.pricing.subscription.initialAmount = 'NA';
                    data.pricing.subscription.recurringAmount = 'NA';
                  }
                  removed.push(data);
                }, err => {
                  this.error = this.helpers.error(err.error.message);
                });
            }else {
              removed.push(data);
            }
          }, err => {
            this.error = this.helpers.error(err.error.message);
          });
      }
    }

    this.packRemoved = removed;
    console.log('this.packRemoved', this.packRemoved);


    // PACKAGE ADDED
    for (let added of this.addEnt) {
      var found = false;
      for (let ent of this.subscriberEnt){
        if (added.id == ent.productGroupId) {
          found = true;
          break;
        }
      }
      if (found == false) {
        packAdded.push(added);
      }
    }
    this.packAdded = packAdded;

    // Detect subscription in packages to add to show / hide schedule cancellation options
    if (this.type == 1) {
      this.detectSubscription(packAdded);
    }
  }

  /**
   * CHECK IF ALL OPPV EVENTS HAVE BEEN VALIDATED
   */
  allOPPVvalidatedCheck() {
    let validated = true;
    this.validDiscount = false;
    for (let oppv of this.packAdded){

      if (oppv.validated == null && this._array.isArray(oppv)) {
        validated = false;
        break;
      }else if (!this._array.isArray(oppv)) {
        if (oppv.pricing.oneOff != null && oppv.validated == null) {
          if (oppv.pricing.oneOff.indicative == true) {
            validated = false;
            break;
          }
        }
      }
    }
    // Valid discount
    for (var i = 0; i < this.invalidDiscount.length; i++) {
      if (this.invalidDiscount[i]) {
        this.validDiscount = true;
      }
    }

    if (this.validDiscount) {
      this.error = {
        message2: 'Invalid discount value.'
      }
    } else if (validated) {
      this.updatedEnt();
    } else {
      this.allOPPVvalidated = true;
    }
  }

  /**
   * UPDATE ENTITLEMENTS
   */
  updatedEnt() {
    var add = [];
    var remove = [];
    var initialIds : any = [];

    for (var a = 0; a < this.subscriberEnt.length; a++) {
      initialIds.push(this.subscriberEnt[a].productGroupId);
    }
    var finalIds : any = [];
    for (var q = 0; q < this.addEnt.length; q++) {
      finalIds.push(this.addEnt[q].id);
    }

    // ADD IDS
    for (var i = 0; i < this.addEnt.length; i++) {
      if (!initialIds.includes(this.addEnt[i].id)) {
        add.push(this.addEnt[i])
      }
    }

    // REMOVE IDS
    for (var b = 0; b < initialIds.length; b++) {
      if (!finalIds.includes(initialIds[b])) {
        remove.push(this.subscriberEnt[b]);
      }
    }

    // ADD NAMES
    for (var r = 0; r < add.length; r++) {
      add[r] = add[r].name;
    }



    // DISCOUNT
    let discount = {};

    for (var i = 0; i < this.packAdded.length; i++ ) {
      if (this._array.isArray(this.packAdded[i])) {
        lastOPPV = this.getLastOPPV(i);
      }else {
        lastOPPV = this.packAdded[i];
      }
      // REPLACE NULL WITH WITH lastOPPV NAME
      add[i] = lastOPPV.name;

      if (this.discount[i] !== undefined) {
        if (this.discount[i] > 0) {
          discount[add[i]] = Number(this.discount[i]);
        }
      }
    }


    // REPLACE NULL WITH WITH lastOPPV NAME
    var lastOPPV;
    for ( i = 0; i < this.packAdded.length; i++ ) {
      if (this._array.isArray(this.packAdded[i])) {
        lastOPPV = this.getLastOPPV(i);
      }else {
        lastOPPV = this.packAdded[i];
      }
      // REPLACE NULL WITH WITH lastOPPV NAME
      add[i] = lastOPPV.name;
    }


    // ADD PRICES
    /******* DATA TO SEND*******/
    this.entUpdate = {
      'subscriberId': this.subscriber['id'],
      'addedProductGroupNames': add,
      'addedOppvProductGroupsPrices': this.oppvPrices,
      'addedProductGroupsDiscounts': discount,
      // 'removedProductGroupNames': remove
    };


    // REMOVE NAMES
    const removeTitles = [];
    for (var t = 0; t < remove.length; t++) {
      // removeTitles[t] = remove[t].productGroupTitleDescription;
      // remove[t] = remove[t].productGroupName;
    }

    // Only add 'removedProductGroupNames' packages if PPV or PPN reg or VIP
    if (this.type == 2 || this.subscriber.vip || this.subscriptionDetected) {
      this.entUpdate['removedProductGroupNames'] = remove;
    } else {
      this.entUpdate['removedProductGroupNames'] = [];
      remove.forEach(e => {
        console.log(remove);
        // if (e.toLowerCase().includes('ppnreg')) {
        if (!e.productTypeStats.toLowerCase().includes('subscription')) {
          this.entUpdate['removedProductGroupNames'].push(e.productGroupName);
        }
      })
    }

    // Update Request
    let panelData;
    if (this.entUpdate['addedProductGroupNames'].length > 0 || this.entUpdate['removedProductGroupNames'].length > 0) {
      this.http.put(this.globals.API_ENDPOINT + '/entitlements', this.entUpdate, {responseType: 'text'})
        .subscribe(res => {
          // ADD SUMMARY TABLE DATA
          for (const added of this.entUpdate['addedProductGroupNames']) {
            panelData = {
              status: 'success',
              response: res,
            };
            panelData.packageData = this.entUpdate;
            panelData.package = this.getProductGroupTitleDescription(added, 1);
            panelData.type = 'add';
            this.successErrorPanel.push(panelData);
          }
          for (const removed of this.entUpdate['removedProductGroupNames']) {
            panelData = {
              status: 'success',
              response: res,
            };
            panelData.packageData = this.entUpdate;
            panelData.package = this.getProductGroupTitleDescription(removed, 2);
            panelData.type = 'cancel';
            this.successErrorPanel.push(panelData);
          }

          // show panel or go back
          if (this.type == 1) {
            this.previewDialog = false;
            this.showSuccessErrorPanel = true;
          } else if (this.type == 2) {
            // Back to subscriber
            this.location.back();
          }
        }, err => {
          // ADD SUMMARY TABLE DATA
          for (const added of this.entUpdate['addedProductGroupNames']){
            panelData = {
              status: 'error',
              response: err,
            };
            panelData.packageData = this.entUpdate;
            panelData.package = this.getProductGroupTitleDescription(added, 1);
            panelData.type = 'add';
            this.successErrorPanel.push(panelData);
          }
          for (const removed of this.entUpdate['removedProductGroupNames']){
            panelData = {
              status: 'error',
              response: err,
            };
            panelData.packageData = this.entUpdate;
            panelData.package = this.getProductGroupTitleDescription(removed, 2);
            panelData.type = 'cancel';
            this.successErrorPanel.push(panelData);
          }

          // Display error
          const error = this.helpers.error(JSON.parse(err.error).message);
          if (error.message.toLowerCase().includes('payment failed')) {
            this.error = {
              message: error.message,
              message2: 'Please update the customer payment details with a valid card and try again.'
            };
          }else {
            this.error = error;
          }
          // show panel or go back
          if (this.type == 1) {
            this.previewDialog = false;
            this.showSuccessErrorPanel = true;
          }
        });
    } else {
      // show panel or go back
      if (this.type == 1) {
        this.previewDialog = false;
        this.showSuccessErrorPanel = true;
      } else if (this.type == 2) {
        // Back to subscriber
        this.location.back();
      }
    }
  }

  /**
   * get product groups title description
   * @param productGroupName
   * @param type --> 1 = added or 2 = remove
   */
  getProductGroupTitleDescription(productGroupName, type) {
    let titleDescription;
    if (type === 1) {
      this.addEnt.forEach(ent => {
        if (ent.name === productGroupName) {
          titleDescription = ent.titleDescription;
        }
      });
    } else if (type === 2) {
      this.subscriberEnt.forEach(ent => {
        if (ent.productGroupName === productGroupName) {
          titleDescription = ent.productGroupTitleDescription;
        }
      });
    }

    return titleDescription;
  }

  /**
   * SHOW OPPVs CARDS
   * @param i
   */
  showOPPVs(i) {
    if (this.showCards == i) {
      this.showCards = null;
    }else {
      this.showCards = i;
    }
  }

  /**
   * SELECT PACKAGES
   * @param pack
   * @returns {(boolean | any)[]}
   */
  isSelected(pack) {
    var p = false;
    var pos;
    for (var i = 0; i < this.addEnt.length; i++) {
      if (this.addEnt[i].id == pack.id) {
        p = true;
        pos = i;
      }
    }
    return [p, pos];
  }

  /**
   * SELECT NORMAL SELLABLE PACKAGE
   * @param product
   * @param index
   */
  selectPackage(product, index) {
    this.PGFound.splice(index , 1);
    if (!this.isSelected(product)[0]) {
      this.addEnt.push(product);
    } else {
      this.addEnt.splice(this.isSelected(product)[1], 1);
    }
  }

  /**
   * SELECT ONLY OPPV CLICKED
   * @param product
   * @param i
   * @param ind
   * @param pG
   */
  selectOPPV(product, i,  ind, pG) {
    // var oppv = this.getLastValidFrom(pG);
    var oppv = pG[pG.length - 1];
    var productIdValidFromId = oppv.productIds[oppv.productIds.length - 1];

    if (this.PGFound[i].length == 0) {
      this.PGFound.splice(i , 1);
      this.showCards = false;
    }

    if (!this.isSelected(product)[0]) {
      // add productIdValidFromId which will be used to get caInstNum
      product['productIdValidFromId'] =  productIdValidFromId;

      // add product to added packages
      this.addEnt.push(product);
    } else {
      this.addEnt.splice(this.isSelected(product)[1], 1);
    }

    // Remove selected from sellables
    this.PGFound[i].splice(ind , 1);
  }

  /**
   * get valid to from last valid from
   * @param pG
   * @returns {any}
   */
  getLastValidFrom(pG) {
    var last = pG[0];
    for (let date of pG){
      if (date.validFrom > last.validFrom) {
        last = date;
      }
    }
    return last;
  }

  /**
   * SELECT ALL OPPV from same group
   * @param i
   */
  selectAllOPPV(i) {
    this.addEnt.push(this.PGFound[i]);
    this.PGFound.splice(i , 1);
  }

  // RESET VALIDATED
  resetValidated() {
    for (let i = 0; i < this.packAdded.length; i ++) {
      this.packAdded[i].validated = null;
    }
  }

  /**
   * Check if cancellation already scheduled
   */
  canxAlreadyScheduled() {
    let found = false;
    for (const canx of  this.scheduledCancellations) {
      if (this.removeDialog.pack.id === canx.subscriptionComponent.productGroup.id) {
        found = true;
        break;
      }
    }
    // If cancellation already scheduled display error
    if (found) {
      // close dialog
      this.removeDialog = null;
      // display error
      this.error = {
        message2: 'This package is already scheduled for cancellation.',
        message3: 'Return to View Account and select Scheduled Cancellations to stop the cancellation.'
      }
    }else {
      this.removeEntitlement();
    }
  }

  /**
   * REMOVE ENTITLEMENT
   */
  removeEntitlement() {

    // pack.id
    // this.scheduledCancellations[i].subscriptionComponent.productGroup.id;
    const pack = this.removeDialog.pack;
    const i = this.removeDialog.i;

    this.addEnt.splice(i , 1);
    if (!this.isSelected(pack)[0] || pack.length > 0) {
      this.PGFound.push(pack);
    } else {
      this.PGFound.splice(this.isSelected(pack)[1], 1);
    }
    this.removeDialog = false;
  }

  /**
   * VALIDATE OPPV EVENT 1/3
   * @param i
   */
  validateOPPVevent(i) {
    var lastOPPV = '';

    // Set productId of single selected
    var productId = this.packAdded[i].productIdValidFromId;

    // Set productId of group selected
    if (this._array.isArray(this.packAdded[i])) {
      lastOPPV = this.getLastOPPV(i);
      productId = this.getProductIdFromLast(lastOPPV);
    }

    // GET caProductId
    let resp;
    this.rest.get('/products/' + productId)
      .subscribe(res => {
        resp = res;
        this.validateOPPVrequest(resp.caProductId, resp.caInstNum, i);
      }, err => {
        // this.error = JSON.parse(err["_body"])
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * VALIDATE OPPV EVENT 2/3
   * @param caProductId
   * @param caInstNum
   * @param i
   */
  validateOPPVrequest(caProductId, caInstNum, i) {

    let url = 'validate-oppv-event-for-subscriber?';
    url +=  'cardSubscriberId=' + this.subscriber.cardSubscriberId;
    url += '&caProductId=' + caProductId;
    url += '&caInstNum=' + caInstNum;

    let resp;
    this.rest.get('/entitlements/' + url)
      .subscribe(res => {
         resp = res;

        this.subscriberPin = resp.pin;
        // Check if client can purchase
        this.validOPPV(resp, i);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })

  }

  /**
   * validOPPV
   * @param res
   * @param i
   */
  validOPPV(res, i) {
    this.OPPVposition = i;
    this.OPPVresponse = res;

    if (!res.validPpvEvent) {
      this.errorOPPVvalid = {
        'error' : 'This OPPV event is invalid.',
        'messages' : res.messages
      };
    }else if (res.blackedOut) {
      this.errorOPPVvalid = {
        'error' : 'The customer cannot purchase this OPPV event',
        'messages' : res.messages
      };
    }else if (res.pinRequired) {
      if (this.count >= 3) {
        this.OPPVvalidatePin = null;
        this.error = {
          message: 'Number of allowed PIN attempts exceeded. Please try again later.'
        }
      }else {
        this.OPPVvalidatePin = {
          'message' : 'Customer must provide their Sky PIN to purchase this OPPV event',
          'messages' : res.messages
        };
      }
    }else if (!res.pinRequired) {
      this.validate();
    }
  }

  /**
   * Pin check
   * @constructor
   */
  PINcheck() {
    if (this.count >= 3) {
      this.OPPVvalidatePin = null;
      this.error = {
        message: 'Number of allowed PIN attempts exceeded. Please try again later.'
      }
    }else if (this.pin === this.subscriberPin) {
      this.OPPVvalidatePin = null;
      this.count = 0;
      this.validate();
    }else {
      if (this.count < 3) {
        this.error = {
          message: 'PIN incorrect'
        };
        this.count++;
      }
    }
  }

  /**
   * OPPV VALIDATE 3/3
   */
  validate() {
    let i = this.OPPVposition;
    let res = this.OPPVresponse;
    // update package info
    this.packAdded[i].validated = true;
    this.packAdded[i].price = res.price;
    this.packAdded[i].pin = res.pin;
    this.packAdded[i].individualSpendLimit = res.individualSpendLimit;

    // OPPV PRICES
    if (this._array.isArray(this.packAdded[i])) {
      var lastOPPV = this.getLastOPPV(i);
      this.oppvPrices[lastOPPV.name] = res.price;

    }else if (!this._array.isArray(this.packAdded[i])) {
      this.oppvPrices[this.packAdded[i].name] = res.price;
    }
  }

  /**
   * Get last oppv
   * @param i
   * @returns {any}
   */
  getLastOPPV(i) {
    // Patch to get always LAST ORDERS instead of last OPPV
    const array = [];
    for (let a  = 0; a < this.packAdded[i].length; a ++) {
      if (this.packAdded[i][a].titleLabel.toLowerCase().includes('last orders')) {
        array.push(this.packAdded[i][a]);
      }
    }

    // return this.packAdded[i][this.packAdded[i].length - 1]
    return array[array.length - 1];
  }

  getProductIdFromLast(OPPV) {
    return OPPV.productIds[0];
  };

  /**
   * PACKAGES FILTER
   */
  filter() {
    var found = [];
    var a = false;
    var b = false;
    var c = false;
    var dupli = false;

    for (var i = 0; i < this.productGroups.length; i++) {
      // check for duplicates
      dupli = false;
      for (var w = 0; w < this.addEnt.length; w++) {
        if (this.productGroups[i].id ==  this.addEnt[w].id) {
          dupli = true;
        }
      }
      if (!dupli) {
        if (!this._array.isArray(this.productGroups[i])) {
          a = this.productGroups[i].name.toLowerCase().includes(this.filterValue.toLowerCase());
          b = this.productGroups[i].titleDescription.toLowerCase().includes(this.filterValue.toLowerCase());
          c = this.productGroups[i].id.toString().includes(this.filterValue.toString());
          if (a == true || b == true || c == true) {
            found.push(this.productGroups[i]);
          }
        } else if (this._array.isArray(this.productGroups[i])) {
        }
      }
    }
    this.PGFound = found;
  }

  /**
   * DISCOUNT VALIDATION
   * @param discount
   * @param i
   */
  discountValidation(discount, i) {
    if (discount != null) {
      this.invalidDiscount[i] = false;
      const regex = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
      if (!regex.test(discount) && discount) {
        this.invalidDiscount[i] = true;
      }
    }
  }

  /**
   *  OPPV FILTER
   */
  filterOPPV() {
    let found = [];
    let a = false;
    let OPPVlist = [];
    let dupli = false;

    if (this.filterValue.length == 0) {
      this.PGFound = this.productGroups;
    }else {
      for (let o = 0; o < this.productGroups.length; o++) {
        for (let e = 0; e < this.productGroups[o].length; e++) {
          OPPVlist.push(this.productGroups[o][e])
        }
      }
      for (let i = 0; i < OPPVlist.length; i++) {
        // check for duplicates
        dupli = false;
        for (let w = 0; w < this.addEnt.length; w++) {
          if (OPPVlist[i].id == this.addEnt[w].id) {
            dupli = true;
          }
        }
        if (!dupli) {
          a = OPPVlist[i].titleDescription.toLowerCase().includes(this.filterValue.toLowerCase());
          if (a == true ) {
            found.push(OPPVlist[i]);
          }
        }
      }
      this.PGFound = found;
    }
  }


  /**
   * Schedule Cancellations
   */
   scheduleCancellations() {
        const data = {
          'subscriberId': this.subscriber.id,
          'entitlementId': null,
          'cancellationDate': null,
          'notes': '',
          'authoriserId': this.authorisedAdminUserId
        };

      let panelData;
      // Resolve promise before continuing
      const promise = ( packRemoved, subscriberEnt) => {
        return new Promise((resolve, reject) => {
          if (packRemoved.length >  0) {
            let index = 0;
            packRemoved.forEach(r => {
              index ++;
              subscriberEnt.forEach(e => {
                if (r.name === e.productGroupName && !r.name.toLowerCase().includes('ppnreg')
                  && r.productTypeStats.toLowerCase().includes('subscription') && !this.subscriber.vip && !this.subscriptionDetected) {
                  r.entId = e.id;
                  r.date = new Date(r.date);
                  // Set Data
                  data.entitlementId = r.entId;
                  data.cancellationDate = + new Date(Date.UTC( r.date.getFullYear(), r.date.getMonth() , r.date.getDate())) / 1000;
                  if (r.reason === undefined) {
                    r.reason = ''
                  }
                  if (r.notes === undefined) {
                    r.notes = ''
                  }
                  data.notes = r.reason + ' - ' + r.notes;
                  this.rest.post('/subscription-component-cancellations', data)
                    .subscribe((res) => {
                      panelData = {
                        status: 'success',
                        response: res,
                        packageData: data,
                        package: r,
                        type: 'cancel'
                      };

                      this.successErrorPanel.push(panelData);
                      // If last resolve promise
                      if (index === packRemoved.length) {
                        resolve();
                      }
                    }, err => {
                      panelData = {
                        status: 'error',
                        response: err,
                        packageData: data,
                        package: r,
                        type: 'cancel'
                      };
                      this.successErrorPanel.push(panelData);
                      resolve();
                      console.error('cancellationError', err.error.message);
                      // this.error = this.helpers.error(err.error.message);
                    });
                } else {
                  // If last resolve promise
                  if (index === packRemoved.length) {
                    resolve();
                  }
                }
              })
            });
          } else {
            resolve()
          }
        });
      };

    // Promise to continue once all cancelled
    promise( this.packRemoved, this.subscriberEnt).then(() => {
      this.allOPPVvalidatedCheck();
    });
  }

  /**
   * overwrite earliest cancellation date
   */
  overwriteEarliestCancellation() {
     if (this.accessGranted) {
       const ent = this.overwriteEnt.ent;
       // Date
       ent.date = new Date( Date.now() + ((this.cancellationNoticePeriod ) * this.globals.ONE_DAY_SECONDS) * 1000);
       // Minimal selectable date
       ent.options.minDate = Date.now() + ((this.cancellationNoticePeriod - 1) * this.globals.ONE_DAY_SECONDS) * 1000;

     } else {
       this.addedRemovedPackages();
     }

  }

  /**
   * Get notice period
   */
  getNoticePeriod() {
     this.rest.get('/subscription-component-cancellations/get-notice-period-days')
       .subscribe(res => {
         this.cancellationNoticePeriod = res['days'];
       }, err => {
         this.error = this.helpers.error(err.error.message);
       })
  }

  /**
   * get earliest cancellation date
   * @param e --> entitlement
   * @param d --> delete entitlement
   */
  getEarliestCancellation(e, d) {
    // productGroupName"PG_RH_PPNReg"
    if (!e.productGroupName.toLowerCase().includes('ppnreg')) {
    return this.rest.get('/entitlements/earliest-cancellation/' + e.id)
      .subscribe(res => {
        // Check if earliest cancellation is in the past
        if ( res['earliestCancellationDate'] < Math.round(Date.now() / 1000)) {
          d.earliestInThePast =  res['earliestCancellationDate'];
          res['earliestCancellationDate'] = Math.round(Date.now() / 1000);
        }
        // earliest cancellation
        const todayPlusNotice = Math.round(Date.now() / 1000) + (this.cancellationNoticePeriod * 24 * 60 * 60);
        if (todayPlusNotice < res['earliestCancellationDate']) {
          d.earliestCancellation = res['earliestCancellationDate'];
        } else {
          d.earliestCancellation = todayPlusNotice;
        }

        // add default date for schedule cancellation datepicker
        d.date = ( d.earliestCancellation) * 1000;
        // datepicker options
        d.options = {
          // Minimal selectable date
          minDate: ( d.earliestCancellation  - this.globals.ONE_DAY_SECONDS) * 1000,
          barTitleIfEmpty: 'Click to select a date',
          displayFormat: 'DD-MMM-YYYY'
        };
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
    }
  }


  /**
   * Detect subscription in packages to add to show / hide schedule cancellation options
   */
  detectSubscription(packages) {
    this.subscriptionDetected = true;
    let count = 0;
    for (const pack of packages ) {
      if (pack.titleLabel.toLowerCase().includes('ppnreg')) {
        count ++;
      }
    }
    if (count === packages.length) {
      this.subscriptionDetected = false;
    }
  }

  /**
   * Get subscriber subscriptions cancellations
   */
  subscriptionsCancellations() {
    this.rest.get('/subscription-component-cancellations?q=subscriberId=' + this.subscriber.id)
      .subscribe( res => {
        this.scheduledCancellations = res;
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * add S. cancellation reason
   * @param ent --> entitlements to be canceled
   * @param e --> event to get value
   */
  addScheduleCancellationReason(ent, e) {
    ent.reason = e.target.value;
  }

  /**
   * Add S. cancellation note
   * @param ent --> entitlements to be canceled
   * @param e --> event to get value
   */
  addScheduleCancellationNote(ent, e) {
     ent.notes = e.target.value;
  }


  /**
   * Auth require error
   * @param e -> event emitted by auth-required component
   */
  authError(e) {
    this.error = e;
    // SET TO FALSE
    // this.cancelAccount = false;
    // this.changeCreditLimit = false;
    // this.delEv = false;
  }

  /**
   * Close all dialogs that require authorization
   * to avoid multiple dialogs showing
   */
  closeDialogs() {
    if (this.requireAdmin == null && this.accessGranted !== 'admin') {
      this.packRemoved[this.overwriteEnt.index].checked = false;
    }
  }

  /**
   * CHECK ADMIN
   */
  checkIfAdmin() {
    if (this.overwriteEnt.checked) {
      this.accessGranted = '';
      this.user = '';
      this.password = '';
      if (this.role === 'admin' || this.role === 'super_admin') {
        this.accessGranted = 'admin';
        this.overwriteEarliestCancellation();
      }else {
        this.requireAdmin = true;
      }
    }else {
      this.addedRemovedPackages();
    }
  }

}


