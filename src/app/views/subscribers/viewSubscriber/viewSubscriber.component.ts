import {Component, OnDestroy, OnInit, Pipe, PipeTransform} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from '../../../services/helpers.service';
import {PostcodeService} from '../../../services/postcode.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RestService} from '../../../services/rest.service';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../../app.global';

import 'rxjs/add/operator/takeUntil';
import {Subject} from 'rxjs/Subject';


@Component({
  selector: 'viewSubscriber',
  templateUrl: 'viewSubscriber.component.html',
  styleUrls: ['viewSubscriber.component.css'],
  providers: [ValidatorsService, HelpersService,
     PostcodeService, RestService, HttpClient]
})
export class ViewSubscriberComponent implements OnInit, OnDestroy {
  role: string;
  subscriber: any = false;
  subscriberId: number;
  entitlements: any = [];
  entitlementsFiltered: any = [];
  error: any;
  status: string;
  active: boolean;
  entAwaitingActivation: boolean;
  processArray: any = process;
  processFound: any = process;
  str = '';
  vipBox = false;
  vipVal: boolean;
  eventHistory: any = [];
  tenEventHistory: any = [];
  eventShow: any = 5;
  addEv = false;
  updEv = false;
  delEv = false;
  note = '';
  eventId: any;
  collapse = true;
  eventHeaders: any;
  pagination: Pagination = <any>{};
  CAoptions = false;
  customer: any;
  billingInfo: any = {};
  changeCreditLimit: any = false;
  creditLimit: any;
  paymentSuccess = false;
  changeDisabled = false;
  vipChangeDisabled = false;
  CSIDisabled = false;
  CSIDisabledCheck = false;
  reauthoriseSuccess = false;
  callbackDialog = false;
  hideDuplicates = true;

  cancelAccount = false;
  waitSubsActive = false;

  submitted = false;
  editVCNForm: FormGroup;
  changeCreditLimitForm: FormGroup;
  eventsFilter: FormGroup;
  invalidFilter = false;
  editViewingCardNumber: any = false;
  newViewingCardNumber: any = '';
  onlyAgent: boolean;

  requireAdmin: any = false;
  accessGranted: any = false;
  user: any;
  password: any;

  processBox = false;
  process6 = false;
  process7 = false;
  process9 = false;
  process10 = false;
  process11 = false;
  process14 = false;
  process15 = false;
  process18 = false;
  process29 = false;
  process32 = false;
  process34 = false;
  process35 = false;

  freeview = false;
  username: string;

  open: any;

  stopCancellationsList = [];
  scheduledCancellations: any;
  scheduledCancellationsDialog = false;
  cancellationEdit: any;
  cancellationDelete: any;
  date =  Date.now();
  options: any;

  private unsubscribe: Subject<void> = new Subject();

  constructor( private router: ActivatedRoute, private route: Router, private fb: FormBuilder, private helpers: HelpersService,
               private validators: ValidatorsService, private postcodeService: PostcodeService, private rest: RestService,
               private http: HttpClient, private globals: Globals) {}

  ngOnInit() {
    sessionStorage.removeItem('cardProcess'); // IMPORTANT
    sessionStorage.removeItem('customer'); // IMPORTANT
    sessionStorage.removeItem('rapb'); // IMPORTANT
    sessionStorage.removeItem('billingInfo'); // IMPORTANT

    // DATEPICKER OPTIONS
    this.options = {
      // minDate: this.date, // Minimal selectable date
      barTitleIfEmpty: 'Click to select a date',
      displayFormat: 'DD-MMM-YYYY'
    };

    // GET QUERY PARAMS
    this.router.queryParams
      .takeUntil(this.unsubscribe)
      .subscribe(params => {
        this.process15 = params['p'];
      });

    // GET USER NAME
    if (sessionStorage.getItem('user') != null) {
      this.username = sessionStorage.getItem('user');
      console.log(this.username);
    }

    // SCROLL TOP
    this.helpers.scrollTop();

    // GET USER ROLE
    if (sessionStorage.getItem('role') != null) {
      this.helpers.getRole()
        .subscribe(res => {
          if (res['roles'][0].name.toLowerCase().includes('super_admin')) {
            this.role = 'super_admin';
            console.log('here', this.role);
          }else {
            console.log('here', this.role);
            this.role = sessionStorage.getItem('role');
          }
        });

    }

    // GET ID FROM PARAMS
    this.router.params
      .takeUntil(this.unsubscribe)
      .map(params => params['id'])
      .subscribe((id) => {
        this.subscriberId = id;
        this.init();
      });

    // FORMS AND VALIDATORS
    this.editVCNForm = this.fb.group({
      'viewingCardNumber': ['', [ Validators.required, Validators.maxLength(9), this.validators.onlyNumbers]]
    });

    this.changeCreditLimitForm = this.fb.group({
      'creditLimit': ['', [ Validators.required, Validators.maxLength(12), this.validators.onlyNumbers]]
    });

    this.eventsFilter = this.fb.group({
      'filterValue': ['', [ this.validators.specialChar]]
    });

    this.subscriberStatus();

    // GET SUBSCRIPTIONS CANCELLATIONS
    this.subscriptionsCancellations();
  };

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  // LOAD DATA WHEN ROUTE CHANGES
  init() {
    sessionStorage.removeItem('cardProcess'); // IMPORTANT
    sessionStorage.removeItem('customer'); // IMPORTANT
    sessionStorage.removeItem('rapb'); // IMPORTANT
    sessionStorage.removeItem('billingInfo'); // IMPORTANT
    // GET USER ROLE
    if (sessionStorage.getItem('role') != null) {
      this.helpers.getRole()
        .subscribe(res => {
          if (res['roles'][0].name.toLowerCase().includes('super_admin')) {
            this.role = 'super_admin'
          }else {
            this.role = sessionStorage.getItem('role');
          }
        });
    }
    // FORMS AND VALIDATORS
    this.editVCNForm = this.fb.group({
      'viewingCardNumber': ['', [ Validators.required, Validators.maxLength(9), this.validators.onlyNumbers]]
    });
    this.changeCreditLimitForm = this.fb.group({
      'creditLimit': ['', [ Validators.required, Validators.maxLength(12), this.validators.onlyNumbers]]
    });
    this.eventsFilter = this.fb.group({
      'filterValue': ['', [ this.validators.specialChar]]
    });
    // GET SUBSCRIBER BY ID
    this.getSubscriber();
    // GET EVENT HISTORY
    this.getHistory();

    this.getTenDaysHistory();
    // GET CUSTOMER
    this.getCustomer();

    // GET SUBSCRIPTIONS CANCELLATIONS
    this.subscriptionsCancellations();

    // SUBSCRIBER STATUS
    this.subscriberStatus();
  }

  // TOKEN CHECK (IF TOKEN THEN PROCESS PROCESS PAYMENT)
  tokenCheck() {
    this.router.queryParams
      .takeUntil(this.unsubscribe)
      .subscribe(params => {
        if (params['token'] != null) {
          this.processPayment();
        }
      });
  }

  // GET CUSTOMER
  private getCustomer() {
    this.rest.get('/customers?q=subscriberId=' + this.subscriberId)
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        this.customer = res[0];
        if (this.customer.email === 'unknown@mediaaccessservices.net') {
          this.customer.email = '';
        }
        // GET BILLING INFO
        this.getBillingInfo();
        this.getBillingDate();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
    // this.subscribersService.getCustomer(this.subscriberId)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     this.customer = res[0];
    //     if (this.customer.email === 'unknown@mediaaccessservices.net') {
    //       this.customer.email = '';
    //     }
    //     // GET BILLING INFO
    //     this.getBillingInfo();
    //     this.getBillingDate();
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   })
  }

  // GET BILLING INFO 1/2
  private getBillingInfo() {
    let resp;
    this.rest.get('/payments/get-billing-info?customerId=' + this.customer.id + '&breakdown=BILLING_ENTRY')
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        resp = res;
        this.billingInfo.owed = resp.balanceOwed;
        this.getBillingInfo2();
        // this.getBillingDate();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
    //
    // this.paymentsService.getUrl('get-billing-info?customerId=' + this.customer.id + '&breakdown=', 'BILLING_ENTRY')
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     const resp = JSON.parse(res['_body']);
    //     this.billingInfo.owed = resp.balanceOwed;
    //     this.getBillingInfo2();
    //     // this.getBillingDate();
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   })
  }

  // GET BILLING INFO 2/2
  /*Has payment card --> Store Billing Info -->TokenCheck*/
  private getBillingInfo2() {
    let resp;
    this.rest.get('/payments/has-payment-card?customerId=' + this.customer.id)
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        resp = res;
        this.billingInfo.hasPaymentCard = resp.hasPaymentCard;
        // STORE BILLING INFO
        sessionStorage.setItem('billingInfo', JSON.stringify(this.billingInfo));
        // CHECK FOR TOKENS
        this.tokenCheck();
        // this.getBillingDate();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
    // this.paymentsService.getUrl('has-payment-card?customerId=', this.customer.id)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     this.billingInfo.hasPaymentCard = JSON.parse(res['_body']).hasPaymentCard;
    //     // STORE BILLING INFO
    //     sessionStorage.setItem('billingInfo', JSON.stringify(this.billingInfo));
    //     // CHECK FOR TOKENS
    //     this.tokenCheck();
    //   }, err => {
    //     // this.error = JSON.parse(err['_body']);
    //     this.error = this.helpers.getError(err);
    //   })
  }

  // GET NEXT BILLING DATE
  getBillingDate() {
    let resp;
    this.rest.get('/payments/get-next-billing-date?customerId=' + this.customer.id)
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        resp = res;
        this.billingInfo.billingDate = resp.nextBillingDate;
        console.log(resp);
        console.log('BILLING DATE', this.billingInfo);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })

    // this.paymentsService.getUrl('get-next-billing-date?customerId=', this.customer.id)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     this.billingInfo.billingDate = JSON.parse(res['_body']).nextBillingDate;
    //     console.log(res);
    //     console.log('BILLING DATE', this.billingInfo);
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   })
  }

  // GET EVENTS HISTORY
  private getHistory() {
    let grouped = 'day';
    if (!this.hideDuplicates) {
      grouped = 'false';
    }
    const urlData = '/event-histories/searchAllGroupedPaged?text=&subscriberId=' + this.subscriberId +
                    '&grouped=' + grouped + '&createdAfter=&page=0&size=' + this.eventShow + '&sortBy=createdAt';
    let resp;
    this.rest.getObserveResponse(urlData)
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        resp = res;
        if (resp.headers.get('link')) {
          this.eventHeaders = resp.headers.get('link').split(',');
          for ( let i = 0; i < this.eventHeaders.length ; i++) {
            this.eventHeaders[i] = this.eventHeaders[i].match(/page=([0-9]+)/);
          }
          this.pagination.first = this.eventHeaders[0][1];
          this.pagination.last = this.eventHeaders[1][1];
        }else {
          this.pagination.last = this.pagination.currentPage;
        }
        this.eventHistory = resp.body;
        console.log('eH', this.eventHistory);
      }, err => {
        // this.error = JSON.parse(err['_body']);
        this.error = this.helpers.error(err.error.message);
      });

    // this.subscribersService.eHistory(urlData)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     if (res.headers.get('link')) {
    //       console.log('asdsdsfsfdssd', res);
    //       this.eventHeaders = res.headers.get('link').split(',');
    //       for ( let i = 0; i < this.eventHeaders.length ; i++) {
    //         this.eventHeaders[i] = this.eventHeaders[i].match(/page=([0-9]+)/);
    //       }
    //       this.pagination.first = this.eventHeaders[0][1];
    //       this.pagination.last = this.eventHeaders[1][1];
    //       // console.log(this.eventHeaders, 'eventHeaders');
    //     }else {
    //       this.pagination.last = this.pagination.currentPage;
    //     }
    //     this.eventHistory = JSON.parse(res['_body']);
    //     console.log('eH', this.eventHistory);
    //   }, err => {
    //     this.error = JSON.parse(err['_body']);
    //     this.error = this.helpers.getError(err);
    //   });

    this.pagination.currentPage = 0;
  }

  /**
   * Get History Number
   */
  private getHistoryNumber() {
    // const urlData = 'event-histories/searchAllGroupedPaged?' + 'q=subscriberId=' + this.subscriberId +
    //   ',grouped=true' + '&sortBy=createdAt&sortOrder=desc&page=0&size=' + this.eventShow;

    let grouped = 'day';
    if (!this.hideDuplicates) {
      grouped = 'false';
    }

    const urlData = '/event-histories/searchAllGroupedPaged?text=&subscriberId=' + this.subscriberId +
                    '&grouped=' + grouped + '&createdAfter=&page=0&size=' + this.eventShow + '&sortBy=createdAt';

    let resp;
    this.rest.getObserveResponse(urlData)
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        resp = res;
        if (resp.headers.get('link')) {
          this.eventHeaders = resp.headers.get('link').split(',');
          for ( let i = 0; i < this.eventHeaders.length ; i++) {
            this.eventHeaders[i] = this.eventHeaders[i].match(/page=([0-9]+)/);
          }
          this.pagination.first = this.eventHeaders[0][1];
          this.pagination.last = this.eventHeaders[1][1];
          // console.log(this.eventHeaders, 'eventHeaders');
        }else {
          this.pagination.last = this.pagination.currentPage;
        }
        this.eventHistory = resp.body;
        console.log('eH', this.eventHistory);
      }, err => {
        // this.error = JSON.parse(err['_body']);
        this.error = this.helpers.error(err.error.message);
      });

    // this.subscribersService.eHistory(urlData)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     if (res.headers.get('link')) {
    //       this.eventHeaders = res.headers.get('link').split(',');
    //       for ( let i = 0; i < this.eventHeaders.length ; i++) {
    //         this.eventHeaders[i] = this.eventHeaders[i].match(/page=([0-9]+)/);
    //       }
    //       this.pagination.first = this.eventHeaders[0][1];
    //       this.pagination.last = this.eventHeaders[1][1];
    //       // console.log(this.eventHeaders, 'eventHeaders');
    //     }else {
    //       this.pagination.last = this.pagination.currentPage;
    //     }
    //     this.eventHistory = JSON.parse(res['_body']);
    //     console.log('eH', this.eventHistory);
    //   }, err => {
    //     this.error = JSON.parse(err['_body']);
    //     this.error = this.helpers.getError(err);
    //   });

    this.pagination.currentPage = 0;
  }

 // GET 10 DAYS HISTORY
  private getTenDaysHistory() {

    const date = (+new Date().getTime() / 1000 - 864000).toFixed(0);

    // let urlData = 'event-histories/searchAllGroupedPaged?' + 'q=subscriberId=' + this.subscriberId +
    //   ',grouped=true' + '&q=createdAfter=' + date;
    let grouped = 'day';
    if (!this.hideDuplicates) {
      grouped = 'false';
    }

    const urlData = '/event-histories/searchAllGroupedPaged?text=&subscriberId=' + this.subscriberId +
      '&grouped=' + grouped + '&createdAfter=' + date + '&page=0&size=5&sortBy=createdAt';

    let resp;
    this.rest.getObserveResponse(urlData)
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        resp = res;
        this.tenEventHistory = resp.body;
        console.log('ten', this.tenEventHistory);
      }, err => {
        // this.error = JSON.parse(err["_body"]);
        this.error = this.helpers.error(err.error.message);
      });

    // this.subscribersService.eHistory(urlData)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     this.tenEventHistory = JSON.parse(res['_body']);
    //     console.log('ten', this.tenEventHistory);
    //   }, err => {
    //     // this.error = JSON.parse(err["_body"]);
    //     this.error = this.helpers.getError(err);
    //   });
  }

  // GET SUBSCRIBER BY ID
  private getSubscriber() {
    this.rest.get('/subscribers/' + this.subscriberId)
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        console.log('subscriber', res);
        this.subscriber = res;
        // Format postcode
        this.subscriber.postcode = this.postcodeService.postcodeFormat(this.subscriber.postcode);
        // Capitalize surname
        this.subscriber.surname = this.helpers.toTitleCase(this.subscriber.surname);

        // FREEVIEW
        if (this.subscriber.addressLine5 === 'FREEVIEW') {
          this.freeview = true;
        }

        // GET ENTITLEMENTS BY ID
        this.getEntitlements();
        // replace null for '' to avoid errors
        for (const key of Object.keys(this.subscriber)){
          if (this.subscriber[key] == null) {
            this.subscriber[key] = '';
          }
        }

        // GET SKYBOX DEVICE
        this.skyboxDevice();

        this.vipVal = this.subscriber.vip;
        // CARD SUBSCRIBER CHECK
        if (this.subscriber.cardSubscriberId.length === 0 || this.subscriber.cardSubscriberId == null ||
          this.subscriber.cardSubscriberId === 'undefined') {
          this.CSIDisabledCheck = true;
          console.log('here');
        }
        sessionStorage.setItem('subscriber', JSON.stringify(this.subscriber));
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });

    // this.subscribersService.getSubscriber(this.subscriberId)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     console.log('subscriber', res);
    //     this.subscriber = res;
    //     // Format postcode
    //     this.subscriber.postcode = this.postcodeService.postcodeFormat(this.subscriber.postcode);
    //     // Capitalize surname
    //     this.subscriber.surname = this.helpers.toTitleCase(this.subscriber.surname);
    //
    //     // FREEVIEW
    //     if (this.subscriber.addressLine5 === 'FREEVIEW') {
    //       this.freeview = true;
    //     }
    //
    //     // GET ENTITLEMENTS BY ID
    //     this.getEntitlements();
    //     // replace null for '' to avoid errors
    //     for (const key of Object.keys(this.subscriber)){
    //       if (this.subscriber[key] == null) {
    //         this.subscriber[key] = '';
    //       }
    //     }
    //
    //     // GET SKYBOX DEVICE
    //     this.skyboxDevice();
    //
    //     this.vipVal = this.subscriber.vip;e
    //     // CARD SUBSCRIBER CHECK
    //     if (this.subscriber.cardSubscriberId.length === 0 || this.subscriber.cardSubscriberId == null ||
    //       this.subscriber.cardSubscriberId === 'undefined') {
    //       this.CSIDisabledCheck = true;
    //       console.log('here');
    //     }
    //     sessionStorage.setItem('subscriber', JSON.stringify(this.subscriber));
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   })
  }

  // GET SKY BOX DEVICE
  private skyboxDevice() {
    let box;
    this.rest.get('/skybox-devices/findOneByViewingCard?viewingCardId='  + this.subscriber.viewingCardId)
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        box = res;
        this.subscriber.skybox = {
          skyBoxType:  box.skyBoxType,
          details: box.details,
          hardwareVersion: ' - ' + box.hardwareVersion,
          checkDigitAlgorithm: ' - ' + box.checkDigitAlgorithm
        };
      }, err => {
        if (this.subscriber.viewingCardId !== '') {
          this.viewingCardInfo();
        } else {
          // Display unknown
          this.subscriber.skybox = {
            skyBoxType:  'Unknown'
          };
        }
      });

    // this.subscribersService.baseUrlGet('skybox-devices/findOneByViewingCard?viewingCardId=' + this.subscriber.viewingCardId)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     const box = JSON.parse(res['_body']);
    //     this.subscriber.skybox = {
    //       skyBoxType:  box.skyBoxType,
    //       details: box.details,
    //       hardwareVersion: ' - ' + box.hardwareVersion,
    //       checkDigitAlgorithm: ' - ' + box.checkDigitAlgorithm
    //     };
    //   }, err => {
    //     if (this.subscriber.viewingCardId !== '') {
    //       this.viewingCardInfo();
    //     } else {
    //       // Display unknown
    //       this.subscriber.skybox = {
    //         skyBoxType:  'Unknown'
    //       };
    //     }
    //   });
    console.log(this.subscriber.skybox);
  }

  // GET VIEWING CARD INFO
  viewingCardInfo() {

    let vcInfo;
    this.rest.get('/viewing-cards/' + this.subscriber.viewingCardId)
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
         vcInfo = res;
        console.log(vcInfo);
        this.subscriber.skybox = {
          skyBoxType:  'Unknown',
          details: 'Unknown [' + vcInfo.stbSerialNumber.substring(0, 4) + ']'
        };
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });

    // this.subscribersService.baseUrlGet('viewing-cards/' + this.subscriber.viewingCardId)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     const vcInfo = JSON.parse(res['_body']);
    //     console.log(vcInfo);
    //     this.subscriber.skybox = {
    //       skyBoxType:  'Unknown',
    //       details: 'Unknown [' + vcInfo.stbSerialNumber.substring(0, 4) + ']'
    //     };
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   });
  }

  /**
   * Get entitlements 1/2
   */
  getEntitlements() {
    // ppv_enabler
    this.rest.get('/entitlements?subscriberId=' + this.subscriberId + '&productType=subscription')
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        this.entitlements = res;

        this.getPPV_enabler();

        this.isActive();
        // this.entitlementsFiltered = res;
        // this.getEntitlementsFiltered(res);
      });

    // this.productsService.getByIdFiltered('E', this.subscriberId, 'subscription')
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     console.log('getByIdFiltered', res);
    //     this.entitlements = res;
    //
    //     this.getPPV_enabler();
    //
    //     this.isActive();
    //     // this.entitlementsFiltered = res;
    //     // this.getEntitlementsFiltered(res);
    //   });
  }

  getPPV_enabler() {
    let resp;
    this.rest.get('/entitlements?subscriberId=' + this.subscriberId + '&productType=ppv_enabler')
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        resp = res;
        for (const r of resp) {
          let found = false;
          this.entitlements.forEach(e => {
            if (e.productGroupName == r.productGroupName) {
              found = true;
            }
          });
          if (!found) {
            this.entitlements.push(r);
          }
        }
        this.isActive();
      });

    // this.productsService.getByIdFiltered('E', this.subscriberId, 'ppv_enabler')
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     console.log('ppv_enabler', res);
    //     for (const r of res){
    //       if (r.productGroupTitleLabel === 'RH_PPNReg' || r.productGroupTitleLabel === 'RH_PPNReg_EUR') {
    //         this.entitlements.push(r)
    //       }
    //     }
    //     this.isActive();
    //   });
  }

  /**
   * Get entitlements 2/2
   * @param filtered: entitlements previously filtered (only subscriptions)
   */
  getEntitlementsFiltered(filtered) {
    let resp;
    this.rest.get('/entitlments/?q=subscriberId=' + this.subscriberId)
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        resp = res;
        this.entitlements = filtered;
        for (const r of resp) {
          if (r.productGroupName === 'PG_RH_PPNReg') {
            this.entitlements.push(r);
          }
        }
        this.isActive();
      })

    // this.productsService.getById('E', this.subscriberId)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //    this.entitlements = filtered;
    //    for (const r of res) {
    //      if (r.productGroupName === 'PG_RH_PPNReg') {
    //        this.entitlements.push(r);
    //      }
    //    }
    //     this.isActive();
    //   })
  }


  // EVENT PAGE
  public eventPage(page) {
    this.pagination.currentPage = page;
    // var urlData = 'event-histories/searchAllGroupedPaged?' + 'q=subscriberId=' + this.subscriberId +
    //   ',grouped=true' + '&sortBy=createdAt&sortOrder=desc&page=' + page + '&size=' + this.eventShow;
    let grouped = 'day';
    if (!this.hideDuplicates) {
      grouped = 'false';
    }

    const urlData = '/event-histories/searchAllGroupedPaged?text=&subscriberId=' + this.subscriberId +
      '&grouped=' + grouped + '&createdAfter=&page=' + page + '&size=' + this.eventShow + '&sortBy=createdAt';

    this.rest.getObserveResponse(urlData)
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        console.log(res);
        this.eventHistory = res.body;
      }, err => {
        // this.error = JSON.parse(err["_body"]);
        this.error = this.helpers.error(err.error.message);
      });

    // this.subscribersService.eHistory(urlData)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     console.log(res);
    //     this.eventHistory = JSON.parse(res['_body']);
    //   }, err => {
    //     // this.error = JSON.parse(err["_body"]);
    //     this.error = this.helpers.getError(err);
    //   })
  }

  // SEARCH EVENT HISTORY
  public eventSearch() {
    let size;
    this.onlyAgent = false;
    this.eventShow = 5;
    if (this.eventsFilter.value.filterValue === '') {
      size = 5;
    }else {
      size = 99999;
    }
    if (this.eventsFilter.invalid) {
      this.invalidFilter = true;
    }else {
      const val = this.eventsFilter.value.filterValue;
      // let url = encodeURI('event-histories/searchAllGroupedPaged?q=subscriberId=' + this.subscriberId
      //   + ',grouped=true' + ',text=*' + val + '*&sortBy=createdAt&sortOrder=desc&page=0&size=' + size );
      let grouped = 'day';
      if (!this.hideDuplicates) {
        grouped = 'false';
      }
      const url = encodeURI('/event-histories/searchAllGroupedPaged?text=' + val + '&subscriberId=' + this.subscriberId +
        '&grouped=' + grouped + '&createdAfter=&page=0&size=' + size + '&sortBy=createdAt');

      let resp;
      this.rest.getObserveResponse(url)
        .subscribe(res => {
          resp = res;
          this.eventHistory = resp.body;
          if (resp.headers.get('link')) {
            this.eventHeaders = resp.headers.get('link').split(',');
            for (let i = 0; i < this.eventHeaders.length ; i++) {
              this.eventHeaders[i] = this.eventHeaders[i].match(/page=([0-9]+)/);
            }
            this.pagination.first = this.eventHeaders[0][1];
            this.pagination.last = this.eventHeaders[1][1];
            this.pagination.currentPage = 0; // when h hide duplicates
            console.log(this.eventHeaders, 'eventHeaders');
          }else {
            this.pagination.last = this.pagination.currentPage;
          }
          this.eventHistory = resp.body;
        }, err => {
          // this.error = JSON.parse(err["_body"]);
          this.error = this.helpers.error(err.error.message);
        });

      // this.subscribersService.eHistory(url)
      //   .takeUntil(this.unsubscribe)
      //   .subscribe(res => {
      //     this.eventHistory = JSON.parse(res['_body']);
      //     if (res.headers.get('link')) {
      //       this.eventHeaders = res.headers.get('link').split(',');
      //       for (let i = 0; i < this.eventHeaders.length ; i++) {
      //         this.eventHeaders[i] = this.eventHeaders[i].match(/page=([0-9]+)/);
      //       }
      //       this.pagination.first = this.eventHeaders[0][1];
      //       this.pagination.last = this.eventHeaders[1][1];
      //       this.pagination.currentPage = 0; // when h hide duplicates
      //       console.log(this.eventHeaders, 'eventHeaders');
      //     }else {
      //       this.pagination.last = this.pagination.currentPage;
      //     }
      //     this.eventHistory = JSON.parse(res['_body']);
      //   }, err => {
      //     // this.error = JSON.parse(err["_body"]);
      //     this.error = this.helpers.getError(err);
      //   });
    }
  }

  // ADD EVENT MANUALLY
  public addEvent() {
    this.addEv = false;
    const data = {
      subscriberId: this.subscriberId,
      cmsId: this.globals.CMSId, // get from app config
      text: this.note + ' [AGENT: ' + this.username + ']',
    };
    this.rest.post('/event-histories/', data)
      .takeUntil(this.unsubscribe)
      .subscribe( res => {
        this.getHistory();
      }, err => {
        // this.error = JSON.parse(err["_body"]);
        this.error = this.helpers.error(err.error.message);
      });

    // this.subscribersService.urlPost('event-histories/', data)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe( res => {
    //     this.getHistory();
    //   }, err => {
    //     // this.error = JSON.parse(err["_body"]);
    //     this.error = this.helpers.getError(err);
    //   });
    this.note = '';
  }

  // EDIT EVENT
  private editEvent(text, id) {
      if (text.includes('[AGENT:')) {
        this.checkIfAdmin();
        this.updEv = true;
        this.note = text;
        this.eventId = id;
      }else {
        this.error = {
          message: 'This note cannot be edited'
        }
      }

  }

  // UPDATE EVENT AFTER EDIT
  public updateEvent() {
    if (!this.note.includes( '[AGENT:')) {
      this.note = this.note +  ' [AGENT: ' + this.username + ']'
    }

    this.updEv = false;
    const data = {
      eventHistoryId: this.eventId,
      text: this.note
    };

    console.log('Event data',  data);
    this.http.put(this.globals.API_ENDPOINT + '/event-histories/', data, {responseType: 'text'})
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        this.getHistory();
      }, err => {
        const error = JSON.parse(err.error);
        this.error = this.helpers.error(error.message);
      });

    // this.subscribersService.urlPut('event-histories/', data)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //   console.log(res);
    //     this.getHistory();
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   });
    this.note = '';
  }

  // DELETE EVENT
  public deleteEvent() {
    this.delEv = false;
    this.accessGranted = false;
    this.user = '';
    this.password = '';

    this.rest.delete('/event-histories/' + this.eventId, '')
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        this.getHistory();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }


  /**
   * ACTIVE / INACTIVE / AWAITING ACTIVATION
   */
  private isActive() {
    this.active = true;
    this.entAwaitingActivation = false;
    if (!this.subscriber.enabled) {
      this.active = false;
      this.entAwaitingActivation = false;
      this.waitSubsActive = true;
    }else {
      if (this.entitlements.length === 0) {
        this.active = false;
        this.entAwaitingActivation = false;
        this.waitSubsActive = false;
      }else {
        for (const a of this.entitlements) {
          if (a.activated === false) {
            this.active = false;
            this.entAwaitingActivation = true;
            this.waitSubsActive = false;
          }
        }
      }
    }
  }

  subscriberStatus() {
    this.rest.get('/subscribers/status/' + this.subscriberId)
    .subscribe(res => {
      this.status = res['status'];
    }, err => {
      this.error = this.helpers.error(err.error.message);
    })
  }


  // SEARCH PROCESS
  public processSearch() {
    this.processFound = [];
    for (let i = 0; i < this.processArray.length; i++) {
      if (this.processArray[i].text.toLowerCase().includes(this.str.toLowerCase())) {
        this.processFound.push(this.processArray[i]);
      }
    }
  }

  // CHANGE CREDIT LIMIT
  public changeCredit() {
    this.user = '';
    this.password = '';

    this.submitted = true;
    if (this.changeCreditLimitForm.valid) {
      this.submitted = false;
      const json = {
        cardSubscriberId: this.subscriber.cardSubscriberId,
        creditLimit: this.changeCreditLimitForm.value.creditLimit
      };
      this.http.put(this.globals.API_ENDPOINT + '/entitlements/change-credit-limit', json, {responseType: 'text'})
        .takeUntil(this.unsubscribe)
        .subscribe(res => {
          this.changeCreditLimit = false;
          this.submitted = false;
          this.accessGranted = false;
          this.getSubscriber();
          this.getHistory();

        }, err => {
          const error = JSON.parse(err.error);
          this.error = this.helpers.error(error.message);
          this.changeCreditLimit = false;
          this.submitted = false;
          this.accessGranted = false;
        });

      // this.productsService.urlProducts('E', 'change-credit-limit', json)
      //   .takeUntil(this.unsubscribe)
      //   .subscribe(res => {
      //     this.changeCreditLimit = false;
      //     this.submitted = false;
      //     this.accessGranted = false;
      //     this.getSubscriber();
      //     this.getHistory();
      //
      //   }, err => {
      //     this.error = this.helpers.getError(err);
      //     this.changeCreditLimit = false;
      //     this.submitted = false;
      //     this.accessGranted = false;
      //   })

    }
  }

  // EDIT VIEWING CARD NUMBER
  public editVCN() {
    this.submitted = true;
    const data = {
      cardSubscriberId : this.subscriber.cardSubscriberId,
      viewingCardNumber: this.editVCNForm.value.viewingCardNumber
    };

    if (this.editVCNForm.valid) {
      this.http.put(this.globals.API_ENDPOINT + '/subscribers/update-viewing-card-number/', data, {responseType: 'text'})
        .takeUntil(this.unsubscribe)
        .subscribe(res => {
          console.log('updateVCN', res);
          this.editViewingCardNumber = false;
          this.helpers.reset();
          this.submitted = false;
          this.editVCNForm.setValue({viewingCardNumber: ''})
        }, err => {
          this.editViewingCardNumber = false;
          const error = JSON.parse(err.error);
          this.error = this.helpers.error(error.message);
          this.editVCNForm.setValue({viewingCardNumber: ''});
          this.submitted = false;
        });

      // this.subscribersService.urlPut('subscribers/update-viewing-card-number', data)
      //   .takeUntil(this.unsubscribe)
      //   .subscribe(res => {
      //     console.log('updateVCN', res);
      //     this.editViewingCardNumber = false;
      //     this.helpers.reset();
      //     this.submitted = false;
      //     this.editVCNForm.setValue({viewingCardNumber: ''})
      //   }, err => {
      //     this.editViewingCardNumber = false;
      //     this.error = this.helpers.getError(err);
      //     this.editVCNForm.setValue({viewingCardNumber: ''});
      //     this.submitted = false;
      //   });
      this.submitted = false;
    }
  }

  // CANCEL ACCOUNT
  public cancelProceed() {
    this.cancelAccount = false;
    this.CAoptions = false;
    this.accessGranted = false;
    this.user = '';
    this.password = '';

    const entitlementsNames = [];
    for (const ent of this.entitlements) {
      entitlementsNames.push(ent.productGroupName)
    }
    const data = {
      'subscriberId': this.subscriber.id,
      'addedProductGroupNames': [],
      'removedProductGroupNames': entitlementsNames,
      'waiveOutstandingBalance': true
    };

    this.http.put(this.globals.API_ENDPOINT + '/entitlements', data, {responseType: 'text'})
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        console.log('update', res);
        this.getSubscriber();
        this.subscriptionsCancellations();
      }, err => {
        const error = JSON.parse(err.error);
        this.error = this.helpers.error(error.message);
      });

    // this.productsService.updateEntitlements('E', data)
    //   .subscribe(res => {
    //       console.log('update', res);
    //       this.getEntitlements();
    //     }, err => {
    //       this.error = this.helpers.getError(err);
    //     }
    //   );
  }

  // CHECK ADMIN
  checkIfAdmin() {
    this.accessGranted = '';
    this.user = '';
    this.password = '';
    if (this.role === 'admin' || this.role === 'super_admin') {
      this.accessGranted = 'admin';
    }else {
     this.requireAdmin = true;
    }

    return this.requireAdmin;
  }

  /**
   * Auth require error
   * @param e -> event emitted by auth-required component
   */
  authError(e) {
    this.error = e;
    // SET TO FALSE
    this.cancelAccount = false;
    this.changeCreditLimit = false;
    this.delEv = false;
  }


  /**
   * VIP
   */
  private vip() {
    this.vipBox = false;

    if (this.billingInfo.owed > 0) {
     this.vipChangeDisabled = true;
      this.getSubscriber();
    }else {
      if (this.vipVal) {
        this.vipVal = false;
      }else {
        this.vipVal = true;
      }
      console.log('VIP', this.vipVal);
      const data = {
        'subscriberId': this.subscriber.id,
        'vip': this.vipVal
      };
      this.http.put(this.globals.API_ENDPOINT + '/subscribers/set-vip/', data, {responseType: 'text'})
        .takeUntil(this.unsubscribe)
        .subscribe(res => {
          console.log('VIP', res);
          this.getSubscriber();
          this.getHistory();
        }, err => {
          const error = JSON.parse(err.error);
          this.error = this.helpers.error(error.message);
          this.getSubscriber();
        });

      // this.subscribersService.urlSubscribers('set-vip', data)
      //   .takeUntil(this.unsubscribe)
      //   .subscribe(res => {
      //     console.log(res);
      //     this.getSubscriber();
      //     this.getHistory();
      //   }, err => {
      //     this.error = this.helpers.getError(err);
      //     this.getSubscriber();
      //   });
    }

  }

  /*vip dialog box no*/
  public vipNo() {
    this.vipBox = false;
    this.getSubscriber();
  }

  // OWED CHECK FOR PROCESS 18 AND 22
  processOwedCheck(pNumber) {
    if (pNumber === 22) {
      sessionStorage.setItem('process22', 'true');
    }else {
      sessionStorage.setItem('process22', 'false');
    }
    if (this.billingInfo.owed > 0) {
      this.process18 = true;
    }else if (this.billingInfo.owed === 0) {
      this.route.navigate(['/process21']);
    }
  }

  // PROCESS 18 PAYMENT
  public proceedPayment() {
    this.process18 = false;

    /*CHECK FOR PAYMENT CARD*/
    if (this.billingInfo.hasPaymentCard) {
      // Process payment
      this.processPayment();
    }else if (!this.billingInfo.hasPaymentCard) {
      // Add Payment card if not found all fields required if customerId not supplied
      const data = {
        currencyCode: this.subscriber.currencyCode,
        title: this.subscriber.title,
        initials: this.subscriber.initials,
        surname: this.subscriber.surname,
        addressLine1: this.subscriber.addressLine1,
        postcode: this.subscriber.postcode,
        country: this.subscriber.countryCode,
        email: this.customer.email,
        returnUrl: window.location.href
      };

      // Data to url string
      const str = Object.keys(data).map(function (key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
      }).join('&');

      let resp;
      this.rest.get('/payments/get-payment-card-url?' + str)
        .subscribe(res => {
          resp = res;
          location.replace(resp.storeCardUrl);
        }, err => {
          // this.error = JSON.parse(err["_body"]);
          this.error = this.helpers.error(err.error.message);
        })

      // this.paymentsService.getUrl('get-payment-card-url?', str)
      //   .subscribe(res => {
      //     console.log(res);
      //     const url = JSON.parse(res['_body']);
      //     location.replace(url.storeCardUrl);
      //   }, err => {
      //     // this.error = JSON.parse(err["_body"]);
      //     this.error = this.helpers.getError(err);
      //   })
    }
  }
  private processPayment() {
    // SETTLE BALANCE
      const data = {
        customerId: this.customer.id,
        amount: this.billingInfo.owed
      };

    this.http.put(this.globals.API_ENDPOINT + '/payments/settle-balance/', data, {responseType: 'text'})
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        if (sessionStorage.getItem('process22') === 'true') {
          this.route.navigate(['/process21']);
          /***when payment get and store billing info***/
        }else {
          this.paymentSuccess = true;
        }
      }, err => {
        const error = JSON.parse(err.error);
        this.error = this.helpers.error(error.message);
      });


      // this.paymentsService.putUrl('settle-balance', data)
      //   .takeUntil(this.unsubscribe)
      //   .subscribe(res => {
      //     console.log(res);
      //     if (sessionStorage.getItem('process22') === 'true') {
      //       this.route.navigate(['/process21']);
      //       /***when payment get and store billing info***/
      //     }else {
      //       this.paymentSuccess = true;
      //     }
      //     // this.balanceSettled = true;
      //   }, err => {
      //     // this.error = JSON.parse(err["_body"]);
      //     this.error = this.helpers.getError(err);
      //   })
  }

  // RE-AUTHORISE SIGNALS
  reauthorise() {
    const json = {
      cardSubscriberId : this.subscriber.cardSubscriberId
    };
    this.http.put(this.globals.API_ENDPOINT + '/entitlements/reauthorise-services/', json, {responseType: 'text'})
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        console.log(res);
        this.reauthoriseSuccess = true;
      }, err => {
        const error = JSON.parse(err.error);
        this.error = this.helpers.error(error.message);
      });
    //
    // this.productsService.urlProducts( 'E', 'reauthorise-services', json)
    //   .takeUntil(this.unsubscribe)
    //   .subscribe(res => {
    //     console.log(res);
    //     this.reauthoriseSuccess = true;
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   })
    }

  // PROCESS BOX CLOSE
  public processBoxClose() {
    this.processBox = false;
    this.process6  = false;
    this.process7  = false;
    this.process9  = false;
    this.process10  = false;
    this.process11  = false;
    this.process14  = false;
    this.process32  = false;
    this.process34  = false;
    this.process35  = false;
  }

  // JOIN CARD
  public joinCard() {
    sessionStorage.setItem('customerId', this.customer.id);
    this.route.navigate(['/joinSubscriber']);
  }

  // CHECK TO ALLOW CHANGING ENTITLEMENTS
  changeDisabledCheck(type) {
    if (this.entAwaitingActivation || this.billingInfo.owed > 0 || this.waitSubsActive || this.billingInfo.hasPaymentCard === undefined) {
      this.changeDisabled = true;
    }else if (this.CSIDisabledCheck) {
      if (type === 'O') {
        this.CSIDisabled = true;
      }else if (type === 'S') {
        this.route.navigate(['/entitlements'], { queryParams: { type: 1 } });
      }
    }else {
      if (type === 'S') {
        this.route.navigate(['/entitlements'], { queryParams: { type: 1 } });
      }else if (type === 'O') {
        this.route.navigate(['/entitlements'], { queryParams: { type: 2 } });
      }
    }
  }

  // REQUEST CALLBACK
  requestCallback() {
    const cardSubscriberId = {
      cardSubscriberId: this.subscriber.cardSubscriberId
    };
    // const subscriberId = {
    //   cardSubscriberId: this.subscriber.id
    // };

    this.http.put(this.globals.API_ENDPOINT + '/subscribers/request-callback/', cardSubscriberId, {responseType: 'text'})
      .takeUntil(this.unsubscribe)
      .subscribe(res => {
        this.callbackDialog = true;
      }, err => {
        const error = JSON.parse(err.error);
        this.error = this.helpers.error(error.message);
      });


    // this.subscribersService.urlSubscribers('request-callback', cardSubscriberId)
    //   .subscribe(res => {
    //     this.callbackDialog = true;
    //   }, err => {
    //     this.error = this.helpers.getError(err);
    //   })
  }

  /**
   * Show only entries added by agents
   */
  onlyAgentFilter() {
    if (this.onlyAgent) {
      console.log(this.onlyAgent);
      this.pagination.currentPage = 0;

      // search for agent word
      // let url = encodeURI('event-histories/searchAllGroupedPaged?q=subscriberId=' + this.subscriberId
      //   + ',text=*agent*&sortBy=createdAt&sortOrder=desc&page=0&size=9999');

      let grouped = 'day';
      if (!this.hideDuplicates) {
        grouped = 'false';
      }


      const url = encodeURI('/event-histories/searchAllGroupedPaged?text=agent&subscriberId=' + this.subscriberId +
        '&grouped=' + grouped + '&createdAfter=&page=0&size=' + 9999 + '&sortBy=createdAt');

      let resp;
      this.rest.getObserveResponse(url)
        .takeUntil(this.unsubscribe)
        .subscribe(res => {
          resp = res;
          this.eventHistory = resp.body;
          if (resp.headers.get('link')) {
            this.eventHeaders = resp.headers.get('link').split(',');
            for (let i = 0; i < this.eventHeaders.length ; i++) {
              this.eventHeaders[i] = this.eventHeaders[i].match(/page=([0-9]+)/);
            }
            this.pagination.first = this.eventHeaders[0][1];
            this.pagination.last = this.eventHeaders[1][1];
            // console.log(this.eventHeaders, 'eventHeaders');
          }else {
            this.pagination.last = this.pagination.currentPage;
          }
          this.eventHistory = resp.body;
        }, err => {
          // this.error = JSON.parse(err["_body"]);
          this.error = this.helpers.error(err.error.message);
        });

      // this.subscribersService.eHistory(url)
      //   .takeUntil(this.unsubscribe)
      //   .subscribe(res => {
      //     this.eventHistory = JSON.parse(res['_body']);
      //     if (res.headers.get('link')) {
      //       this.eventHeaders = res.headers.get('link').split(',');
      //       for (let i = 0; i < this.eventHeaders.length ; i++) {
      //         this.eventHeaders[i] = this.eventHeaders[i].match(/page=([0-9]+)/);
      //       }
      //       this.pagination.first = this.eventHeaders[0][1];
      //       this.pagination.last = this.eventHeaders[1][1];
      //       // console.log(this.eventHeaders, 'eventHeaders');
      //     }else {
      //       this.pagination.last = this.pagination.currentPage;
      //     }
      //     this.eventHistory = JSON.parse(res['_body']);
      //   }, err => {
      //     // this.error = JSON.parse(err["_body"]);
      //     this.error = this.helpers.getError(err);
      //   });
    } else {
      this.eventSearch();
    }
  }

  /**
   * delete scheduled cancellation
   * @param id --> s.cancellation id
   */
  deleteScheduledCancellation(stopCancellationList) {
    this.stopCancellationsList = [];
    let error;
    const errorList = [];

    let index = 0;
    for (const id of stopCancellationList) {
      index ++;
      const data = {
        'authoriserId': 47,
        'id': id
      };
     this.rest.deleteWithBody('/subscription-component-cancellations', data)
        .subscribe(() => {
          this.subscriptionsCancellations();
          this.init();
        }, err => {
          this.scheduledCancellationsDialog = null;
          // display error
          error = this.helpers.error(err.error.message);
          error.message = error.message + ' ID: ' + id;
          error.message2 = '(' + this.helpers.errorDetail(err.error) + ')';
          errorList.push(error);
          this.error = {
            errorList: errorList
          }
        });
    }
  }

  // edit scheduled cancellations --> not in use
  updateScheduleCancellation(c) {
   const data = {
      'authoriserId': 47,
      'cancellationDate': + new Date(Date.UTC(+ c.cancellationDate.getFullYear(), + c.cancellationDate.getMonth() ,
        + c.cancellationDate.getDate())) / 1000,
      'entitlementId': 295922,
      'notes':  c.notes,
      'subscriberId': this.subscriberId
    };
    console.log(c);

    this.rest.post('/subscription-component-cancellations', data)
      .subscribe(res => {
        console.log(res);
    this.cancellationEdit = null;
        this.subscriptionsCancellations();
      }, err =>  {
    this.cancellationEdit = null;
        this.error = this.helpers.error(err.error.message);
      })

  }

  /**
   * Close all dialogs that require authorization
   * to avoid multiple dialogs showing
   */
  closeDialogs() {
    if (this.requireAdmin == null && this.accessGranted !== 'admin') {
      this.updEv = false;
      this.cancelAccount = false;
      this.changeCreditLimit = false;
      this.delEv = false;
      this.cancellationDelete = false;
    }
  }

  /**
   * Get subscriber subscriptions cancellations
   */
  subscriptionsCancellations() {

    console.log('cancellations');

    this.rest.get('/subscription-component-cancellations?q=subscriberId=' + this.subscriberId)
      .subscribe( res => {
        this.scheduledCancellations = res;
        console.log('subscriptionsubscriptionsubscription', this.scheduledCancellations);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * STOP CANCELLATIONS
   * @param canx --> scheduled cancellations id to be stopped
   */
  stopCancellations (canx, checked) {
    const index  = this.stopCancellationsList.indexOf(canx);
    if (index === -1 ) {
      if (checked) {
        this.stopCancellationsList.push(canx);
      }
    } else {
      if (!checked) {
        this.stopCancellationsList.splice(index, 1);
      }
    }
    console.log(this.stopCancellationsList, checked, index);
  }
}

@Pipe({ name: 'pc' })
export class PcPipe implements PipeTransform {
  transform(value: any) {
    // value = value.match(/^([A-Z]{1,2}\d{1,2}[A-Z]?)\s*(\d[A-Z]{2})$/);
    // value.shift();
    // value.join(' ');

    value = value.toUpperCase();
    const exceptions = [ 'UK', 'ROI', 'IOM', 'CI', 'BFPO' ];

    // If Postcode Exception type, return
    exceptions.forEach(exp =>  {
      if (value ===  exp)  {
        return value;
      }
    });

    // If length is < 4, return
    if (value.length < 4) {
      return value;
    }

    const suffix = value.substring(value.length - 3);
    const prefix = value.substring(0, value.length - 3);
    return prefix + ' ' + suffix;
  }
}


// PAGINATION INTERFACE
interface Pagination {
  first: any,
  last: any,
  currentPage: any,
}

// PROCESSES LIST
const process = [
  {
    link: '/process3',
    text: '3. I need to activate my new viewing card and have a phone line/broadband connected to my set top box',
    // Card subscriber Id
    CSI: true
  },
  {
    link: '/process4',
    text: '4. I need to activate my viewing card but do not have a phone line/broadband connected to my set top box',
    CSI: true
  },
  {
    link: '/process5',
    text: '5. I need to make my existing viewing card work with my new set top box',
    CSI: true
  },
  {
    link: '/process6',
    text: '6. I’ve received a new viewing card, my Sky package is OK but I cannot see my subscription channels anymore.',
    CSI: false
  },
  {
    link: '/process7',
    text: '7. I have two Sky boxes, I want to move my subscription to the one in the other room.',
    CSI: false
  },
  {
    link: '/process8',
    text: '8. I am moving house/ commercial premise and I want to keep my subscription',
    CSI: false
  },
  {
    link: '/process9',
    text: '9. I would like to remove channels from my subscription to reduce my costs.',
    CSI: false
  },
  {
    link: '/process10',
    text: '10. I would like to stop viewing my subscription in another room and reduce my costs, I do not have Sky Q.',
    CSI: false
  },
  {
    link: '/process11',
    text: '11. I have cancelled my Sky Q Multiscreen subscription with Sky, so I cannot view' +
    ' my subscription channels from you in other rooms, why am I still paying extra to you.',
    CSI: false
  },
  {
    link: '/process12',
    text: '12. I have cancelled my Sky Q Multiscreen subscription with Sky, so I cannot view ' +
    'my subscription channels from you on the Sky Q app on my iPad, why am I still paying extra to you.',
    CSI: false
  },
  {
    link: '/process13',
    text: '13. I would like to cancel all my subscriptions.',
    CSI: false
  },
  {
    link: '/process14',
    text: '14. I would like to add additional channels to my subscription package.',
    CSI: false
  },
  {
    link: '/process15',
    text: '15. I would like to view my subscription channels in another room and I do not have Sky Q.',
    CSI: false
  },
  {
    link: '/process16',
    text: '16. I have Sky Q Minis and would like to view my subscription channels in another room.',
    CSI: true
  },
  {
    link: '/process17',
    text: '17. I have Sky Q and would like to view my subscription channels on my iPad and other devices.',
    CSI: false
  },
  {
    link: '/process18',
    text: '18. I didn’t pay my bill and have had a subscription suspended; ' +
    'but have now paid my bill and would like my subscription to be resumed.',
    CSI: false
  },
  {
    link: '/process19',
    text: '19. I would like to know my current PIN and/or Parental Rating settings',
    CSI: true
  },
  {
    link: '/process20',
    text: '20. I would like to change my PIN and/or Parental Rating settings',
    CSI: false
  },
  {
    link: '/process21',
    text: '21. I am paying for my subscription but I cannot see the channels.',
    CSI: true
  },
  {
    link: '/process22',
    text: '22. I’ve upgraded to Sky Q, my Sky package is OK but now I cannot see my subscription channels',
    CSI: false
  },
  {
    link: '/process23',
    text: '23. I’ve upgraded to Sky Q with Sky Q Minis and I pay you to watch ' +
    'my subscription in another room but it’s not working on the Sky Q Minis.',
    CSI: true
  },
  {
    link: '/process24',
    text: '24. I’ve upgraded to Sky Q with Sky Q Minis, Sky left my old box in the' +
    ' extra room, and I pay you extra to watch my subscription in that room, can you make it work on the Sky Q Minis.',
    CSI: true
  },
  {
    link: '/process25/n',
    text: '25. I have a problem and the agent decided I need a replacement viewing card (Automatic)',
    CSI: true
  },
  {
    link: '/process25/y',
    text: '26. I have a problem and the agent decided I need a replacement viewing ' +
    'card, I need it urgently for a live event tomorrow (Manual/Special)',
    CSI: true
  },
  {
    link: '/process27',
    text: '27. I would like to cancel my request for a replacement viewing card',
    CSI: true
  },
  {
    link: '/process28',
    text: '28. I have ordered a new / replacement viewing card but you ' +
    'have emailed me to tell me it cannot be delivered.',
    CSI: false
  },
  {
    link: '/process29',
    text: '29. I would like to request you to resend the viewing card that did ' +
    'not arrive (but Sky say the card was returned undelivered)',
    CSI: false
  },
  {
    link: '/process30', // REDIRECT TO PROCESS 28
    text: '30. I would like to request re-issue of the viewing card that did not arrive (assumed lost in post)',
    CSI: false
  },
  // {
  //   link: '/process31',
  //   text: '31. I cancelled all my services on Sky except your subscription (CMS becomes parent – Notification from Sky)',
  //   CSI: false
  // },
  {
    link: '/process32',
    text: '32. I would like to buy a subscription and I already have an existing account with you',
    CSI: false
  },
  {
    link: '/process33',
    text: '33. I have lost the ability to view my automatically entitled (Free to View) Channel(s)',
    CSI: true,
  },
  {
    link: '/process34',
    text: '34. I would like to register and buy a ppv event and I already have an existing and active viewing card.',
    CSI: true,
  },
  {
    link: '/process35',
    text: '35. I am an existing customer and would like to buy an OPPV event from you.',
    CSI: true,
  }
];



