"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var validators_service_1 = require("../../../services/validators.service");
var subscribers_service_1 = require("../../../services/subscribers.service");
var products_service_1 = require("../../../services/products.service");
var payments_service_1 = require("../../../services/payments.service");
var helpers_service_1 = require("../../../services/helpers.service");
var user_service_1 = require("../../../services/user.service");
var app_settings_1 = require("../../../app.settings");
var core_2 = require("@angular/core");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/throw");
require("rxjs/Rx");
var ViewSubscriberComponent = /** @class */ (function () {
    function ViewSubscriberComponent(router, route, subscribersService, productsService, paymentsService, fb, helpers, userService, validators) {
        this.router = router;
        this.route = route;
        this.subscribersService = subscribersService;
        this.productsService = productsService;
        this.paymentsService = paymentsService;
        this.fb = fb;
        this.helpers = helpers;
        this.userService = userService;
        this.validators = validators;
        this.subscriber = false;
        this.entitlements = [];
        this.processArray = process;
        this.processFound = process;
        this.str = '';
        this.vipBox = false;
        this.eventHistory = [];
        this.tenEventHistory = [];
        this.eventShow = 5;
        this.addEv = false;
        this.updEv = false;
        this.delEv = false;
        this.note = '';
        this.collapse = true;
        this.pagination = {};
        this.CAoptions = false;
        this.billingInfo = {};
        this.changeCreditLimit = false;
        this.paymentSuccess = false;
        this.changeDisabled = false;
        this.vipChangeDisabled = false;
        this.CSIDisabled = false;
        this.CSIDisabledCheck = false;
        this.reauthoriseSuccess = false;
        this.callbackDialog = false;
        this.cancelAccount = false;
        this.waitSubsActive = false;
        this.submitted = false;
        this.invalidFilter = false;
        this.editViewingCardNumber = false;
        this.newViewingCardNumber = '';
        this.requireAdmin = false;
        this.accessGranted = false;
        this.processBox = false;
        this.process6 = false;
        this.process7 = false;
        this.process9 = false;
        this.process10 = false;
        this.process11 = false;
        this.process14 = false;
        this.process15 = false;
        this.process18 = false;
        this.process29 = false;
        this.process32 = false;
        this.process34 = false;
        this.process35 = false;
        this.freeview = false;
    }
    ViewSubscriberComponent.prototype.ngOnInit = function () {
        var _this = this;
        sessionStorage.removeItem('cardProcess'); // IMPORTANT
        sessionStorage.removeItem('customer'); // IMPORTANT
        sessionStorage.removeItem('rapb'); // IMPORTANT
        sessionStorage.removeItem('billingInfo'); // IMPORTANT
        // GET QUERY PARAMS
        this.router.queryParams
            .subscribe(function (params) {
            _this.process15 = params['p'];
        });
        // GET USER NAME
        if (sessionStorage.getItem('user') != null) {
            this.username = sessionStorage.getItem('user');
            console.log(this.username);
        }
        // SCROLL TOP
        this.helpers.scrollTop();
        // GET USER ROLE
        if (sessionStorage.getItem('role') != null) {
            this.role = sessionStorage.getItem('role');
            console.log(this.role);
        }
        // GET ID FROM PARAMS
        this.router.params
            .map(function (params) { return params['id']; })
            .subscribe(function (id) {
            _this.subscriberId = id;
            _this.init();
        });
        // FORMS AND VALIDATORS
        this.editVCNForm = this.fb.group({
            'viewingCardNumber': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(9), this.validators.onlyNumbers]]
        });
        this.changeCreditLimitForm = this.fb.group({
            'creditLimit': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(12), this.validators.onlyNumbers]]
        });
        this.eventsFilter = this.fb.group({
            'filterValue': ['', [this.validators.specialChar]]
        });
        // GET SUBSCRIBER BY ID
        this.getSubscriber();
        // GET EVENT HISTORY
        this.getHistory();
        this.getTenDaysHistory();
        // GET CUSTOMER
        this.getCustomer();
    };
  // RELOAD DATA
    ViewSubscriberComponent.prototype.init = function () {
        sessionStorage.removeItem('cardProcess'); // IMPORTANT
        sessionStorage.removeItem('customer'); // IMPORTANT
        sessionStorage.removeItem('rapb'); // IMPORTANT
        sessionStorage.removeItem('billingInfo'); // IMPORTANT
        // GET USER ROLE
        if (sessionStorage.getItem('role') != null) {
            this.role = sessionStorage.getItem('role');
            console.log(this.role);
        }
        // FORMS AND VALIDATORS
        this.editVCNForm = this.fb.group({
            'viewingCardNumber': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(9), this.validators.onlyNumbers]]
        });
        this.changeCreditLimitForm = this.fb.group({
            'creditLimit': ['', [forms_1.Validators.required, forms_1.Validators.maxLength(12), this.validators.onlyNumbers]]
        });
        this.eventsFilter = this.fb.group({
            'filterValue': ['', [this.validators.specialChar]]
        });
        // GET SUBSCRIBER BY ID
        this.getSubscriber();
        // GET EVENT HISTORY
        this.getHistory();
        this.getTenDaysHistory();
        // GET CUSTOMER
        this.getCustomer();
    };
    // TOKEN CHECK // IF TOKEN THEN PROCESS PROCESS PAYMENT
    ViewSubscriberComponent.prototype.tokenCheck = function () {
        var _this = this;
        this.router.queryParams
            .subscribe(function (params) {
            if (params['token'] != null) {
                _this.processPayment();
            }
        });
    };
    // GET CUSTOMER
    ViewSubscriberComponent.prototype.getCustomer = function () {
        var _this = this;
        this.subscribersService.getCustomer(this.subscriberId)
            .subscribe(function (res) {
            _this.customer = res[0];
            if (_this.customer.email == 'unknown@mediaaccessservices.net') {
                _this.customer.email = '';
            }
            // GET BILLING INFO
            _this.getBillingInfo();
            _this.getBillingDate();
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // GET BILLING INFO 1/2
    ViewSubscriberComponent.prototype.getBillingInfo = function () {
        var _this = this;
        this.paymentsService.getUrl('get-billing-info?customerId=' + this.customer.id + '&breakdown=', 'BILLING_ENTRY')
            .subscribe(function (res) {
            var resp = JSON.parse(res['_body']);
            _this.billingInfo.owed = resp.balanceOwed;
            _this.getBillingInfo2();
            // this.getBillingDate();
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // GET BILLING INFO 2/2
    /*Has payment card --> Store Billing Info -->TokenCheck*/
    ViewSubscriberComponent.prototype.getBillingInfo2 = function () {
        var _this = this;
        this.paymentsService.getUrl('has-payment-card?customerId=', this.customer.id)
            .subscribe(function (res) {
            _this.billingInfo.hasPaymentCard = JSON.parse(res['_body']).hasPaymentCard;
            // STORE BILLING INFO
            sessionStorage.setItem('billingInfo', JSON.stringify(_this.billingInfo));
            // CHECK FOR TOKENS
            _this.tokenCheck();
        }, function (err) {
            // this.error = JSON.parse(err['_body']);
            _this.error = _this.helpers.getError(err);
        });
    };
    // GET NEXT BILLING DATE
    ViewSubscriberComponent.prototype.getBillingDate = function () {
        var _this = this;
        this.paymentsService.getUrl('/get-next-billing-date?customerId=', this.customer.id)
            .subscribe(function (res) {
            _this.billingInfo.billingDate = JSON.parse(res['_body']).nextBillingDate;
            console.log(res);
            console.log('BILLING DATE', _this.billingInfo);
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // /simplified/event-histories?text=&subscriberId=8&grouped=&createdAfter=&page=0&size=50&sortBy=id
    // /event-histories?text=&subscriberId=79967&grouped=&createdAfter=&page=0&size=2&sortBy=id
    // NEW HISTORY
    // getHistoryNew() {
    //   const urlData = 'event-histories?text=&subscriberId=' + this.subscriberId + '&grouped=&createdAfter=&page=0&size=2&sortBy=createdAt';
    //
    //   this.subscribersService.eHistory(urlData)
    //     .subscribe(res => console.log('NEWWWWWW', res), err =>  this.error = JSON.parse(err['_body']))
    // }
    // GET EVENTS HISTORY
    ViewSubscriberComponent.prototype.getHistory = function () {
        // let urlData = 'event-histories?' + 'q=subscriberId=' + this.subscriberId +
        //   ',grouped=true' + '&sortBy=createdAt&sortOrder=desc&page=0&size=5';
        var _this = this;
        var urlData = 'event-histories?text=&subscriberId=' + this.subscriberId + '&grouped=day&createdAfter=&page=0&size=5&sortBy=createdAt';
        this.subscribersService.eHistory(urlData)
            .subscribe(function (res) {
            if (res.headers.get('link')) {
                console.log('asdsdsfsfdssd', res);
                _this.eventHeaders = res.headers.get('link').split(',');
                for (var i = 0; i < _this.eventHeaders.length; i++) {
                    _this.eventHeaders[i] = _this.eventHeaders[i].match(/page=([0-9]+)/);
                }
                _this.pagination.first = _this.eventHeaders[0][1];
                _this.pagination.last = _this.eventHeaders[1][1];
                // console.log(this.eventHeaders, 'eventHeaders');
            }
            else {
                _this.pagination.last = _this.pagination.currentPage;
            }
            _this.eventHistory = JSON.parse(res['_body']);
            console.log('eH', _this.eventHistory);
        }, function (err) {
            _this.error = JSON.parse(err['_body']);
            _this.error = _this.helpers.getError(err);
        });
        this.pagination.currentPage = 0;
    };
    ViewSubscriberComponent.prototype.getHistoryNumber = function () {
        var _this = this;
        var urlData = 'event-histories?' + 'q=subscriberId=' + this.subscriberId +
            ',grouped=true' + '&sortBy=createdAt&sortOrder=desc&page=0&size=' + this.eventShow;
        this.subscribersService.eHistory(urlData)
            .subscribe(function (res) {
            if (res.headers.get('link')) {
                _this.eventHeaders = res.headers.get('link').split(',');
                for (var i = 0; i < _this.eventHeaders.length; i++) {
                    _this.eventHeaders[i] = _this.eventHeaders[i].match(/page=([0-9]+)/);
                }
                _this.pagination.first = _this.eventHeaders[0][1];
                _this.pagination.last = _this.eventHeaders[1][1];
                // console.log(this.eventHeaders, 'eventHeaders');
            }
            else {
                _this.pagination.last = _this.pagination.currentPage;
            }
            _this.eventHistory = JSON.parse(res['_body']);
            console.log('eH', _this.eventHistory);
        }, function (err) {
            _this.error = JSON.parse(err['_body']);
            _this.error = _this.helpers.getError(err);
        });
        this.pagination.currentPage = 0;
    };
    // GET 10 DAYS HISTORY
    ViewSubscriberComponent.prototype.getTenDaysHistory = function () {
        var _this = this;
        var date = (+new Date().getTime() / 1000 - 864000).toFixed(0);
        // let urlData = 'event-histories?' + 'q=subscriberId=' + this.subscriberId +
        //   ',grouped=true' + '&q=createdAfter=' + date;
        var urlData = 'event-histories?text=&subscriberId=' + this.subscriberId +
            '&grouped=day&createdAfter=' + date + '&page=0&size=5&sortBy=createdAt';
        this.subscribersService.eHistory(urlData)
            .subscribe(function (res) {
            _this.tenEventHistory = JSON.parse(res['_body']);
            console.log('ten', _this.tenEventHistory);
        }, function (err) {
            // this.error = JSON.parse(err["_body"]);
            _this.error = _this.helpers.getError(err);
        });
    };
    // GET SUBSCRIBER BY ID
    ViewSubscriberComponent.prototype.getSubscriber = function () {
        var _this = this;
        this.subscribersService.getSubscriber(this.subscriberId)
            .subscribe(function (res) {
            console.log('subscriber', res);
            _this.subscriber = res;
            // FREEVIEW
            if (_this.subscriber.addressLine5 == 'FREEVIEW') {
                _this.freeview = true;
            }
            // GET ENTITLEMENTS BY ID
            _this.getEntitlements();
            // replace null for '' to avoid errors
            for (var _i = 0, _a = Object.keys(_this.subscriber); _i < _a.length; _i++) {
                var key = _a[_i];
                if (_this.subscriber[key] == null) {
                    _this.subscriber[key] = '';
                }
            }
            _this.vipVal = _this.subscriber.vip;
            // CARD SUBSCRIBER CHECK
            if (_this.subscriber.cardSubscriberId.length === 0 || _this.subscriber.cardSubscriberId == null ||
                _this.subscriber.cardSubscriberId === 'undefined') {
                _this.CSIDisabledCheck = true;
                console.log('here');
            }
            sessionStorage.setItem('subscriber', JSON.stringify(_this.subscriber));
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // GET ENTITLEMENTS
    ViewSubscriberComponent.prototype.getEntitlements = function () {
        var _this = this;
        this.productsService.getById('E', this.subscriberId)
            .subscribe(function (res) {
            console.log('entitlements', res);
            _this.entitlements = res;
            _this.isActive();
        });
    };
    // EVENT PAGE
    ViewSubscriberComponent.prototype.eventPage = function (page) {
        var _this = this;
        this.pagination.currentPage = page;
        // var urlData = 'event-histories?' + 'q=subscriberId=' + this.subscriberId +
        //   ',grouped=true' + '&sortBy=createdAt&sortOrder=desc&page=' + page + '&size=' + this.eventShow;
        var urlData = 'event-histories?text=&subscriberId=' + this.subscriberId +
            '&grouped=day&createdAfter=&page=' + page + '&size=' + this.eventShow + '&sortBy=createdAt';
        this.subscribersService.eHistory(urlData)
            .subscribe(function (res) {
            console.log(res);
            _this.eventHistory = JSON.parse(res['_body']);
        }, function (err) {
            // this.error = JSON.parse(err["_body"]);
            _this.error = _this.helpers.getError(err);
        });
    };
    // SEARCH EVENT HISTORY
    ViewSubscriberComponent.prototype.eventSearch = function () {
        var _this = this;
        var size;
        this.onlyAgent = false;
        this.eventShow = 5;
        if (this.eventsFilter.value.filterValue == '') {
            size = 5;
        }
        else {
            size = 99999;
        }
        if (this.eventsFilter.invalid) {
            this.invalidFilter = true;
        }
        else {
            var val = this.eventsFilter.value.filterValue;
            // let url = encodeURI('event-histories?q=subscriberId=' + this.subscriberId
            //   + ',grouped=true' + ',text=*' + val + '*&sortBy=createdAt&sortOrder=desc&page=0&size=' + size );
            var url = encodeURI('event-histories?text=' + val + '&subscriberId=' + this.subscriberId +
                '&grouped=day&createdAfter=&page=0&size=' + size + '&sortBy=createdAt');
            this.subscribersService.eHistory(url)
                .subscribe(function (res) {
                _this.eventHistory = JSON.parse(res['_body']);
                if (res.headers.get('link')) {
                    _this.eventHeaders = res.headers.get('link').split(',');
                    for (var i = 0; i < _this.eventHeaders.length; i++) {
                        _this.eventHeaders[i] = _this.eventHeaders[i].match(/page=([0-9]+)/);
                    }
                    _this.pagination.first = _this.eventHeaders[0][1];
                    _this.pagination.last = _this.eventHeaders[1][1];
                    // console.log(this.eventHeaders, 'eventHeaders');
                }
                else {
                    _this.pagination.last = _this.pagination.currentPage;
                }
                _this.eventHistory = JSON.parse(res['_body']);
            }, function (err) {
                // this.error = JSON.parse(err["_body"]);
                _this.error = _this.helpers.getError(err);
            });
        }
    };
    // ADD EVENT MANUALLY
    ViewSubscriberComponent.prototype.addEvent = function () {
        var _this = this;
        this.addEv = false;
        var data = {
            subscriberId: this.subscriberId,
            cmsId: app_settings_1.AppSettings.CMSId,
            text: this.note + ' [AGENT: ' + this.username + ']',
        };
        this.subscribersService.urlPost('event-histories', data)
            .subscribe(function (res) {
            _this.getHistory();
        }, function (err) {
            // this.error = JSON.parse(err["_body"]);
            _this.error = _this.helpers.getError(err);
        });
        this.note = '';
    };
    // EDIT EVENT
    ViewSubscriberComponent.prototype.editEvent = function (text, id) {
        this.updEv = true;
        this.note = text;
        this.eventId = id;
        console.log('eventId', this.eventId);
    };
    // UPDATE EVENT AFTER EDIT
    ViewSubscriberComponent.prototype.updateEvent = function () {
        var _this = this;
        this.updEv = false;
        var data = {
            eventHistoryId: this.eventId,
            text: this.note
        };
        console.log('Event data', data);
        this.subscribersService.urlPut('event-histories', data)
            .subscribe(function (res) {
            console.log(res);
            _this.getHistory();
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
        this.note = '';
    };
    // DELETE EVENT
    ViewSubscriberComponent.prototype.deleteEvent = function () {
        var _this = this;
        this.delEv = false;
        this.accessGranted = false;
        this.user = '';
        this.password = '';
        this.subscribersService.urlDelete('event-histories/' + this.eventId)
            .subscribe(function (res) {
            _this.getHistory();
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // ACTIVE INACTIVE
    ViewSubscriberComponent.prototype.isActive = function () {
        this.active = true;
        this.entAwaitingActivation = false;
        if (!this.subscriber.enabled) {
            this.active = false;
            this.entAwaitingActivation = false;
            this.waitSubsActive = true;
        }
        else {
            if (this.entitlements.length == 0) {
                this.active = false;
                this.entAwaitingActivation = false;
                this.waitSubsActive = false;
            }
            else {
                for (var _i = 0, _a = this.entitlements; _i < _a.length; _i++) {
                    var a = _a[_i];
                    if (a.activated == false) {
                        this.active = false;
                        this.entAwaitingActivation = true;
                        this.waitSubsActive = false;
                    }
                }
            }
        }
    };
    // SEARCH PROCESS
    ViewSubscriberComponent.prototype.processSearch = function () {
        this.processFound = [];
        for (var i = 0; i < this.processArray.length; i++) {
            if (this.processArray[i].text.toLowerCase().includes(this.str.toLowerCase())) {
                this.processFound.push(this.processArray[i]);
            }
        }
    };
    // CHANGE CREDIT LIMIT
    ViewSubscriberComponent.prototype.changeCredit = function () {
        var _this = this;
        this.user = '';
        this.password = '';
        this.submitted = true;
        if (this.changeCreditLimitForm.valid) {
            this.submitted = false;
            var json = {
                cardSubscriberId: this.subscriber.cardSubscriberId,
                creditLimit: this.changeCreditLimitForm.value.creditLimit
            };
            this.productsService.urlProducts('E', 'change-credit-limit', json)
                .subscribe(function (res) {
                _this.changeCreditLimit = false;
                _this.submitted = false;
                _this.accessGranted = false;
                _this.getSubscriber();
                _this.getHistory();
            }, function (err) {
                _this.error = _this.helpers.getError(err);
                _this.changeCreditLimit = false;
                _this.submitted = false;
                _this.accessGranted = false;
            });
        }
    };
    // EDIT VIEWING CARD NUMBER
    ViewSubscriberComponent.prototype.editVCN = function () {
        var _this = this;
        this.submitted = true;
        var data = {
            cardSubscriberId: this.subscriber.cardSubscriberId,
            viewingCardNumber: this.editVCNForm.value.viewingCardNumber
        };
        if (this.editVCNForm.valid) {
            this.subscribersService.urlPut('subscribers/update-viewing-card-number', data)
                .subscribe(function (res) {
                console.log('updateVCN', res);
                _this.editViewingCardNumber.false;
                _this.helpers.reset();
                _this.submitted = false;
                _this.editVCNForm.setValue({ viewingCardNumber: '' });
            }, function (err) {
                _this.editViewingCardNumber = false;
                _this.error = _this.helpers.getError(err);
                _this.editVCNForm.setValue({ viewingCardNumber: '' });
                _this.submitted = false;
            });
            this.submitted = false;
        }
    };
    // CANCEL ACCOUNT
    ViewSubscriberComponent.prototype.cancelProceed = function () {
        var _this = this;
        this.cancelAccount = false;
        this.CAoptions = false;
        this.accessGranted = false;
        this.user = '';
        this.password = '';
        var entitlementsNames = [];
        for (var _i = 0, _a = this.entitlements; _i < _a.length; _i++) {
            var ent = _a[_i];
            entitlementsNames.push(ent.productGroupName);
        }
        var data = {
            "subscriberId": this.subscriber.id,
            "addedProductGroupNames": [],
            "removedProductGroupNames": entitlementsNames
        };
        this.productsService.updateEntitlements('E', data)
            .subscribe(function (res) {
            console.log('update', res);
            _this.getEntitlements();
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // CHECK ADMIN
    ViewSubscriberComponent.prototype.checkIfAdmin = function () {
        this.accessGranted = '';
        this.user = '';
        this.password = '';
        if (this.role == 'admin') {
            this.accessGranted = 'admin';
        }
        else {
            this.requireAdmin = true;
        }
    };
    // AUTHORIZE
    ViewSubscriberComponent.prototype.authorize = function () {
        var _this = this;
        this.requireAdmin = false;
        this.helpers.adminRequired(this.user, this.password)
            .subscribe(function (res) {
            var resp = JSON.parse(res['_body']);
            if (resp.roles[0].name == 'ROLE_ADMIN') {
                _this.accessGranted = 'admin';
            }
            else {
                _this.error = { message: 'Access Denied' };
                // SET TO FALSE AND EMPTY USER/PASS
                _this.cancelAccount = false;
                _this.changeCreditLimit = false;
                _this.delEv = false;
                _this.user = '';
                _this.password = '';
            }
        }, function (err) {
            _this.error = _this.helpers.getError(err);
            _this.cancelAccount = false;
            _this.changeCreditLimit = false;
            _this.delEv = false;
            _this.user = '';
            _this.password = '';
        });
    };
    // VIP
    ViewSubscriberComponent.prototype.vip = function () {
        var _this = this;
        this.vipBox = false;
        if (this.billingInfo.owed > 0) {
            this.vipChangeDisabled = true;
            this.getSubscriber();
        }
        else {
            if (this.vipVal) {
                this.vipVal = false;
            }
            else {
                this.vipVal = true;
            }
            console.log('VIP', this.vipVal);
            var data = {
                "subscriberId": this.subscriber.id,
                "vip": this.vipVal
            };
            this.subscribersService.urlSubscribers('set-vip', data)
                .subscribe(function (res) {
                console.log(res);
                _this.getSubscriber();
                _this.getHistory();
            }, function (err) {
                _this.error = _this.helpers.getError(err);
                _this.getSubscriber();
            });
        }
    };
    /*vip dialog box no*/
    ViewSubscriberComponent.prototype.vipNo = function () {
        this.vipBox = false;
        this.getSubscriber();
    };
    // OWED CHECK FOR PROCESS 18 AND 22
    ViewSubscriberComponent.prototype.processOwedCheck = function (pNumber) {
        if (pNumber == 22) {
            sessionStorage.setItem('process22', 'true');
        }
        else {
            sessionStorage.setItem('process22', 'false');
        }
        if (this.billingInfo.owed > 0) {
            this.process18 = true;
        }
        else if (this.billingInfo.owed == 0) {
            this.route.navigate(['/process21']);
        }
    };
    // PROCESS 18 PAYMENT
    ViewSubscriberComponent.prototype.proceedPayment = function () {
        var _this = this;
        this.process18 = false;
        /*CHECK FOR PAYMENT CARD*/
        if (this.billingInfo.hasPaymentCard) {
            // Process payment
            this.processPayment();
        }
        else if (!this.billingInfo.hasPaymentCard) {
            // Add Payment card if not found all fields required if customerId not supplied
            var data = {
                currencyCode: this.subscriber.currencyCode,
                title: this.subscriber.title,
                initials: this.subscriber.initials,
                surname: this.subscriber.surname,
                addressLine1: this.subscriber.addressLine1,
                postcode: this.subscriber.postcode,
                country: this.subscriber.countryCode,
                email: this.customer.email,
                returnUrl: window.location.href
            };
            // Data to url string
            var str = Object.keys(data).map(function (key) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
            }).join('&');
            this.paymentsService.getUrl('get-payment-card-url?', str)
                .subscribe(function (res) {
                console.log(res);
                var url = JSON.parse(res['_body']);
                location.replace(url.storeCardUrl);
            }, function (err) {
                // this.error = JSON.parse(err["_body"]);
                _this.error = _this.helpers.getError(err);
            });
        }
    };
    ViewSubscriberComponent.prototype.processPayment = function () {
        var _this = this;
        // SETTLE BALANCE
        var data = {
            customerId: this.customer.id,
            amount: this.billingInfo.owed
        };
        this.paymentsService.putUrl('settle-balance', data)
            .subscribe(function (res) {
            console.log(res);
            if (sessionStorage.getItem('process22') == 'true') {
                _this.route.navigate(['/process21']);
                /***when payment get and store billing info***/
            }
            else {
                _this.paymentSuccess = true;
            }
            // this.balanceSettled = true;
        }, function (err) {
            // this.error = JSON.parse(err["_body"]);
            _this.error = _this.helpers.getError(err);
        });
    };
    // RE-AUTHORISE SIGNALS
    ViewSubscriberComponent.prototype.reauthorise = function () {
        var _this = this;
        var json = {
            cardSubscriberId: this.subscriber.cardSubscriberId
        };
        this.productsService.urlProducts('E', 'reauthorise-services', json)
            .subscribe(function (res) {
            console.log(res);
            _this.reauthoriseSuccess = true;
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    // PROCESS BOX CLOSE
    ViewSubscriberComponent.prototype.processBoxClose = function () {
        this.processBox = false;
        this.process6 = false;
        this.process7 = false;
        this.process9 = false;
        this.process10 = false;
        this.process11 = false;
        this.process14 = false;
        this.process32 = false;
        this.process34 = false;
        this.process35 = false;
    };
    // JOIN CARD
    ViewSubscriberComponent.prototype.joinCard = function () {
        sessionStorage.setItem('customerId', this.customer.id);
        this.route.navigate(['/joinSubscriber']);
    };
    // CHECK TO ALLOW CHANGING ENTITLEMENTS
    ViewSubscriberComponent.prototype.changeDisabledCheck = function (type) {
        if (this.entAwaitingActivation || this.billingInfo.owed > 0 || this.waitSubsActive) {
            this.changeDisabled = true;
        }
        else if (this.CSIDisabledCheck) {
            if (type === 'O') {
                this.CSIDisabled = true;
            }
            else if (type === 'S') {
                this.route.navigate(['/entitlements'], { queryParams: { type: 1 } });
            }
        }
        else {
            if (type === 'S') {
                this.route.navigate(['/entitlements'], { queryParams: { type: 1 } });
            }
            else if (type === 'O') {
                this.route.navigate(['/entitlements'], { queryParams: { type: 2 } });
            }
        }
    };
    // REQUEST CALLBACK
    ViewSubscriberComponent.prototype.requestCallback = function () {
        var _this = this;
        var cardSubscriberId = {
            cardSubscriberId: this.subscriber.cardSubscriberId
        };
        // const subscriberId = {
        //   cardSubscriberId: this.subscriber.id
        // };
        this.subscribersService.urlSubscribers('request-callback', cardSubscriberId)
            .subscribe(function (res) {
            _this.callbackDialog = true;
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    ViewSubscriberComponent.prototype.onlyAgentFilter = function () {
        var _this = this;
        if (this.onlyAgent) {
            console.log(this.onlyAgent);
            this.pagination.currentPage = 0;
            // search for agent word
            // let url = encodeURI('event-histories?q=subscriberId=' + this.subscriberId
            //   + ',text=*agent*&sortBy=createdAt&sortOrder=desc&page=0&size=9999');
            var url = encodeURI('event-histories?text=agent&subscriberId=' + this.subscriberId +
                '&grouped=day&createdAfter=&page=0&size=' + 9999 + '&sortBy=createdAt');
            this.subscribersService.eHistory(url)
                .subscribe(function (res) {
                _this.eventHistory = JSON.parse(res['_body']);
                if (res.headers.get('link')) {
                    _this.eventHeaders = res.headers.get('link').split(',');
                    for (var i = 0; i < _this.eventHeaders.length; i++) {
                        _this.eventHeaders[i] = _this.eventHeaders[i].match(/page=([0-9]+)/);
                    }
                    _this.pagination.first = _this.eventHeaders[0][1];
                    _this.pagination.last = _this.eventHeaders[1][1];
                    // console.log(this.eventHeaders, 'eventHeaders');
                }
                else {
                    _this.pagination.last = _this.pagination.currentPage;
                }
                _this.eventHistory = JSON.parse(res['_body']);
            }, function (err) {
                // this.error = JSON.parse(err["_body"]);
                _this.error = _this.helpers.getError(err);
            });
        }
        else {
            this.eventSearch();
        }
    };
    ViewSubscriberComponent = __decorate([
        core_1.Component({
            selector: 'viewSubscriber',
            templateUrl: 'viewSubscriber.component.html',
            styleUrls: ['viewSubscriber.component.css'],
            providers: [validators_service_1.ValidatorsService, subscribers_service_1.SubscribersService, products_service_1.ProductsService, payments_service_1.PaymentsService, helpers_service_1.HelpersService, user_service_1.UserService]
        })
    ], ViewSubscriberComponent);
    return ViewSubscriberComponent;
}());
exports.ViewSubscriberComponent = ViewSubscriberComponent;
/*
 TODO: convert pipe to service
 {{separatePoscode(postcode)}}
 */
var PcPipe = /** @class */ (function () {
    function PcPipe() {
    }
    PcPipe.prototype.transform = function (value) {
        // value = value.match(/^([A-Z]{1,2}\d{1,2}[A-Z]?)\s*(\d[A-Z]{2})$/);
        // value.shift();
        // value.join(' ');
        value = value.toUpperCase();
        var exceptions = ['UK', 'ROI', 'IOM', 'CI', 'BFPO'];
        // If Postcode Exception type, return
        exceptions.forEach(function (exp) {
            if (value === exp) {
                return value;
            }
        });
        // If length is < 4, return
        if (value.length < 4) {
            return value;
        }
        var suffix = value.substring(value.length - 3);
        var prefix = value.substring(0, value.length - 3);
        return prefix + ' ' + suffix;
    };
    PcPipe = __decorate([
        core_2.Pipe({ name: 'pc' })
    ], PcPipe);
    return PcPipe;
}());
exports.PcPipe = PcPipe;
// PROCESSES LIST
var process = [
    {
        link: '/process3',
        text: '3. I need to activate my new viewing card and have a phone line/broadband connected to my set top box',
        // Card subscriber Id
        CSI: true
    },
    {
        link: '/process4',
        text: '4. I need to activate my viewing card but do not have a phone line/broadband connected to my set top box',
        CSI: true
    },
    {
        link: '/process5',
        text: '5. I need to make my existing viewing card work with my new set top box',
        CSI: true
    },
    {
        link: '/process6',
        text: '6. I’ve received a new viewing card, my Sky package is OK but I cannot see my subscription channels anymore.',
        CSI: false
    },
    {
        link: '/process7',
        text: '7. I have two Sky boxes, I want to move my subscription to the one in the other room.',
        CSI: false
    },
    {
        link: '/process8',
        text: '8. I am moving house/ commercial premise and I want to keep my subscription',
        CSI: false
    },
    {
        link: '/process9',
        text: '9. I would like to remove channels from my subscription to reduce my costs.',
        CSI: false
    },
    {
        link: '/process10',
        text: '10. I would like to stop viewing my subscription in another room and reduce my costs, I do not have Sky Q.',
        CSI: false
    },
    {
        link: '/process11',
        text: '11. I have cancelled my Sky Q Multiscreen subscription with Sky, so I cannot view' +
            ' my subscription channels from you in other rooms, why am I still paying extra to you.',
        CSI: false
    },
    {
        link: '/process12',
        text: '12. I have cancelled my Sky Q Multiscreen subscription with Sky, so I cannot view ' +
            'my subscription channels from you on the Sky Q app on my iPad, why am I still paying extra to you.',
        CSI: false
    },
    {
        link: '/process13',
        text: '13. I would like to cancel all my subscriptions.',
        CSI: false
    },
    {
        link: '/process14',
        text: '14. I would like to add additional channels to my subscription package.',
        CSI: false
    },
    {
        link: '/process15',
        text: '15. I would like to view my subscription channels in another room and I do not have Sky Q.',
        CSI: false
    },
    {
        link: '/process16',
        text: '16. I have Sky Q Minis and would like to view my subscription channels in another room.',
        CSI: true
    },
    {
        link: '/process17',
        text: '17. I have Sky Q and would like to view my subscription channels on my iPad and other devices.',
        CSI: false
    },
    {
        link: '/process18',
        text: '18. I didn’t pay my bill and have had a subscription suspended; ' +
            'but have now paid my bill and would like my subscription to be resumed.',
        CSI: false
    },
    {
        link: '/process19',
        text: '19. I would like to know my current PIN and/or Parental Rating settings',
        CSI: true
    },
    {
        link: '/process20',
        text: '20. I would like to change my PIN and/or Parental Rating settings',
        CSI: false
    },
    {
        link: '/process21',
        text: '21. I am paying for my subscription but I cannot see the channels.',
        CSI: true
    },
    {
        link: '/process22',
        text: '22. I’ve upgraded to Sky Q, my Sky package is OK but now I cannot see my subscription channels',
        CSI: false
    },
    {
        link: '/process23',
        text: '23. I’ve upgraded to Sky Q with Sky Q Minis and I pay you to watch ' +
            'my subscription in another room but it’s not working on the Sky Q Minis.',
        CSI: true
    },
    {
        link: '/process24',
        text: '24. I’ve upgraded to Sky Q with Sky Q Minis, Sky left my old box in the' +
            ' extra room, and I pay you extra to watch my subscription in that room, can you make it work on the Sky Q Minis.',
        CSI: true
    },
    {
        link: '/process25/n',
        text: '25. I have a problem and the agent decided I need a replacement viewing card (Automatic)',
        CSI: true
    },
    {
        link: '/process25/y',
        text: '26. I have a problem and the agent decided I need a replacement viewing ' +
            'card, I need it urgently for a live event tomorrow (Manual/Special)',
        CSI: true
    },
    {
        link: '/process27',
        text: '27. I would like to cancel my request for a replacement viewing card',
        CSI: true
    },
    {
        link: '/process28',
        text: '28. I have ordered a new / replacement viewing card but you ' +
            'have emailed me to tell me it cannot be delivered.',
        CSI: false
    },
    {
        link: '/process29',
        text: '29. I would like to request you to resend the viewing card that did ' +
            'not arrive (but Sky say the card was returned undelivered)',
        CSI: false
    },
    {
        link: '/process28',
        text: '30. I would like to request re-issue of the viewing card that did not arrive (assumed lost in post)',
        CSI: false
    },
    // {
    //   link: '/process31',
    //   text: '31. I cancelled all my services on Sky except your subscription (CMS becomes parent – Notification from Sky)',
    //   CSI: false
    // },
    {
        link: '/process32',
        text: '32. I would like to buy a subscription and I already have an existing account with you',
        CSI: false
    },
    {
        link: '/process33',
        text: '33. I have lost the ability to view my automatically entitled (Free to View) Channel(s)',
        CSI: true,
    },
    {
        link: '/process34',
        text: '34. I would like to register and buy a ppv event and I already have an existing and active viewing card.',
        CSI: true,
    },
    {
        link: '/process35',
        text: '35. I am an existing customer and would like to buy an OPPV event from you.',
        CSI: true,
    }
];
