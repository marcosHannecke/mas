"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var viewSubscriber_component_1 = require("./viewSubscriber.component");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var process7_component_1 = require("../../process/process7/process7.component");
var highlightReverse_pipe_1 = require("../../../highlightReverse.pipe");
var viewSubscriber_router_1 = require("./viewSubscriber.router");
var app_module_1 = require("../../../app.module");
var ViewSubscriberViewModule = /** @class */ (function () {
    function ViewSubscriberViewModule() {
    }
    ViewSubscriberViewModule = __decorate([
        core_1.NgModule({
            declarations: [viewSubscriber_component_1.ViewSubscriberComponent, highlightReverse_pipe_1.HighlightPipe, highlightReverse_pipe_1.ReversePipe, highlightReverse_pipe_1.OrderValidFrom, process7_component_1.Process7Component, viewSubscriber_component_1.PcPipe],
            imports: [common_1.CommonModule, forms_1.FormsModule, forms_1.ReactiveFormsModule, router_1.RouterModule, viewSubscriber_router_1.viewSubscriberRouter, app_module_1.MaterialModule],
        })
    ], ViewSubscriberViewModule);
    return ViewSubscriberViewModule;
}());
exports.ViewSubscriberViewModule = ViewSubscriberViewModule;
