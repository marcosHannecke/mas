import {Routes, RouterModule} from "@angular/router";
import {ChangePackagesComponent} from "./changePackages.component";

const ENTITLEMENTSSUBSCRIBER_ROUTER: Routes = [
  {
    path: '',
    component: ChangePackagesComponent,
  },
];

export const changePackagesRouter = RouterModule.forChild(ENTITLEMENTSSUBSCRIBER_ROUTER);
