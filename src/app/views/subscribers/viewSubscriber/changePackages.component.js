"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var products_service_1 = require("../../../services/products.service");
var helpers_service_1 = require("../../../services/helpers.service");
var ChangePackagesComponent = /** @class */ (function () {
    function EntitlementsComponent(router, productsService, location, helpers) {
        this.router = router;
        this.productsService = productsService;
        this.location = location;
        this.helpers = helpers;
        this.productGroups = [];
        this.PGFound = [];
        this.subscribers = [];
        this.subscriberEnt = [];
        this.addEnt = [];
        this.previewDialog = false;
        this.removeDialog = false;
        this.view1 = true;
        this.view2 = false;
        this.discount = [];
        this.invalidDiscount = [];
        this.validDiscount = false;
        this.OPPV = [];
        this.oppvPrices = {};
        this.allOPPVvalidated = false;
        this.count = 0;
        this.pleaseWait = false;
        this._array = Array;
    }
    EntitlementsComponent.prototype.ngOnInit = function () {
        var _this = this;
        // GET QUERY PARAMS
        this.router.queryParams
            .subscribe(function (params) {
            _this.type = params['type'];
        });
        this.helpers.scrollTop();
        // GET SUBSCRIBER STORED
        this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
        if (this.type == 1) {
            if (sessionStorage.getItem('sellables') != null) {
                this.OPPVs(this.productGroups = JSON.parse(sessionStorage.getItem('sellables')));
            }
            else {
                this.pleaseWait = true;
                // GET SELLABLE PACKAGES
                var array = ['subscriberId=' + this.subscriber['id']];
                this.productsService.getSellables(array)
                    .subscribe(function (res) {
                    console.log('sellables', res);
                    _this.OPPVs(res);
                    _this.pleaseWait = false;
                }, function (err) {
                    // this.error = JSON.parse(err["_body"]);
                    _this.error = _this.helpers.getError(err);
                });
            }
        }
        else if (this.type == 2) {
            if (sessionStorage.getItem('OPPVsellables') != null) {
                this.productGroups = JSON.parse(sessionStorage.getItem('OPPVsellables'));
                this.PGFound = JSON.parse(sessionStorage.getItem('OPPVsellables'));
            }
            else {
                this.pleaseWait = true;
                // GET OPPV
                this.getOPPV();
            }
        }
        // GET THE SUBSCRIBER ENTITLEMENTS
        this.productsService.getById('E', this.subscriber['id'])
            .subscribe(function (res) {
            console.log('entitlements', res);
            _this.subscriberEnt = res;
            _this.getProductGroups();
        }, function (err) {
            // this.error = JSON.parse(error["_body"])
            _this.error = _this.helpers.getError(err);
        });
    };
  // REMOVE OPPV FROM SELLABLES
    EntitlementsComponent.prototype.OPPVs = function (res) {
        var sellables = [];
        // Remove OPPV from /get-sellables
        for (var i = 0; i < res.length; i++) {
            if (res[i].pricing.oneOff == null || res[i].pricing.oneOff.indicative == false) {
                sellables.push(res[i]);
            }
        }
        sellables.sort(this.helpers.alphabetical);
        this.productGroups = sellables;
        this.PGFound = sellables;
    };
  // GET OPPV
    EntitlementsComponent.prototype.getOPPV = function () {
        var _this = this;
        var arr = [];
        var array = ['subscriberId=' + this.subscriber['id'], 'productTypesOnly=OPPV_ONLY|IPPV_OPPV'];
        this.productsService.getSellablesGroupedByProduct('PG', array)
            .subscribe(function (res) {
            console.log('OPPV!!!!', res);
            arr = Object.keys(res).map(function (key) { return res[key]; });
            console.log(arr.sort(_this.helpers.validFromOrder));
            _this.productGroups = arr;
            _this.PGFound = arr;
            _this.pleaseWait = false;
            console.log('arr', arr);
        }, function (err) {
            // this.error = JSON.parse(err["_body"])
            _this.error = _this.helpers.getError(err);
        });
    };
    // GET PRODUCT GROUPS FROM ENTITLEMENTS
    EntitlementsComponent.prototype.getProductGroups = function () {
        var _this = this;
        // var pGroup = [];
        for (var i = 0; i < this.subscriberEnt.length; i++) {
            this.productsService.getProductGroupsById(this.subscriberEnt[i].productGroupId)
                .subscribe(function (res) {
                // pGroup.push(JSON.parse(res["_body"]));
                _this.addEnt.push(JSON.parse(res['_body']));
            }, function (err) {
                // this.error = JSON.parse(err["_body"])
                _this.error = _this.helpers.getError(err);
            });
        }
    };
    // PREVIEW DIALOG
    EntitlementsComponent.prototype.addedRemovedPackages = function () {
        var _this = this;
        this.previewDialog = true;
        this.discount = [];
        this.invalidDiscount = [];
        var removed = [];
        var packAdded = [];
        this.oppvPrices = {};
        // PACKAGE REMOVED
        for (var _i = 0, _a = this.subscriberEnt; _i < _a.length; _i++) {
            var ent = _a[_i];
            var found = true;
            for (var _b = 0, _c = this.addEnt; _b < _c.length; _b++) {
                var added = _c[_b];
                if (added.id == ent.productGroupId) {
                    found = false;
                    break;
                }
            }
            if (found == true) {
                this.productsService.getProductGroupsById(ent.productGroupId)
                    .subscribe(function (res) {
                    removed.push(JSON.parse(res['_body']));
                }, function (err) {
                    // this.error = JSON.parse(err["_body"])
                    _this.error = _this.helpers.getError(err);
                });
            }
        }
        this.packRemoved = removed;
        // PACKAGE ADDED
        for (var _d = 0, _e = this.addEnt; _d < _e.length; _d++) {
            var added = _e[_d];
            var found = false;
            for (var _f = 0, _g = this.subscriberEnt; _f < _g.length; _f++) {
                var ent = _g[_f];
                if (added.id == ent.productGroupId) {
                    found = true;
                    break;
                }
            }
            if (found == false) {
                packAdded.push(added);
            }
        }
        this.packAdded = packAdded;
    };
    // CHECK IF ALL OPPV EVENTS HAVE BEEN VALIDATED
    EntitlementsComponent.prototype.allOPPVvalidatedCheck = function () {
        var validated = true;
        this.validDiscount = false;
        for (var _i = 0, _a = this.packAdded; _i < _a.length; _i++) {
            var oppv = _a[_i];
            if (oppv.validated == null && this._array.isArray(oppv)) {
                validated = false;
                break;
            }
            else if (!this._array.isArray(oppv)) {
                if (oppv.pricing.oneOff != null && oppv.validated == null)
                    if (oppv.pricing.oneOff.indicative == true) {
                        validated = false;
                        break;
                    }
            }
        }
        // Valid discount
        for (var i = 0; i < this.invalidDiscount.length; i++) {
            if (this.invalidDiscount[i]) {
                this.validDiscount = true;
            }
        }
        if (this.validDiscount) {
            this.error = {
                message2: 'Invalid discount value.'
            };
        }
        else if (validated) {
            this.updatedEnt();
        }
        else {
            this.allOPPVvalidated = true;
        }
    };
    // UPDATE ENTITLEMENTS
    EntitlementsComponent.prototype.updatedEnt = function () {
        var _this = this;
        var add = [];
        var remove = [];
        var initialIds = [];
        for (var a = 0; a < this.subscriberEnt.length; a++) {
            initialIds.push(this.subscriberEnt[a].productGroupId);
        }
        var finalIds = [];
        for (var q = 0; q < this.addEnt.length; q++) {
            finalIds.push(this.addEnt[q].id);
        }
        // ADD IDS
        for (var i = 0; i < this.addEnt.length; i++) {
            if (!initialIds.includes(this.addEnt[i].id)) {
                add.push(this.addEnt[i]);
            }
        }
        // REMOVE IDS
        for (var b = 0; b < initialIds.length; b++) {
            if (!finalIds.includes(initialIds[b])) {
                remove.push(this.subscriberEnt[b]);
            }
        }
        // ADD NAMES
        for (var r = 0; r < add.length; r++) {
            add[r] = add[r].name;
        }
        // REMOVE NAMES
        for (var t = 0; t < remove.length; t++) {
            remove[t] = remove[t].productGroupName;
        }
        // DISCOUNT
        var discount = {};
        for (var i = 0; i < this.packAdded.length; i++) {
            if (this._array.isArray(this.packAdded[i])) {
                lastOPPV = this.getLastOPPV(i);
            }
            else {
                lastOPPV = this.packAdded[i];
            }
            // REPLACE NULL WITH WITH lastOPPV NAME
            add[i] = lastOPPV.name;
            if (this.discount[i] !== undefined) {
                if (this.discount[i] > 0) {
                    discount[add[i]] = this.discount[i];
                }
            }
        }
        console.log('discount', discount);
        // REPLACE NULL WITH WITH lastOPPV NAME
        var lastOPPV;
        for (var i = 0; i < this.packAdded.length; i++) {
            if (this._array.isArray(this.packAdded[i])) {
                lastOPPV = this.getLastOPPV(i);
            }
            else {
                lastOPPV = this.packAdded[i];
            }
            // REPLACE NULL WITH WITH lastOPPV NAME
            add[i] = lastOPPV.name;
        }
        // ADD PRICES
        console.log(this.oppvPrices, 'addedProductGroupNames');
        /******* DATA TO SEND*******/
        this.entUpdate = {
            "subscriberId": this.subscriber['id'],
            "addedProductGroupNames": add,
            "addedOppvProductGroupsPrices": this.oppvPrices,
            "addedProductGroupsDiscounts": discount,
            "removedProductGroupNames": remove
        };
        // console.log(JSON.stringify(this.entUpdate), 'this.entUpdate');
        this.productsService.updateEntitlements('E', this.entUpdate)
            .subscribe(function (res) {
            console.log('update', res);
            _this.location.back();
        }, function (err) {
            var error = _this.helpers.getError(err);
            if (error.message.toLowerCase().includes('payment failed')) {
                error.message2 = 'Please update the customers payment details with a valid card and try again.';
                _this.error = error;
            }
            else {
                _this.error = error;
            }
        });
    };
    // SHOW OPPVs CARDS
    EntitlementsComponent.prototype.showOPPVs = function (i) {
        if (this.showCards == i) {
            this.showCards = null;
        }
        else {
            this.showCards = i;
        }
    };
    // SELECT PACKAGES
    EntitlementsComponent.prototype.isSelected = function (pack) {
        var p = false;
        var pos;
        for (var i = 0; i < this.addEnt.length; i++) {
            if (this.addEnt[i].id == pack.id) {
                p = true;
                pos = i;
            }
        }
        return [p, pos];
    };
    // SELECT NORMAL SELLABLE PACKAGE
    EntitlementsComponent.prototype.selectPackage = function (product, index) {
        this.PGFound.splice(index, 1);
        if (!this.isSelected(product)[0]) {
            this.addEnt.push(product);
        }
        else {
            this.addEnt.splice(this.isSelected(product)[1], 1);
        }
    };
    // SELECT ONLY OPPV CLICKED
    EntitlementsComponent.prototype.selectOPPV = function (product, i, ind, pG) {
        console.log('pG', pG);
        // var oppv = this.getLastValidFrom(pG);
        var oppv = pG[pG.length - 1];
        var productIdValidFromId = oppv.productIds[oppv.productIds.length - 1];
        if (this.PGFound[i].length == 0) {
            this.PGFound.splice(i, 1);
            this.showCards = false;
        }
        if (!this.isSelected(product)[0]) {
            // add productIdValidFromId which will be used to get caInstNum
            product['productIdValidFromId'] = productIdValidFromId;
            console.log('product', product);
            // add product to added packages
            this.addEnt.push(product);
        }
        else {
            this.addEnt.splice(this.isSelected(product)[1], 1);
        }
        // Remove selected from sellables
        this.PGFound[i].splice(ind, 1);
    };
    // get valid to from last valid from
    EntitlementsComponent.prototype.getLastValidFrom = function (pG) {
        var last = pG[0];
        for (var _i = 0, pG_1 = pG; _i < pG_1.length; _i++) {
            var date = pG_1[_i];
            if (date.validFrom > last.validFrom) {
                last = date;
                console.log(last);
            }
        }
        return last;
    };
    // SELECT ALL OPPV from same group
    EntitlementsComponent.prototype.selectAllOPPV = function (i) {
        this.addEnt.push(this.PGFound[i]);
        this.PGFound.splice(i, 1);
    };
    // RESET VALIDATED
    EntitlementsComponent.prototype.resetValidated = function () {
        for (var i = 0; i < this.packAdded.length; i++) {
            this.packAdded[i].validated = null;
        }
    };
    // REMOVE ENTITLEMENT
    EntitlementsComponent.prototype.removeEntitlement = function () {
        var pack = this.removeDialog.pack;
        var i = this.removeDialog.i;
        this.addEnt.splice(i, 1);
        if (!this.isSelected(pack)[0] || pack.length > 0) {
            this.PGFound.push(pack);
        }
        else {
            this.PGFound.splice(this.isSelected(pack)[1], 1);
        }
        this.removeDialog = false;
    };
    // VALIDATE OPPV EVENT 1/3
    EntitlementsComponent.prototype.validateOPPVevent = function (i) {
        var _this = this;
        var lastOPPV = '';
        // Set productId of single selected
        var productId = this.packAdded[i].productIdValidFromId;
        // Set productId of group selected
        if (this._array.isArray(this.packAdded[i])) {
            lastOPPV = this.getLastOPPV(i);
            productId = this.getProductIdFromLast(lastOPPV);
        }
        console.log(productId);
        // GET caProductId
        this.productsService.getProductById('P', productId)
            .subscribe(function (res) {
            var resp = JSON.parse(res['_body']);
            _this.validateOPPVrequest(resp.caProductId, resp.caInstNum, i);
        }, function (err) {
            // this.error = JSON.parse(err["_body"])
            _this.error = _this.helpers.getError(err);
        });
    };
    // VALIDATE OPPV EVENT 2/3
    EntitlementsComponent.prototype.validateOPPVrequest = function (caProductId, caInstNum, i) {
        var _this = this;
        console.log('caProductId', caProductId, 'caInstNum', caInstNum);
        var url = 'validate-oppv-event-for-subscriber?';
        url += 'cardSubscriberId=' + this.subscriber.cardSubscriberId;
        url += '&caProductId=' + caProductId;
        url += '&caInstNum=' + caInstNum;
        this.productsService.urlGet('E', url)
            .subscribe(function (res) {
            var resp = JSON.parse(res['_body']);
            console.log(resp);
            // let i = this.OPPVposition;
            // let resp = this.OPPVresponse;
            // update package info
            // this.packAdded[i].validated = true;
            // this.packAdded[i].price = resp.price;
            // this.packAdded[i].pin = resp.pin;
            // this.packAdded[i].individualSpendLimit = resp.individualSpendLimit;
            // OPPV PRICES
            // if(this._array.isArray(this.packAdded[i])){
            //   var lastOPPV = this.getLastOPPV(i);
            //   this.oppvPrices[lastOPPV.name] = resp.price;
            //
            //   console.log('oppvPrices', this.oppvPrices)
            // }else if(!this._array.isArray(this.packAdded[i])){
            //   this.oppvPrices[this.packAdded[i].name] = resp.price;
            //   console.log('oppvPrices', this.oppvPrices)
            // }
            _this.subscriberPin = resp.pin;
            // Check if client can purchase
            _this.validOPPV(resp, i);
        }, function (err) {
            _this.error = _this.helpers.getError(err);
        });
    };
    EntitlementsComponent.prototype.validOPPV = function (res, i) {
        this.OPPVposition = i;
        this.OPPVresponse = res;
        if (!res.validPpvEvent) {
            this.errorOPPVvalid = {
                'error': 'This OPPV event is invalid.',
                'messages': res.messages
            };
        }
        else if (res.blackedOut) {
            this.errorOPPVvalid = {
                'error': 'The customer cannot purchase this OPPV event',
                'messages': res.messages
            };
        }
        else if (res.pinRequired) {
            if (this.count >= 3) {
                this.OPPVvalidatePin = null;
                this.error = {
                    message: 'Number of allowed PIN attempts exceeded. Please try again later.'
                };
            }
            else {
                this.OPPVvalidatePin = {
                    'message': 'Customer must provide their Sky PIN to purchase this OPPV event',
                    'messages': res.messages
                };
            }
        }
        else if (!res.pinRequired) {
            this.validate();
        }
    };
    EntitlementsComponent.prototype.PINcheck = function () {
        if (this.count >= 3) {
            this.OPPVvalidatePin = null;
            this.error = {
                message: 'Number of allowed PIN attempts exceeded. Please try again later.'
            };
        }
        else if (this.pin === this.subscriberPin) {
            this.OPPVvalidatePin = null;
            this.count = 0;
            this.validate();
        }
        else {
            if (this.count < 3) {
                this.error = {
                    message: 'PIN incorrect'
                };
                this.count++;
            }
        }
    };
    // OPPV VALIDATE 3/3
    EntitlementsComponent.prototype.validate = function () {
        var i = this.OPPVposition;
        var res = this.OPPVresponse;
        // update package info
        this.packAdded[i].validated = true;
        this.packAdded[i].price = res.price;
        this.packAdded[i].pin = res.pin;
        this.packAdded[i].individualSpendLimit = res.individualSpendLimit;
        // OPPV PRICES
        if (this._array.isArray(this.packAdded[i])) {
            var lastOPPV = this.getLastOPPV(i);
            this.oppvPrices[lastOPPV.name] = res.price;
            console.log('oppvPrices', this.oppvPrices);
        }
        else if (!this._array.isArray(this.packAdded[i])) {
            this.oppvPrices[this.packAdded[i].name] = res.price;
            console.log('oppvPrices', this.oppvPrices);
        }
    };
    EntitlementsComponent.prototype.getLastOPPV = function (i) {
        return this.packAdded[i][this.packAdded[i].length - 1];
    };
    EntitlementsComponent.prototype.getProductIdFromLast = function (OPPV) {
        return OPPV.productIds[0];
    };
  // PACKAGES FILTER
    EntitlementsComponent.prototype.filter = function () {
        var found = [];
        var a = false;
        var b = false;
        var c = false;
        var dupli = false;
        for (var i = 0; i < this.productGroups.length; i++) {
            // check for duplicates
            dupli = false;
            for (var w = 0; w < this.addEnt.length; w++) {
                if (this.productGroups[i].id == this.addEnt[w].id) {
                    dupli = true;
                }
            }
            if (!dupli) {
                if (!this._array.isArray(this.productGroups[i])) {
                    a = this.productGroups[i].name.toLowerCase().includes(this.filterValue.toLowerCase());
                    b = this.productGroups[i].titleDescription.toLowerCase().includes(this.filterValue.toLowerCase());
                    c = this.productGroups[i].id.toString().includes(this.filterValue.toString());
                    if (a == true || b == true || c == true) {
                        found.push(this.productGroups[i]);
                    }
                }
                else if (this._array.isArray(this.productGroups[i])) {
                }
            }
        }
        this.PGFound = found;
    };
    // DISCOUNT VALIDATION
    EntitlementsComponent.prototype.discountValidation = function (discount, i) {
        if (discount != null) {
            this.invalidDiscount[i] = false;
            var regex = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
            if (!regex.test(discount) && discount) {
                this.invalidDiscount[i] = true;
            }
        }
    };
    // OPPV FILTER
    EntitlementsComponent.prototype.filterOPPV = function () {
        var found = [];
        var a = false;
        var OPPVlist = [];
        var dupli = false;
        if (this.filterValue.length == 0) {
            this.PGFound = this.productGroups;
        }
        else {
            for (var o = 0; o < this.productGroups.length; o++) {
                for (var e = 0; e < this.productGroups[o].length; e++) {
                    OPPVlist.push(this.productGroups[o][e]);
                }
            }
            for (var i = 0; i < OPPVlist.length; i++) {
                // check for duplicates
                dupli = false;
                for (var w = 0; w < this.addEnt.length; w++) {
                    if (OPPVlist[i].id == this.addEnt[w].id) {
                        dupli = true;
                    }
                }
                if (!dupli) {
                    a = OPPVlist[i].titleDescription.toLowerCase().includes(this.filterValue.toLowerCase());
                    if (a === true) {
                        found.push(OPPVlist[i]);
                    }
                }
            }
            this.PGFound = found;
        }
    };
    EntitlementsComponent = __decorate([
        core_1.Component({
            selector: 'entitlements',
            templateUrl: './entitlements.component.html',
            styles: ['.product-desc{height: 130px; overflow:hidden} .ibox-content .row{min-height: 100px}' +
                    '.activeEntitlements .product-box:hover {box-shadow: none;}' +
                    '.product-box{border: none} .product-btn button{ background: white} ' +
                    '.product-btn button:hover{ background: #e23237; color: white !important}' +
                    '.productDescription{-webkit-line-clamp: 2;  overflow: hidden; display: -webkit-box;  -webkit-box-orient: vertical;}' +
                    ' .productDescription:hover{-webkit-line-clamp: 6;}' +
                    '.showOPPVs{position: relative !important; top: -24px !important; left: 0 !important; transition: all 0.5s;} ' +
                    '.showOPPVs .ibox-content {background: rgb(255, 250, 233) !important}' +
                    '.angle{position: absolute; right: 5px; bottom: 10px; width: 20px; height: 20px; font-size: 18px; color: #0570e2;}' +
                    '.iboxShadow{}' +
                    '.pin{width: 150px; margin: 0 auto;}'],
            providers: [products_service_1.ProductsService, helpers_service_1.HelpersService]
        })
    ], EntitlementsComponent);
    return EntitlementsComponent;
}());
exports.EntitlementsComponent = ChangePackagesComponent;
