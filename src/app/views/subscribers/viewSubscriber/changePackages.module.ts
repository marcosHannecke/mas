import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {changePackagesRouter} from './changePackages.router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule} from '@angular/router';
import {ChangePackagesComponent} from './changePackages.component';
import {NgDatepickerModule} from 'ng2-datepicker';
import {AuthRequiredModule} from '../../../components/common/auth-required/auth-required.module';
import {AuthRequiredComponent} from '../../../components/common/auth-required/auth-required.component';
import {MaterialModule} from '../../../app.module';
import {ErrorDetailPipe} from '../../../highlightReverse.pipe';



@NgModule({
  declarations: [ChangePackagesComponent, ErrorDetailPipe],
  imports     : [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, changePackagesRouter, NgDatepickerModule, MaterialModule, AuthRequiredModule]
})

export class EntitlementsViewModule {}
