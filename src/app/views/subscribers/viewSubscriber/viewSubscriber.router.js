"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var viewSubscriber_component_1 = require("./viewSubscriber.component");
var VIEWSUBSCRIBER_ROUTER = [
    {
        path: '',
        component: viewSubscriber_component_1.ViewSubscriberComponent,
    }
];
exports.viewSubscriberRouter = router_1.RouterModule.forChild(VIEWSUBSCRIBER_ROUTER);
