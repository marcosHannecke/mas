import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {PcPipe, ViewSubscriberComponent} from "./viewSubscriber.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule} from '@angular/router';
import {Process7Component} from "../../process/process7/process7.component";

import {HighlightPipe, ReversePipe, OrderValidFrom, PostcodePipe} from '../../../highlightReverse.pipe';
import {viewSubscriberRouter} from "./viewSubscriber.router";
import {MaterialModule} from "../../../app.module";
import {AuthRequiredModule} from '../../../components/common/auth-required/auth-required.module';
import {NgDatepickerModule} from 'ng2-datepicker';


@NgModule({
  declarations: [ViewSubscriberComponent, HighlightPipe, ReversePipe, OrderValidFrom, Process7Component, PcPipe],
  imports     : [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, viewSubscriberRouter, MaterialModule, AuthRequiredModule,
    NgDatepickerModule],
})

export class ViewSubscriberViewModule {}
