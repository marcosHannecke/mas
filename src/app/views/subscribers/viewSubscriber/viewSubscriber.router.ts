import {Routes, RouterModule} from '@angular/router';
import {ViewSubscriberComponent} from './viewSubscriber.component';

const VIEWSUBSCRIBER_ROUTER: Routes = [
  {
    path: '',
    component: ViewSubscriberComponent,
  }
];

export const viewSubscriberRouter = RouterModule.forChild(VIEWSUBSCRIBER_ROUTER);
