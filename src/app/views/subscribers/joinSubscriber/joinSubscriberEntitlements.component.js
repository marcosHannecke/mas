"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var subscribers_service_1 = require("../../../services/subscribers.service");
var helpers_service_1 = require("../../../services/helpers.service");
var products_service_1 = require("../../../services/products.service");
var JoinSubscriberEntitlementsComponent = /** @class */ (function () {
    function JoinSubscriberEntitlementsComponent(route, router, subscribersService, productsService, helpers) {
        this.route = route;
        this.router = router;
        this.subscribersService = subscribersService;
        this.productsService = productsService;
        this.helpers = helpers;
        this.overlay = false;
        this.productGroups = [];
        this.PGFound = [];
        this.packagesSelected = [];
        this.submitted = false;
        this.entitlements = false;
        this.view1 = true;
        this.view2 = false;
        this.joined = false;
        this.successMessage = false;
        this.cardTokens = {
            token: '',
            tokenId: ''
        };
        this._array = Array;
        this.showCards = null;
        this.OPPV = [];
        this.oppvPrices = {};
        this.allOPPVvalidated = false;
        this.count = 0;
        this.previewDialog = false;
        this.discount = [];
        this.invalidDiscount = [];
        this.validDiscount = false;
        this.pleaseWait = true;
    }
    JoinSubscriberEntitlementsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.error = {
            subscriber: false,
            vip: false
        };
        this.router.queryParams
            .subscribe(function (res) {
            _this.hasPaymentCard = res['hasPaymentCard'];
            console.log(_this.hasPaymentCard);
        });
        // GET CARD TOKENS
        this.router.queryParams
            .subscribe(function (params) {
            _this.cardTokens.token = params['token'];
            _this.cardTokens.tokenId = params['tokenId'];
        });
        if ((this.cardTokens.token == undefined || this.cardTokens.tokenId == undefined
            || this.cardTokens.token == '' || this.cardTokens.tokenId == '') && this.hasPaymentCard != 1) {
            this.cardError = {
                message: 'Sorry we could not collect your payment details, ',
            };
        }
        else {
            sessionStorage.setItem('cardTokens', JSON.stringify(this.cardTokens));
        }
        // GET STORED DATA FROM FORM
        this.subscriber = JSON.parse(sessionStorage.getItem('joinSubscriber'));
        if (sessionStorage.getItem('joinSubscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('joinSubscriber'));
            var array = ['cardHolderType=' + this.subscriber.cardHolderType, 'currencyCode=' + this.subscriber.currencyCode];
            this.productsService.getSellables(array)
                .subscribe(function (res) {
                var sellables = [];
                // Remove OPPV from /get-sellables
                for (var i = 0; i < res.length; i++) {
                    if (res[i].pricing.oneOff == null || res[i].pricing.oneOff.indicative == false) {
                        if (!res[i].titleDescription.toLowerCase().includes('retention')) {
                            sellables.push(res[i]);
                        }
                    }
                }
                sellables.sort(_this.helpers.alphabetical);
                if (_this.subscriber.cardSubscriberId == null) {
                    _this.productGroups = sellables;
                    _this.PGFound = sellables;
                }
                else {
                    _this.addOppvGroups(sellables);
                }
                _this.pleaseWait = false;
            }, function (err) {
                _this.error = _this.helpers.getError(err);
            });
        }
        else {
            this.route.navigate(['/joinSubscriber']);
            // this.productsService.getProducts('PG')
            //   .subscribe(res => {
            //     console.log('HEREEEEEEAAAAAAAAAAAAA', res);
            //     this.productGroups = res;
            //     this.PGFound = res;
            //     this.pleaseWait = false;
            //   }, err => {
            //     this.error = this.helpers.getError(err);
            //   })
        }
        console.log('subscriber', this.subscriber);
        console.log(this.cardTokens);
    };
    ;
    // ADD OPPV GROUPS
    JoinSubscriberEntitlementsComponent.prototype.addOppvGroups = function (sellables) {
        var _this = this;
        var array = ['cardHolderType=' + this.subscriber.cardHolderType, 'currencyCode=' +
                this.subscriber.currencyCode, 'productTypesOnly=OPPV_ONLY|IPPV_OPPV'];
        var arr = [];
        this.productsService.getSellablesGroupedByProduct('PG', array)
            .subscribe(function (res) {
            arr = Object.keys(res).map(function (key) { return res[key]; });
            sellables = sellables.concat(arr);
            _this.productGroups = sellables;
            _this.PGFound = sellables;
            _this.pleaseWait = false;
            console.log('OPPV sellables', sellables);
        }, function (err) {
            // this.error = JSON.parse(err["_body"])
            _this.error = _this.helpers.getError(err);
        });
    };
    // RESET ROUTE
    JoinSubscriberEntitlementsComponent.prototype.reset = function () {
        var _this = this;
        var url = window.location.href;
        var array = url.split('/');
        var path = array[array.length - 1];
        this.route.navigateByUrl('blank').then(function () {
            _this.route.navigateByUrl(path);
        });
    };
    // PREVIEW DIALOG
    JoinSubscriberEntitlementsComponent.prototype.addedRemovedPackages = function () {
        this.previewDialog = true;
        this.discount = [];
        this.invalidDiscount = [];
    };
    // JOIN SUBSCRIBER
    JoinSubscriberEntitlementsComponent.prototype.joinSubscriber = function () {
        var _this = this;
        var array = [];
        for (var i = 0; i < this.packagesSelected.length; i++) {
            if (this._array.isArray(this.packagesSelected[i])) {
                array[i] = this.packagesSelected[i][this.packagesSelected[i].length - 1].name;
            }
            else {
                array[i] = this.packagesSelected[i].name;
            }
        }
        // add product groups
        this.subscriber.productGroupNames = array;
        // OPPV prices
        this.subscriber.oppvProductGroupsPrices = this.oppvPrices;
        // add discount
        var discount = {};
        for (var i = 0; i < array.length; i++) {
            if (this.discount[i] !== undefined) {
                if (this.discount[i] > 0) {
                    discount[array[i]] = this.discount[i];
                }
            }
        }
        this.subscriber.productGroupsDiscounts = discount;
        // add card tokens
        this.subscriber.token = this.cardTokens.token;
        this.subscriber.tokenId = this.cardTokens.tokenId;
        // DELETE cardSubscriberId from existing customer/subscriber
        if (sessionStorage.getItem('cardSubscriberId') != null) {
            delete this.subscriber.cardSubscriberId;
        }
        // JOIN subscriber
        this.subscribersService.join(this.subscriber)
            .subscribe(function (res) {
            _this.joined = JSON.parse(res["_body"]);
            console.log('join', res);
            _this.successMessage = true;
            // Success - set vip status
            // this.vip(this.joined.id);
        }, function (err) {
            _this.error.subscriber = _this.helpers.getError(err);
        });
    };
    // VIP
    // vip(id) {
    //   if(this.vipVal == 'true'){
    //     var data = {
    //       "subscriberId": id,
    //       "vip": this.vipVal
    //     };
    //
    //     this.subscribersService.urlSubscribers(set-vip', data)
    //       .subscribe(res => {
    //         console.log(res);
    //       }, err => {
    //         this.error.vip = JSON.parse(err["_body"]);
    //       });
    //   }
    // }
    JoinSubscriberEntitlementsComponent.prototype.filter = function () {
        var found = [];
        var a = false;
        for (var i = 0; i < this.productGroups.length; i++) {
            console.log(this.productGroups[i].titleDescription);
            if (true) {
                if (!this._array.isArray(this.productGroups[i])) {
                    a = this.productGroups[i].titleDescription.toLowerCase().includes(this.filterValue.toLowerCase());
                    if (a === true) {
                        found.push(this.productGroups[i]);
                    }
                }
            }
        }
        this.PGFound = found;
    };
    // SELECT PACKAGES
    JoinSubscriberEntitlementsComponent.prototype.isSelected = function (pack) {
        var p = false;
        var pos;
        for (var i = 0; i < this.packagesSelected.length; i++) {
            if (this.packagesSelected[i].id == pack.id) {
                p = true;
                pos = i;
            }
        }
        return [p, pos];
    };
    // SHOW OPPVs CARDS
    JoinSubscriberEntitlementsComponent.prototype.showOPPVs = function (i) {
        if (this.showCards == i) {
            this.showCards = null;
        }
        else {
            this.showCards = i;
        }
    };
    // SELECT NORMAL SELLABLE PACKAGE
    JoinSubscriberEntitlementsComponent.prototype.selectPackage = function (product, index) {
        this.PGFound.splice(index, 1);
        if (!this.isSelected(product)[0]) {
            this.packagesSelected.push(product);
        }
        else {
            this.packagesSelected.splice(this.isSelected(product)[1], 1);
        }
    };
    // SELECT ONLY OPPV CLICKED
    JoinSubscriberEntitlementsComponent.prototype.selectOPPV = function (product, i, ind, pG) {
        // var oppv = this.getLastValidFrom(pG);
        var oppv = pG[pG.length - 1];
        var productIdValidFromId = oppv.productIds[oppv.productIds.length - 1];
        if (this.PGFound[i].length == 0) {
            this.PGFound.splice(i, 1);
            this.showCards = false;
        }
        if (!this.isSelected(product)[0]) {
            // add productIdValidFromId which will be used to get caInstNum
            product['productIdValidFromId'] = productIdValidFromId;
            console.log(product);
            // add product to added packages
            this.packagesSelected.push(product);
        }
        else {
            this.packagesSelected.splice(this.isSelected(product)[1], 1);
        }
        // Remove selected from sellables
        this.PGFound[i].splice(ind, 1);
    };
    // CHECK IF ALL OPPV EVENTS HAVE BEEN VALIDATED
    JoinSubscriberEntitlementsComponent.prototype.allOPPVvalidatedCheck = function () {
        var validated = true;
        for (var _i = 0, _a = this.packagesSelected; _i < _a.length; _i++) {
            var oppv = _a[_i];
            if (oppv.validated == null && this._array.isArray(oppv)) {
                validated = false;
                break;
            }
            else if (!this._array.isArray(oppv)) {
                if (oppv.pricing.oneOff != null && oppv.validated == null)
                    if (oppv.pricing.oneOff.indicative == true) {
                        validated = false;
                        break;
                    }
            }
        }
        // Valid discount
        this.validDiscount = false;
        for (var i = 0; i < this.invalidDiscount.length; i++) {
            if (this.invalidDiscount[i]) {
                this.validDiscount = true;
            }
        }
        if (this.validDiscount) {
            this.errorDiscount = {
                message2: 'Invalid discount value.'
            };
        }
        else if (validated) {
            this.joinSubscriber();
        }
        else {
            this.allOPPVvalidated = true;
        }
    };
    // get valid to from last valid from
    JoinSubscriberEntitlementsComponent.prototype.getLastValidFrom = function (pG) {
        var last = pG[0];
        for (var _i = 0, pG_1 = pG; _i < pG_1.length; _i++) {
            var date = pG_1[_i];
            if (date.validFrom > last.validFrom) {
                last = date;
            }
        }
        return last;
    };
    // SELECT ALL OPPV from same group
    JoinSubscriberEntitlementsComponent.prototype.selectAllOPPV = function (i) {
        this.packagesSelected.push(this.PGFound[i]);
        this.PGFound.splice(i, 1);
    };
    // REMOVE ENTITLEMENT
    JoinSubscriberEntitlementsComponent.prototype.removePackage = function (pack, i) {
        console.log(this.packagesSelected);
        this.packagesSelected.splice(i, 1);
        if (!this.isSelected(pack)[0] || pack.length > 0) {
            this.PGFound.push(pack);
        }
        else {
            this.PGFound.splice(this.isSelected(pack)[1], 1);
        }
    };
    // VALIDATE OPPV EVENT 1/2
    JoinSubscriberEntitlementsComponent.prototype.validateOPPVevent = function (i) {
        var _this = this;
        var lastOPPV = '';
        // Set productId of single selected
        var productId = this.packagesSelected[i].productIdValidFromId;
        // Set productId of group selected
        if (this._array.isArray(this.packagesSelected[i])) {
            lastOPPV = this.getLastOPPV(i);
            productId = this.getProductIdFromLast(lastOPPV);
        }
        // GET caProductId
        this.productsService.getProductById('P', productId)
            .subscribe(function (res) {
            var resp = JSON.parse(res["_body"]);
            _this.validateOPPVrequest(resp.caProductId, resp.caInstNum, i);
        }, function (err) {
            // this.error = JSON.parse(err["_body"])
            _this.error = _this.helpers.getError(err);
        });
    };
    // VALIDATE OPPV EVENT 2/2
    JoinSubscriberEntitlementsComponent.prototype.validateOPPVrequest = function (caProductId, caInstNum, i) {
        var _this = this;
        var url = 'validate-oppv-event-for-subscriber?';
        url += 'cardSubscriberId=' + this.subscriber.cardSubscriberId;
        url += '&caProductId=' + caProductId;
        url += '&caInstNum=' + caInstNum;
        console.log('url', caProductId, caInstNum);
        this.productsService.urlGet('E', url)
            .subscribe(function (res) {
            var resp = JSON.parse(res["_body"]);
            console.log(resp);
            // update package info
            // this.packagesSelected[i].validated = true;
            // this.packagesSelected[i].price = resp.price;
            // this.packagesSelected[i].pin = resp.pin;
            // this.packagesSelected[i].individualSpendLimit = resp.individualSpendLimit;
            //
            // // OPPV PRICES
            // if(this._array.isArray(this.packagesSelected[i])){
            //   var lastOPPV = this.getLastOPPV(i);
            //   this.oppvPrices[lastOPPV.name] = resp.price;
            //
            //   console.log('oppvPrices', this.oppvPrices)
            // }else if(!this._array.isArray(this.packagesSelected[i])){
            //   this.oppvPrices[this.packagesSelected[i].name] = resp.price;
            //   console.log('oppvPrices', this.oppvPrices)
            // }
            _this.subscriberPin = resp.pin;
            // Check if client can purchase
            _this.validOPPV(resp, i);
        }, function (err) {
            // this.error = JSON.parse(err["_body"])
            _this.error = _this.helpers.getError(err);
        });
    };
    JoinSubscriberEntitlementsComponent.prototype.validOPPV = function (res, i) {
        this.OPPVposition = i;
        this.OPPVresponse = res;
        if (!res.validPpvEvent) {
            this.errorOPPVvalid = {
                'error': 'This OPPV event is invalid.',
                'messages': res.messages
            };
        }
        else if (res.blackedOut) {
            this.errorOPPVvalid = {
                'error': 'The customer cannot purchase this OPPV event',
                'messages': res.messages
            };
        }
        else if (res.pinRequired) {
            if (this.count >= 3) {
                this.OPPVvalidatePin = null;
                this.err = {
                    message: 'Number of allowed PIN attempts exceeded. Please try again later.'
                };
            }
            else {
                this.OPPVvalidatePin = {
                    'message': 'Customer must provide their Sky PIN to purchase this OPPV event',
                    'messages': res.messages
                };
            }
        }
        else if (!res.pinRequired) {
            this.validate();
        }
    };
    JoinSubscriberEntitlementsComponent.prototype.PINcheck = function () {
        if (this.count >= 3) {
            this.OPPVvalidatePin = null;
            this.err = {
                message: 'Number of allowed PIN attempts exceeded. Please try again later.'
            };
        }
        else if (this.pin === this.subscriberPin) {
            this.OPPVvalidatePin = null;
            this.count = 0;
            this.validate();
        }
        else {
            if (this.count < 3) {
                this.err = {
                    message: 'PIN incorrect'
                };
                this.count++;
            }
        }
    };
    // OPPV VALIDATE 3/3
    JoinSubscriberEntitlementsComponent.prototype.validate = function () {
        var i = this.OPPVposition;
        var res = this.OPPVresponse;
        // update package info
        this.packagesSelected[i].validated = true;
        this.packagesSelected[i].price = res.price;
        this.packagesSelected[i].pin = res.pin;
        this.packagesSelected[i].individualSpendLimit = res.individualSpendLimit;
        // OPPV PRICES
        if (this._array.isArray(this.packagesSelected[i])) {
            var lastOPPV = this.getLastOPPV(i);
            this.oppvPrices[lastOPPV.name] = res.price;
            console.log('oppvPrices', this.oppvPrices);
        }
        else if (!this._array.isArray(this.packagesSelected[i])) {
            this.oppvPrices[this.packagesSelected[i].name] = res.price;
            console.log('oppvPrices', this.oppvPrices);
        }
    };
    JoinSubscriberEntitlementsComponent.prototype.getLastOPPV = function (i) {
        return this.packagesSelected[i][this.packagesSelected[i].length - 1];
    };
    JoinSubscriberEntitlementsComponent.prototype.getProductIdFromLast = function (OPPV) {
        return OPPV.productIds[0];
    };
    ;
    // DISCOUNT VALIDATION
    JoinSubscriberEntitlementsComponent.prototype.discountValidation = function (discount, i) {
        if (discount != null) {
            this.invalidDiscount[i] = false;
            var regex = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
            if (!regex.test(discount) && discount) {
                this.invalidDiscount[i] = true;
            }
        }
    };
    JoinSubscriberEntitlementsComponent = __decorate([
        core_1.Component({
            selector: 'joinSubscriberEntitlements',
            templateUrl: 'joinSubscriberEntitlements.component.html',
            styles: ['.product-desc{height: 130px; overflow:hidden} .ibox-content .row{min-height: 100px}' +
                    '.activeEntitlements .product-box:hover {box-shadow: none;}' +
                    '.product-box{border: none} .product-btn button{ background: white} .product-btn button:hover{ background: #e23237; color: white !important}' +
                    '.productDescription{-webkit-line-clamp: 2;  overflow: hidden; display: -webkit-box;  -webkit-box-orient: vertical;} .productDescription:hover{-webkit-line-clamp: 6;}' +
                    '.showOPPVs{position: relative !important; top: -24px !important; left: 0 !important; transition: all 0.5s;} .showOPPVs .ibox-content {background: rgb(255, 250, 233) !important}' +
                    '.angle{position: absolute; right: 5px; bottom: 10px; width: 20px; height: 20px; font-size: 18px; color: #0570e2;}'],
            providers: [subscribers_service_1.SubscribersService, products_service_1.ProductsService, helpers_service_1.HelpersService]
        })
    ], JoinSubscriberEntitlementsComponent);
    return JoinSubscriberEntitlementsComponent;
}());
exports.JoinSubscriberEntitlementsComponent = JoinSubscriberEntitlementsComponent;
