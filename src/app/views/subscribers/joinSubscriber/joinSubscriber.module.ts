import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {JoinSubscriberComponent} from "./joinSubscriber.component";
import {JoinSubscriberEntitlementsComponent} from "./joinSubscriberEntitlements.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule} from '@angular/router';


@NgModule({
  declarations: [JoinSubscriberComponent, JoinSubscriberEntitlementsComponent],
  imports     : [BrowserModule, FormsModule, ReactiveFormsModule, RouterModule],
})

export class JoinSubscriberViewModule {}
