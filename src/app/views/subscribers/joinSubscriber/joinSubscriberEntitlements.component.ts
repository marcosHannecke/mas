import { Component, OnInit, Input } from '@angular/core';
import {HelpersService} from '../../../services/helpers.service';
import {Router, ActivatedRoute, NavigationExtras} from '@angular/router';
import { RestService } from '../../../services/rest.service';
import { HttpClient } from '@angular/common/http';
import {Globals} from '../../../app.global';

@Component({
  selector: 'joinSubscriberEntitlements',
  templateUrl: 'joinSubscriberEntitlements.component.html',
  styles: ['.product-desc{height: 130px; overflow:hidden} .ibox-content .row{min-height: 100px}' +
  '.activeEntitlements .product-box:hover {box-shadow: none;}' +
  '.product-box{border: none} .product-btn button{ background: white} .product-btn button:hover{ background: #e23237; color: white !important}' +
  '.productDescription{-webkit-line-clamp: 2;  overflow: hidden; display: -webkit-box;  -webkit-box-orient: vertical;} .productDescription:hover{-webkit-line-clamp: 6;}' +
  '.showOPPVs{position: relative !important; top: -24px !important; left: 0 !important; transition: all 0.5s;} .showOPPVs .ibox-content {background: rgb(255, 250, 233) !important}' +
  '.angle{position: absolute; right: 5px; bottom: 10px; width: 20px; height: 20px; font-size: 18px; color: #0570e2;}'],
  providers: [RestService, HelpersService]
})
export class JoinSubscriberEntitlementsComponent implements OnInit {
  subscriber: any;
  error: any;
  errorDiscount: any;
  err: any;
  cardError: any;
  overlay: boolean = false;
  productGroups: any = [];
  PGFound: any = [];
  packagesSelected: any[] = [];
  submitted: boolean = false;
  entitlements: boolean = false;
  view1: boolean = true;
  view2: boolean = false;
  filterValue: string;
  joined: any = false;
  successMessage: boolean = false;
  hasPaymentCard: any;
  vipVal: any;
  cardTokens = {
    token: '',
    tokenId: '',
    expire: ''
  };

  private _array = Array;
  showCards: boolean = null;
  OPPV: any = [];
  OPPVsOnly: any;
  oppvPrices: any = {};
  allOPPVvalidated: boolean = false;
  OPPVvalidatePin: any;
  subscriberPin: any;
  pin: any;
  errorOPPVvalid: any;
  OPPVposition: any;
  OPPVresponse: any;
  count = 0;

  previewDialog: boolean = false;
  discount: any[] = [];
  invalidDiscount: any[] = [];
  validDiscount = false;

  pleaseWait = true;
  date = Math.round(new Date().getTime() / 1000) + 24 * 60 * 60;



  constructor(private route: Router, private router: ActivatedRoute, private globals: Globals,
              private helpers: HelpersService, private rest: RestService, private http: HttpClient) {}

  ngOnInit() {
    this.error = {
      subscriber : false,
      vip: false
    };

    this.router.queryParams
      .subscribe(res => {
        this.hasPaymentCard = res['hasPaymentCard'];
        console.log(this.hasPaymentCard);
      });

    // GET CARD TOKENS
    // this.router.queryParams
    //   .subscribe(params => {
    //     this.cardTokens.token = params['token'];
    //     this.cardTokens.tokenId = params['tokenId'];
    //     this.cardTokens.expire = params['exp'];
    //   });

    if (sessionStorage.getItem('cardTokens') != null) {
      const cardTokens = JSON.parse(sessionStorage.getItem('cardTokens'));
      this.cardTokens.token = cardTokens['token'];
      this.cardTokens.tokenId = cardTokens['tokenId'];
      this.cardTokens.expire = cardTokens['expire'];
    }

    if ((this.cardTokens.token == undefined || this.cardTokens.tokenId == undefined
        || this.cardTokens.token == '' || this.cardTokens.tokenId == '') && this.hasPaymentCard != 1) {
      this.cardError = {
        message : 'Sorry we could not collect your payment details, ',
        // message2: 'Please Check details were correct.'
      }
    }else {
      sessionStorage.setItem('cardTokens', JSON.stringify(this.cardTokens));
    }

    // GET STORED DATA FROM FORM
    this.subscriber = JSON.parse(sessionStorage.getItem('joinSubscriber'));
    if (sessionStorage.getItem('joinSubscriber') != null) {
      /**
       * Get Sellables
       */
      this.subscriber = JSON.parse(sessionStorage.getItem('joinSubscriber'));
      let resp;
      this.rest.get('/product-groups/search-sellable?q=' + 'cardHolderType=' + this.subscriber.cardHolderType
        + ',currencyCode=' + this.subscriber.currencyCode + ',maxValidFrom=' + this.date)
        .subscribe(res => {
          resp = res;
          console.log(resp, resp.length);
          const sellables = [];

          // Remove OPPV from /get-sellables
          for (let i = 0; i < resp.length; i++) {
            if (resp[i].pricing.oneOff == null || resp[i].pricing.oneOff.indicative == false) {
              if (!resp[i].titleDescription.toLowerCase().includes('retention')) {
                sellables.push(resp[i]);
              }
            }
          }


          // Sort Sellables
          function displayingOrder(a, b) {
            if (a.displayingOrder < b.displayingOrder) {return -1; }
            if (a.displayingOrder > b.displayingOrder) { return 1; }
            return 0;
          }

          sellables.sort(displayingOrder);

          // sellables.sort(this.helpers.alphabetical);

          // If new join or join from existing customer
          if (this.subscriber.cardSubscriberId == null) {
            this.productGroups = sellables;
            this.PGFound = sellables;
          }else {
            this.productGroups = sellables;
            this.PGFound = sellables;
            /** Add OPPVs to sellables **/
            // this.addOppvGroups(sellables);
          }
          this.pleaseWait = false;
        }, err => {
          this.error = this.helpers.error(err.error.message);
        });
    }else {
      this.route.navigate(['/joinSubscriber']);
    }
  };

  // ADD OPPV GROUPS
  addOppvGroups(sellables) {
    // const array = ['cardHolderType=' +  this.subscriber.cardHolderType, 'currencyCode=' +
    // this.subscriber.currencyCode, 'productTypesOnly=OPPV_ONLY|IPPV_OPPV' ];
    // let arr =  [];
    //
    // this.rest.get('/product-groups/search-sellable/grouped-by-product?q=' + encodeURIComponent(array) +
    //   ',maxValidFrom=' + this.date)
    //   .subscribe(res => {
    //     arr = Object.keys(res).map(key => res[key]);
    //     sellables = sellables.concat(arr);
    //
    //     this.productGroups = sellables;
    //     this.PGFound = sellables;
    //     this.pleaseWait = false;
    //     console.log('OPPV sellables', sellables);
    //   }, err => {
    //     // this.error = JSON.parse(err["_body"])
    //     this.error = this.helpers.error(err.error.message);
    //
    //   });

    // this.productsService.getSellablesGroupedByProduct('PG', array)
    // .subscribe(res => {
    //   arr = Object.keys(res).map(key => res[key]);
    //   sellables = sellables.concat(arr);
    //
    //   this.productGroups = sellables;
    //   this.PGFound = sellables;
    //   this.pleaseWait = false;
    //   console.log('OPPV sellables', sellables);
    // }, err => {
    //   // this.error = JSON.parse(err["_body"])
    //   this.error = this.helpers.getError(err);
    //
    // })
}


  // RESET ROUTE
  reset() {
    var url = window.location.href;
    var array = url.split('/');
    var path = array[array.length - 1];

    this.route.navigateByUrl('blank').then(() => {
      this.route.navigateByUrl(path);
    })
  }

  // PREVIEW DIALOG
  addedRemovedPackages() {
    this.previewDialog = true;
    this.discount = [];
    this.invalidDiscount = [];
  }

  // JOIN SUBSCRIBER
  joinSubscriber() {
    var array = [];
    for(var i = 0; i < this.packagesSelected.length; i++ ){
      if(this._array.isArray(this.packagesSelected[i])){
        array[i] = this.packagesSelected[i][this.packagesSelected[i].length -1].name;
      }else{
        array[i] = this.packagesSelected[i].name;
      }
    }

    // add product groups
    this.subscriber.productGroupNames = array;

    // OPPV prices
    this.subscriber.oppvProductGroupsPrices = this.oppvPrices;

    // add discount
    var discount  = {};
    for(var i = 0; i < array.length; i++ ){

      if(this.discount[i] !== undefined){
        if(this.discount[i] > 0){
          discount[array[i]] = this.discount[i]
        }
      }
    }
    this.subscriber.productGroupsDiscounts = discount;

    // add card tokens
    this.subscriber.token = this.cardTokens.token;
    this.subscriber.tokenId = this.cardTokens.tokenId;
    this.subscriber.expiryDate = this.helpers.undefinedToEmptyString(this.cardTokens.expire);

    // Initials to uppercase
    this.subscriber.initials = this.subscriber.initials.toUpperCase();
    // Capitalize surname
    this.subscriber.surname = this.helpers.toTitleCase(this.subscriber.surname);

    // DELETE cardSubscriberId from existing customer/subscriber
    if (sessionStorage.getItem('cardSubscriberId') != null) {
      delete this.subscriber.cardSubscriberId;
    }

    // JOIN subscriber
    this.http.put(this.globals.API_ENDPOINT + '/subscribers/join', this.subscriber, {responseType: 'text'})
      .subscribe(res => {
        this.joined = JSON.parse(res);
        console.log('join', res);
        this.successMessage = true;
        // Success - set vip status
        // this.vip(this.joined.id);
      }, err => {
        err = JSON.parse(err.error);
        console.log(err);
        if (err.fieldErrors) {
          this.error.subscriber = err.fieldErrors[0];
        }else {
          this.error.subscriber = this.helpers.error(err.message);
        }
      });
  }

  // VIP
  // vip(id) {
  //   if(this.vipVal == 'true'){
  //     var data = {
  //       "subscriberId": id,
  //       "vip": this.vipVal
  //     };
  //
  //     this.subscribersService.urlSubscribers(set-vip', data)
  //       .subscribe(res => {
  //         console.log(res);
  //       }, err => {
  //         this.error.vip = JSON.parse(err["_body"]);
  //       });
  //   }
  // }


  filter() {
    var found = [];
    var a = false;

    for (var i = 0; i < this.productGroups.length; i++) {
      console.log(this.productGroups[i].titleDescription)
      if(true){
        if(!this._array.isArray(this.productGroups[i])) {
          a = this.productGroups[i].titleDescription.toLowerCase().includes(this.filterValue.toLowerCase());
          if (a === true) {
            found.push(this.productGroups[i]);
          }
        }
      }
    }
    this.PGFound = found;
  }

  // SELECT PACKAGES
  isSelected(pack) {
    var p = false;
    var pos;
    for (var i = 0; i < this.packagesSelected.length; i++) {
      if (this.packagesSelected[i].id == pack.id) {
        p = true;
        pos = i;
      }
    }
    return [p, pos];
  }

  // SHOW OPPVs CARDS
  showOPPVs(i){
    if(this.showCards == i){
      this.showCards = null;
    }else{
      this.showCards = i;
    }
  }

  // SELECT NORMAL SELLABLE PACKAGE
  selectPackage(product, index) {
    this.PGFound.splice(index , 1);
    if (!this.isSelected(product)[0]) {
      this.packagesSelected.push(product);
    } else {
      this.packagesSelected.splice(this.isSelected(product)[1], 1);
    }
  }

  // SELECT ONLY OPPV CLICKED
  selectOPPV(product, i,  ind, pG) {
    // var oppv = this.getLastValidFrom(pG);
    var oppv = pG[pG.length -1];

    var productIdValidFromId = oppv.productIds[oppv.productIds.length-1];

    if(this.PGFound[i].length == 0){
      this.PGFound.splice(i , 1);
      this.showCards = false;
    }

    if (!this.isSelected(product)[0]) {
      // add productIdValidFromId which will be used to get caInstNum
      product['productIdValidFromId'] =  productIdValidFromId;

      console.log(product);
      // add product to added packages
      this.packagesSelected.push(product);
    } else {
      this.packagesSelected.splice(this.isSelected(product)[1], 1);
    }

    // Remove selected from sellables
    this.PGFound[i].splice(ind , 1);
  }

  // CHECK IF ALL OPPV EVENTS HAVE BEEN VALIDATED
  allOPPVvalidatedCheck(){
    let validated = true;
    for(let oppv of this.packagesSelected){

      if(oppv.validated == null && this._array.isArray(oppv)){
        validated = false;
        break;
      }else if(!this._array.isArray(oppv)){
        if(oppv.pricing.oneOff !=null && oppv.validated == null)
          if(oppv.pricing.oneOff.indicative == true){
            validated = false;
            break;
          }
      }
    }

    // Valid discount
    this.validDiscount = false;
    for(var i = 0; i < this.invalidDiscount.length; i++){
      if(this.invalidDiscount[i]){
        this.validDiscount = true;
      }
    }

    if (this.validDiscount) {
      this.errorDiscount = {
        message2: 'Invalid discount value.'
      }
    } else if(validated){
      this.joinSubscriber();
    }else{
      this.allOPPVvalidated = true;
    }
  }

  // get valid to from last valid from
  getLastValidFrom(pG){
    var last = pG[0];
    for(let date of pG){
      if(date.validFrom > last.validFrom){
        last = date;
      }
    }
    return last;
  }

  // SELECT ALL OPPV from same group
  selectAllOPPV(i){
    this.packagesSelected.push(this.PGFound[i]);
    this.PGFound.splice(i , 1);
  }

  // REMOVE ENTITLEMENT
  removePackage(pack, i){
    console.log(this.packagesSelected);

    this.packagesSelected.splice(i , 1);
    if (!this.isSelected(pack)[0] || pack.length > 0) {
      this.PGFound.push(pack);
    } else {
      this.PGFound.splice(this.isSelected(pack)[1], 1);
    }
  }

  // VALIDATE OPPV EVENT 1/2
  validateOPPVevent(i) {
    let lastOPPV = '';
    // Set productId of single selected
    let productId = this.packagesSelected[i].productIdValidFromId;
    // Set productId of group selected
    if (this._array.isArray(this.packagesSelected[i])){
      lastOPPV = this.getLastOPPV(i);
      productId = this.getProductIdFromLast(lastOPPV);
    }

    // GET caProductId
    this.rest.get('/products/' + productId)
      .subscribe(res => {
        const resp = res;
        this.validateOPPVrequest(resp['caProductId'], resp['caInstNum'], i);
      }, err => {
        // this.error = JSON.parse(err["_body"])
        this.error = this.helpers.error(err.error.message);

      });

    // this.productsService.getProductById('P', productId )
    //   .subscribe(res => {
    //     const resp = res;
    //     this.validateOPPVrequest(resp['caProductId'], resp['caInstNum'], i);
    //   }, err => {
    //     // this.error = JSON.parse(err["_body"])
    //     this.error = this.helpers.getError(err);
    //
    //   })
  }
  // VALIDATE OPPV EVENT 2/2
  validateOPPVrequest(caProductId, caInstNum, i) {
    let url = 'validate-oppv-event-for-subscriber?';
    url +=  'cardSubscriberId=' + this.subscriber.cardSubscriberId;
    url += '&caProductId=' + caProductId;
    url += '&caInstNum=' + caInstNum;

    console.log('url', caProductId, caInstNum );
    let resp;
    this.rest.get('/entitlements/' + url)
      .subscribe(res => {
        resp = res;
        console.log(resp);
        // update package info
        // this.packagesSelected[i].validated = true;
        // this.packagesSelected[i].price = resp.price;
        // this.packagesSelected[i].pin = resp.pin;
        // this.packagesSelected[i].individualSpendLimit = resp.individualSpendLimit;
        //
        // // OPPV PRICES
        // if(this._array.isArray(this.packagesSelected[i])){
        //   var lastOPPV = this.getLastOPPV(i);
        //   this.oppvPrices[lastOPPV.name] = resp.price;
        //
        //   console.log('oppvPrices', this.oppvPrices)
        // }else if(!this._array.isArray(this.packagesSelected[i])){
        //   this.oppvPrices[this.packagesSelected[i].name] = resp.price;
        //   console.log('oppvPrices', this.oppvPrices)
        // }

        this.subscriberPin = resp.pin;
        // Check if client can purchase
        this.validOPPV(resp, i);
      }, err => {
        // this.error = JSON.parse(err["_body"])
        this.error = this.helpers.error(err.error.message);
      })

    // this.productsService.urlGet('E', url)
    //   .subscribe(res=>{
    //     var resp = JSON.parse(res["_body"]);
    //     console.log(resp);
    //     // update package info
    //     // this.packagesSelected[i].validated = true;
    //     // this.packagesSelected[i].price = resp.price;
    //     // this.packagesSelected[i].pin = resp.pin;
    //     // this.packagesSelected[i].individualSpendLimit = resp.individualSpendLimit;
    //     //
    //     // // OPPV PRICES
    //     // if(this._array.isArray(this.packagesSelected[i])){
    //     //   var lastOPPV = this.getLastOPPV(i);
    //     //   this.oppvPrices[lastOPPV.name] = resp.price;
    //     //
    //     //   console.log('oppvPrices', this.oppvPrices)
    //     // }else if(!this._array.isArray(this.packagesSelected[i])){
    //     //   this.oppvPrices[this.packagesSelected[i].name] = resp.price;
    //     //   console.log('oppvPrices', this.oppvPrices)
    //     // }
    //
    //     this.subscriberPin = resp.pin;
    //     // Check if client can purchase
    //     this.validOPPV(resp, i);
    //   }, err =>{
    //     // this.error = JSON.parse(err["_body"])
    //     this.error = this.helpers.getError(err);
    //   })
  }


  validOPPV(res, i) {
    this.OPPVposition = i;
    this.OPPVresponse = res;

    if(!res.validPpvEvent){
      this.errorOPPVvalid = {
        'error' : 'This OPPV event is invalid.',
        'messages' : res.messages
      };
    }else if(res.blackedOut){
      this.errorOPPVvalid = {
        'error' : 'The customer cannot purchase this OPPV event',
        'messages' : res.messages
      };
    }else if(res.pinRequired){
      if(this.count >= 3){
        this.OPPVvalidatePin = null;
        this.err = {
          message: 'Number of allowed PIN attempts exceeded. Please try again later.'
        }
      }else{
        this.OPPVvalidatePin = {
          'message' : 'Customer must provide their Sky PIN to purchase this OPPV event',
          'messages' : res.messages
        };
      }
    }else if(!res.pinRequired){
      this.validate();
    }
  }

  PINcheck(){
    if(this.count >= 3){
      this.OPPVvalidatePin = null;
      this.err = {
        message: 'Number of allowed PIN attempts exceeded. Please try again later.'
      }
    }else if(this.pin === this.subscriberPin) {
      this.OPPVvalidatePin = null;
      this.count = 0;
      this.validate();
    }else{
      if(this.count < 3){
        this.err = {
          message: 'PIN incorrect'
        }
        this.count++;
      }
    }
  }

  // OPPV VALIDATE 3/3
  validate(){
    let i = this.OPPVposition;
    let res = this.OPPVresponse;
    // update package info
    this.packagesSelected[i].validated = true;
    this.packagesSelected[i].price = res.price;
    this.packagesSelected[i].pin = res.pin;
    this.packagesSelected[i].individualSpendLimit = res.individualSpendLimit;

    // OPPV PRICES
    if(this._array.isArray(this.packagesSelected[i])){
      var lastOPPV = this.getLastOPPV(i);
      this.oppvPrices[lastOPPV.name] = res.price;

      console.log('oppvPrices', this.oppvPrices)
    }else if(!this._array.isArray(this.packagesSelected[i])){
      this.oppvPrices[this.packagesSelected[i].name] = res.price;
      console.log('oppvPrices', this.oppvPrices)
    }
  }

  getLastOPPV(i) {
    return this.packagesSelected[i][this.packagesSelected[i].length-1]
  }
  getProductIdFromLast(OPPV){
    return OPPV.productIds[0];
  };


  // DISCOUNT VALIDATION
  discountValidation(discount, i) {
    if(discount != null){
      this.invalidDiscount[i] = false;
      var regex = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
      if (!regex.test(discount) && discount){
        this.invalidDiscount[i] = true;
      }
    }
  }

}
