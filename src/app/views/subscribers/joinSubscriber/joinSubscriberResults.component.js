"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var subscribers_service_1 = require('../../../services/subscribers.service');
var products_service_1 = require('../../../services/products.service');
var JoinSubscriberResultsComponent = (function () {
    function JoinSubscriberResultsComponent(router, subscribersService, productsService) {
        this.router = router;
        this.subscribersService = subscribersService;
        this.productsService = productsService;
        this.searchParams = {};
    }
    JoinSubscriberResultsComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Get params for search
        this.router
            .queryParams
            .subscribe(function (params) {
            _this.searchParams.postcode = params['pc'];
            _this.searchParams.viewingCardNumber = params['vcn'];
            _this.searchParams.addressLine1 = params['ad1'];
            _this.searchParams.addressLine2 = params['ad2'];
            _this.searchParams.surname = params['s'];
        });
        // SEARCH LOCAL ON INIT
        this.subscribersService.searchSubscriberLocal(this.searchParams)
            .subscribe(function (res) {
            if (res.length == 0) {
            }
            else if (res.length == 1) {
                console.log(_this.results);
            }
            else if (res.length > 1) {
                _this.results = res;
            }
        }, function (error) {
            _this.error = JSON.parse(error["_body"]);
        });
    };
    ;
    // SEARCH REMOTE
    JoinSubscriberResultsComponent.prototype.searchRemote = function () {
        var _this = this;
        this.subscribersService.searchSubscriberRemote(this.searchParams)
            .subscribe(function (res) {
            console.log(res);
            if (res.length == 0) {
                _this.error = { message: 'No match' };
            }
            else if (res.length == 1) {
                _this.remoteResults = res;
                sessionStorage.setItem('subscriber', JSON.stringify(res[0]));
            }
            else { }
        }, function (error) {
            _this.error = JSON.parse(error["_body"]);
        });
    };
    JoinSubscriberResultsComponent = __decorate([
        core_1.Component({
            selector: 'joinSubscriberResults',
            templateUrl: 'joinSubscriberResults.component.html',
            providers: [subscribers_service_1.SubscribersService, products_service_1.ProductsService]
        })
    ], JoinSubscriberResultsComponent);
    return JoinSubscriberResultsComponent;
}());
exports.JoinSubscriberResultsComponent = JoinSubscriberResultsComponent;
