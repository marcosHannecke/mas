import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {ValidatorsService} from '../../../services/validators.service';
import {HelpersService} from '../../../services/helpers.service';
import {Router, ActivatedRoute, NavigationExtras} from '@angular/router';
import {JoinSubscriber} from '../../../JoinSubscriber';
import {RestService} from '../../../services/rest.service';
import {PostcodeService} from '../../../services/postcode.service';
import {Globals} from '../../../app.global';


declare var $: any;


@Component({
  selector: 'joinSubscriber',
  templateUrl: 'joinSubscriber.component.html',
  styles: ['.findAddress{position: absolute; background: white; z-index: 100; } ' +
  '.findAddress{border-bottom: 1px solid #ececec; border-left: 1px solid #ececec; border-right: 1px solid #ececec}' +
  '.findAddress ul{padding: 0; margin: 0; max-height: 400px; overflow-y: scroll; min-width: 450px} ' +
  '.findAddress li{padding: 5px 15px;} .findAddress li:hover{background: #f9f9f9}' +
  '.details .fa{color: white;padding: 4px 6px; border-radius: 50%;}' +
  '.details .fa-map-marker{background: #f7a230; padding: 4px 7px;}' +
  '.details .fa-phone{background: #1c7fe9 }'],
  providers: [ ValidatorsService, HelpersService, RestService, PostcodeService]
})
export class JoinSubscriberComponent implements OnInit {
  entitlements: any;
  form: FormGroup;
  error: any;
  overlay: boolean = false;
  joinSubscriber: JoinSubscriber = <any>{};
  submitted: boolean = false;
  match: boolean = false;
  subscriber ;
  addressManually : boolean = false;
  addressList : any;
  hasPaymentCard : boolean = false;
  noEmail: boolean = false;
  role: string;
  VCNcheck = false;
  disableAddress: any = false;
  oneResult = false;

  payMethDialog = false;
  payMeth: any;
  invalidPostcode = false;

  emailVerificationResponse: any;
  didYouMean: any;

  remoteMatch: any;
  remoteMatchDialog: any;
  url: any;

  editVCN = false;
  hideFormFields: any;
  disableSurname = true;

  constructor(private helpers: HelpersService, private validators: ValidatorsService, private router: Router,
              private fb: FormBuilder, private rest: RestService, private postcodeService: PostcodeService,
              private globals: Globals) {}

  ngOnInit() {
    this.url = this.globals.URL;

    // GET USER ROLE
    if (sessionStorage.getItem('role') != null) {
      this.role = sessionStorage.getItem('role');
      console.log(this.role);
    }

    this.helpers.scrollTop();

    /*
     * -- CustomerId indicates that user is coming from customer page --
     * CHECK PAYMENT IF & AND ENABLE EDIT VCN
     * Hide marketing preferences and email
     */
    if (sessionStorage.getItem('customerId')) {
      // Hide form fields
      this.hideFormFields = true;
      this.noEmail = true;
      // VCN editable
      /** this.editVCN = true; **/
      // check payment
      this.joinSubscriber.customerId = sessionStorage.getItem('customerId');
      console.log('cID', this.joinSubscriber.customerId);

      this.rest.get('/payments/has-payment-card?customerId=' + this.joinSubscriber.customerId)
        .subscribe(res => {
          this.hasPaymentCard = res['hasPaymentCard'];
          console.log('PC', this.hasPaymentCard)
        }, err => {
          console.log(err);
          if (err.error) {
            this.error = this.helpers.error(err.error.message);
          } else {
            this.error = {
              message: err.message
            }
          }
        })
    }

    if (sessionStorage.getItem('joinSubscriber') != null) {   // saved on one remote match result
      this.subscriber = JSON.parse(sessionStorage.getItem('joinSubscriber'));
      // FORM WITH VALUES STORED
      this.form = this.fb.group({
        'cardHolderType': [this.subscriber.cardHolderType, Validators.required],
        'viewingCardNumber': [this.subscriber.viewingCardNumber,
          [Validators.required, Validators.maxLength(9), this.validators.onlyNumbers]],
        'title': [this.subscriber.title, Validators.required],
        'initials': [this.subscriber.initials.toUpperCase(), [Validators.required, Validators.maxLength(3), this.validators.letterSpaces]],
        'surname': [this.subscriber.surname,
          [Validators.required, Validators.minLength(2), Validators.maxLength(35), this.validators.letterSpaces]],
        'addressLine1': [ this.subscriber.addressLine1,
          [Validators.required, Validators.minLength(2), Validators.maxLength(35), this.validators.specialChar]],
        'addressLine2': [this.subscriber.addressLine2,
          [Validators.required, Validators.minLength(2), Validators.maxLength(35), this.validators.specialChar]],
        'addressLine3': [this.subscriber.addressLine3, Validators.minLength(2)],
        'addressLine4': [this.subscriber.addressLine4, Validators.minLength(2)],
        'addressLine5': [this.subscriber.addressLine5, Validators.minLength(2)],
        'postcode': [this.subscriber.postcode.toUpperCase(), [Validators.required, Validators.maxLength(9)]],
        'countryCode': [this.subscriber.countryCode],
        'phones': this.fb.group({
          'homeTelNr': ['', [Validators.maxLength(15), this.validators.onlyNumbers]],
          'workTelNr': ['', [Validators.maxLength(15), this.validators.onlyNumbers]],
          'mobTelNr': ['', [Validators.maxLength(15), this.validators.onlyNumbers]],
        }, {validator: this.validators.phones}),
        'cliTelNr': [''],
        'currencyCode': [this.subscriber.currencyCode],
        'vip': ['false', Validators.required],
        'email': ['', [ Validators.maxLength(128)]],
        'marketingPreferences': this.fb.group({ // <-- the child FormGroup
          'FP_POST': false,
          'FP_EMAIL': false,
          'FP_PHONE': false,
          'FP_SMS': false,
          'TP_POST': false,
          'TP_EMAIL': false,
          'TP_PHONE': false,
          'TP_SMS': false
        })
      });
    }else {
      // Check if coming from customer or search (search remote multiple match)
      if (sessionStorage.getItem('subscriber')) {
        this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
        // Check for null and replace with empty string
        this.subscriber = this.helpers.nullCheck(this.subscriber);
      }else {
        this.subscriber = this.helpers.emptySubscriber();
      }

      // GET SEARCH FORM IF STORED
      if (sessionStorage.getItem('searchForm')) {
        const searchForm = JSON.parse(sessionStorage.getItem('searchForm'));
        this.subscriber.viewingCardNumber = searchForm.viewingCardNumber;
        this.subscriber.surname = searchForm.surname;
        this.subscriber.addressLine1 = searchForm.addressLine1;
        this.subscriber.addressLine2 = searchForm.addressLine2;
        this.subscriber.addressLine3 = searchForm.addressLine3;
        this.subscriber.addressLine4 = searchForm.addressLine4;
        this.subscriber.addressLine5 = searchForm.addressLine5;
        this.subscriber.postcode = searchForm.postcode;
      }

      // enable edit subscriber
      if (this.subscriber.surname.length === 0) {
        this.disableSurname = false;
      }

      // FORM WITH VIEWING CARD NUMBER
      this.form = this.fb.group({
        'cardHolderType': ['DTH', Validators.required],
        'viewingCardNumber': [this.subscriber.viewingCardNumber,
          [Validators.required, Validators.maxLength(9),  this.validators.onlyNumbers]],
        'title': [this.subscriber.title, Validators.required],
        'initials': [this.subscriber.initials.toUpperCase(), [Validators.required, Validators.maxLength(3), this.validators.letterSpaces]],
        'surname': [this.subscriber.surname,
          [Validators.required, Validators.minLength(2), Validators.maxLength(35), this.validators.letterSpaces]],
        'addressLine1': [this.subscriber.addressLine1,
          [Validators.required, Validators.minLength(2), Validators.maxLength(35), this.validators.specialChar]],
        'addressLine2': [this.subscriber.addressLine2,
          [Validators.required, Validators.minLength(2), Validators.maxLength(35), this.validators.specialChar]],
        'addressLine3': [this.subscriber.addressLine3, Validators.minLength(2)],
        'addressLine4': [this.subscriber.addressLine4, Validators.minLength(2)],
        'addressLine5': [this.subscriber.addressLine5, Validators.minLength(2)],
        'postcode': [this.subscriber.postcode.toUpperCase(), [Validators.required, Validators.minLength(2), Validators.maxLength(9)]],
        'countryCode': [this.subscriber.countryCode],
        'phones': this.fb.group({
          'homeTelNr': ['', [Validators.maxLength(15), this.validators.onlyNumbers]],
          'workTelNr': ['', [Validators.maxLength(15), this.validators.onlyNumbers]],
          'mobTelNr': ['', [Validators.maxLength(15), this.validators.onlyNumbers]],
        }, {validator: this.validators.phones}),
        'cliTelNr': [''],
        'currencyCode': [this.subscriber.currencyCode],
        'vip': ['false', Validators.required],
        'email': ['', [Validators.maxLength(128)]],
        'marketingPreferences': this.fb.group({
          'FP_POST': false,
          'FP_EMAIL': false,
          'FP_PHONE': false,
          'FP_SMS': false,
          'TP_POST': false,
          'TP_EMAIL': false,
          'TP_PHONE': false,
          'TP_SMS': false
        })
      });
    }

    // SHOW ADDRESS IF PROVIDED
    if (this.form.value.addressLine1.length > 0) {
      this.addressManually = true;
    }

    // CLEAN SPECIAL CHARACTERS FROM ADDRESS
    this.form.controls['addressLine1'].setValue(this.subscriber.addressLine1.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
    this.form.controls['addressLine2'].setValue(this.subscriber.addressLine2.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
    this.form.controls['addressLine3'].setValue(this.subscriber.addressLine3.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
    this.form.controls['addressLine4'].setValue(this.subscriber.addressLine4.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
    this.form.controls['addressLine5'].setValue(this.subscriber.addressLine5.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));

    // IF ONE RESULT FROM SKY DON'T ALLOW CHANGING ADDRESS
    if (sessionStorage.getItem('oneResult') != null) {
      this.oneResult = true;

      // this.form.controls['viewingCardNumber'].reset({ value: this.subscriber.viewingCardNumber, disabled: true });
      // this.form.controls['countryCode'].reset({ value: this.subscriber.countryCode, disabled: true });
      // this.form.controls['currencyCode'].reset({ value: this.subscriber.currencyCode, disabled: true });
      // this.form.controls['addressLine1'].reset({ value: this.subscriber.addressLine1, disabled: true });
      // this.form.controls['addressLine2'].reset({ value: this.subscriber.addressLine2, disabled: true });
      // this.form.controls['addressLine3'].reset({ value: this.subscriber.addressLine3, disabled: true });
      // this.form.controls['addressLine4'].reset({ value: this.subscriber.addressLine4, disabled: true });
      // this.form.controls['addressLine5'].reset({ value: this.subscriber.addressLine5, disabled: true });
      // this.form.controls['postcode'].reset({ value: this.subscriber.postcode, disabled: true });
    }
    this.detectCountryFromPostcode(this.form.value.postcode);
  };

  // SHOW WARNING IF COUNTRY AND CURRENCY DON'T MATCH
  countryCurrencyMatch() {
    this.match = false;
    if ((this.form.value.currencyCode === 'GBP' && this.form.value.countryCode === 'IRL')
      || (this.form.value.currencyCode === 'EUR' && this.form.value.countryCode === 'GBR')) {
      this.match = true;
    }
  }

  /**
   * SEARCH LOCAL TO CHECK IF VIEWING CARD NUMBER ALREADY EXISTS
   */

  checkVCN() {
    const sL = {
      'viewingCardNumber': this.form.value.viewingCardNumber
    };

    let resp;
    this.rest.get('/subscribers/search-local?postcode=&addressLine1=&addressLine2=&viewingCardNumber=' + sL.viewingCardNumber + '&surname=')
      .subscribe(res => {
        resp = res;
        if (resp.length === 0) {
          console.log('VCN', res);
          this.submitted = true;
          if (this.form.valid === true && !this.hasPaymentCard && this.form.value.vip == 'false') {
            this.payMethDialog = true;
          } else {
            this.processForm();
          }
        } else {
          this.VCNcheck = true;
        }
      }, err => {
        console.log(err);
        if (err.error.message.toLocaleLowerCase().includes('not found')) {
          if (this.form.valid === true && !this.hasPaymentCard && this.form.value.vip == 'false') {
            this.payMethDialog = true;
          } else {
            this.processForm();
          }
        } else {
          if (err.error) {
            this.error = this.helpers.error(err.error.message);
          } else {
            this.error = {
              message: err.message
            }
          }
        }
      });
  }

  // checkVCN() {
  //   if (this.form.valid) {
  //     const sL = {
  //       'viewingCardNumber': this.form.value.viewingCardNumber
  //     };
  //     let resp;
  //     this.rest.get('/subscribers/search-local?postcode=&addressLine1=&addressLine2=&viewingCardNumber='
  //       + sL.viewingCardNumber + '&surname=')
  //       .subscribe(res => {
  //         resp = res;
  //         if (resp.length === 0) {
  //           console.log('VCN', res);
  //           this.submitted = true;
  //             this.remoteSearch();
  //         } else {
  //           this.VCNcheck = true;
  //         }
  //       }, err => {
  //         console.log(err);
  //         if (err.error.message.toLocaleLowerCase().includes('not found')) {
  //               this.remoteSearch();
  //         } else {
  //           if (err.error) {
  //             this.error = this.helpers.error(err.error.message);
  //           } else {
  //             this.error = {
  //               message: err.message
  //             }
  //           }
  //         }
  //       });
  //   }else {
  //     this.form.updateValueAndValidity();
  //     this.submitted = true;
  //   }
  // }
  //


  /**
   * REMOTE SEARCH
   */
  // remoteSearch() {
  //   let json;
  //   json = this.form.value;
  //   json.addressLine1 = this.form.value.addressLine1;
  //   json.addressLine2 = this.form.value.addressLine2;
  //
  //   let remoteResults;
  //   this.rest.get('/subscribers/search-remote?postcode=' + json.postcode + '&addressLine1=' + json.addressLine1
  //     + '&addressLine2=' + json.addressLine2 + '&surname=' + json.surname)
  //     .subscribe(res => {
  //         remoteResults = res;
  //         console.log(res);
  //         if (remoteResults.length === 0) {
  //           // NO MATCH
  //           this.error = {message: 'Sky have no record of this customer. Please check the Viewing Card Number and Address'};
  //         } else if (remoteResults.length === 1) {
  //           // ONE MATCH
  //           // replace special characters in surname
  //           res[0] = this.helpers.removeSpecialCharacters(res[0]);
  //           // Format postcode
  //           res[0].postcode = this.postcodeService.postcodeFormat(res[0].postcode);
  //           // Capitalize Surname
  //           res[0].surname = this.helpers.toTitleCase(res[0].surname);
  //           this.remoteMatch = res;
  //           // Show one match dialog
  //           this.remoteMatchDialog = true;
  //         } else if (remoteResults.length > 1) {
  //           // MULTIPLE MATCH
  //           if (this.form.valid === true && !this.hasPaymentCard && this.form.value.vip == 'false') {
  //             this.payMethDialog = true;
  //           } else {
  //             this.processForm();
  //           }
  //         }
  //       },
  //       err => {
  //         // MULTIPLE MATCH
  //         if (err.error.message.toLowerCase().includes('multiple')) {
  //           if (this.form.valid === true && !this.hasPaymentCard && this.form.value.vip == 'false') {
  //             this.payMethDialog = true;
  //           } else {
  //             this.processForm();
  //           }
  //         } else {
  //           this.error = this.helpers.error(err.error.message);
  //         }
  //       });
  // }


  // PROCESS AND VALIDATE FORM WHEN SUBMITTED
  processForm() {
    this.payMethDialog = false;

    this.form.updateValueAndValidity();
    this.submitted = true;
    this.addressManually = true;

    // UPDATE JOIN SUBSCRIBER JSON TO SEND
    this.joinSubscriber = this.form.value;
    this.joinSubscriber.homeTelNr = this.form.value.phones.homeTelNr;
    this.joinSubscriber.workTelNr = this.form.value.phones.workTelNr;
    this.joinSubscriber.mobTelNr = this.form.value.phones.mobTelNr;
    if (sessionStorage.getItem('customerId')) {
      this.joinSubscriber.customerId = sessionStorage.getItem('customerId');
    }
    delete this.joinSubscriber.phones;

    // ADD cardSubscriberId from search remote result (Search flow)
    if (sessionStorage.getItem('remoteMatch') != null) {
      this.joinSubscriber.cardSubscriberId = JSON.parse(sessionStorage.getItem('remoteMatch'))[0].cardSubscriberId;
    }

    // ADD cardSubscriberId from existing customer/subscriber
    if (sessionStorage.getItem('cardSubscriberId') != null && sessionStorage.getItem('cardSubscriberId').length > 0) {
      this.joinSubscriber.cardSubscriberId = JSON.parse(sessionStorage.getItem('cardSubscriberId'));
    }

    // set email to unknown@mediaaccessservices.net if empty
    if (this.form.value.email === '') {
      // this.form.patchValue({email: 'unknown@mediaaccessservices.net' });
      this.joinSubscriber.email = 'unknown@mediaaccessservices.net';
    }

    sessionStorage.setItem('joinSubscriber', JSON.stringify(this.joinSubscriber));

    if (this.form.valid === true && !this.hasPaymentCard && this.form.value.vip == 'false') {

      const data = {
        currencyCode: this.joinSubscriber.currencyCode,
        title: this.joinSubscriber.title,
        initials: this.joinSubscriber.initials,
        surname: this.joinSubscriber.surname,
        addressLine1: this.joinSubscriber.addressLine1,
        postcode: this.joinSubscriber.postcode,
        country: this.joinSubscriber.countryCode,
        email: this.joinSubscriber.email,


        returnUrl: this.globals.PAYMENT_RETURN_DOMAIN + '/#/new-payment',
        directDebit: this.payMeth
      };


      // Data to url string
      const str = Object.keys(data).map(function (key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
      }).join('&');

      this.rest.get('/payments/get-payment-card-url?' + str)
        .subscribe(res => {
          console.log( res);
          // location.replace(res['storeCardUrl']);
          sessionStorage.setItem('payment-url', res['storeCardUrl']);
          sessionStorage.setItem('return-url', data.returnUrl);
          sessionStorage.setItem('paymentPath', 'join');
          this.router.navigate(['/payment']);
        }, err => {
          console.log(err);
          if (err.error) {
            this.error = this.helpers.error(err.error.message);
          } else {
            this.error = {
              message: err.message
            }
          }
        })
    }else if ((this.form.valid === true && this.hasPaymentCard) || (this.form.valid === true && this.joinSubscriber.vip)){
      this.router.navigate(['/joinSubscriberEntitlements'], { queryParams: { hasPaymentCard: '1' }});
    }
  }

  // FIND ADDRESS
  findAddress() {
    const postcode = this.form.value.postcode.replace(" ", "");
    this.rest.get('/postcode?postcode=' + postcode)
      .subscribe(res => {
        this.addressList = res;
        if (this.addressList.length == 0) {
          this.addressList = [{
            notFound : 'NO ADDRESS FOUND'
          }]
        }
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  // SET ADDRESS
  setAddress(address) {
    if (address.notFound) {
      this.addressList = null;
    }else {
      this.addressList = null;
      this.addressManually = true;
      const add = this.helpers.nullCheck(address);
      this.form.controls['addressLine1'].setValue(add.addressLine1.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.form.controls['addressLine2'].setValue(add.addressLine2.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.form.controls['addressLine3'].setValue(add.addressLine3.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.form.controls['addressLine4'].setValue(add.addressLine4.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
      this.form.controls['addressLine5'].setValue(add.addressLine5.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
    }
  }


  // Detect country and currency code from post
  detectCountryFromPostcode(pc) {
    if (pc.toLowerCase() === 'roi') {
      this.invalidPostcode = false;
      this.form.patchValue({
        countryCode: 'IRL',
        currencyCode: 'EUR'
      });
    } else if ((pc.toLowerCase() === 'ci' || pc.toLowerCase() === 'iom') && this.role === 'admin' ) {
      this.invalidPostcode = false;
      this.form.patchValue({
        countryCode: 'GBR',
        currencyCode: 'GBP'
      });
    } else {
      let resp;
      const postcode = this.form.value.postcode.replace(' ', '');
      this.rest.get('/postcode?postcode=' + postcode)
        .subscribe(res => {
          resp = res;
          if (resp) {
            this.invalidPostcode = false;
            this.form.patchValue({
              countryCode: 'GBR',
              currencyCode: 'GBP'
            });
          } else {
            this.form.controls['postcode'].setErrors({'incorrect': true});
            this.invalidPostcode = true;
          }
        }, err => {
          this.error = {
            message: 'Error in postcode validation'
          }
        });
    }
  }


  /**
   * Email Verification
   */
  validateEmail() {
    let email;
    if (this.form.value.email) {
      this.emailVerificationResponse = 'loading';
      email = this.form.value.email;
      const data  = {
        email: email,
        type: 'KICKBOX'
      };

      let resp;
      this.rest.post('/email-verifications', data)
        .subscribe(res => {
          resp = res;
          this.emailVerificationResponse = resp.body.verificationResults.result;
          // set did you mean value
          if (resp.body.verificationResults.didYouMean != null) {
            this.didYouMean = resp.body.verificationResults.didYouMean;
          }
        }, err => {
          if (err.error.message.includes('403')) {
            this.emailVerificationResponse = 'Insufficient Balance';
          }else {
            this.emailVerificationResponse = false;
            this.error = this.helpers.error(err.error.message);
          }
        });
    }
  }


  /**
   * Patch Email value
   */
  patchEmail() {
    this.form.patchValue({
      email: this.didYouMean
    });
    this.emailVerificationResponse = false;
  }

  /**
   * NO EMAIL CHECKBOX
   */
  noEmailChecked() {
    this.noEmail = !this.noEmail;
    if (this.noEmail) {
      this.form.patchValue({email: '' });
      this.emailVerificationResponse = null;
    }else {
      this.form.patchValue({email: '' })
    }
  }


  log() {
    console.log(this.form)
  }
}
