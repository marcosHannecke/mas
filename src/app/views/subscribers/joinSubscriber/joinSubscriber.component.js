"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var subscribers_service_1 = require("../../../services/subscribers.service");
var products_service_1 = require("../../../services/products.service");
var postcode_service_1 = require("../../../services/postcode.service");
var validators_service_1 = require("../../../services/validators.service");
var helpers_service_1 = require("../../../services/helpers.service");
var payments_service_1 = require("../../../services/payments.service");
var JoinSubscriberComponent = /** @class */ (function () {
    function JoinSubscriberComponent(helpers, paymentsService, validators, router, subscribersService, fb, productsService, postcodeService) {
        this.helpers = helpers;
        this.paymentsService = paymentsService;
        this.validators = validators;
        this.router = router;
        this.subscribersService = subscribersService;
        this.fb = fb;
        this.productsService = productsService;
        this.postcodeService = postcodeService;
        this.overlay = false;
        this.joinSubscriber = {};
        this.submitted = false;
        this.match = false;
        this.addressManually = false;
        this.hasPaymentCard = false;
        this.noEmail = false;
        this.VCNcheck = false;
        this.disableAddress = false;
        this.oneResult = false;
        this.payMethDialog = false;
    }
    JoinSubscriberComponent.prototype.ngOnInit = function () {
        var _this = this;
        // GET USER ROLE
        if (sessionStorage.getItem('role') != null) {
            this.role = sessionStorage.getItem('role');
            console.log(this.role);
        }
        this.helpers.scrollTop();
        // CHECK PAYMENT IF CUSTOMER ID
        if (sessionStorage.getItem('customerId')) {
            this.joinSubscriber.customerId = sessionStorage.getItem('customerId');
            console.log('cID', this.joinSubscriber.customerId);
            this.paymentsService.getUrl('has-payment-card?customerId=', this.joinSubscriber.customerId)
                .subscribe(function (res) {
                _this.hasPaymentCard = JSON.parse(res['_body']).hasPaymentCard;
                console.log('PC', _this.hasPaymentCard);
            }, function (err) {
                _this.error = JSON.parse(err['_body']);
            });
        }
        if (sessionStorage.getItem('joinSubscriber') != null) {
            this.subscriber = JSON.parse(sessionStorage.getItem('joinSubscriber'));
            // FORM WITH VALUES STORED
            this.form = this.fb.group({
                "cardHolderType": [this.subscriber.cardHolderType, forms_1.Validators.required],
                "viewingCardNumber": [this.subscriber.viewingCardNumber,
                    [forms_1.Validators.required, forms_1.Validators.maxLength(9), this.validators.onlyNumbers]],
                "title": [this.subscriber.title, forms_1.Validators.required],
                "initials": [this.subscriber.initials, [forms_1.Validators.required, forms_1.Validators.maxLength(3), this.validators.letterSpaces]],
                "surname": [this.subscriber.surname,
                    [forms_1.Validators.required, forms_1.Validators.minLength(2), forms_1.Validators.maxLength(35), this.validators.letterSpaces]],
                "addressLine1": [this.subscriber.addressLine1,
                    [forms_1.Validators.required, forms_1.Validators.minLength(2), forms_1.Validators.maxLength(35), this.validators.specialChar]],
                "addressLine2": [this.subscriber.addressLine2,
                    [forms_1.Validators.required, forms_1.Validators.minLength(2), forms_1.Validators.maxLength(35), this.validators.specialChar]],
                "addressLine3": ['', forms_1.Validators.minLength(2)],
                "addressLine4": ['', forms_1.Validators.minLength(2)],
                "addressLine5": ['', forms_1.Validators.minLength(2)],
                "postcode": [this.subscriber.postcode, [forms_1.Validators.required, forms_1.Validators.maxLength(9)]],
                "countryCode": [this.subscriber.countryCode],
                "phones": this.fb.group({
                    "homeTelNr": ['', [forms_1.Validators.maxLength(15), this.validators.onlyNumbers]],
                    "workTelNr": ['', [forms_1.Validators.maxLength(15), this.validators.onlyNumbers]],
                    "mobTelNr": ['', [forms_1.Validators.maxLength(15), this.validators.onlyNumbers]],
                }, { validator: this.validators.phones }),
                "cliTelNr": [''],
                "currencyCode": [this.subscriber.currencyCode],
                "vip": ['false', forms_1.Validators.required],
                "email": ['', [forms_1.Validators.required, forms_1.Validators.maxLength(128), this.validators.email]],
            });
        }
        else {
            // Check if coming from customer or search
            if (sessionStorage.getItem('subscriber')) {
                this.subscriber = JSON.parse(sessionStorage.getItem('subscriber'));
                // Check for null and replace with empty string
                this.subscriber = this.helpers.nullCheck(this.subscriber);
            }
            else {
                this.subscriber = this.helpers.emptySubscriber();
            }
            // GET SEARCH FORM IF STORED
            if (sessionStorage.getItem('searchForm')) {
                var searchForm = JSON.parse(sessionStorage.getItem('searchForm'));
                this.subscriber.viewingCardNumber = searchForm.viewingCardNumber;
                this.subscriber.surname = searchForm.surname;
                this.subscriber.addressLine1 = searchForm.addressLine1;
                this.subscriber.addressLine2 = searchForm.addressLine2;
                this.subscriber.postcode = searchForm.postcode;
            }
            // FORM WITH VIEWING CARD NUMBER
            this.form = this.fb.group({
                "cardHolderType": ['DTH', forms_1.Validators.required],
                "viewingCardNumber": [this.subscriber.viewingCardNumber,
                    [forms_1.Validators.required, forms_1.Validators.maxLength(9), this.validators.onlyNumbers]],
                "title": [this.subscriber.title, forms_1.Validators.required],
                "initials": [this.subscriber.initials, [forms_1.Validators.required, forms_1.Validators.maxLength(3), this.validators.letterSpaces]],
                "surname": [this.subscriber.surname,
                    [forms_1.Validators.required, forms_1.Validators.minLength(2), forms_1.Validators.maxLength(35), this.validators.letterSpaces]],
                "addressLine1": [this.subscriber.addressLine1,
                    [forms_1.Validators.required, forms_1.Validators.minLength(2), forms_1.Validators.maxLength(35), this.validators.specialChar]],
                "addressLine2": [this.subscriber.addressLine2,
                    [forms_1.Validators.required, forms_1.Validators.minLength(2), forms_1.Validators.maxLength(35), this.validators.specialChar]],
                "addressLine3": ['', forms_1.Validators.minLength(2)],
                "addressLine4": ['', forms_1.Validators.minLength(2)],
                "addressLine5": ['', forms_1.Validators.minLength(2)],
                "postcode": [this.subscriber.postcode, [forms_1.Validators.required, forms_1.Validators.minLength(2), forms_1.Validators.maxLength(9)]],
                "countryCode": [this.subscriber.countryCode],
                "phones": this.fb.group({
                    "homeTelNr": ['', [forms_1.Validators.maxLength(15), this.validators.onlyNumbers]],
                    "workTelNr": ['', [forms_1.Validators.maxLength(15), this.validators.onlyNumbers]],
                    "mobTelNr": ['', [forms_1.Validators.maxLength(15), this.validators.onlyNumbers]],
                }, { validator: this.validators.phones }),
                "cliTelNr": [''],
                "currencyCode": [this.subscriber.currencyCode],
                "vip": ['false', forms_1.Validators.required],
                "email": ['', [forms_1.Validators.required, forms_1.Validators.maxLength(128), this.validators.email]],
            });
        }
        // SHOW ADDRESS IF PROVIDED
        if (this.form.value.addressLine1.length > 0) {
            this.addressManually = true;
        }
        // IF ONE RESULT FROM SKY DON'T ALLOW CHANGING ADDRESS
        if (sessionStorage.getItem('oneResult') != null) {
            this.oneResult = true;
            // this.form.controls['viewingCardNumber'].reset({ value: this.subscriber.viewingCardNumber, disabled: true });
            // this.form.controls['countryCode'].reset({ value: this.subscriber.countryCode, disabled: true });
            // this.form.controls['currencyCode'].reset({ value: this.subscriber.currencyCode, disabled: true });
            // this.form.controls['addressLine1'].reset({ value: this.subscriber.addressLine1, disabled: true });
            // this.form.controls['addressLine2'].reset({ value: this.subscriber.addressLine2, disabled: true });
            // this.form.controls['addressLine3'].reset({ value: this.subscriber.addressLine3, disabled: true });
            // this.form.controls['addressLine4'].reset({ value: this.subscriber.addressLine4, disabled: true });
            // this.form.controls['addressLine5'].reset({ value: this.subscriber.addressLine5, disabled: true });
            // this.form.controls['postcode'].reset({ value: this.subscriber.postcode, disabled: true });
        }
        this.log();
    };
    ;
    // SHOW WARNING IF COUNTRY AND CURRENCY DON'T MATCH
    JoinSubscriberComponent.prototype.countryCurrencyMatch = function () {
        this.match = false;
        if ((this.form.value.currencyCode == 'GBP' && this.form.value.countryCode == 'IRL')
            || (this.form.value.currencyCode == 'EUR' && this.form.value.countryCode == 'GBR')) {
            this.match = true;
        }
    };
    // CHECK IF VIEWING CARD NUMBER EXISTS
    JoinSubscriberComponent.prototype.checkVCN = function () {
        var _this = this;
        var sL = {
            'viewingCardNumber': this.form.value.viewingCardNumber
        };
        this.subscribersService.searchSubscriberLocal(sL)
            .subscribe(function (res) {
            if (res.length == 0) {
                _this.submitted = true;
                if (_this.form.valid === true && !_this.hasPaymentCard && _this.form.value.vip == 'false') {
                    _this.payMethDialog = true;
                }
                else {
                    _this.processForm();
                }
            }
            else {
                _this.VCNcheck = true;
            }
        });
    };
    // PROCESS AND VALIDATE FORM WHEN SUBMITTED
    JoinSubscriberComponent.prototype.processForm = function () {
        var _this = this;
        this.payMethDialog = false;
        console.log(this.form.value);
        this.form.updateValueAndValidity();
        this.submitted = true;
        this.addressManually = true;
        // UPDATE JOIN SUBSCRIBER JSON TO SEND
        this.joinSubscriber = this.form.value;
        this.joinSubscriber.homeTelNr = this.form.value.phones.homeTelNr;
        this.joinSubscriber.workTelNr = this.form.value.phones.workTelNr;
        this.joinSubscriber.mobTelNr = this.form.value.phones.mobTelNr;
        if (sessionStorage.getItem('customerId')) {
            this.joinSubscriber.customerId = sessionStorage.getItem('customerId');
        }
        delete this.joinSubscriber.phones;
        // ADD cardSubscriberId from search remote result
        if (sessionStorage.getItem('remoteMatch') != null) {
            this.joinSubscriber.cardSubscriberId = JSON.parse(sessionStorage.getItem('remoteMatch'))[0].cardSubscriberId;
        }
        // ADD cardSubscriberId from existing customer/subscriber
        if (sessionStorage.getItem('cardSubscriberId') != null && sessionStorage.getItem('cardSubscriberId').length > 0) {
            this.joinSubscriber.cardSubscriberId = JSON.parse(sessionStorage.getItem('cardSubscriberId'));
        }
        sessionStorage.setItem('joinSubscriber', JSON.stringify(this.joinSubscriber));
        if (this.form.valid === true && !this.hasPaymentCard && this.form.value.vip == 'false') {
            var data = {
                currencyCode: this.joinSubscriber.currencyCode,
                title: this.joinSubscriber.title,
                initials: this.joinSubscriber.initials,
                surname: this.joinSubscriber.surname,
                addressLine1: this.joinSubscriber.addressLine1,
                postcode: this.joinSubscriber.postcode,
                country: this.joinSubscriber.countryCode,
                email: this.joinSubscriber.email,
                returnUrl: window.location.href + 'Entitlements',
                directDebit: this.payMeth
            };
            // Data to url string
            var str = Object.keys(data).map(function (key) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
            }).join('&');
            this.paymentsService.getUrl('get-payment-card-url?', str)
                .subscribe(function (res) {
                console.log(res);
                var url = JSON.parse(res['_body']);
                location.replace(url.storeCardUrl);
            }, function (err) {
                _this.error = JSON.parse(err["_body"]);
            });
        }
        else if ((this.form.valid === true && this.hasPaymentCard) || (this.form.valid === true && this.joinSubscriber.vip)) {
            this.router.navigate(['/joinSubscriberEntitlements'], { queryParams: { hasPaymentCard: '1' } });
        }
    };
    // FIND ADDRESS
    JoinSubscriberComponent.prototype.findAddress = function () {
        var _this = this;
        var postcode = this.form.value.postcode.replace(" ", "");
        this.postcodeService.postcode(postcode)
            .subscribe(function (res) {
            _this.addressList = JSON.parse(res['_body']);
            if (_this.addressList.length == 0) {
                _this.addressList = [{
                        notFound: "NO ADDRESS FOUND"
                    }];
            }
        }, function (err) {
            _this.error = JSON.parse(err['_body']);
        });
    };
    // SET ADDRESS
    JoinSubscriberComponent.prototype.setAddress = function (address) {
        if (address.notFound) {
            this.addressList = null;
        }
        else {
            this.addressList = null;
            this.addressManually = true;
            var add = this.helpers.nullCheck(address);
            this.form.controls['addressLine1'].setValue(add.addressLine1.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.form.controls['addressLine2'].setValue(add.addressLine2.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.form.controls['addressLine3'].setValue(add.addressLine3.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.form.controls['addressLine4'].setValue(add.addressLine4.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
            this.form.controls['addressLine5'].setValue(add.addressLine5.replace(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/, ' '));
        }
    };
    // NO EMAIL CHECKBOX
    JoinSubscriberComponent.prototype.noEmailChecked = function () {
        this.noEmail = !this.noEmail;
        if (this.noEmail) {
            this.form.patchValue({ email: 'unknown@mediaaccessservices.net' });
        }
        else {
            this.form.patchValue({ email: '' });
        }
    };
    JoinSubscriberComponent.prototype.log = function () {
        console.log(this.form);
    };
    JoinSubscriberComponent = __decorate([
        core_1.Component({
            selector: 'joinSubscriber',
            templateUrl: 'joinSubscriber.component.html',
            styles: ['.findAddress{position: absolute; background: white; z-index: 100; } ' +
                    '.findAddress{border-bottom: 1px solid #ececec; border-left: 1px solid #ececec; border-right: 1px solid #ececec}' +
                    '.findAddress ul{padding: 0; margin: 0; max-height: 400px; overflow-y: scroll; min-width: 450px} ' +
                    '.findAddress li{padding: 5px 15px;} .findAddress li:hover{background: #f9f9f9}'],
            providers: [postcode_service_1.PostcodeService, subscribers_service_1.SubscribersService, products_service_1.ProductsService, validators_service_1.ValidatorsService, payments_service_1.PaymentsService, helpers_service_1.HelpersService]
        })
    ], JoinSubscriberComponent);
    return JoinSubscriberComponent;
}());
exports.JoinSubscriberComponent = JoinSubscriberComponent;
