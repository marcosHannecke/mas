import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HelpersService } from '../../../services/helpers.service';
import {Globals} from '../../../app.global';
import {Chart} from 'angular-highcharts';
import {ValidatorsService} from '../../../services/validators.service';
import { RestService } from '../../../services/rest.service';
import {HttpClient, HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [HelpersService, ValidatorsService, RestService]
})
export class UsersComponent implements OnInit, OnDestroy {
  // PAGINATION
  showNumber = 12;
  p = 1;

  users: Array<User>;
  usersFound: Array<User>;
  userSelected: User;
  editDetails = false;
  addUserView = false;
  filter: string;
  dialog: any;
  error: any;
  success: any;
  form: FormGroup;
  addForm: FormGroup;
  totalUsersEnabled: number;
  totalUsersDisabled: number;
  totalUsersLocked: number;
  submitted = false;

  emailVerificationResponse: any;
  didYouMean: any;

  // HIGH CHARTS
  usersChart: any;

  constructor(private helpers: HelpersService, private fb: FormBuilder,
              private validators: ValidatorsService, private rest: RestService,
              private http: HttpClient, private globals: Globals) { }
  ngOnInit() {
    this.getUsers();
  }
  ngOnDestroy() {
    sessionStorage.removeItem('adminPanelUserSelected');
  }

  getUsers() {
    let resp: any;
    // keep same user after changes
    let stored = false;
    if (this.userSelected != null) {
      sessionStorage.setItem('adminPanelUserSelected', JSON.stringify(this.userSelected));
    }
    if (sessionStorage.getItem('adminPanelUserSelected') != null) {
       stored = JSON.parse(sessionStorage.getItem('adminPanelUserSelected'));
    }

    this.rest.getObserveResponse('/users?page=0&size=9999')
      .subscribe(
        res => {
          resp = res.body;
          this.users = this.usersFound = resp.reverse();
          // keep same user after changes
          if (stored) {
            this.userSelected = JSON.parse(sessionStorage.getItem('adminPanelUserSelected'));
            console.log('users', sessionStorage.getItem('adminPanelUserSelected'));
          } else {
            this.userSelected = this.users[0];
          }
          this.getTotalUsers();
        },
        err => {
          this.error = this.helpers.error(err.error.message);
        })
  }

  /**
   * Generate users Highchart
   */
  getChart() {
    // HIGH CHARTS YEAR
    this.usersChart = new Chart({
      chart: {
        height: 300,
        type: 'bar',
      },
      colors: ['#0f3b68', '#e23237', '#ccc'],
      title: {
        text: ''
      },
      xAxis: {
        categories: ['Enabled', 'Disabled', 'Locked' ]
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of users'
        }
      },
      legend: {
        reversed: true
      },
      plotOptions: {
        series: {
          stacking: 'normal'
        }
      },
      series: [{
        name: 'Enabled',
        data: [this.totalUsersEnabled, 0, 0]
      },
        {
          name: 'Disabled',
          data: [0, this.totalUsersDisabled, 0]
        },
        {
          name: 'Locked',
          data: [0, 0, this.totalUsersLocked]
        }]
    });
  }
  /**
   * Get users Enabled, Disabled, Locked
   */
  getTotalUsers() {
    this.totalUsersDisabled = 0;
    this.totalUsersEnabled = 0;
    this.totalUsersLocked = 0;

    for (const u of this.users) {
      if (u.enabled) {
        this.totalUsersEnabled += 1;
      }
      if (!u.enabled) {
        this.totalUsersDisabled += 1;
      }
      if (u.locked) {
        this.totalUsersLocked += 1;
      }
    }
    this.getChart();
  }

  /**
   * Show add new user form
   */
  addNewUser() {
   this.addUserView = !this.addUserView;
   this.submitted = false;
   this.editDetails = false;
   this.emailVerificationResponse = false;
   this.addForm = this.fb.group({
     'name': ['', [Validators.required, Validators.minLength(2), Validators.maxLength(255)]],
     'email': ['', [Validators.required, Validators.minLength(8), Validators.maxLength(255)]],
     'password': ['', [Validators.required, Validators.minLength(8), Validators.maxLength(255), this.validators.forceSpecialChar]],
     'role': [ 'ROLE_USER', [Validators.required]],
     'notes': [ '', [ Validators.maxLength(100)]],
   })
  }

  /**
   * POST new user
   */
  add() {
    if (this.emailVerificationResponse ===  'undeliverable'  || !this.emailVerificationResponse
      || this.emailVerificationResponse === null) {
      this.error = {
        message: 'A valid email is required'
      }
    }else {
      const data = this.addForm.value;
      data.cmsId = this.globals.CMSId.toString();
      if (this.addForm.valid) {

        this.rest.post('/users/', this.addForm.value)
          .subscribe(() => {
            this.success = {
              message: 'New user added'
            };
            // Reset storage and user selected to go to new user created
            sessionStorage.removeItem('adminPanelUserSelected');
            this.userSelected = null;

            this.getUsers();
            this.addUserView = !this.addUserView;
          }, err => {
            if (err.error.message.includes('fieldErrors')) {
              this.error = err.error.message.fieldErrors[0];
            } else {
              this.error = this.helpers.error(err.error.message);
            }
          });
      }
    }
  }

  /**
   * Show edit user form
   */
  editUser() {
    this.editDetails = !this.editDetails;
    this.submitted = false;
    this.emailVerificationResponse = false;
    this.form = this.fb.group({
      'name': [this.userSelected.name, [ Validators.minLength(2), Validators.maxLength(255)]],
      'email': [this.userSelected.email, [Validators.minLength(8), Validators.maxLength(255)]],
      'password': ['', [Validators.minLength(8), Validators.maxLength(255)]],
      'notes': [this.userSelected.notes, [Validators.maxLength(100)]],
      'role': [ this.userSelected.roles, [Validators.required]],
    });
  }

  /**
   * Updated user details
   */
  updateDetails() {
    if (this.emailVerificationResponse ===  'undeliverable'  || !this.emailVerificationResponse
      || this.emailVerificationResponse === null) {
      this.error = {
        message: 'A valid email is required'
      }
    }else {


      const updatedDetails = {
        newName: this.form.value.name,
        newPassword: this.form.value.password,
        newEmail: this.form.value.email,
        role: this.form.value.role,
        id: this.userSelected.id,
        notes: this.form.value.notes
      };

      if (this.form.value.name === this.userSelected.name) {
        delete updatedDetails.newName;
      }
      if (this.form.value.email === this.userSelected.email) {
        delete updatedDetails.newEmail;
      }
      if (this.form.value.password === '') {
        delete updatedDetails.newPassword;
      }

      if (this.form.valid) {
        // put request with response type text
        this.http.put(this.globals.API_ENDPOINT + '/users', updatedDetails, {responseType: 'text'})
          .subscribe(
            () => {
              // Reset storage and user selected to go to new user created
              sessionStorage.removeItem('adminPanelUserSelected');
              this.userSelected = null;
              this.editDetails = !this.editDetails;
              this.getUsers();
            },
            err => {
              err = JSON.parse(err.error);
              console.log('here', err);
              if (err.message.fieldErrors) {
                this.error = err.message.fieldErrors[0];
              } else {
                this.error = this.helpers.error(err.message);
              }
            });
      }
    }
  }

  /**
   * Filter
   */
  userFilter() {
    this.usersFound = this.helpers.jsonFilter(this.users, this.filter)
  }

  /**
   * Update enabled status
   */
  updateEnabled() {
    this.dialog = null;
    let url;
    if (this.userSelected.enabled) {
      url = '/users/enable/'
    }else if (!this.userSelected.enabled) {
      url = '/users/disable/'
    }

    this.rest.put(url + this.userSelected.id, '')
      .subscribe(() => {
        if (this.userSelected.enabled) {
          this.success = {
            message: 'User ' + this.userSelected.name + ' is now enabled'
          }
        }else if (!this.userSelected.enabled) {
          this.success = {
            message: 'User ' + this.userSelected.name + ' is now disabled'
          }
        }
        this.getUsers();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Unlock user
   */
  unlock() {
    this.rest.put('/users/unlock/' + this.userSelected.id, '')
      .subscribe( () => {
        this.success = {
          message: 'User unlocked'
        };
        this.userSelected.locked = false;
        this.getUsers();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Email Verification
   */
  validateEmail(email) {
    if (email) {
      this.emailVerificationResponse = 'loading';
      // email = this.form.value.email;
      const data  = {
        email: email,
        type: 'KICKBOX'
      };

      let resp;
      this.rest.post('/email-verifications', data)
        .subscribe(res => {
          resp = res;
          this.emailVerificationResponse = resp.body.verificationResults.result;
          // set did you mean value
          if (resp.body.verificationResults.didYouMean != null) {
            this.didYouMean = resp.body.verificationResults.didYouMean;
          }
        }, err => {
          this.emailVerificationResponse = false;
          this.helpers.error(err.error.message);
        });
    }
  }

  /**
   * Patch Email value
   */
  patchEmail() {
    this.form.patchValue({
      email: this.didYouMean
    });
    this.emailVerificationResponse = false;
  }


}

/**
 * User interface
 */
interface User {
  email: string,
  enabled: boolean,
  id: number,
  locked: boolean,
  name: string,
  password ?: string,
  roles: string,
  notes ?: string
}
