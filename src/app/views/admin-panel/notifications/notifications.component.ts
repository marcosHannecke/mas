import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HelpersService } from '../../../services/helpers.service';
import {RestService} from '../../../services/rest.service';
import { DatepickerOptions } from 'ng2-datepicker';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css'],
  providers: [HelpersService, RestService]
})
export class NotificationsComponent implements OnInit {

  // PAGINATION
  itemsPage = 4;
  page = 1;

  notifications: any;
  notification: Notification = new Notification();

  // date = + Date.now();
  date1= + Date.now();
  date2 = + Date.now();
  date = new Date();
  unixDate: any;

  edit: any;

  editNotificationDialog = false;
  dialog: any;
  success: any;
  error: any;
  options: DatepickerOptions;

  constructor(private toastr: ToastrService, private helpers: HelpersService, private rest: RestService) {}

  ngOnInit() {
    // Get date to min date Datepicker options
    const date = new Date();
    date.setDate(date.getDate() - 1);
    this.options = {
      minDate: date, // Minimal selectable date
      barTitleIfEmpty: 'Click to select a date',
      displayFormat: 'DD-MMM-YYYY'

    };


    console.log('toastr service', this.toastr);
    this.unixDate = Math.trunc(+ this.date / 1000) ;
    this.notification.type = 'success';
    this.getNotifications();
  }

  /**
   * Get notifications
   */
  getNotifications() {
    this.rest.get('/maintenance-notifications?page=0&size=100')
      .subscribe(res => {
        this.notifications = res;
        this.notifications.sort(function(a, b) {
          return b.activeTo - a.activeTo;
        });
        if (this.notifications.length === 0) {
          this.notifications = null;
        }
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Show notification example
   */
  show() {
    if (this.notification.type === 'success') {
      this.toastr.success(this.notification.text, this.notification.title, this.notification.options);
    }else if (this.notification.type === 'info') {
      this.toastr.info(this.notification.text, this.notification.title, this.notification.options);
    } else if (this.notification.type === 'warning') {
      this.toastr.warning(this.notification.text, this.notification.title, this.notification.options);
    } else if (this.notification.type === 'error') {
      this.toastr.error(this.notification.text, this.notification.title, this.notification.options);
    }
    console.log(this.toastr);
  }

  /**
   * Delete notification
   * @param {number} id
   */
  deleteNotification(id: number) {
    this.rest.delete('/maintenance-notifications/', id)
      .subscribe(() => {
        this.dialog = null;
        this.getNotifications();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   *  Add notification
   */
  addNotification() {
    const position = this.formatPosition(this.notification.options.positionClass);

    const dateFrom = + this.date1 / 1000;
    const dateTo = + this.date2 / 1000;

    // Check if valid date period
    if (dateFrom <= 0 || dateTo <= 0) {
      this.error = {
        message: 'Invalid period selected'
      };
    }else {
      const json = {
        'activeFrom': dateFrom,
        'activeTo': dateTo,
        'displayDuration': this.notification.options.timeOut,
        'enableHtml': this.notification.options.enableHtml,
        'logIn': true,
        'logOut': this.notification.options.logOut,
        'message': this.notification.text,
        'notificationType': this.notification.type.toUpperCase(),
        'position': position,
        'showCloseButton': this.notification.options.closeButton,
        'showInLoginPage': this.notification.options.showInLoginPage,
        'title': this.notification.title,
      };

      this.rest.post('/maintenance-notifications/', json)
        .subscribe(res => {
          console.log(res);
          this.getNotifications();
          this.success = {
            message: 'Notification added'
          }
        }, err => {
          console.log(err);
          if (err.error.fieldErrors) {
            this.error = {
              fieldErrors: err.error.fieldErrors
            }
          } else {
            this.error = this.helpers.error(err.error.message);
          }
        });
    }
  }

  /**
   * Format position
   * @param position
   */
  private formatPosition(position) {
    if (position === 'toast-top-right') {
      return 'TR'
    }  else if (position === 'toast-bottom-right') {
      return 'BR'
    } else if (position === 'toast-top-left') {
      return 'TL'
    } else if (position === 'toast-bottom-left') {
      return 'BL'
    }else if (position === 'toast-top-full-width') {
      return 'TFW'
    }else if (position === 'toast-bottom-full-width') {
      return 'BFW'
    }else if (position === 'toast-top-center') {
      return 'TC'
    }else if (position === 'toast-bottom-center') {
      return 'TB'
    }
    return 'TR'
  }
  /**
   * Get notification position
   * @param not -> notification.position
   * @returns {string}
   */
  private getPosition(not) {
    if (not === 'TR') {
      return 'toast-top-right'
    }  else if (not === 'BR') {
      return 'toast-bottom-right'
    } else if (not === 'TL') {
      return 'toast-top-left'
    } else if (not === 'BL') {
      return 'toast-bottom-left'
    }else if (not === 'TFW') {
      return 'toast-top-full-width'
    }else if (not === 'BFW') {
      return 'toast-bottom-full-width'
    }else if (not === 'TC') {
      return 'toast-top-center'
    }else if (not === 'TB') {
      return 'toast-bottom-center'
    }else if (not === 'CO') {
      return 'toast-top-right'
    }
    return 'toast-top-right'
  }

  /**
   * Activates edit mode
   * @param not -> notification selected to edit
   */
  editMode(not) {
    this.edit = true;
    this.notification.type = not.notificationType.toLocaleLowerCase();
    this.notification.title = not.title;
    this.notification.text = not.message;
    this.notification.options.positionClass = this.getPosition(not.position);
    this.notification.options.timeOut = not.displayDuration;
    this.notification.options.disableTimeOut = false;
    this.notification.options.closeButton = not.showCloseButton;
    this.notification.options.enableHtml = not.enableHtml;
    this.notification.options.showInLoginPage = not.showInLoginPage;
    this.notification.options.logOut = not.logOut;
    this.notification.id = not.id;

    this.date1 =  not.activeFrom * 1000;
    this.date2 = not.activeTo * 1000;


    console.log(this.date1, this.date2)
  }

  /**
   * Exit edit mode
   */
  exitEditMode() {
    this.edit = false;
    this.notification = new Notification();
    this.date1 = + Date.now();
    this.date2 = + Date.now();
    this.notification.type = 'success';
  }

  /**
   * Saves edited notification
   */
  editNotification() {
    this.editNotificationDialog = null;

    const position = this.formatPosition(this.notification.options.positionClass);
    const dateFrom = + this.date1 / 1000;
    const dateTo = + this.date2 / 1000;

    console.log(dateFrom, dateTo);
    const json = {
      'id': this.notification.id,
      'activeFrom': dateFrom,
      'activeTo': dateTo,
      'displayDuration': this.notification.options.timeOut,
      'enableHtml': this.notification.options.enableHtml,
      'logIn': true,
      'logOut': this.notification.options.logOut,
      'message': this.notification.text,
      'notificationType': this.notification.type.toUpperCase(),
      'position': position,
      'showCloseButton': this.notification.options.closeButton,
      'showInLoginPage': this.notification.options.showInLoginPage,
      'title': this.notification.title,
    };

    this.rest.put('/maintenance-notifications/', json)
      .subscribe(res => {
        console.log(res);
        this.getNotifications();
        this.success = {
          message: 'Notification edited'
        };
        this.exitEditMode();
      }, err => {
        this.exitEditMode();
        console.log(err);
        if (err.error.fieldErrors != null) {
          this.error = {
            fieldErrors: err.error.fieldErrors
          }
        } else {
          this.error = this.helpers.error(err.error.message);
        }
      })
  }
}

class Notification {
  id ?: number;
  type: string;
  title: string;
  text: string;
  options = {
    timeOut: 10000,
    extendedTimeOut: 0, // Time to close after a user hovers notification
    disableTimeOut: false,
    closeButton: true,
    positionClass: 'toast-top-right',
    enableHtml: false,
    progressBar: true,
    easing: 'ease-out',
    tapToDismiss: true,
    showInLoginPage: true,
    logOut: 'DISABLED'
    // animate: 'flyLeft',
  }
}

