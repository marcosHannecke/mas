import {Component} from '@angular/core';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css'],
})
export class AdminPanelComponent {

  success: any;
  error: any;
  dialog: any;

  tab1Active = true;
  tab2Active = false;
  tab3Active = false;
  tab4Active = false;

  constructor() {}
}

