import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ValidatorsService} from "../../../services/validators.service";
import {RestService} from "../../../services/rest.service";
import {HelpersService} from "../../../services/helpers.service";
import {DatepickerOptions} from 'ng2-datepicker';
import {HttpClient} from '@angular/common/http';
import {Globals} from "../../../app.global";
import {forEach} from "@angular/router/src/utils/collection";
import {DragulaService} from "ng2-dragula";


@Component({
  selector: 'app-product-groups',
  templateUrl: './product-groups.component.html',
  styleUrls: ['./product-groups.component.css'],
  providers: [ValidatorsService, HelpersService]
})
export class ProductGroupsComponent implements OnInit {

  error: any;
  success: any;

  createForm: FormGroup;
  editForm: FormGroup;
  submitted = false;

  view = {
    main: true,
    createProductGroup: false,
    editProductGroup: false,
    productGroupOrder: false
  };

  package = {
    titleLabel: '',
    titleDescription: '',
    currency: 'GBP',
    contractLength: '',
    initialPeriod: '',
    recurringPeriod: '',
    initialAmount: '',
    recurringAmount: '',
    validFrom: Date.now(),
    validTo: Date.now(),
    price: '',
    requiredBillingPeriods: ''
  };

  sellingConstraints: any;
  addedSellingConstrains = [];
  realexMerchantOptions = [];
  surfAndPayOptions = [];

  searchOthers: any;
  others = [];
  searchPpnEnablers: any;
  ppnEnablers = [];
  searchSubscriptions: any;
  subscriptions = [];
  search: any;
  searchResults: any;
  productGroupSelected: any;
  contractLengths: any;
  subscriptionPeriods: any;
  categories: any;
  categoriesSelected = [];

  options: DatepickerOptions;
  dateFrom = + Date.now();
  dateTo = + Date.now();
  sellableWarning: any;
  salesItems: any;
  sellables: any;
  currencySellables = [];

  currencySelected = 'GBP';

  constructor(private fb: FormBuilder, private validators: ValidatorsService, private rest: RestService,
              private helpers: HelpersService, private http: HttpClient, private globals: Globals, private dragulaService: DragulaService){
              dragulaService.drop.subscribe((value) => {
              this.drop(value);
            });
  }

  ngOnInit() {
    this.default();
  }

  /**
   * Default values
   */
  default() {
    this.package = {
      titleLabel: '',
      titleDescription: '',
      currency: 'GBP',
      contractLength: '',
      initialPeriod: '',
      recurringPeriod: '',
      initialAmount: '',
      recurringAmount: '',
      validFrom: Date.now(),
      validTo: Date.now(),
      price: '',
      requiredBillingPeriods: ''
    };
    this.categoriesSelected = [];
    this.others = [];
    this.subscriptions = [];
    this.ppnEnablers = [];
    this.addedSellingConstrains = [];
    this.realexMerchantOptions = [];
    this.surfAndPayOptions = [];
    this.submitted = false;
    this.searchResults = null;

    // Create Product Group Form
    this.createForm = this.fb.group({
      titleLabel: ['', Validators.required],
      titleDescription: ['', Validators.required],
      currency: ['GBP', Validators.required],
      contractLength: [''],
      initialPeriod: [''],
      recurringPeriod: [''],
      initialAmount: [''],
      recurringAmount: [''],
      requiredBillingPeriods: ['', this.validators.onlyNumbers],
      validFrom: ['', Validators.required ],
      validTo: ['', Validators.required ],
      realexMerchant: ['', Validators.required],
      surfAndPayMerchant: ['', Validators.required],
      price: [''],
      suppressIppvBillingPpvService: [false, Validators.required],
      sellable: [false, Validators.required],
      salesItemGroup: ['', Validators.required],
    });

    // Get Category Set
    this.getSellingConstrains();

    // Get payment Merchants
    this.getPaymentMerchants();

    // Get Product Set Ids
    this.getProductSetIds();

    // Contract lengths
    this.getContractLengths();

    // Subscription Periods
    this.getSubscriptionPeriods();

    // Categories
    this.getCategories();

    // Get sales items groups
    this.getAllSalesItems();

  }


  /**
   *  Dragula drop
   * @param args
   */
  private drop(args) {
    let [e, el] = args;
    let data;
    this.currencySellables.forEach((s, i) => {
      console.log(el.id, s.id);
      if (el.id == s.id) {

        data = {
          "cmsId": 9,
          "displayingOrder": i + 1,
          "productGroupId": el.id
        };
        // PUT request
        this.http.put(this.globals.API_ENDPOINT + '/product-groups/setDisplayingOrder', data, {responseType: 'text'})
          .subscribe(res => {
            this.getSellables();
            console.log('displayingOrder', res);
          }, err => {
            err = JSON.parse(err.error);
            console.log(err);
            if (err.fieldErrors) {
              this.error = err.fieldErrors[0];
            } else {
              this.error = this.helpers.error(err.message);
            }
          })
      }
    })
  }

  /**
   * Get sellable by currency
   */
  sellablesByCurrency() {
    this.currencySellables = [];
    this.sellables.forEach(s => {
      if (s.currencyCode == this.currencySelected) {
        this.currencySellables.push(s);
      }
    })
  }

  /**
   * Get sellables
   */
  getSellables(){
    this.sellables = null;
    this.currencySellables = null;

    function displayingOrder(a, b) {
      if (a.displayingOrder < b.displayingOrder) {return -1; }
      if (a.displayingOrder > b.displayingOrder) { return 1; }
      return 0;
    }
    this.rest.get('/product-groups/search-sellable?q=')
      .subscribe((res: any) => {
        res.sort(displayingOrder);
        this.sellables = res;
        this.sellablesByCurrency();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });
  }

  /**
   * Change action view
   * @param view
   */
 changeView(view) {
    Object.keys(this.view).forEach(v => {
      this.view[v] = view === v;
    })
 }

  /**
   * Get sales items groups
   */
  getAllSalesItems() {
    // sort sales items group array by display order
    function displayingOrder(a, b) {
      if (a.displayingOrder < b.displayingOrder) {return -1; }
      if (a.displayingOrder > b.displayingOrder) { return 1; }
      return 0;
    }
    // GET request
    this.rest.get('/sales-item-groups/findAllDto')
      .subscribe((res: any) => {
        this.salesItems = res;
        // sort sales items group array by display order
        this.salesItems.sort(displayingOrder);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });
  }

  /**
   * Assign product group to a sales item group
   * @param pgId
   */
  assignToSalesItem(pgId) {
    const data = {
      'cmsId': 9,
      'productGroupId': pgId,
      'salesItemGroupId': this.createForm.value.salesItemGroup
    };

    // PUT request
    this.http.put(this.globals.API_ENDPOINT + '/product-groups/addExistingProductGroupToSaleItemGroup/', data, {responseType: 'text'})
      .subscribe(res => {
        console.log('salesitem', res);
      }, err => {
        err = JSON.parse(err.error);
        console.log(err);
        if (err.fieldErrors) {
          this.error = err.fieldErrors[0];
        } else {
          this.error = this.helpers.error(err.message);
        }
      })
  }

  /**
   * Category Set
   */
 getSellingConstrains() {
   this.rest.get('/subscribers/findAllCardHolderType')
     .subscribe(res => {
       this.sellingConstraints = res;
       for (const cat of this.sellingConstraints) {
         this.createForm.addControl(cat.name,  new FormControl(false));
       }
     }, err => {
       this.error = this.helpers.error(err.error.message);
     } )
 }

  /**
   * Get categories
   */
 getCategories() {
   this.rest.get('/product-group-categories/findAllDto')
     .subscribe(res => {
       this.categories = res;
     }, err => {
       this.error = this.helpers.error(err.error.message)
     })
  }

  /**
   * Get all-different-payment-merchants-config
   */
 getPaymentMerchants() {
   this.rest.get('/product-groups/search-all-different-payment-merchants-config')
     .subscribe( (res: any) => {
       const merchantOpt = res['listOfDifferentPaymentMerchantsConfigs'];
       merchantOpt.forEach(opt => {
         if (opt && opt['REALEX_MERCHANT']  && opt['SURF_AND_PAY_MERCHANT'] ) {
           if (!this.realexMerchantOptions.includes(opt['REALEX_MERCHANT'])) {
             this.realexMerchantOptions.push(opt['REALEX_MERCHANT']);
            }
           if (!this.surfAndPayOptions.includes(opt['SURF_AND_PAY_MERCHANT'])) {
             this.surfAndPayOptions.push(opt['SURF_AND_PAY_MERCHANT']);
           }
         }
       });
       console.log(this.realexMerchantOptions, this.surfAndPayOptions)
     }, err => {
       this.error = this.helpers.error(err.error.message)
     })
 }

  /**
   * Get Product Set Ids
   */
 getProductSetIds() {

   // Search Others
   this.rest.get('/products/searchOthers')
     .subscribe(res => {
       this.searchOthers = res;
       console.log(this.searchOthers);
     }, err => {
       this.error = this.helpers.error(err.error.message)
     });

   // Search Ppn Enablers
   this.rest.get('/products/searchPpnEnablers')
     .subscribe(res => {
       this.searchPpnEnablers = res;
       console.log(this.searchPpnEnablers);

     }, err => {
       this.error = this.helpers.error(err.error.message)
     });

   // Search subscriptions
   this.rest.get('/products/searchSubscriptions')
     .subscribe(res => {
       this.searchSubscriptions = res;
       console.log(this.searchSubscriptions);
     }, err => {
       this.error = this.helpers.error(err.error.message)
     });
 }

  /**
   * Get contract lengths
   */
  getContractLengths() {
    this.rest.get('/product-group-subscription-infos/findAllSubscriptionContractLengths')
      .subscribe(res => {
        this.contractLengths = res;
        console.log('getContractLengths', res);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Get subscriptionPeriods
   */
  getSubscriptionPeriods() {
    this.rest.get('/product-group-subscription-infos/findAllSubscriptionPeriods')
      .subscribe(res => {
        this.subscriptionPeriods = res;
        console.log('getSubscriptionPeriods', res);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }
  /**
   * add to product list
   * @param cat --> category
   */
  addTo(cat, p, checked) {
    if (checked) {
      if (p.validTo*1000 > this.package.validTo){
          this.package.validTo = p.validTo*1000;
      }
      if (cat === 'O') {
        this.others.push(p);
      } else if (cat === 'P') {
        this.ppnEnablers.push(p);
      } else if (cat === 'S') {
        this.subscriptions.push(p);
      }
    } else {

      if (cat === 'O') {
        this.others.forEach((o, i) => {
          if (o.id === p.id) {
            this.others.splice(i, 1);
          }
        })
      } else if (cat === 'P') {
        this.ppnEnablers.forEach((o, i) => {
          if (o.id === p.id) {
            this.ppnEnablers.splice(i, 1);
          }
        })
      } else if (cat === 'S') {
        this.subscriptions.forEach((o, i) => {
          if (o.id === p.id) {
            this.subscriptions.splice(i, 1);
          }
        })
      }
      // Change default datepicker dates
      let maxDate = + Date.now();
      this.others.forEach((o, i) => {
        if (o.validTo*1000 > maxDate){
          maxDate = o.validTo*1000;
        }
      });

      this.ppnEnablers.forEach((o, i) => {
        if (o.validTo*1000 > maxDate){
          maxDate = o.validTo*1000;
        }
      });

      this.subscriptions.forEach((o, i) => {
        if (o.validTo*1000 > maxDate){
          maxDate = o.validTo*1000;
        }
      });
      this.package.validTo = maxDate;
    }
  }

  /**
   * Add category
   */
  addCategory(event, c) {
    if (event) {
      this.categoriesSelected.push(c)
    }else {
      this.categoriesSelected.forEach((cat, i) => {
        if (cat.id === c.id) {
          this.categoriesSelected.splice(i, 1);
        }
      })
    }
  }

  /**
   * Add selling constrains
   * @param e -> event
   * @param sC -> selling constrain
   */
  addSellingConstrains(e, sC) {
    if (e.target.checked) {
      this.addedSellingConstrains.push(sC);
    } else {
      this.addedSellingConstrains.forEach((s, i) => {
        if (s === sC) {
          this.addedSellingConstrains.splice(i, 1)
        }
      })
    }
  }

  /**
   * Create product group
   */
  createProductGroup() {
    this.submitted = true;
    console.log(this.createForm);

    if (this.subscriptions.length === 0) {
      this.createForm.controls['price'].setValidators([Validators.required]);
    } else {
      this.createForm.controls['contractLength'].setValidators([Validators.required]);
      this.createForm.controls['contractLength'].updateValueAndValidity();

      this.createForm.controls['initialAmount'].setValidators([Validators.required]);
      this.createForm.controls['initialAmount'].updateValueAndValidity();

      this.createForm.controls['initialPeriod'].setValidators([Validators.required]);
      this.createForm.controls['initialPeriod'].updateValueAndValidity();

      this.createForm.controls['recurringAmount'].setValidators([Validators.required]);
      this.createForm.controls['recurringAmount'].updateValueAndValidity();

      this.createForm.controls['recurringPeriod'].setValidators([Validators.required]);
      this.createForm.controls['recurringPeriod'].updateValueAndValidity();

      this.createForm.controls['requiredBillingPeriods'].setValidators([Validators.required]);
      this.createForm.controls['requiredBillingPeriods'].updateValueAndValidity();

    }

    if (this.createForm.valid && this.categoriesSelected.length > 0 && this.addedSellingConstrains.length > 0 &&
      (this.subscriptions.length > 0 || this.others.length > 0 || this.ppnEnablers.length > 0)) {
      const data = {
        'categorySet': [], // added later
        'cmsId': 9,
        'currency': this.createForm.value.currency,
        'paymentMerchantsConfig': {
          'REALEX_MERCHANT': this.createForm.value.realexMerchant.toLowerCase(),
          'SURF_AND_PAY_MERCHANT': this.createForm.value.surfAndPayMerchant.toLowerCase()
        },
        'price': this.createForm.value.price,
        'productGroupSubscriptionInfo': {
          'contractLength': this.createForm.value.contractLength,
          'initialAmount': this.createForm.value.initialAmount,
          'initialPeriod': this.createForm.value.initialPeriod,
          'recurringAmount': this.createForm.value.recurringAmount,
          'recurringPeriod': this.createForm.value.recurringPeriod,
          'requiredBillingPeriods': this.createForm.value.requiredBillingPeriods
        },
        'productSetId': [], // added later
        // 'salesItemGroupId': 0,
        'sellingConstraints': [], // added later
        'suppressIppvBillingPpvService': this.createForm.value.suppressIppvBillingPpvService,
        'titleDescription': this.createForm.value.titleDescription,
        'titleLabel': this.createForm.value.titleLabel,
        'validFrom': Math.round(+ this.createForm.value.validFrom / 1000 ),
        'validTo': Math.round(+ this.createForm.value.validTo / 1000 ),
      };

      // add productSetId
      for (const ppn of this.ppnEnablers) {
        data.productSetId.push(ppn.id)
      }
      for (const sub of this.subscriptions) {
        data.productSetId.push(sub.id)
      }
      for (const o of this.others) {
        data.productSetId.push(o.id)
      }
      // add categorySet
      for (const cat of this.categoriesSelected) {
        data.categorySet.push(cat.id)
      }
      // add selling constrains
      for (const sC of this.addedSellingConstrains) {
        data.sellingConstraints.push(sC.name)
      }

      // remove fields if subscription or not
      if (this.subscriptions.length === 0) {
        delete data.productGroupSubscriptionInfo;
      } else {
        delete data.price
      }

      this.rest.post('/product-groups', data)
        .subscribe((res: any) => {
          // assign to a sales item group
          this.assignToSalesItem(res['body']['id']);

          // sellable
          if (this.createForm.value.sellable) {
            console.log('this.createForm.value.sellable', this.createForm.value.sellable);
            this.makeSellable(res['body']['id'], true, 'created');
          } else {
            this.success = {
              message: 'Package has been created successfully '
            };
          }
        }, err => {
          if (err.error.fieldErrors) {
            console.log(err.error.fieldErrors);
            this.error = err.error.fieldErrors;
          }else {
            this.error = this.helpers.error(err.error.message);
          }
        });
    }
  }

   /**
   * Make Product groups sellable / not sellable
   * @param e --> created / updated
   * @param sellable --> make sellable or not sellable
   * @param id --> PG id
   */
  makeSellable(id, sellable, e: string) {
    let url = '/product-groups/enableProductGroup/';
    let message = '';
    if (!sellable) {
      url = '/product-groups/disableProductGroup/';
      message = 'not'
    }

    // update request
    this.http.put(this.globals.API_ENDPOINT + url + id, {responseType: 'text'})
      .subscribe(() => {
        this.success = {
          message: ` Package has been ${e} successfully and has been  made ${message} sellable`
        };
      }, err => {
        if (err.error.fieldErrors) {
          console.log(err.error.fieldErrors);
          this.error = err.error.fieldErrors;
        }else {
          this.error = this.helpers.error(err.error.message);
        }
      })
  }


  /*****************************
   * EDIT PRODUCT GROUPS OPTION
   ****************************/

  /**
   * Search Product Groups
   */
  searchProductGroups() {
   if (this.search.length > 0) {
      this.rest.get(`/product-groups/find-by-title?titleLabel=${this.search}`)
        .subscribe(res => {
          console.log(res);
          this.searchResults = res;
        }, err => {
          this.error = this.helpers.error(err.error.message);
        })
    } else {
     this.searchResults = null;
   }
  }

  /**
   * Edit product group form
   * @param productGroupSelected
   */
  editPGForm(productGroupSelected) {
    // Categories selected
    this.categoriesSelected = [];
    this.categories.forEach(c => {
      c.checked = false;
      if (productGroupSelected.categorySet.includes(c.name)) {
          c.checked = true;
          this.categoriesSelected.push(c);
      }
    });

    // Selling constraints selected
    this.sellingConstraints.forEach(sc => {
      sc.checked = false;
      if (productGroupSelected.sellingConstraints.includes(sc.name)) {
        sc.checked = true;
        this.addedSellingConstrains.push(sc);
      }
    });

    // dates * 1000 to have datepicker valid value
    const dateFrom = productGroupSelected.validFrom * 1000;
    const dateTo = productGroupSelected.validTo * 1000;

    // Create Product Group Form
    this.editForm = this.fb.group({
      titleLabel: [productGroupSelected.titleLabel, Validators.required],
      titleDescription: [productGroupSelected.titleDescription, Validators.required],
      requiredBillingPeriods: [productGroupSelected.requiredBillingPeriods],
      initialAmount: [productGroupSelected.initialAmount ],
      recurringAmount: [productGroupSelected.recurringAmount],
      validFrom: [dateFrom, Validators.required ],
      validTo: [dateTo, Validators.required ],
      suppressIppvBillingPpvService: [productGroupSelected.suppressIppvBillingPpvService, Validators.required ],
      price: [''],
      sellable: [productGroupSelected.crmSellable, Validators.required],
    });
    // show dialog with form
    this.productGroupSelected = productGroupSelected;

  }

  /**
   * Save edited Product Group
   */
  saveEditedProductGroup() {
    this.submitted = true;
    // update validators if subscription / ppv
    if (!this.productGroupSelected.productTypeStats.toLowerCase().includes('subscription')) {
      this.editForm.controls['price'].setValidators([Validators.required]);
    } else {
      this.editForm.controls['initialAmount'].setValidators([Validators.required]);
      this.editForm.controls['initialAmount'].updateValueAndValidity();

      this.editForm.controls['recurringAmount'].setValidators([Validators.required]);
      this.editForm.controls['recurringAmount'].updateValueAndValidity();

      this.editForm.controls['requiredBillingPeriods'].setValidators([Validators.required]);
      this.editForm.controls['requiredBillingPeriods'].updateValueAndValidity();
    }

    console.log(this.editForm);

    if (this.editForm.valid && this.categoriesSelected.length > 0 && this.addedSellingConstrains.length > 0) {
      const data = {
        'categorySet': [], // added later
        'cmsId': 9,
        'id': this.productGroupSelected.id,
        'price': this.editForm.value.price,
        'productGroupSubscriptionInfo': {
          'initialAmount': this.editForm.value.initialAmount,
          'recurringAmount': this.editForm.value.recurringAmount,
          'requiredBillingPeriods': this.editForm.value.requiredBillingPeriods
        },
        'sellingConstraints': [],
        'suppressIppvBillingPpvService': this.editForm.value.suppressIppvBillingPpvService,
        'titleDescription': this.editForm.value.titleDescription,
        // 'titleLabel': this.editForm.value.titleLabel,
        'validFrom': Math.round(this.editForm.value.validFrom / 1000),
        'validTo': Math.round(this.editForm.value.validTo / 1000)
      };

      // add title label if different
      if (this.editForm.value.titleLabel != this.productGroupSelected.titleLabel) {
        Object.assign(data, {titleLabel: this.editForm.value.titleLabel});
      }

      // add categorySet
      for (const cat of this.categoriesSelected) {
        data.categorySet.push(cat.id)
      }
      // add selling constrains
      for (const sC of this.addedSellingConstrains) {
        data.sellingConstraints.push(sC.name)
      }

      // remove fields if subscription or not
      if (!this.productGroupSelected.productTypeStats.toLowerCase().includes('subscription')) {
        delete data.productGroupSubscriptionInfo;
      } else {
        delete data.price
      }

      // Update Request
      this.http.put(this.globals.API_ENDPOINT + '/product-groups',  data, {responseType: 'text'})
        .subscribe(res => {
          console.log('response', this.editForm.value.sellable);
          // sellable ?
          if (this.editForm.value.sellable) {
            this.makeSellable(this.productGroupSelected.id, true, 'updated');
          } else {
            this.makeSellable(this.productGroupSelected.id, false, 'updated');
          }
          this.productGroupSelected = null;
          this.searchResults = null;
          this.categoriesSelected = [];
        }, err => {
          this.productGroupSelected = null;
          this.searchResults = null;
          err = JSON.parse(err.error);
          console.log(err);
          if (err.fieldErrors) {
            this.error = err.fieldErrors[0];
          } else {
            this.error = this.helpers.error(err.message);
          }
        });

    }
  }

  /**
   * Selling Constrains Checked
   */
  sCchecked(sC) {
    console.log('load');

    this.productGroupSelected.sellingConstraints.forEach(s => {
      console.log(sC.name, s);
      if (sC.name == s) {
        sC.checked = true;
      }
    });
    sC.checked = true;
  }
}
