import {Component, OnInit} from '@angular/core';
import {RestService} from '../../../services/rest.service';
import {HelpersService} from '../../../services/helpers.service';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../../app.global';
import {DragulaService} from 'ng2-dragula';

interface SalesItem {
  id: number;
  accountId: number;
  name: string;
  displayingOrder: number;
  productGroupResponseDtoList: Array<any>;
  packagesFound ?: Array<any>;
  active ?: boolean;
  createdAt: number;
}

@Component({
  selector: 'app-sales-items',
  templateUrl: './sales-items.component.html',
  styleUrls: ['./sales-items.component.css'],
  providers: [HelpersService, HttpClient, DragulaService]
})
export class SalesItemsComponent implements OnInit {
  error: any;
  success: any;
  filter: string;
  now = Date.now();

  changeName = false;
  newName: string;
  salesItems: Array<SalesItem>;
  availablePackages: Array<any>;
  availablePackagesFound: Array<any>;
  packagesToAdd: Array<any>;
  repackagesList: Array<any>;
  notSaved: boolean;
  displayOrder: number;
  displayValidToInThePast: boolean;

  // Dialogs
  savePackageDialog: any;
  createNewSalesItemDialog: any;
  deleteSalesItemDialog: any;
  addPackageDialog: any;
  changesSavedDialog: any;

  constructor(private rest: RestService, private helpers: HelpersService, private http: HttpClient, private globals: Globals,
              private dragulaService: DragulaService) {
      this.dragulaService.setOptions('first-bag', {});
      this.dragulaService.dropModel.subscribe((value) => {
      this.onDropModel(value);
    });
  }

  /**
   * Dragula
   * @param args
   */
  private onDropModel(args) {
    let [e, el] = args;


    // Update order
    this.salesItems.forEach((si, i) => {
      this.changeName = false;

      if (si.id == Number(el.id) - 99) { // +99 to avoid to identical ids in the html (sales-items.component.html line ~ 9 )

      const data = {
        'cmsId': 9,
        'salesItemGroupId': si.id,
        'displayingOrder': i + 1
      };

      this.http.put(this.globals.API_ENDPOINT + '/sales-item-groups/setDisplayingOrder', data, {responseType: 'text'})
        .subscribe(() => {
            this.getAllSalesItems()
        }, err => {
          err = JSON.parse(err.error);
          console.log(err);
          if (err.fieldErrors) {
            this.error = err.fieldErrors[0];
          } else {
            this.error = this.helpers.error(err.message);
          }
        })
    }
    })
  }

  ngOnInit() {
    // set display order option selected
    this.displayOrder = 1;
    // displayValidToInThePast
    this.displayValidToInThePast = false;
    // Get all sales items groups
    this.getAllSalesItems();
  }

  /**
   * Get All sales items
   * @param {number} active
   */
  getAllSalesItems(active?: number) {
    // sort sales items group array by display order
    function displayingOrder(a, b) {
      if (a.displayingOrder < b.displayingOrder) {return -1; }
      if (a.displayingOrder > b.displayingOrder) { return 1; }
      return 0;
    }
    // GET request
    this.rest.get('/sales-item-groups/findAllDto')
      .subscribe((res: any) => {
        this.salesItems = res;
        // sort sales items group array by display order
        this.salesItems.sort(displayingOrder);
        // assign active and original property
        this.salesItems.forEach((si, index) => {
          si.packagesFound = si.productGroupResponseDtoList;
          if (active) {
            si.active = index === active;
          } else {
            si.active = index === 0;
          }
          this.setOriginals(si);
        });
        this.notSaved = false;
        this.repackagesList = [];
        console.log(this.salesItems);
      }, err => {
        this.error = this.helpers.error(err.error.message);
      });
  }

  /**
   * Sales item packages originally from getSalesItems call.
   * Used to determine add / remove packages
   * @param si
   */
  setOriginals(si) {
    for (const pack of si.packagesFound) {
      pack.original = true;
    }
  }

  /**
   * Sales Item Package Filter
   * @param {SalesItem} si --> Sale item  group selected
   */
  filterPackages(si: SalesItem) {
    si.packagesFound = this.helpers.jsonFilter(si.productGroupResponseDtoList, this.filter);
  }

  /**
   * Reset values to default when change Sales Item group
   */
  resetToDefault() {
    this.displayValidToInThePast = false;
    this.changeName = false;
    this.filter = '';
    this.salesItems.forEach((si) => {
      si.packagesFound = si.productGroupResponseDtoList;
    });
  }

  /**
   * Save Sales Item Group Name
   * @param si --> sales item
   * @param i --> index
   */
  saveName(si, i) {
    this.changeName = false;
    const data = {
      'cmsId': 9,
      'id': si.id,
      'newName': this.newName,
    };

    this.http.put(this.globals.API_ENDPOINT + '/sales-item-groups', data, {responseType: 'text'})
      .subscribe(() => {
        this.getAllSalesItems(i)
      }, err => {
        err = JSON.parse(err.error);
        console.log(err);
        if (err.fieldErrors) {
          this.error = err.fieldErrors[0];
        } else {
          this.error = this.helpers.error(err.message);
        }
      })
  }

  /**
   * Remove Package from Sales item Group
   * @param si --> Sale Item Group
   * @param pdID --> Package to  removed
   */
  removePackage(si, pack) {
    this.notSaved = si;
    si.packagesFound.forEach((p, i) => {
      if (pack.original) {
        this.repackagesList.push(pack);
      }
      if (p.id === pack.id) {
        si.packagesFound.splice(i, 1);
        return;
      }
    })
  }

  /**
   * Create new sales item
   * @param name
   */
  createNewSalesItem(name) {
    // Close dialog
    this.createNewSalesItemDialog = null;

    const data = {
      'cmsId': 9,
      // 'displayingOrder': this.salesItems.length + 1,
      'name': name
    };

    // POST request
    this.rest.post('/sales-item-groups', data)
      .subscribe(res => {
        console.log(res);
        this.getAllSalesItems();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Delete Sales Item group
   * @param si
   */
  deleteSalesItem(si) {
    // Close dialog
    this.deleteSalesItemDialog = null;

    // Data
    const data = {
      'cmsId': 9,
      'id': si.id
    };

    // DELETE request
    this.rest.deleteWithBody('/sales-item-groups', data)
      .subscribe(res => {
        console.log(res);
        this.getAllSalesItems();
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Get available packages
   */
  getAvailablePackages(si) {
    // Open available packages dialog
    this.addPackageDialog = si;
    // reset packages
    this.packagesToAdd = [];
    this.availablePackages = [];

    // Get request
    this.rest.get('/product-groups/search-no-sales-item')
      .subscribe((res: any) => {
        this.availablePackages = res;
        this.availablePackagesFound = res;
      }, err => {
        this.error = this.helpers.error(err.error.message);
      })
  }

  /**
   * Add packages Filter
   */
  availablePackagesFilter(filter) {
    this.availablePackagesFound = this.helpers.jsonFilter(this.availablePackages, filter);
  }

  /**
   * Select packages to add
   * @param pg --> package clicked
   */
  selectPack(pg) {
    pg.selected = !pg.selected;
    if (pg.selected) {
      this.packagesToAdd.push(pg);
    } else {
      this.packagesToAdd.forEach((p, i) => {
        if (p.id === pg.id) {
          this.packagesToAdd.splice(i, 1);
          return;
        }
      })
    }
    console.log(this.packagesToAdd);
  }

  /**
   * Add packages selected to si packages list;
   * @param si --> sales item group
   */
  addPackages(si) {
    this.notSaved = si;
    si.packagesFound.push(...this.packagesToAdd);
    this.addPackageDialog = null;
  }

  /**
   * Save Changes 1/2
   * @param si --> sales item group
   */
  saveChanges({si, i}) {
    // close dialog
    this.savePackageDialog = null;

    let total = 0;
    total += this.repackagesList.length;
    total += si.packagesFound.length;

    // Add packages
    for (const pack of si.packagesFound) {
      total --;
      if (!pack.original) {
        // data to send
        const data = {
          'cmsId': 9,
          'productGroupId': pack.id,
          'salesItemGroupId': si.id
        };
        this.saveChangesRequest(data, total, i);
      }
    }
    // Remove packages
    for (const pack of this.repackagesList) {
      total --;
      // data to send
      const data = {
        'cmsId': 9,
        'productGroupId': pack.id,
        // 'salesItemGroupId': si.id
      };
      this.saveChangesRequest(data, total, i);
    }
  }

  /**
   * Save Changes request 2/2
   * @param data
   */
  saveChangesRequest(data, total, i) {
    // PUT request
    this.http.put(this.globals.API_ENDPOINT + '/product-groups/addExistingProductGroupToSaleItemGroup/', data, {responseType: 'text'})
      .subscribe(res => {
        console.log(res);
        if (total === 0) {
          this.success = {
            message: 'Packages have been saved'
          };
          this.getAllSalesItems(i);
          this.notSaved = false;
        }
      }, err => {
        err = JSON.parse(err.error);
        console.log(err);
        if (err.fieldErrors) {
          this.error = err.fieldErrors[0];
        } else {
          this.error = this.helpers.error(err.message);
        }
      })
  }

  /**
   * Check if changes in packages have been saved
   */
  changesSavedCheck() {
    if (this.notSaved) {
      this.salesItems.forEach((si, i) => {
        if (si.active) {
          this.changesSavedDialog = {si: this.notSaved, i: i };
          return;
        }
      })
    }
  }

}
