import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {mainViewComponent} from './views/main-view/main-view.component';
import {LoginComponent} from './views/login/login.component';
import {blankComponent} from './components/common/layouts/blank.component';
import {basicComponent} from './components/common/layouts/basic.component';
import {LoggedInGuard} from './services/logged-in.guard';
import {NewsubscriberComponent} from './views/subscribers/newsubscriber/newsubscriber.component';
import {JoinSubscriberComponent} from './views/subscribers/joinSubscriber/joinSubscriber.component';
import {JoinSubscriberEntitlementsComponent} from './views/subscribers/joinSubscriber/joinSubscriberEntitlements.component';
import {Process3Component, Process4Component} from './views/process/process3_4/process3_4.component';
import {Process5Component} from './views/process/process5/process5.component';
import {CustomerComponent} from './views/customer/customer.component';
import {Process8Component} from './views/process/process8/process8.component';
import {Process19Component} from './views/process/process19/process19.component';
import {Process20Component} from './views/process/process20/process20.component';
import {Process25Component} from './views/process/process25_26/process25.component';
import {Process27Component} from './views/process/process27/process27.component';
import {Process21Component} from './views/process/process21/process21.component';
import {Process23Component} from './views/process/process23/process23.component';
import {Process24Component} from './views/process/process24/process24.component';
import {Process32Component} from './views/process/process32/process32.component';
import {Process33Component} from './views/process/process33/process33.component';
import {Process16Component} from './views/process/process16/process16.component';
import {Process17Component} from './views/process/process17/process17.component';
import {EditCustomerComponent} from './views/customer/editCustomer.component';
import {NewsubscriberEntitlementsComponent} from './views/subscribers/newsubscriber/newsubscriberEntitlements.component';
import {Process28Component} from './views/process/process28/process28.component';
import {AdminRequiredService} from './services/adminRequired.service';
import {StatsComponent} from './views/stats/stats.component';
import {HistoriesComponent} from './views/histories/histories.component';
import {RecentAccountsComponent} from './views/recent-accounts/recent-accounts.component';
import {AdminPanelComponent} from './views/admin-panel/admin-panel.component';
import {AdvancedSearchComponent} from './views/search/advanced-search/advanced-search.component';
import {PasswordEmailComponent} from './views/login/password-email/password-email.component';
import {PasswordRecoveryComponent} from './views/login/password-recovery/password-recovery.component';
import {Process29Component} from './views/process/process29/process29.component';
import {Process30Component} from './views/process/process30/process30.component';
import {SavePaymentComponent} from './views/save-payment/save-payment.component';
import {NewJoinPaymentComponent} from './views/new-join-payment/new-join-payment.component';
import {PaymentIframeComponent} from './views/payment-iframe/payment-iframe.component';
import {SearchComponent} from './views/search/search.component';


export const ROUTES: Routes = [
  // Main redirect
  { path: '', component: LoginComponent, pathMatch: 'full' },
  { path: 'password-email', component: PasswordEmailComponent, pathMatch: 'full' },
  { path: 'password-recovery', component: PasswordRecoveryComponent, pathMatch: 'full' },
  // { path: 'mainView', component: mainViewComponent,  canActivate: [LoggedInGuard]},

  {path: 'save-payment', component: SavePaymentComponent},
  {path: 'new-payment', component: NewJoinPaymentComponent},
  // App views
  {
    path: '',
    component: basicComponent,
    canActivate: [LoggedInGuard],
    children: [
      {path: 'mainView', component: mainViewComponent},
      {path: 'customer/:id', component: CustomerComponent},
      {path: 'editCustomer', component: EditCustomerComponent},
      {path: 'payment', component: PaymentIframeComponent},
      {path: 'newsubscriber', component: NewsubscriberComponent},
      {path: 'newsubscriberEntitlements', component: NewsubscriberEntitlementsComponent},
      {path: 'joinSubscriber', component: JoinSubscriberComponent},
      {path: 'joinSubscriberEntitlements', component: JoinSubscriberEntitlementsComponent},
      // {path: 'search', loadChildren: './views/subscribers/search/search.module#SearchViewModule'},
      {path: 'search', component: SearchComponent},
      {path: 'advanced-search', component: AdvancedSearchComponent},
      {path: 'results', loadChildren: './views/search/searchResults/results.module#ResultsViewModule'},
      {path: 'entitlements', loadChildren: './views/subscribers/viewSubscriber/changePackages.module#EntitlementsViewModule'},
      {path: 'viewSubscriber/:id', loadChildren: './views/subscribers/viewSubscriber/viewSubscriber.module#ViewSubscriberViewModule'},
      {path: 'billingHistory/:id', loadChildren: './views/billing-history/billing-history.module#BillingHistoryViewModule'},
      // {path: 'billingHistory/:id', loadChildren: './views/billing-history/billing-history.module#BillingHistoryViewModule',
      //   canActivate: [AdminRequiredService]},
      {path: 'process4', component: Process4Component},
      {path: 'process3', component: Process3Component},
      {path: 'process5', component: Process5Component},
      {path: 'process8', component: Process8Component},
      {path: 'process16', component: Process16Component},
      {path: 'process17', component: Process17Component},
      {path: 'process19', component: Process19Component},
      {path: 'process20', component: Process20Component},
      {path: 'process21', component: Process21Component},
      {path: 'process23', component: Process23Component},
      {path: 'process24', component: Process24Component},
      {path: 'process25/:u', component: Process25Component},
      {path: 'process27', component: Process27Component},
      {path: 'process28', component: Process28Component},
      {path: 'process29', component: Process29Component},
      {path: 'process30', component: Process30Component},
      {path: 'process32', component: Process32Component},
      {path: 'process33', component: Process33Component},
      {path: 'stats', component: StatsComponent,  canActivate: [AdminRequiredService]},
      {path: 'recent-accounts', component: RecentAccountsComponent},
      {path: 'histories/:id', component: HistoriesComponent},
      {path: 'admin-panel', component: AdminPanelComponent, canActivate: [AdminRequiredService]},
    ]
  },
  {
    path: '', component: blankComponent, canActivate: [LoggedInGuard],
    children: [
      { path: 'login', component: LoginComponent },
    ]
  },

  // Handle all other routes
  { path: '**', redirectTo: '' }
];

export const appRouter: ModuleWithProviders = RouterModule.forRoot(ROUTES);

