"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var animations_1 = require("@angular/platform-browser/animations");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var http_service_1 = require("./services/http.service");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var app_routes_1 = require("./app.routes");
var app_component_1 = require("./app.component");
// App views
var main_view_module_1 = require("./views/main-view/main-view.module");
var newsubscriber_module_1 = require("./views/subscribers/newsubscriber/newsubscriber.module");
var joinSubscriber_module_1 = require("./views/subscribers/joinSubscriber/joinSubscriber.module");
var register_module_1 = require("./views/register/register.module");
var login_module_1 = require("./views/login/login.module");
var logged_in_guard_1 = require("./services/logged-in.guard");
var user_service_1 = require("./services/user.service");
var subscribers_service_1 = require("./services/subscribers.service");
// App modules/components
var layouts_module_1 = require("./components/common/layouts/layouts.module");
var process_module_1 = require("./views/process/process.module");
var customer_module_1 = require("./views/customer/customer.module");
var angular_datatables_1 = require("angular-datatables");
var advancedSearch_module_1 = require("./views/subscribers/advancedSearch/advancedSearch.module");
// import { DialogsComponent } from './src/app/components/common/dialogs/dialogs.component';
var keepalive_1 = require("@ng-idle/keepalive");
var angular2_moment_1 = require("angular2-moment");
var adminRequired_service_1 = require("./services/adminRequired.service");
var stats_component_1 = require("./views/stats/stats.component");
var ppv_stats_component_1 = require("./views/ppv-stats/ppv-stats.component");
var angular_highcharts_1 = require("angular-highcharts");
var material_1 = require("@angular/material");
var histories_component_1 = require("./views/histories/histories.component");
var ngx_pagination_1 = require("ngx-pagination");
var highlightReverse_pipe_1 = require("./highlightReverse.pipe");
function useFactory(backend, options) {
    return new http_service_1.HttpService(backend, options);
}
exports.useFactory = useFactory;
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        core_1.NgModule({
            exports: [
                material_1.MatTooltipModule,
                material_1.MatInputModule,
                material_1.MatButtonModule,
                material_1.MatSidenavModule,
                material_1.MatSlideToggleModule,
                material_1.MatDatepickerModule,
                material_1.MatNativeDateModule
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());
exports.MaterialModule = MaterialModule;
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                stats_component_1.StatsComponent,
                ppv_stats_component_1.PpvStatsComponent,
                histories_component_1.HistoriesComponent,
                highlightReverse_pipe_1.CapitalizePipe
                // BillingHistoryComponent,
                // DialogsComponent,
            ],
            imports: [
                // Angular modules
                platform_browser_1.BrowserModule,
                animations_1.BrowserAnimationsModule,
                http_1.HttpModule,
                forms_1.FormsModule,
                router_1.RouterModule,
                forms_1.ReactiveFormsModule,
                // Views
                customer_module_1.CustomerViewModule,
                newsubscriber_module_1.NewSubscriberViewModule,
                joinSubscriber_module_1.JoinSubscriberViewModule,
                advancedSearch_module_1.AdvancedSearchViewModule,
                main_view_module_1.MainViewModule,
                login_module_1.LoginModule,
                process_module_1.ProcessViewModule,
                // BillingHistoryViewModule,
                // ViewSubscriberViewModule,
                register_module_1.RegisterModule,
                angular_datatables_1.DataTablesModule,
                // Modules
                layouts_module_1.LayoutsModule,
                app_routes_1.appRouter,
                keepalive_1.NgIdleKeepaliveModule.forRoot(),
                angular2_moment_1.MomentModule,
                angular_highcharts_1.ChartModule,
                MaterialModule,
                ngx_pagination_1.NgxPaginationModule,
            ],
            exports: [],
            providers: [
                { provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy },
                {
                    provide: http_service_1.HttpService,
                    deps: [http_1.XHRBackend, http_1.RequestOptions],
                    useFactory: useFactory
                },
                logged_in_guard_1.LoggedInGuard, user_service_1.UserService, subscribers_service_1.SubscribersService, adminRequired_service_1.AdminRequiredService
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
// platformBrowserDynamic().bootstrapModule(AppModule);
