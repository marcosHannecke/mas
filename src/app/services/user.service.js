"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// user.service.ts
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var app_settings_1 = require("../app.settings");
var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.loggedIn = false;
        this.loggedIn = !!sessionStorage.getItem('auth_token');
        this.Authorization = 'Basic cmVzdDpDUFQ3bnhCMzhIZjdQQnY=';
    }
    UserService.prototype.login = function (username, password) {
        var _this = this;
        if (username == "admin") {
            sessionStorage.setItem('username', 'admin');
        }
        sessionStorage.setItem('user', username);
        var creds = 'client_id=rest'
            + '&grant_type=password&username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password);
        // Headers
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
        headers.append('Authorization', this.Authorization);
        return this.http.post(app_settings_1.AppSettings.API_IP + '/oauth/token', creds, { headers: headers })
            .map(function (res) { return res.json(); })
            .map(function (res) {
            console.log('res', res);
            if (res.access_token) {
                sessionStorage.setItem('auth_token', res.access_token);
                sessionStorage.setItem('expires_in', res.expires_in);
                sessionStorage.setItem('refresh_token', res.refresh_token);
                _this.loggedIn = true;
            }
            return res.access_token;
        });
    };
    UserService.prototype.tokenRefresh = function () {
        var _this = this;
        var token = sessionStorage.getItem('refresh_token');
        console.log(token);
        var refreshToken = 'grant_type=refresh_token&refresh_token=' + encodeURIComponent(sessionStorage.getItem('refresh_token'));
        // Headers
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
        headers.append('Authorization', this.Authorization);
        return this.http.post(app_settings_1.AppSettings.API_IP + '/oauth/token', refreshToken, { headers: headers })
            .map(function (res) { return res.json(); })
            .map(function (res) {
            console.log('res', res);
            if (res.access_token) {
                sessionStorage.setItem('auth_token', res.access_token);
                sessionStorage.setItem('expires_in', res.expires_in);
                sessionStorage.setItem('refresh_token', res.refresh_token);
                _this.loggedIn = true;
            }
            return res.access_token;
        });
    };
    // updateToken(): Observable<boolean> {
    //   // let body: string = 'refresh_token=' + this.refreshToken + '&grant_type=refresh_token';
    //   //
    //   // return this.http.post(tokenEndPointUrl, body, this.options)
    //   //   .map((response: Response) => {
    //   //     var returnedBody: any = response.json();
    //   //     if (typeof returnedBody.access_token !== 'undefined'){
    //   //       localStorage.setItem(this.tokenKey, returnedBody.access_token);
    //   //       localStorage.setItem(this.refreshTokenKey, returnedBody.refresh_token);
    //   //       return true;
    //   //     }
    //   //     else {
    //   //       return false;
    //   //     }
    //   //   })
    // }
    UserService.prototype.logout = function () {
        sessionStorage.clear();
        this.loggedIn = false;
        console.log(this.loggedIn);
    };
    UserService.prototype.loggedInCheck = function () {
        //   return this.loggedIn;
        // return tokenNotExpired();
    };
    UserService = __decorate([
        core_1.Injectable()
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
