"use strict";
/**
 * helpers.service.ts
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var app_settings_1 = require("../app.settings");
var HelpersService = /** @class */ (function () {
    function HelpersService(router, http) {
        this.router = router;
        this.http = http;
        this.headers = new http_1.Headers();
        this.headers.append('Content-Type', 'application/json');
        this.options = new http_1.RequestOptions({ headers: this.headers });
    }
    HelpersService.prototype.getRole = function () {
        return this.http.get(app_settings_1.AppSettings.API_IP + '/users/about-me', this.options)
            .map(function (res) {
            return res;
        });
    };
    HelpersService.prototype.adminRequired = function (user, pass) {
        return this.http.get(app_settings_1.AppSettings.API_IP + '/users/about-user?username='
            + encodeURIComponent(user) + '&password=' + encodeURIComponent(pass), this.options)
            .map(function (res) {
            return res;
        });
    };
    // RESET SESSION STORAGE
    HelpersService.prototype.resetSession = function () {
        var SS = sessionStorage;
        for (var item in SS) {
            if (item != 'auth_token' && item != 'role' && item != 'expires_in' && item != 'refresh_token' && item != 'username' && item != 'user') {
                sessionStorage.removeItem(item);
            }
        }
    };
    // RESET ROUTE
    HelpersService.prototype.reset = function () {
        var _this = this;
        var path = this.router.url;
        this.router.navigateByUrl('blank').then(function () {
            _this.router.navigateByUrl(path);
        });
    };
    // EMPTY SUBSCRIBER
    HelpersService.prototype.emptySubscriber = function () {
        var emptySubscriber = {
            "cardHolderType": '',
            "title": 'Mr',
            "initials": '',
            "surname": '',
            "addressLine1": '',
            "addressLine2": '',
            "addressLine3": '',
            "addressLine4": '',
            "addressLine5": '',
            "postcode": '',
            "countryCode": 'GBR',
            "workTelNr": '',
            "mobTelNr": '',
            "cliTelNr": '',
            "currencyCode": 'GBP',
            "phones": '',
            "cardSubscriberId": '',
            "productGroupNames": '',
            "viewingCardNumber": '',
            "email": '',
            "customerId": '',
        };
        return emptySubscriber;
    };
    // NULL CHECK
    HelpersService.prototype.nullCheck = function (obj) {
        for (var _i = 0, _a = Object.keys(obj); _i < _a.length; _i++) {
            var key = _a[_i];
            if (obj[key] == null) {
                obj[key] = '';
            }
        }
        return obj;
    };
    HelpersService.prototype.getError = function (error) {
        var err = JSON.parse(error['_body']);
        err = err.message.split('}');
        err = err[0].split('{');
        err = {
            message: err[1]
        };
        return err;
    };
    HelpersService.prototype.scrollTop = function () {
        setTimeout(function () {
            window.scrollTo(0, 0);
        }, 50);
    };
    HelpersService.prototype.alphabetical = function (a, b) {
        console.log('here');
        // if(a.titleDescription < b.titleDescription)
        //   return -1;
        // if(a.titleDescription > b.titleDescription)
        //   return 1;
        // return 0;
        var sortOrder = [
            'TV Xcellence (New GBP Customer)',
            'TV Xcellence (Reinstate GBP Customer)',
            'TV Xcellence Annual (New GBP Customer)',
            'TV Xcellence Annual (Reinstate GBP Customer)',
            'TV Xcellence (GBP Customer Retention1)',
            'TV Xcellence (GBP Customer Retention2)',
            'Premier (New GBP Customer)',
            'Premier (Reinstate GBP Customer)',
            'Premier Annual (New GBP Customer)',
            'Premier Annual (Reinstate GBP Customer)',
            'Premier (GBP Customer Retention1)',
            'Premier (GBP Customer Retention2)',
            'Red Hot Monthly (New GBP Customer)',
            'Red Hot Monthly (Reinstate GBP Customer)',
            'Red Hot Monthly (GBP Customer Retention)',
            'Pay Per Night (New GBP Customer)'
        ];
        return sortOrder.indexOf(a.titleDescription) - sortOrder.indexOf(b.titleDescription);
    };
    HelpersService.prototype.sellableOrder = function (a, b) {
        if (a.titleDescription < b.titleDescription)
            return -1;
        if (a.titleDescription > b.titleDescription)
            return 1;
        return 0;
    };
    HelpersService.prototype.validFromOrder = function (a, b) {
        if (a[0].validFrom < b[0].validFrom)
            return -1;
        if (a[0].validFrom > b[0].validFrom)
            return 1;
        return 0;
    };
    HelpersService.prototype.removeSpecialCharacters = function (obj) {
        for (var o in obj) {
            if (obj[o] != null) {
                obj[o] = obj[o].toString();
                obj[o] = obj[o].replace(/[^a-zA-Z 0-9]+/g, '');
            }
        }
        return obj;
    };
    HelpersService = __decorate([
        core_1.Injectable()
    ], HelpersService);
    return HelpersService;
}());
exports.HelpersService = HelpersService;
