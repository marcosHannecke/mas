// user.service.ts
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import {Globals} from '../app.global';
// import { tokenNotExpired } from 'angular2-jwt';
import {Observable} from 'rxjs/Rx';
import {catchError} from 'rxjs/operators';

@Injectable()
export class UserService {
  private loggedIn = false;
  readonly Authorization: string;
  private expires: any;

  constructor(private http: HttpClient, private globals: Globals) {
    this.loggedIn = !!sessionStorage.getItem('auth_token');
    this.Authorization = 'Basic cmVzdDpDUFQ3bnhCMzhIZjdQQnY=';
  }

  /**
   * Login service
   * @param {string} username
   * @param {string} password
   * @returns {Observable<Object>}
   */
   login(username: string, password: string): Observable<any> {
    // Store username in sessionStorage
    if (username === 'admin') {
      sessionStorage.setItem('username', 'admin')
    }
    sessionStorage.setItem('user', username);

    const creds = 'client_id=rest'
      + '&grant_type=password&username=' + encodeURIComponent(username) + '&password=' + encodeURIComponent(password);

     const httpOptions = {
       headers: new HttpHeaders({
         'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
         'Authorization':  this.Authorization
       })
     };
     return this.http.post(this.globals.API_IP + '/oauth/token',  creds, httpOptions)
       .shareReplay();
  }

  /**
   * Refresh Token
   * @returns {Observable<Object>}
   */
  tokenRefresh() {
    const refreshToken = 'grant_type=refresh_token&refresh_token=' + encodeURIComponent(sessionStorage.getItem('refresh_token'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        'Authorization':  this.Authorization
      }),
    };

    return this.http.post(this.globals.API_IP + '/oauth/token',  refreshToken, httpOptions)
    .subscribe((res) => {
      console.log('res', res);
      if (res['access_token']) {
        sessionStorage.setItem('auth_token', res['access_token']);
        sessionStorage.setItem('expires_in', res['expires_in']);
        sessionStorage.setItem('refresh_token', res['refresh_token']);
        this.loggedIn = true;
      }
    })
  }

  logout() {
    sessionStorage.clear();
    this.loggedIn = false;
    console.log(this.loggedIn);
  }

  loggedInCheck() {
    //   return this.loggedIn;
    // return tokenNotExpired();
  }


}
