"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AdminRequiredService = /** @class */ (function () {
    // private expires : any;
    function AdminRequiredService(router, user) {
        this.router = router;
        this.user = user;
    }
    // canActivate() {
    //   return this.user.isLoggedIn();
    // }
    AdminRequiredService.prototype.canActivate = function () {
        if (sessionStorage.getItem('role') == 'admin') {
            // admin in so return true
            return true;
        }
        // not admin in so redirect to login page
        this.router.navigate(['/']);
        return false;
    };
    AdminRequiredService = __decorate([
        core_1.Injectable()
    ], AdminRequiredService);
    return AdminRequiredService;
}());
exports.AdminRequiredService = AdminRequiredService;
