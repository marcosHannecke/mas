import {Injectable} from '@angular/core';
import {Globals} from '../app.global';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class PostcodeService {

  constructor( private globals: Globals, private http: HttpClient) {}

  /**
   * GET ADDRESS BY POSTCODE
   * @param pC --> postcode
   * @returns {Observable<Object>}
   */
  postcode(pC) {
    return this.http.get(this.globals.API_ENDPOINT + '/postcode?postcode=' + pC, {responseType: 'json'});
  }

  /**
   * FORMAT POSTCODE
   * @param pC --> postcode
   * @returns {any}
   */
  postcodeFormat(pC) {
    pC = pC.toUpperCase();
    const exceptions = [ 'UK', 'ROI', 'IOM', 'CI', 'BFPO' ];

    // If Postcode Exception type, return
    exceptions.forEach(exp =>  {
      if (pC ===  exp)  {
        return pC;
      }
    });

    // If length is < 4, return
    if (pC.length < 4) {
      return pC;
    }

    const suffix = pC.substring(pC.length - 3);
    const prefix = pC.substring(0, pC.length - 3);
    return prefix + ' ' + suffix;
  }

}

