/**
 * Service to load stats KPIS
 */

import { Injectable } from '@angular/core';
import {RestService} from './rest.service';
import {Globals} from '../app.global';
import {HelpersService} from './helpers.service';

@Injectable()
export class DashboardService {
  date = Math.floor(Date.now() / 1000);

  constructor(private rest: RestService, private gb: Globals, private helpers: HelpersService) { }

  /**
   * 1.2.3 Trends -> Subscriptions, Registered PPN, Active PPN
   */

  getSRADefault(date, lastValue ?) {   // Default calculation
    console.log(date);
    date = new Date(date);
    date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
    date = Math.floor(date / 1000);

     return this.rest.getNoSimplified('/report/trend/compared-with-previous-month?&timestamp='
      + (date) + '&breakdown=operation&breakdown=day&breakdown=type')
      .subscribe( res => {
        sessionStorage.setItem('dashboardTrends', JSON.stringify(res));
      }, err => {
        if (lastValue) {
          sessionStorage.setItem('dashboardTrends', lastValue);
        }
        console.log('dashboardKPIsError', err)
      });
  }

   getSRA(date, lastValue ?) {   // Calculation with datepicker
    console.log('firstDayCurrentMonth', date);
    date = new Date(date);
    date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
    date = Math.floor(date / 1000);

    // date = new Date(date.getFullYear(), date.getMonth(), 0);
    return this.rest.getNoSimplified('/report/trend?compareToTimestamp=' + date + '&timestamp=' + (+ this.gb.yesterday / 1000)
      + '&breakdown=operation&breakdown=day&breakdown=type')
      .subscribe( res => {
        sessionStorage.setItem('dashboardTrends', JSON.stringify(res));
      }, err => {
        if (lastValue) {
          sessionStorage.setItem('dashboardTrends', lastValue);
        }
        console.log('dashboardKPIsError', err)
      });
  }

  /**
   * 4.Monthly ARPU
   * @param lastValue: last value saved in case of error
   */
  getMonthlyARPUDefault(date, lastValue ?) {
    date = Math.floor(date / 1000);
    this.rest.getNoSimplified('/report/arpu-monthly-trend/compared-with-previous-month?&timestamp=' + date
      + '&breakdown=day&breakdown=operation&breakdown=type')
      .subscribe( res => {
        sessionStorage.setItem('dashboardMonthlyArpu', JSON.stringify(res));
      }, err => {
        if (lastValue) {
          sessionStorage.setItem('dashboardMonthlyArpu', lastValue);
        }
        console.log('dashboardKPIsError', err)
      });
  }

  getMonthlyARPU(date, lastValue ?) {
    let now = new Date(Date.now());
    now = new Date(Date.UTC( now.getFullYear(), now.getMonth(), 0));
    this.rest.getNoSimplified('/report/arpu-monthly-trend?compareToTimestamp=' + (+ date / 1000 ) + '&timestamp=' + (+ now / 1000 ) +
      '&breakdown=day&breakdown=operation&breakdown=type')
      .subscribe( res => {
        sessionStorage.setItem('dashboardMonthlyArpu', JSON.stringify(res));
      }, err => {
        if (lastValue) {
          sessionStorage.setItem('dashboardMonthlyArpu', lastValue);
        }
        console.log('dashboardKPIsError', err)
      });
  }

  /**
   * MonthlyARPU graph
   */
  getMonthlyARPUGraph() {
    // this.rest.getNoSimplified('/report/business-volume-graph?&timestamp=' + (this.date - this.gb.ONE_DAY_SECONDS  )
    this.rest.getNoSimplified('/report/arpu-monthly-graph?&timestamp=' + (+ this.gb.lastDayPreviousOfMonth / 1000) +
      '&breakdown=day&breakdown=country')
      .subscribe( res => {
        sessionStorage.setItem('dashboardARPUGraph', JSON.stringify(res));
      }, err => {
        console.log('dashboardKPIsError', err)
      });
  }
  /**
   * 5.Annual ARPU
   * @param lastValue: last value saved in case of error
   */
  getAnnualARPUDefault( lastValue ?) {
    this.rest.getNoSimplified('/report/arpu-annual-trend/compared-with-previous-month?&timestamp=' + (+ this.gb.lastDayPreviousOfMonth / 1000)
      + '&breakdown=day&breakdown=operation&breakdown=type')
      .subscribe( res => {
        sessionStorage.setItem('dashboardAnnualArpu', JSON.stringify(res));
      }, err => {
        if (lastValue) {
          sessionStorage.setItem('dashboardAnnualArpu', lastValue);
        }
        console.log('dashboardKPIsError', err)
      });
  }

  getAnnualARPU(date, lastValue ?) {
    let now = new Date(Date.now());
    now = new Date(Date.UTC( now.getFullYear(), now.getMonth(), 0));
    this.rest.getNoSimplified('/report/arpu-annual-trend?compareToTimestamp=' + (+ date / 1000 ) + '&timestamp=' + (+ now / 1000 ) +
      '&breakdown=day&breakdown=operation&breakdown=type')
      .subscribe( res => {
        sessionStorage.setItem('dashboardAnnualArpu', JSON.stringify(res));
      }, err => {
        if (lastValue) {
          sessionStorage.setItem('dashboardAnnualArpu', lastValue);
        }
        console.log('dashboardKPIsError', err)
      });
  }


  // aagetAnnualARPU(date, lastValue ?) {
  //   date = new Date(date);
  //   date = new Date(date.getFullYear(), date.getMonth(), 0);
  //   this.rest.getNoSimplified('/report/arpu-annual/compared-with-previous-month?&timestamp=' + (+ this.gb.lastDayPreviousOfYear / 1000 )
  //     + '&breakdown=day&breakdown=operation&breakdown=type')
  //     .subscribe( res => {
  //       sessionStorage.setItem('dashboardAnnualArpu', JSON.stringify(res));
  //     }, err => {
  //       if (lastValue) {
  //         sessionStorage.setItem('dashboardAnnualArpu', lastValue);
  //       }
  //       console.log('dashboardKPIsError', err)
  //     });
  // }

  /**
   * 6.Monthly Churn
   * @param lastValue: last value saved in case of error
   */
  getMonthlyChurnDefault(date, lastValue ?) {
    date = new Date(date);
    date = new Date(Date.UTC( date.getFullYear(), date.getMonth(), 0));
    date = Math.floor(date / 1000);
    this.rest.getNoSimplified('/report/churn?&timestamp=' + date
      + '&breakdown=type&breakdown=operation')
      .subscribe( res => {
        sessionStorage.setItem('dashboardMonthlyChurn', JSON.stringify(res));
      }, err => {
        if (lastValue) {
          sessionStorage.setItem('dashboardMonthlyChurn', lastValue);
        }
        console.log('dashboardKPIsError', err);
      });
  }

  getMonthlyChurn(date, lastValue ?) {
   let now = new Date(Date.now());
    now = new Date(Date.UTC( now.getFullYear(), now.getMonth(), 0));

    date = new Date(date);
    date = new Date(Date.UTC( date.getFullYear(), date.getMonth(), 0));

    this.rest.getNoSimplified( '/report/churn?compareToTimestamp=' + (+ date / 1000 ) + '&timestamp=' +  (+ now / 1000 )
      + '&breakdown=type&breakdown=operation')
      .subscribe( res => {
        sessionStorage.setItem('dashboardMonthlyChurn', JSON.stringify(res));
      }, err => {
        if (lastValue) {
          sessionStorage.setItem('dashboardMonthlyChurn', lastValue);
        }
        console.log('dashboardKPIsError', err);
      });
  }

  /**
   * Monthly Churn graph
   */
  getMonthlyChurnGraph() {
    // this.rest.getNoSimplified('/report/business-volume-graph?&timestamp=' + (this.date - this.gb.ONE_DAY_SECONDS  )
    this.rest.getNoSimplified('/report/churn-graph?&timestamp=' + (+ this.gb.lastDayPreviousOfMonth / 1000) +
      '&breakdown=day&breakdown=operation')
      .subscribe( res => {
        sessionStorage.setItem('dashboardMonthlyChurnGraph', JSON.stringify(res));
      }, err => {
        console.log('dashboardKPIsError', err)
      });
  }
  /**
   * 7.Monthly Revenue
   * @param lastValue: last value saved in case of error
   */
  getMRDefault(date, lastValue ?) {
    date = Math.floor(date / 1000);
    this.rest.getNoSimplified('/report/revenue-trend/compared-with-previous-month?&timestamp=' + date
      + '&breakdown=day&breakdown=operation&breakdown=type')
      .subscribe( res => {
        sessionStorage.setItem('dashboardMonthlyR', JSON.stringify(res));
      }, err => {
        if (lastValue) {
          sessionStorage.setItem('dashboardMonthlyR', lastValue);
        }
        console.log('dashboardKPIsError', err)
      });
  }

  getMR(date, lastValue ?) {
    this.rest.getNoSimplified('/report/revenue-trend?compareToTimestamp=' + (+ date / 1000 ) + '&timestamp='
      + (+ this.gb.lastDayPreviousOfMonth / 1000) + '&breakdown=day&breakdown=operation&breakdown=type')
      .subscribe( res => {
        sessionStorage.setItem('dashboardMonthlyR', JSON.stringify(res));
      }, err => {
        if (lastValue) {
          sessionStorage.setItem('dashboardMonthlyR', lastValue);
        }
        console.log('dashboardKPIsError', err)
      });
  }

  /**
   * 8.Business volume Graph
   */
  getBusinessVolume() {
    // this.rest.getNoSimplified('/report/business-volume-graph?&timestamp=' + (this.date - this.gb.ONE_DAY_SECONDS  )
    this.rest.getNoSimplified('/report/business-volume-graph?&timestamp=' + (this.gb.yesterdayMilliseconds / 1000  )
      + '&breakdown=type&breakdown=day&breakdown=country&breakdown=operation')
      .subscribe( res => {
        sessionStorage.setItem('dashboardBusinessVolume', JSON.stringify(res));
      }, err => {
        console.log('dashboardKPIsError', err)
      });
  }

  /**
   * 9.Monthly Revenue Graph
   */
  getMonthlyRevenue() {
    // this.rest.getNoSimplified('/report/revenue?&timestamp=' + (this.date - this.gb.ONE_DAY_SECONDS )
    this.rest.getNoSimplified('/report/revenue?&timestamp=' + (this.gb.yesterdayMilliseconds / 1000 )
      + '&breakdown=operation&breakdown=day&breakdown=country')
      .subscribe( res => {
        sessionStorage.setItem('dashboardMonthlyRevenue', JSON.stringify(res));
      }, err => {
        console.log('dashboardKPIsError', err)
      });
  }

}
