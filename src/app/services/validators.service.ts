// CUSTOM VALIDATION
import {Injectable} from '@angular/core';
import {  FormControl } from '@angular/forms';

@Injectable()
export class ValidatorsService {
  constructor() {}

    // Only numbers
    onlyNumbers(val: FormControl): any {
      if (val.value.match(/.*[^0-9].*/)) {
        return {onlyNumbers : true};
      }
      return null;
    }

    // ZeroHundred
    ZeroHundred(val: FormControl): any {
      if ( Number(val.value) > 100 || Number(val.value) < 0 ) {
        return {ZeroHundred : true};
      }
      return null;
    }

    // Only money
    onlyMoney(val: FormControl): any {
      if (!val.value.match(/(?=.*\d)(([1-9]\d{0,2}(,\d{3})*)|0)?(\.\d{1,2})?$/)) {
        return {onlyMoney : true};
      }
      return null;
    }

    // Special characters not allowed
    specialChar(val: FormControl): any {
      if (val.value.match(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/)) {
        return {specialChar : true};
      }
      return null;
    }

    // force special characters
    forceSpecialChar(val: FormControl): any {
      if (!val.value.match(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/)) {
        return {forceSpecialChar : true};
      }
      return null;
    }

    // Check for letters and spaces
    letterSpaces(val: FormControl): any {
      if (!val.value.match(/^[a-zA-Z\s]*$/)) {
        return {letterSpaces : true};
      }
      return null;
    }

    // Valid a least 1 of 3
    phones(group: FormControl)  {
      if (group.value.homeTelNr.length > 0  || group.value.workTelNr.length > 0  || group.value.mobTelNr.length > 0 ) {
        return null;
      }
      return {valid : true};
    }

    // Email
    email( val: FormControl ): any {
      if (!val.value.match(/\S+@\S+\.\S+/)) {
        return {email : true};
      }
      return null;
    }
}

