import {Globals} from '../app.global';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs/Observable';
import {Injectable} from "@angular/core";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService, private globals: Globals) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('this.auth.isAuthenticated()', this.auth.isAuthenticated());
    // Add token bearer unless exceptions
    if (request.url !== this.globals.URL +  '/vca-rest-api/oauth/token' && request.url.search('/forgotten-password') === -1) {
      // if logged in
      if (this.auth.isAuthenticated()) {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${this.auth.getToken()}`
          },
        });
      }
    }
    return next.handle(request)
  }
}
/*************************************************************************************
 * Httpclient explanation link
 * https://blog.hackages.io/angular-http-httpclient-same-but-different-86a50bbcc450
 *************************************************************************************/
