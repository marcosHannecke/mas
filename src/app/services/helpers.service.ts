/**
 * helpers.service.ts
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { RestService} from './rest.service';
import {Globals} from '../app.global';

@Injectable()
export class HelpersService {
  constructor(private router: Router, private  http: HttpClient, private rest: RestService, private globals: Globals) {}

  getRole() {
    return this.http.get(this.globals.API_IP + '/principals/about-me', {responseType: 'json'})
  }

  adminRequired(user, pass) {
    return this.http.get(this.globals.API_IP + '/principals/about-user?username='
      + encodeURIComponent(user) + '&password=' + encodeURIComponent(pass), {responseType: 'json'})
  }

  // RESET SESSION STORAGE
  resetSession() {
    for (const item in sessionStorage) {
      if (item !== 'auth_token' && item !== 'role' && item !== 'expires_in' && item
        !== 'refresh_token' && item !== 'username' && item !== 'user' && item !== 'notifications' && item !== 'environment'
        && item !== 'dashboardTrends' && item !== 'dashboardMonthlyRevenue' && item !== 'dashboardMonthlyChurn'
        && item !== 'dashboardAnnualArpu' && item !== 'dashboardMonthlyArpu' && item !== 'dashboardBusinessVolume'
        && item !== 'dashboardMonthlyR') {
        sessionStorage.removeItem(item)
      }
    }
  }
  // RESET ROUTE
  reset() {
    const path = this.router.url;
    this.router.navigateByUrl('blank').then(() => {
      this.router.navigateByUrl(path);
    })
  }

  // EMPTY SUBSCRIBER
  emptySubscriber() {
    return {
      'cardHolderType': '',
      'title': 'Mr',
      'initials': '',
      'surname': '',
      'addressLine1': '',
      'addressLine2': '',
      'addressLine3': '',
      'addressLine4': '',
      'addressLine5': '',
      'postcode': '',
      'countryCode': 'GBR',
      'workTelNr': '',
      'mobTelNr': '',
      'cliTelNr': '',
      'currencyCode': 'GBP',
      'phones': '',
      'cardSubscriberId' : '',
      'productGroupNames' : '',
      'viewingCardNumber': '',
      'email': '',
      'customerId': '',
    };
  }

  // NULL CHECK
  nullCheck(obj) {
    for (const key of Object.keys(obj)) {
      if (obj[key] == null) {
        obj[key] = '';
      }
    }
    return obj;
  }

  getError(error) {
    let err = JSON.parse(error['_body']);
    err = err.message.split('}');
    err = err[0].split('{');
    err = {
      message: err[1]
    };
    return err;
  }
  error(error) {
    error = error.split('}');
    error = error[0].split('{');
    error = {
      message: error[1]
    };
    return error;
  }
  errorDetail(error) {
    const err = error.message.split('DETAIL:');
    return err[1].substring(2, err[1].length - 1);
  }
  getErrorDetail(error) {
    let err = JSON.parse(error['_body']);
    err = err.message.split('DETAIL:');

    return err[1].substring(2, err[1].length - 1);
  }

  scrollTop() {
    setTimeout(function(){
      window.scrollTo(0, 0);
    }, 50);
  }

  alphabetical(a, b) {
    const sortOrder = [
      'TV Xcellence (New GBP Customer)',
      'TV Xcellence (Reinstate GBP Customer)',
      'TV Xcellence Annual (New GBP Customer)',
      'TV Xcellence Annual (Reinstate GBP Customer)',
      'TV Xcellence (GBP Customer Retention1)',
      'TV Xcellence (GBP Customer Retention2)',
      'Premier (New GBP Customer)',
      'Premier (Reinstate GBP Customer)',
      'Premier Annual (New GBP Customer)',
      'Premier Annual (Reinstate GBP Customer)',
      'Premier (GBP Customer Retention1)',
      'Premier (GBP Customer Retention2)',
      'Red Hot Monthly (New GBP Customer)',
      'Red Hot Monthly (Reinstate GBP Customer)',
      'Red Hot Monthly (GBP Customer Retention)',
      'Pay Per Night (New GBP Customer)'
    ];
    return sortOrder.indexOf(a.titleDescription) - sortOrder.indexOf(b.titleDescription);
  }


  sellableOrder(a, b) {
    if (a.titleDescription < b.titleDescription) {
      return -1;
    }
    if (a.titleDescription > b.titleDescription) {
      return 1;
    }
    return 0;
  }

  validFromOrder(a, b) {
    if (a[0].validFrom < b[0].validFrom) {
      return -1;
    }
    if (a[0].validFrom > b[0].validFrom) {
      return 1;
    }
    return 0;
  }

  sortId(a, b) {
    if (a.id < b.id) {
      return -1;
    }
    if (a.id > b.id) {
      return 1;
    }
    return 0;
  }

  removeSpecialCharacters(obj) {
    for (const o in obj) {
      if (obj[o] != null) {
        obj[o] = obj[o].toString();
        obj[o] = obj[o].replace(/[^a-zA-Z 0-9]+/g, '');
      }
    }
   return obj;
  }

  toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1); });
  }

  undefinedToEmptyString(str) {
    if (str === 'undefined') {
      return '';
    }
    return str;
  }

  jsonFilter(obj, str) {
    const foundArray = [];
    let found;
    for (const u of obj) {
      found = false;
      for (const key in u) {
        if (u[key] != null) {
          if (u[key].toString().toLowerCase().includes(str.toLowerCase())) {
            found = true;
          }
        }
      }
      if (found) {
        foundArray.push(u);
      }
    }
    return foundArray;
  }

  dateToUTC(date) {
    date = new Date(date);
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    date = new Date(Date.UTC(year, month, day));
    date = date.getTime();
    return date ;
  }

  /**
   *  Check if subscriber is active
   * @param id
   * @returns {string}
   */
  isActive(id) {
    let entitlements = [];
    let subs;
    let ppns;
    // filter entitlements subscriptions
    this.rest.get('/entitlements?subscriberId=' + id + '&productType=subscription' )
      .subscribe(res => {
        subs = res;
        if (subs.length != 0) {
          entitlements = entitlements.concat(subs);
        }
        console.log(entitlements);
      }, err => {
        console.log(err);
    });

    // filter entitlements ppvs
    this.rest.get('/entitlements?subscriberId=' + id + '&productType=ppv_enabler' )
      .subscribe(res => {
        ppns = res;
        if (ppns.length != 0) {
          entitlements = entitlements.concat(ppns);
        }
        console.log(entitlements);
      }, err => {
        console.log(err);
      });
  }
}
