import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {UserService} from './user.service';

@Injectable()
export class AdminRequiredService implements CanActivate {
  constructor(private router: Router, private user: UserService) {}
  canActivate() {
    if (sessionStorage.getItem('role') === 'admin') {
      // admin in so return true
      return true;
    }
    // not admin in so redirect to login page
    this.router.navigate(['/']);
    return false;
  }
}
