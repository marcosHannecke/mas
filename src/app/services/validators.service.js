"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// CUSTOM VALIDATION
var core_1 = require("@angular/core");
var ValidatorsService = /** @class */ (function () {
    function ValidatorsService() {
    }
    // Only numbers
    ValidatorsService.prototype.onlyNumbers = function (val) {
        if (val.value.match(/.*[^0-9].*/)) {
            return { onlyNumbers: true };
        }
        return null;
    };
    // Special characters not allowed
    ValidatorsService.prototype.specialChar = function (val) {
        if (val.value.match(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/)) {
            return { specialChar: true };
        }
        return null;
    };
    // Check for letters and spaces
    ValidatorsService.prototype.letterSpaces = function (val) {
        if (!val.value.match(/^[a-zA-Z\s]*$/)) {
            return { letterSpaces: true };
        }
        return null;
    };
    // Valid a least 1 of 3
    ValidatorsService.prototype.phones = function (group) {
        if (group.value.homeTelNr.length > 0 || group.value.workTelNr.length > 0 || group.value.mobTelNr.length > 0) {
            return null;
        }
        return { valid: true };
    };
    // Email
    ValidatorsService.prototype.email = function (val) {
        if (!val.value.match(/\S+@\S+\.\S+/)) {
            return { email: true };
        }
        return null;
    };
    ValidatorsService = __decorate([
        core_1.Injectable()
    ], ValidatorsService);
    return ValidatorsService;
}());
exports.ValidatorsService = ValidatorsService;
