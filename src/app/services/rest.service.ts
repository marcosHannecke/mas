import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Globals} from '../app.global';

@Injectable()
export class RestService {
  constructor(private http: HttpClient, private globals: Globals) {
  }

  /**
   * GET request
   * @param url --> url for request
   * @returns {Observable<Object>}
   */
  get(url) {
   // return this.http.get(this.globals.API_ENDPOINT  + url, {responseType: 'json'})
   return this.http.get(this.globals.API_ENDPOINT  + url, {responseType: 'json'})
  }

  /**
   * GET request observe response
   * @param url --> url for request
   * @returns {Observable<Object>}
   */
  getObserveResponse(url) {
    return this.http.get(this.globals.API_ENDPOINT  + url, {responseType: 'json', observe: 'response'})
  }

  /**
   * GET request without /simplified
   * @param url --> url for request
   * @returns {Observable<Object>}
   */
  getNoSimplified(url) {
    return this.http.get(this.globals.API_IP + url, {responseType: 'json'})
  }


  /**
   * POST request
   * @param url --> url for request
   * @param data --> data for post request
   * @returns {Observable<Object>}
   */
  post(url, data) {
    return this.http.post(this.globals.API_ENDPOINT + url, data, {responseType: 'json', observe: 'response'})
  }


  /**
   * POST request without '/vca-rest-api/simplified'
   * @param url --> url for request
   * @param data --> data for post request
   * @returns {Observable<Object>}
   */
  postNoSimplified(url, data) {
    return this.http.post(this.globals.API_IP + url, data, {responseType: 'json', observe: 'response'})
  }


  /**
   * PUT request
   * @param url --> url for request
   * @param data --> data for put request
   * @returns {Observable<Object>}
   */
  put(url, data) {
    return this.http.put(this.globals.API_ENDPOINT + url, data, {responseType: 'json', observe: 'response'})
  }

  /**
   * PUT request without '/vca-rest-api/simplified'
   * @param url --> url for request
   * @param data --> data for put request
   * @returns {Observable<Object>}
   */
  // putNoSimplified(url, data) {
  //   return this.http.put(this.globals.API_IP + url, data, {responseType: 'json', observe: 'response'})
  // }

  /**
   * DELETE request
   * @param url
   * @param data
   * @returns {Observable<Object>}
   */
  delete(url, data?) {
    return this.http.delete(this.globals.API_ENDPOINT  + url + data)
  }

  deleteWithBody(url, data?) {
    return this.http.request('DELETE', this.globals.API_ENDPOINT + url, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: data,
      responseType: 'json',
      observe: 'response'
    });
  }

}
