import { Injectable } from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import { UserService } from './user.service';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class LoggedInGuard implements CanActivate {
  // private expires : any;
  constructor(private router: Router, private user: UserService) {}


  canActivate() {
    if (sessionStorage.getItem('auth_token')) {
      // logged in so return true
      return true;

    }

    // not logged in so redirect to login page
    this.router.navigate(['/']);
    return false;
  }

}
