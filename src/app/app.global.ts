// globals.ts
import { Injectable } from '@angular/core';
import {AppSettings} from './app.settings';

@Injectable()
export class Globals {
  // TIME GLOBALS
  public ONE_DAY_SECONDS = 24 * 60 * 60 ;
  public today = new Date();
  public todayTimestamp = Math.floor(+ this.today / 1000);
  public firstDayCurrentMonth = new Date(Date.UTC(this.today.getFullYear(), this.today.getMonth() , 1));
  public lastDayPreviousOfMonth = new Date(Date.UTC(this.today.getFullYear(), this.today.getMonth(), 0));
  public lastDayPreviousTwoMonth = new Date(Date.UTC(this.lastDayPreviousOfMonth.getFullYear(), this.lastDayPreviousOfMonth.getMonth(), 0));
  public lastDayPreviousOfYear = new Date(this.today.getFullYear(), 0, 0);
  public lastYear = new Date(Date.UTC(this.today.getFullYear() - 1, 0, 0));
  public yesterdayDay = this.today.getDate() - 1;
  public yesterday = new Date(Date.UTC(this.today.getFullYear(), this.today.getMonth(), this.yesterdayDay));
  public firstDayYesterdayMonth = new Date(Date.UTC(this.today.getFullYear(), this.yesterday.getMonth() , 1));
  public yesterdayMilliseconds = + this.yesterday;
  public monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  // CMS ID
  public CMSId = 9; // TODO: replace all hardcoded

  // ENVIRONMENT GLOBALS
  API_ENDPOINT: string;
  API_IP: string;
  URL: any;
  PAYMENT_RETURN_DOMAIN: string;

  constructor() {
    if (AppSettings.environment === 'live') {
      /*******************************************************
       PRODUCTION

       IMPORTANT:
       - /app.module.ts --> Uncomment Raven and add to providers to track errors
       *******************************************************/

      this.API_ENDPOINT = 'https://vca.mediaaccessservices.net/vca-rest-api/simplified';
      this.API_IP = 'https://vca.mediaaccessservices.net/vca-rest-api';
      this.URL = 'https://vca.mediaaccessservices.net';
      this.PAYMENT_RETURN_DOMAIN = 'https://vca.mediaaccessservices.net';
    }  else  if (AppSettings.environment === 'stage') {
      if (sessionStorage.getItem('environment') === 'test') {
        /**
         * TEST
         */
        this.API_ENDPOINT = 'http://172.16.0.67:8080/vca-rest-api/simplified';
        this.API_IP  = 'http://172.16.0.67:8080/vca-rest-api';
        this.URL = 'http://172.16.0.67:8080';
        this.PAYMENT_RETURN_DOMAIN = 'http://stage.vca.mediaaccessservices.net';
      }else {
        /**
         * DEFAULT STAGE
         */
        this.API_ENDPOINT = 'http://stage.vca.mediaaccessservices.net/vca-rest-api/simplified';
        this.API_IP = 'http://stage.vca.mediaaccessservices.net/vca-rest-api';
        this.URL = 'http://stage.vca.mediaaccessservices.net';
        this.PAYMENT_RETURN_DOMAIN = 'http://stage.vca.mediaaccessservices.net';
      }

    } else if (AppSettings.environment === 'local') {
      /**
       * LOCAL (used to test backend application)
       */
      this.API_ENDPOINT = 'http://localhost:8080/vca-rest-api/simplified';
      this.API_IP = 'http://localhost:8080/vca-rest-api';
      this.URL = 'http://localhost:8080';
      this.PAYMENT_RETURN_DOMAIN = 'http://localhost:8080';
    }
  }

}
