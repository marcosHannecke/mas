"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppSettings = /** @class */ (function () {
    function AppSettings() {
    }
    /** OLD TEST **/
    // public static API_ENDPOINT= 'http://172.16.0.67:8080/vca-rest-api/simplified';
    // public static  API_IP = 'http://172.16.0.67:8080/vca-rest-api';
    /** STAGE **/
    AppSettings.API_ENDPOINT = 'http://stage.vca.mediaaccessservices.net/vca-rest-api/simplified';
    AppSettings.API_IP = 'http://stage.vca.mediaaccessservices.net/vca-rest-api';
    /** LIVE **/
    // public static API_ENDPOINT= 'https://vca.mediaaccessservices.net/vca-rest-api/simplified';
    // public static  API_IP = 'https://vca.mediaaccessservices.net/vca-rest-api';
    AppSettings.CMSId = 9;
    return AppSettings;
}());
exports.AppSettings = AppSettings;
