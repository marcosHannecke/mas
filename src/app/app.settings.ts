
export class AppSettings {
  /**
   * SET ENVIRONMENT
   * @type {string}
   * -----VALID VALUES -----
   * LOCAL --> 'local'
   * STAGE / TEST --> 'stage'
   * PRODUCTION --> 'live'
   */
  public static environment = 'stage';
  public static version = '1.25.3';
}

