import {PipeTransform, Pipe} from '@angular/core';
import {forEach} from "@angular/router/src/utils/collection";

@Pipe({ name: 'highlight' })
export class HighlightPipe implements PipeTransform {
  transform(text: string, search): string {
    var pattern = search.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    pattern = pattern.split(' ').filter((t) => {
      return t.length > 0;
    }).join('|');
    var regex = new RegExp(pattern, 'gi');
    return search ? text.replace(regex, (match) => `<span class="highlight">${match}</span>`) : text;
  }
}

@Pipe({
  name: 'reverse'
})
export class ReversePipe {
  transform(value) {
    return value.slice().reverse();
  }
}

@Pipe({
  name: 'orderValidFrom'
})
export class OrderValidFrom implements PipeTransform {
  transform(array: Array<any>, args: string): Array<any> {

    console.log('hereaa', args);
    if (typeof args[0] === 'undefined') {
      return array;
    }

    let direction = args[0][0];
    array.sort((a: any, b: any) => {
      let left = Number(new Date(a[0].validFrom));
      let right = Number(new Date(b[0].validFrom));
      return (direction === '-') ? right - left : left - right;
    });
    return array;
  }
}

@Pipe({name: 'capitalize'})
export class CapitalizePipe implements PipeTransform {

  transform(value: any) {
    if (value) {
      return value.charAt(0).toUpperCase() + value.slice(1);
    }
    return value;
  }
}

@Pipe({ name: 'errorDetail' })
export class ErrorDetailPipe implements PipeTransform {
  transform(value: any) {
    value = value.split('DETAIL: [');
    value = value[1].split('] EXCEPTION');
    return value[0];
  }
}

@Pipe({name: 'postcode'})
export class PostcodePipe implements PipeTransform {
  transform(value: any) {
    // value = value.match(/^([A-Z]{1,2}\d{1,2}[A-Z]?)\s*(\d[A-Z]{2})$/);
    // value.shift();
    // value.join(' ');

    value = value.toUpperCase();
    const exceptions = [ 'UK', 'ROI', 'IOM', 'CI', 'BFPO' ];

    // If Postcode Exception type, return
    exceptions.forEach(exp =>  {
      if (value ===  exp)  {
        return value;
      }
    });

    // If length is < 4, return
    if (value.length < 4) {
      return value;
    }

    const suffix = value.substring(value.length - 3);
    const prefix = value.substring(0, value.length - 3);
    return prefix + ' ' + suffix;
  }
}


@Pipe({name: 'toNumber'})
export class StringToNumberPipe implements PipeTransform {
  transform(value: string): number {
    return Number(value);
  }
}


