import {Component, OnInit, ViewContainerRef} from '@angular/core';
import { correctHeight, detectBody } from './app.helpers';
import { UserService} from './services/user.service';
import {Observable} from 'rxjs/Rx';
import {NavigationStart, Router} from '@angular/router';
import {Globals} from './app.global';
import { ToastrService } from 'ngx-toastr';

import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import {RestService} from './services/rest.service';
import {AppSettings} from './app.settings';

declare const jQuery: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService, RestService]
})
export class AppComponent implements OnInit {
  expires: any;

  idleState: string;
  timedOut = false;
  lastPing?: Date = null;
  date = new Date();

  hideNotifications = false;
  notification: Notification = new Notification();

  notificationTitles: Array<string>;


  constructor(public router: Router, private _user: UserService, private idle: Idle, private keepalive: Keepalive,
              private toastr: ToastrService, private rest: RestService, private globals: Globals) {
    // stop console.log
    if (AppSettings.environment === 'live') {
      console.log = (...args) => {};
    }

    this.notification.type = 'warning';

    // sets an idle timeout of 2 hours.
    idle.setIdle(7200);
    // sets a timeout period of 5 seconds. after the user will be considered timed out.
    idle.setTimeout(5);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe(() => this.idleState = null);
    idle.onTimeout.subscribe(() => {
      this.idleState = null;
      this.timedOut = true;
      this.logOut();
    });
    // idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    idle.onTimeoutWarning
      .subscribe((countdown) =>
        this.idleState = 'You will time out in ' + countdown + ' seconds! Please click or move the mouse to cancel.');

    // sets the ping interval to 15 seconds
    keepalive.interval(15);

    keepalive.onPing.subscribe(() => this.lastPing = new Date());

    this.reset();

    // TODO This gets the auth  token every 5 minutes, upto a max of 250 times.
    // Please revisit these parameters based on need/performance
    // Handover Video Number #7
    this.expires *= 1000;
    Observable.interval(1000 * 5 * 60 )
      .take(250)
      .subscribe(() => {
        if (sessionStorage.getItem('auth_token')) {
          this._user.tokenRefresh();
        }
      })
  }

  ngOnInit() {
  // this.showCustom();
    // Run correctHeight function on load and resize window event
    jQuery(window).bind('load resize', function() {
      correctHeight();
      detectBody();
    });

    /**
     * Detect Route to show notification in login page
     */
    this.router.events.subscribe((event) => {
        if (event instanceof NavigationStart) {
          this.getNotifications();
        // console.log(event.url);
        //   if (event.url === '/') {
        //     this.notification.type = 'success';
        //   } else {
        //     this.notification.type = 'warning'
        //   }
        //
        //   this.showNotification();
        }
        });

      }


  /**
   * Get notifications
   */
  getNotifications() {
    this.rest.get('/maintenance-notifications?page=0&size=100')
      .subscribe(res => {
        this.showNotification(res);
      }, err => {
        console.log(err);
      })

    // this.adminPanelService.getUrlHttpClient('/maintenance-notifications?page=0&size=100')
    //   .subscribe(res => {
    //     this.showNotification(res);
    //   }, err => {
    //     console.log(err);
    //   })
  }

  /**
   * Show notification
   * @param notifications -> list of notifications
   */
  private showNotification(notifications ?: any) {

    const actualDate = Math.trunc(+ this.date / 1000);
    this.notificationTitles = [];
    if (sessionStorage.getItem('notifications') != null) {
      this.notificationTitles = JSON.parse(sessionStorage.getItem('notifications'));
    }

    for (const not of notifications) {

      if (actualDate >= not.activeFrom  && actualDate <= not.activeTo) {
        const position = this.getPosition(not.position); // get position
        // Set notification options
        not.options = {
          timeOut: not.displayDuration,
          extendedTimeOut: 0, // Time to close after a user hovers notification
          disableTimeOut: false,
          closeButton: not.showCloseButton,
          positionClass: position,
          enableHtml: not.enableHtml,
          progressBar: true,
          easing: 'ease-out',
          tapToDismiss: false
          // animate: 'flyLeft',
        };
        if (this.router.url === '/' ) {
          if (not.showInLoginPage) {
            if (!this.checkDuplicateNotifications(not.title)) {
              // Show notification
              if (not.notificationType === 'SUCCESS') {
                this.toastr.success(not.message, not.title, not.options);
              } else if (not.notificationType === 'INFO') {
                this.toastr.info(not.message, not.title, not.options);
              } else if (not.notificationType === 'WARNING') {
                this.toastr.warning(not.message, not.title, not.options);
              } else if (not.notificationType === 'ERROR') {
                this.toastr.error(not.message, not.title, not.options);
              }

              this.notificationTitles.push(not.title);
              sessionStorage.setItem('notifications', JSON.stringify(this.notificationTitles));
            }
          }
        }else {
          if (!this.checkDuplicateNotifications(not.title)) {
            // Show notification
            if (not.notificationType === 'SUCCESS') {
              this.toastr.success(not.message, not.title, not.options);
            } else if (not.notificationType === 'INFO') {
              this.toastr.info(not.message, not.title, not.options);
            } else if (not.notificationType === 'WARNING') {
              this.toastr.warning(not.message, not.title, not.options);
            } else if (not.notificationType === 'ERROR') {
              this.toastr.error(not.message, not.title, not.options);
            }

            this.notificationTitles.push(not.title);
            sessionStorage.setItem('notifications', JSON.stringify(this.notificationTitles));
          }
        }
        if (this.router.url !== '/') {
          this.notLogOut(not);
        }
      }
    }
  }

  checkDuplicateNotifications(notTitle): boolean {
    const notifications = JSON.parse(sessionStorage.getItem('notifications'));
    if (notifications != null) {
      for (let i = 0; i < notifications.length; i++) {
        if (notTitle === notifications[i]) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Log out user if notification indicates so
   * @param not -> notification
   */
  notLogOut(not) {
    // console.log(not.logOut);
    let role;
    if (not.logOut === 'ALL') {
      this.rest.get('/principals/about-me')
        .subscribe(res => {
           role = res;
          if (role.roles[0].name === 'ROLE_SUPERVISOR' || role.roles[0].name === 'ROLE_USER') {
            this.logOut();
          }
        });

      // this.subscribersService.getRole()
      //   .subscribe(res => {
      //     const role = JSON.parse(res['_body']);
      //     if (role.roles[0].name === 'ROLE_SUPERVISOR' || role.roles[0].name === 'ROLE_USER') {
      //       this.logOut();
      //     }
      //   });
    } else if (not.logOut === 'USERS') {
      this.rest.get('/principals/about-me')
        .subscribe(res => {
           role = res;
          if (role.roles[0].name === 'ROLE_USER') {
            this.logOut();
          }
        });

      // this.subscribersService.getRole()
      //    .subscribe(res => {
      //      const role = JSON.parse(res['_body']);
      //      if (role.roles[0].name === 'ROLE_USER') {
      //        this.logOut()
      //      }
      //    });
    }
  }

  /**
   * TODO:
   * change idle to logout users
   */

  /**
   * Get notification position
   * @param not -> notification.position
   * @returns {string}
   */
  private getPosition(not) {
    if (not === 'TR') {
      return 'toast-top-right'
    }  else if (not === 'BR') {
      return 'toast-bottom-right'
    } else if (not === 'TL') {
      return 'toast-top-left'
    } else if (not === 'BL') {
      return 'toast-bottom-left'
    }else if (not === 'TFW') {
      return 'toast-top-full-width'
    }else if (not === 'BFW') {
      return 'toast-bottom-full-width'
    }else if (not === 'TC') {
      return 'toast-top-center'
    }else if (not === 'TB') {
      return 'toast-bottom-center'
    }else if (not === 'CO') {
      return 'toast-top-right'
    }
    return 'toast-top-right'
  }
  // RESET IDLE
  reset() {
    this.idle.watch();
    // this.idleState = 'Started.';
    this.timedOut = false;
  }
  // LOG OUT
  logOut() {
    this._user.logout();
    this.router.navigate(['/login']);
  }
}

class Notification {
  type: string;
  title: string;
  text: string;
  options = {
    timeOut: 86400000, // default 1 day
    extendedTimeOut: 0, // Time to close after a user hovers notification
    disableTimeOut: true,
    closeButton: true,
    positionClass: 'toast-top-right',
    enableHtml: false,
    progressBar: true,
    easing: 'ease-out',
    tapToDismiss: false
    // animate: 'flyLeft',
  }
}


