"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_helpers_1 = require("../../../app.helpers");
var user_service_1 = require("../../../services/user.service");
var helpers_service_1 = require("../../../services/helpers.service");
var TopnavbarComponent = /** @class */ (function () {
    function TopnavbarComponent(router, userService, helpers) {
        this.router = router;
        this.userService = userService;
        this.helpers = helpers;
        this.userMenu = false;
    }
    TopnavbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.helpers.getRole()
            .subscribe(function (res) {
            console.log(res);
            var resp = JSON.parse(res['_body']);
            if (resp.roles.length > 0) {
                if (resp.roles[0].name == 'ROLE_ADMIN') {
                    _this.role = 'admin';
                }
                else if (resp.roles[0].name == 'ROLE_USER') {
                    _this.role = 'user';
                }
            }
            else {
                _this.role = 'user';
            }
        });
        this.user = sessionStorage.getItem('user').toUpperCase();
    };
    TopnavbarComponent.prototype.toggleNavigation = function () {
        jQuery("body").toggleClass("mini-navbar");
        app_helpers_1.smoothlyMenu();
    };
    TopnavbarComponent.prototype.logOut = function () {
        this.userService.logout();
    };
    TopnavbarComponent = __decorate([
        core_1.Component({
            selector: 'topnavbar',
            templateUrl: 'topnavbar.template.html',
            styles: ['.userMenu{position: absolute; border: 1px solid #eeeeee; padding: 10px; background: white;}'],
            providers: [user_service_1.UserService, helpers_service_1.HelpersService]
        })
    ], TopnavbarComponent);
    return TopnavbarComponent;
}());
exports.TopnavbarComponent = TopnavbarComponent;
