import { Component, OnInit } from '@angular/core';
import { smoothlyMenu } from '../../../app.helpers';
import {Router} from '@angular/router';
import {UserService} from '../../../services/user.service';
import {HelpersService} from '../../../services/helpers.service';
import {Globals} from '../../../app.global';

declare var jQuery: any;

@Component({
    selector: 'topnavbar',
    templateUrl: 'topnavbar.template.html',
    styles: ['.userMenu{position: absolute; border: 1px solid #eeeeee; padding: 10px; background: white;}'],
    providers: [UserService, HelpersService]
})
export class TopnavbarComponent implements OnInit {
    role: string;
    user: any;
    userMenu = false;
    environment: String;

    constructor(private router: Router, private userService: UserService, private helpers: HelpersService, public globals: Globals) {}

    ngOnInit() {
      this.helpers.getRole()
        .subscribe(res => {
          console.log(res);
          const resp = res;
          if (resp['roles'].length > 0) {
            if (resp['roles'][0].name === 'ROLE_ADMIN' || resp['roles'][0].name === 'ROLE_SUPER_ADMIN'
              || resp['roles'][0].name === 'ROLE_SUPERVISOR') {
              this.role = 'admin'
            } else if (resp['roles'][0].name === 'ROLE_USER') {
              this.role = 'user'
            }
          }else {
            this.role = 'user'
          }
        });
      this.user = sessionStorage.getItem('user').toUpperCase();

      this.checkEnvironment();
    }
    toggleNavigation(): void {
        jQuery('body').toggleClass('mini-navbar');
        smoothlyMenu();
    }

    checkEnvironment() {
      switch (sessionStorage.getItem('environment')) {
        case 'stage':
          this.environment = 'STAGE';
          break;
        case 'test':
          this.environment = 'TEST';
          break;
        default:
          this.environment = 'STAGE';
      }


    }

    logOut() {
      this.userService.logout();
    }
}
