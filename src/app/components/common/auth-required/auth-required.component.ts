import {Component, OnInit, Output, Input, EventEmitter} from '@angular/core';
import {HelpersService} from '../../../services/helpers.service';

@Component({
  selector: 'app-auth-required',
  templateUrl: './auth-required.component.html',
  styleUrls: ['./auth-required.component.css'],
  providers: [ HelpersService]
})
export class AuthRequiredComponent implements OnInit {

  @Input()
  requireAdmin: boolean;

  @Output() requireAdminDialog: EventEmitter<any> = new EventEmitter();
  @Output() accessGranted: EventEmitter<any> = new EventEmitter();
  @Output() authorisingUserId: EventEmitter<any> = new EventEmitter();
  @Output() error: EventEmitter<any> = new EventEmitter();

  // Credentials
  user: string;
  password: string;

  constructor(private helpers: HelpersService) { }
  ngOnInit() {}

  /**
   * Authorise
   */
  authorize() {

    // Request to check admin
    this.helpers.adminRequired(this.user, this.password)
      .subscribe(res => {
        this.user = null;
        this.password = null;
        if (res['roles'][0].name === 'ROLE_ADMIN' || res['roles'][0].name === 'ROLE_SUPER_ADMIN'
          || res['roles'][0].name === 'ROLE_SUPERVISOR') {
          this.authorisingUserId.emit(res['id']);
          this.accessGranted.emit('admin');
        }else {
          this.error.emit({message : 'Access Denied'});
        }
        // Close dialog
        this.close(false);
      },  err => {
        this.error.emit(this.helpers.error(err.error.message));
        this.user = '';
        this.password = '';
        // Close dialog
        this.close(false);
      })
  }

  /**
   * Close dialog
   */
  close(resetUserPassword) {
    if (resetUserPassword) {
      this.user = '';
      this.password = '';
    }

    this.requireAdminDialog.emit(null);
  }
}
