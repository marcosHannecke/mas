import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRequiredComponent } from './auth-required.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    FormsModule,
    CommonModule
  ],
  declarations: [AuthRequiredComponent],
  exports: [AuthRequiredComponent]
})
export class AuthRequiredModule { }
