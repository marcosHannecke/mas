import {Component} from '@angular/core';
import {AppSettings} from '../../../app.settings';

@Component({
    selector: 'footer',
    templateUrl: 'footer.template.html'
})
export class FooterComponent {
  version = AppSettings.version;
  constructor() {}
}
