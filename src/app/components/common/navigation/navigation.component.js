"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var helpers_service_1 = require("../../../services/helpers.service");
var NavigationComponent = /** @class */ (function () {
    function NavigationComponent(router, userService, helpers) {
        this.router = router;
        this.userService = userService;
        this.helpers = helpers;
    }
    NavigationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.helpers.getRole()
            .subscribe(function (res) {
            console.log(res);
            var resp = JSON.parse(res['_body']);
            if (resp.roles.length > 0) {
                if (resp.roles[0].name == 'ROLE_ADMIN') {
                    _this.role = 'admin';
                }
                else if (resp.roles[0].name == 'ROLE_USER') {
                    _this.role = 'user';
                }
            }
            else {
                _this.role = 'user';
            }
        });
    };
    // ngAfterViewInit() {
    //     jQuery('#side-menu').metisMenu();
    //
    // }
    NavigationComponent.prototype.activeRoute = function (routename) {
        return this.router.url.indexOf(routename) > -1;
    };
    NavigationComponent.prototype.logOut = function () {
        this.userService.logout();
    };
    NavigationComponent = __decorate([
        core_1.Component({
            selector: 'navigation',
            templateUrl: 'navigation.template.html',
            providers: [helpers_service_1.HelpersService]
        })
    ], NavigationComponent);
    return NavigationComponent;
}());
exports.NavigationComponent = NavigationComponent;
