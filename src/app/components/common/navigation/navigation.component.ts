import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../../services/user.service';
import {HelpersService} from '../../../services/helpers.service';

declare const jQuery: any;

@Component({
    selector: 'navigation',
    templateUrl: 'navigation.template.html',
    providers: [HelpersService]
})

export class NavigationComponent implements OnInit {
    public role: string;

    constructor(private router: Router, private userService: UserService, private helpers: HelpersService) {}

    ngOnInit() {
      this.helpers.getRole()
        .subscribe(res => {
          // const resp = JSON.parse(res['_body']);
          const resp = res;
          if (resp['roles'].length > 0) {
            if (resp['roles'][0].name === 'ROLE_ADMIN' || resp['roles'][0].name === 'ROLE_SUPER_ADMIN') {
              this.role = 'admin'
            }else if (resp['roles'][0].name === 'ROLE_SUPERVISOR') {
              this.role = 'supervisor'
            } else if (resp['roles'][0].name === 'ROLE_USER') {
              this.role = 'user'
            }
          }else {
            this.role = 'user'
          }
        })
    }

    activeRoute(routename: string): boolean {
        return this.router.url.indexOf(routename) > -1;
    }
    logOut() {
      this.userService.logout();
    }

}
