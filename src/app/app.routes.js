"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var main_view_component_1 = require("./views/main-view/main-view.component");
var login_component_1 = require("./views/login/login.component");
var register_component_1 = require("./views/register/register.component");
var blank_component_1 = require("./components/common/layouts/blank.component");
var basic_component_1 = require("./components/common/layouts/basic.component");
var logged_in_guard_1 = require("./services/logged-in.guard");
var newsubscriber_component_1 = require("./views/subscribers/newsubscriber/newsubscriber.component");
var joinSubscriber_component_1 = require("./views/subscribers/joinSubscriber/joinSubscriber.component");
var joinSubscriberEntitlements_component_1 = require("./views/subscribers/joinSubscriber/joinSubscriberEntitlements.component");
var process3_4_component_1 = require("./views/process/process3_4/process3_4.component");
var process5_component_1 = require("./views/process/process5/process5.component");
var customer_component_1 = require("./views/customer/customer.component");
var process8_component_1 = require("./views/process/process8/process8.component");
var process19_component_1 = require("./views/process/process19/process19.component");
var process20_component_1 = require("./views/process/process20/process20.component");
var process25_component_1 = require("./views/process/process25_26/process25.component");
var process27_component_1 = require("./views/process/process27/process27.component");
var process21_component_1 = require("./views/process/process21/process21.component");
var process23_component_1 = require("./views/process/process23/process23.component");
var process24_component_1 = require("./views/process/process24/process24.component");
var process32_component_1 = require("./views/process/process32/process32.component");
var process33_component_1 = require("./views/process/process33/process33.component");
var process16_component_1 = require("./views/process/process16/process16.component");
var process17_component_1 = require("./views/process/process17/process17.component");
var editCustomer_component_1 = require("./views/customer/editCustomer.component");
var newsubscriberEntitlements_component_1 = require("./views/subscribers/newsubscriber/newsubscriberEntitlements.component");
var process28_component_1 = require("./views/process/process28/process28.component");
var advancedSearch_component_1 = require("./views/subscribers/advancedSearch/advancedSearch.component");
var adminRequired_service_1 = require("./services/adminRequired.service");
var stats_component_1 = require("./views/stats/stats.component");
var histories_component_1 = require("./views/histories/histories.component");
exports.ROUTES = [
    // Main redirect
    { path: '', component: login_component_1.loginComponent, pathMatch: 'full' },
    // { path: 'mainView', component: mainViewComponent,  canActivate: [LoggedInGuard]},
    // App views
    {
        path: '',
        component: basic_component_1.basicComponent,
        canActivate: [logged_in_guard_1.LoggedInGuard],
        children: [
            { path: 'mainView', component: main_view_component_1.mainViewComponent },
            { path: 'customer/:id', component: customer_component_1.CustomerComponent },
            { path: 'editCustomer', component: editCustomer_component_1.EditCustomerComponent },
            { path: 'newsubscriber', component: newsubscriber_component_1.NewsubscriberComponent },
            { path: 'newsubscriberEntitlements', component: newsubscriberEntitlements_component_1.NewsubscriberEntitlementsComponent },
            { path: 'listSubscribers', loadChildren: './views/subscribers/listSubscribers/listSubscribers.module#ListSubscribersViewModule' },
            { path: 'joinSubscriber', component: joinSubscriber_component_1.JoinSubscriberComponent },
            { path: 'joinSubscriberEntitlements', component: joinSubscriberEntitlements_component_1.JoinSubscriberEntitlementsComponent },
            { path: 'search', loadChildren: './views/subscribers/search/search.module#SearchViewModule' },
            { path: 'advancedSearch', component: advancedSearch_component_1.AdvancedSearch, canActivate: [adminRequired_service_1.AdminRequiredService] },
            { path: 'results', loadChildren: './views/subscribers/search/results.module#ResultsViewModule' },
            { path: 'entitlements', loadChildren: './views/subscribers/viewSubscriber/entitlements.module#EntitlementsViewModule' },
            { path: 'viewSubscriber/:id', loadChildren: './views/subscribers/viewSubscriber/viewSubscriber.module#ViewSubscriberViewModule' },
            { path: 'billingHistory/:id', loadChildren: './views/billing-history/billing-history.module#BillingHistoryViewModule', canActivate: [adminRequired_service_1.AdminRequiredService] },
            { path: 'process4', component: process3_4_component_1.Process4Component },
            { path: 'process3', component: process3_4_component_1.Process3Component },
            { path: 'process5', component: process5_component_1.Process5Component },
            { path: 'process8', component: process8_component_1.Process8Component },
            { path: 'process16', component: process16_component_1.Process16Component },
            { path: 'process17', component: process17_component_1.Process17Component },
            { path: 'process19', component: process19_component_1.Process19Component },
            { path: 'process20', component: process20_component_1.Process20Component },
            { path: 'process21', component: process21_component_1.Process21Component },
            { path: 'process23', component: process23_component_1.Process23Component },
            { path: 'process24', component: process24_component_1.Process24Component },
            { path: 'process25/:u', component: process25_component_1.Process25Component },
            { path: 'process27', component: process27_component_1.Process27Component },
            { path: 'process28', component: process28_component_1.Process28Component },
            { path: 'process32', component: process32_component_1.Process32Component },
            { path: 'process33', component: process33_component_1.Process33Component },
            { path: 'stats', component: stats_component_1.StatsComponent },
            { path: 'histories/:id', component: histories_component_1.HistoriesComponent },
        ]
    },
    {
        path: '', component: blank_component_1.blankComponent, canActivate: [logged_in_guard_1.LoggedInGuard],
        children: [
            { path: 'login', component: login_component_1.loginComponent },
            { path: 'register', component: register_component_1.registerComponent }
        ]
    },
    // Handle all other routes
    { path: '**', redirectTo: '' }
];
exports.appRouter = router_1.RouterModule.forRoot(exports.ROUTES);
