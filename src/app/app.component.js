"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_helpers_1 = require("./app.helpers");
var user_service_1 = require("./services/user.service");
var Rx_1 = require("rxjs/Rx");
var core_2 = require("@ng-idle/core");
var AppComponent = /** @class */ (function () {
    function AppComponent(router, _user, idle, keepalive) {
        var _this = this;
        this.router = router;
        this._user = _user;
        this.idle = idle;
        this.keepalive = keepalive;
        this.timedOut = false;
        this.lastPing = null;
        // sets an idle timeout of 2 hours.
        idle.setIdle(7200);
        // sets a timeout period of 5 seconds. after the user will be considered timed out.
        idle.setTimeout(5);
        // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
        idle.setInterrupts(core_2.DEFAULT_INTERRUPTSOURCES);
        idle.onIdleEnd.subscribe(function () { return _this.idleState = null; });
        idle.onTimeout.subscribe(function () {
            _this.idleState = null;
            _this.timedOut = true;
            _this.logOut();
        });
        // idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
        idle.onTimeoutWarning
            .subscribe(function (countdown) {
            return _this.idleState = 'You will time out in ' + countdown + ' seconds! Please click or move the mouse to cancel.';
        });
        // sets the ping interval to 15 seconds
        keepalive.interval(15);
        keepalive.onPing.subscribe(function () { return _this.lastPing = new Date(); });
        this.reset();
        this.expires = sessionStorage.getItem('expires_in');
        this.expires *= 1000;
        Rx_1.Observable.interval(1000 * 5 * 60)
            .take(250)
            .subscribe(function () {
            console.log('here', _this.expires);
            if (sessionStorage.getItem('auth_token')) {
                console.log(_this.expires);
                _this._user.tokenRefresh()
                    .subscribe(function (res) {
                    console.log(res);
                });
            }
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        // Run correctHeight function on load and resize window event
        jQuery(window).bind('load resize', function () {
            app_helpers_1.correctHeight();
            app_helpers_1.detectBody();
        });
    };
    // RESET IDLE
    AppComponent.prototype.reset = function () {
        this.idle.watch();
        // this.idleState = 'Started.';
        this.timedOut = false;
    };
    // LOG OUT
    AppComponent.prototype.logOut = function () {
        this._user.logout();
        this.router.navigate(['/login']);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css'],
            providers: [user_service_1.UserService]
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
